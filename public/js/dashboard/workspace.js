function Directory(name, topics, projectColors) {
    this.name = name
    this.topics = []
    this.id = 'dir-' + name
    this.projectColors = projectColors
    var self = this

    this.addTopic = function (topic) {
        this.topics.push(topic)
        var container = $('#' + this.id)
        container.find('.list-topic').append(
            TEMPLATE.DASHBOARD.DATASET.CONTAINER
                .replace('$DATASETNAME$', topic.name)
                .replace('$BACKGROUND$', self.projectColors[topic.project])
                .replace('$FUNC$', 'onclick="onClickDataSet(this)"')
                .replace('$DATA$', 'data-topic="' + topic.topic + '" data-dir="' + name + '"')
        )
    }

    this.removeTopic = function (topicid) {
        this.topics.forEach(function (storedTopic, index) {
            if (storedTopic.topic == topicid) {
                self.topics.splice(index, 1)
            }
        })
        var container = $('#' + this.id)
        var target = container.find('.dataset[data-topic="' + topicid + '"]')
        target.fadeOut("fast", function () {
            target.remove()
            adjustHeightDirectory()
        })
    }

    this.destroy = function () {
        $('#' + this.id).parent().fadeOut()
    }

    this.updateCounter = function () {
        var container = $('#' + this.id)
        API.DASHBOARD.getVerbatimCountForTopics(this.topics)
            .then(function (count) {
                container.find('.statistic').find('.value').text(count)
            })
    }

    this.init = function () {
        // adding dir to dashoard
        LCONFIG.CONTAINER.LISTDIR
            .append(
                TEMPLATE.DASHBOARD.DIR.CONTAINER
                    .replace('$ID$', this.id)
                    //.replace('$DIRNAME$', name.toUpperCase())  // DEPRECATE DIRECTORIES
                    .replace('$DIRNAME$', "___")
                    .replace('$DATA$', 'data-name="' + name + '"')
            )
        var container = $('#' + this.id)
        // initiate popup trigger
        if (this.name == "unnamed") {
            container.find('.popup-trigger').remove()
        } else {
            container.find('.popup-trigger').popup()
        }

        // add topics into dir
        topics.forEach(function (topic) {
            self.addTopic(topic)
        })
    }
    this.init()
}

function Project(data) {
    this.data = data
    var self = this

    this.showStats = function () {
        $('#tmp-project').remove()
        $('.project[data-project="' + self.data._id + '"]').addClass('project-visible')
        API.DASHBOARD.getProjectDetails(self.data._id)
            .then(function (r) {

                // total count
                LCONFIG.STATS.PROJECT.INFO.find('.list').removeClass('hide')
                LCONFIG.STATS.PROJECT.INFO.find('.total-count').text(r.totalCount)

                // chart topics
                LCONFIG.STATS.PROJECT.TOPIC.css({
                    height: '370px'
                })
                LCONFIG.STATS.PROJECT.TOPIC.find('svg').remove()
                new BarPlot(
                    LCONFIG.STATS.PROJECT.TOPIC,
                    "Datasets",
                    {top: 40, right: 50, bottom: 70, left: 70},
                    false, // ifFrequency
                    false // ifColor
                ).create(
                    '#' + LCONFIG.STATS.PROJECT.TOPIC.attr('id'),
                    r.topicsCount,
                    "Verbatims"
                )

                // DEPRECATE ORGANIZATIONS AND USERS CHART
                //// chart organizations
                //LCONFIG.STATS.PROJECT.TEAM.css({
                //    height: '370px'
                //})
                //LCONFIG.STATS.PROJECT.TEAM.find('svg').remove()
                //new BarPlot(
                //    LCONFIG.STATS.PROJECT.TEAM,
                //    "Teams",
                //    {top: 40, right: 0, bottom: 70, left: 70},
                //    false, // ifFrequency
                //    false // ifColor
                //).create(
                //    '#' + LCONFIG.STATS.PROJECT.TEAM.attr('id'),
                //    r.orgsTopicCount,
                //    "Datasets"
                //)
                //
                //// chart users
                //LCONFIG.STATS.PROJECT.USER.css({
                //    height: '370px'
                //})
                //LCONFIG.STATS.PROJECT.USER.find('svg').remove()
                //new BarPlot(
                //    LCONFIG.STATS.PROJECT.USER,
                //    "Users",
                //    {top: 40, right: 0, bottom: 70, left: 70},
                //    false, // ifFrequency
                //    false // ifColor
                //).create(
                //    '#' + LCONFIG.STATS.PROJECT.USER.attr('id'),
                //    r.usersTopicCount,
                //    "Datasets"
                //)

            })
        self.showDatasets()
    }

    this.showDatasets = function () {
        UserWorkspace.showProjectDatasets(self.data._id)
    }

    this.init = function () {
        var container = $('#list-project')
        container.append(
            TEMPLATE.DASHBOARD.PROJECT.CONTAINER
                .replace('$NAME$', self.data.name)
                .replace('$BACKGROUND$', self.data.color)
                .replace('$DATA$', 'data-project="' + self.data._id + '"')
                .replace('$FUNC$', 'onclick="onClickProject(this)"')
        )
    }

    this.init()
}

function Workspace() {
    var self = this
    this.directories = {}
    this.projects = {}
    this.projectColors

    this.init = function () {
        var self = this
        API.DASHBOARD.getUser()
            .then(function (user) {
                self.user = user
                // projects
                API.DASHBOARD.getProjects().then(function (projects) {
                    projects.forEach(function (project) {
                        self.projects[project._id] = new Project(project)
                    })

                    // by default show first project statistics
                    var firstProject = $('#list-project').find('.project').first()
                    firstProject.click()
                    UserWorkspace.showProjectStats(firstProject.data().project)
                })
            })
    }

    //
    // Projects
    //
    this.showProjectStats = function (projectid) {
        $('.project').removeClass('project-visible')
        self.projects[projectid].showStats()
    }

    //
    // Datasets
    //
    this.showProjectDatasets = function (projectid) {
        API.DASHBOARD.getUserTopics(self.user._id, "dashboard", projectid)
            .then(function (r) {
                $('.container-dir').parent().remove()
                self.directories = {}
                _.keys(r.topics).forEach(function (dir) {
                    self.projectColors = r.projectColors
                    self.directories[dir] = new Directory(dir, r.topics[dir], r.projectColors)
                })
                self.addDefaultEventsDirectories()
                $('.dataset').first().click()
            })
    }

    this.addDefaultEventsDirectories = function () {
        adjustDefaultDimensions()
        addEventDroppable()
        addEventDraggable()
        this.updateAllDirectoryVerbatimCounts()
    }

    this.updateAllDirectoryVerbatimCounts = function () {
        _.keys(self.directories).forEach(function (dirname) {
            if (!_.isNull(self.directories[dirname]))
                self.directories[dirname].updateCounter()
        })
    }

    this.refreshTopic = function (topicname) {
        API.DASHBOARD.getTopicDetails(topicname).then(function (topic) {
            $('.dataset[data-topic="' + topicname + '"]').find('span').text(topic.name)
            $('.dataset[data-topic="' + topicname + '"]').css('background', self.projectColors[topic.project])
            UserWorkspace.showTopicDetails(topic)
        })
    }

    this.addDirectory = function () {
        var container = LCONFIG.CONTAINER.MODAL.NEWDIR
        var dirname = container.find('input').val().trim()
        if (dirname == "") {
            container.find('.error-message').text('Directory name cannot be empty!')
        } else {
            if (!_.isUndefined(self.directories[dirname])) {
                container.find('.error-message').text('Directory names have to be unique!')
            } else {
                self.directories[dirname] = new Directory(dirname, [], self.projectColors)
                container.modal('hide')
                addEventDroppable()
            }
        }
    }

    this.removeDirectory = function (o) {
        var r = confirm("Are you sure you want to delete this directory ?")
        if (r) {
            var dirname = $(o).parent().data().name
            if (self.directories[dirname].topics.length > 0) {
                self.directories[dirname].topics.forEach(function (topic) {
                    setTimeout(function () {
                        self.moveTopicToDirectory(self.user._id, dirname, "unnamed", topic.topic)
                        setTimeout(function () {
                            if (!_.isNull(self.directories[dirname])) {
                                self.directories[dirname].destroy()
                                self.directories[dirname] = null
                            }
                        })
                    }, 200)
                })
            } else {
                self.directories[dirname].destroy()
                self.directories[dirname] = null
            }
        }
    }

    this.onDropDirectory = function (event, ui) {
        var dir = $(this).parent()
        var item = ui.draggable

        if (dir.data().name != item.data().dir) {
            self.moveTopicToDirectory(self.user._id, item.data().dir, dir.data().name, item.data().topic)
        }
    }

    this.moveTopicToDirectory = function (username, previousdirname, newdirname, topicname) {
        self.directories[previousdirname].removeTopic(topicname)
        API.DASHBOARD.updateUserTopic(username, "movetopic", {topic: topicname, to: newdirname})
            .then(function (r) {
                self.directories[newdirname].addTopic(r.t)
                addEventDraggable()
                self.updateAllDirectoryVerbatimCounts()
            })
    }

    this.showTopicDetails = function (d) {
        var container = $('#list-detail')
        container.empty()
        // topic title
        container.parent().parent().parent().find('.topic-title')
            .text(d.name)
            .data('topic', d.topic)
        container.parent().parent().parent().parent().parent().find('#edit-topic-trigger').css('display', 'block')
        // project
        container.append(
            TEMPLATE.DASHBOARD.DETAILS.TABLE.ROW
                .replace('$DESC$', "Project")
                .replace('$VALUE$', d.project)
        )
        // directory
        container.append(
            TEMPLATE.DASHBOARD.DETAILS.TABLE.ROW
                .replace('$DESC$', "Directory")
                .replace('$VALUE$', d.dir)
        )
        // organizations
        var htmlOrganizations = ""
        if (!_.isUndefined(d.organizations)) {
            d.organizations.forEach(function (organizations) {
                htmlOrganizations += TEMPLATE.DASHBOARD.DETAILS.TABLE.USER
                    .replace('$TEXT$', organizations)
                    .replace('$COLOR$', "teal")
            })
        }
        container.append(
            TEMPLATE.DASHBOARD.DETAILS.TABLE.ROW
                .replace('$DESC$', "Teams")
                .replace('$VALUE$', htmlOrganizations)
        )
        // users
        var htmlUsers = ""
        if (!_.isUndefined(d.users)) {
            d.users.forEach(function (username) {
                htmlUsers += TEMPLATE.DASHBOARD.DETAILS.TABLE.USER
                    .replace('$TEXT$', username)
                    .replace('$COLOR$', "grey")
            })
        }
        container.append(
            TEMPLATE.DASHBOARD.DETAILS.TABLE.ROW
                .replace('$DESC$', "Users")
                .replace('$VALUE$', htmlUsers)
        )
    }

}

function _ModalTopic() {
    var self = this

    this.open = function (topicname) {
        API.DASHBOARD.getTopicDetails(topicname).then(function (topic) {
            var container = $('#modal-topic')
            container.find('.error.message').removeClass('visible')
            // topic id
            container.find('input[name="topicid"]').val(topic.topic)
            // topic name
            container.find('input[name="name"]').val(topic.name)
            // DEPRECATE CHOOSING PROJECT
            // projects
            //container.find('select[name="project"]').dropdown('set exactly', topic.project)
            container.find('input[name="project"]').val(topic.project)
            // organizations
            container.find('select[name="organizations"]').dropdown('set exactly', topic.organizations)
            // users
            container.find('select[name="users"]').dropdown('set exactly', topic.users)
            LCONFIG.CONTAINER.MODAL.EDITTOPIC.modal('show')
        })
    }

    this.submitForm = function () {
        var container = $('#modal-topic')
        var data = container.find('form').form('get values')
        if (_.isNull(data.project)) {
            container.find('.error.message').addClass('visible')
        } else {
            if (_.isNull(data.organizations))
                data.organizations = []
            if (_.isNull(data.users))
                data.users = []

            API.DASHBOARD.updateTopicDetails(data).then(function (r) {
                self.open(data.topicid)
                UserWorkspace.refreshTopic(data.topicid)
                if ($('.project[data-project="' + data.project + '"').hasClass('project-visible')) {
                    UserWorkspace.projects[data.project].showStats()
                }
            })
        }
    }

}

var UserWorkspace = new Workspace()
var ModalTopic = new _ModalTopic()
