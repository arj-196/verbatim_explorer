function _ProjectModal () {
    var self = this
    this.modal = $('#modal-project')

    this.createProject = function () {
        self.modal.find('.error.message').removeClass('visible')
        self.modal.find('input[name="action"]').val("create")
        self.modal.find('input[name="name"]').val("")
        self.modal.modal('show')
    }

    this.editProject = function (o) {
        var $this = $(o)
        var data = $this.parent().data()
        self.modal.find('.error.message').removeClass('visible')
        self.modal.find('input[name="action"]').val("edit")
        self.modal.find('input[name="id"]').val(data.project)
        self.modal.find('input[name="name"]').val("")
        self.modal.modal('show')
    }

    this.submit = function () {
        var data = {
            name: self.modal.find('.form').find('input[name="name"]').val(),
            action: self.modal.find('.form').find('input[name="action"]').val(),
            id: self.modal.find('.form').find('input[name="id"]').val(),
        }
        var ifContinue = true
        if(data.name.trim() == "") {
            ifContinue = false
            self.modal.find('.error.message').addClass('visible')
                .find('.header').text('Project name cannot be empty!')
        }

        if(ifContinue) {
            API.DASHBOARD.createProject(data).then(function (r) {
                if(!r.s) {
                    self.modal.find('.error.message').addClass('visible')
                        .find('.header').html(r.m)
                } else {
                    window.location.reload()
                }
            })
        }
    }

}
var ProjectModal = new _ProjectModal()

