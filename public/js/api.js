var CORE = {
    ajax: function (url, method, data, progressDownload) {
        return $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                ////Upload progress
                //xhr.upload.addEventListener("progress", function(evt){
                //    if (evt.lengthComputable) {
                //        var percentComplete = evt.loaded / evt.total;
                //        //Do something with upload progress
                //        //console.log("upload", percentComplete);
                //    }
                //}, false);

                //Download progress
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;

                        if (!_.isUndefined(progressDownload)) {
                            progressDownload(percentComplete)
                        }
                    }
                }, false);
                return xhr;
            },
            url: url,
            method: method,
            dataType: 'json',
            data: data
        })
    },

    handleRequest: function (request, resolve, reject) {
        request.done(function (r) {
            resolve(r)
        })
        request.fail(function (r) {
            reject(r)
        })

    }
}

var API = {
    VERBATIMS: {
        getTopics: function () {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/get/topics', 'GET', null),
                    resolve, reject
                )
            })
        },

        getTopicCount: function (topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/get/topics/count', 'GET', {topic: topic}),
                    resolve, reject
                )
            })
        },

        getVerbatimByID: function (id) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/get/verbatim/by/id', 'GET', {id: id}),
                    resolve, reject
                )
            })
        },

        getVerbatims: function (topic, iteration) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/get/verbatims', 'GET', {topic: topic, iteration: iteration}),
                    resolve, reject
                )
            })
        },

        getKeywords: function (topic, keyword) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/only/keyword', 'POST', {
                        topic: topic,
                        keyword: keyword
                    }),
                    resolve, reject
                )
            })
        },

        getKeywordsRegex: function (topic, keyword) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/only/keyword/regex', 'POST', {
                        topic: topic,
                        keyword: keyword
                    }),
                    resolve, reject
                )
            })
        },

        getTerms: function (topic, keyword) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/only/term', 'POST', {
                        topic: topic,
                        keyword: keyword
                    }),
                    resolve, reject
                )
            })
        },

        getTermsRegex: function (topic, keyword) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/only/terms/regex', 'POST', {
                        topic: topic,
                        keyword: keyword
                    }),
                    resolve, reject
                )
            })
        },

        getTags: function (topic, keyword) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/only/tag', 'POST', {
                        topic: topic,
                        keyword: keyword
                    }),
                    resolve, reject
                )
            })
        },
        getConceptsForSearch: function (topic, keyword) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/only/concepts', 'POST', {
                        topic: topic,
                        keyword: keyword
                    }),
                    resolve, reject
                )
            })
        },

        getVerbatimsByKeywordForCommandList: function (topic, commandList, ITERATION) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/keyword', 'POST', {
                        topic: topic,
                        commandList: JSON.stringify(commandList),
                        iteration: ITERATION
                    }),
                    resolve, reject
                )
            })
        },

        searchByTerm: function (topic, commandList, ITERATION) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/term', 'POST', {
                        topic: topic,
                        commandList: JSON.stringify(commandList),
                        iteration: ITERATION
                    }),
                    resolve, reject
                )
            })
        },

        searchByTag: function (topic, commandList, ITERATION) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/tag', 'POST', {
                        topic: topic,
                        commandList: JSON.stringify(commandList),
                        iteration: ITERATION
                    }),
                    resolve, reject
                )
            })
        },

        getWordCloudKeyword: function (query, topic, options) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/wordcloud/keyword', 'POST', {
                        topic: topic,
                        options: JSON.stringify(options),
                        query: query
                    }),
                    resolve, reject
                )
            })
        },
        getWordCloudTerm: function (query, topic, options) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/wordcloud/term', 'POST', {
                        topic: topic,
                        options: JSON.stringify(options),
                        query: query
                    }),
                    resolve, reject
                )
            })
        },
        getWordCloudConcept: function (query, topic, options) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/wordcloud/concept', 'POST', {
                        topic: topic,
                        options: JSON.stringify(options),
                        query: query
                    }),
                    resolve, reject
                )
            })
        },

        getWordCloudNoun: function (query, topic, options) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/wordcloud/noun', 'POST', {
                        topic: topic,
                        options: JSON.stringify(options),
                        query: query
                    }),
                    resolve, reject
                )
            })
        },

        getWordCloudVerb: function (query, topic, options) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/wordcloud/verb', 'POST', {
                        topic: topic,
                        options: JSON.stringify(options),
                        query: query
                    }),
                    resolve, reject
                )
            })
        },

        getWordCloudAdjective: function (query, topic, options) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/wordcloud/adjective', 'POST', {
                        topic: topic,
                        options: JSON.stringify(options),
                        query: query
                    }),
                    resolve, reject
                )
            })
        },

        searchByConcept: function (topic, commandList, languageMap, ITERATION) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/concept', 'POST', {
                        topic: topic,
                        commandList: JSON.stringify(commandList),
                        languagemap: JSON.stringify(languageMap),
                        iteration: ITERATION
                    }),
                    resolve, reject
                )
            })
        },

        searchByNoun: function (topic, word, ITERATION) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/noun', 'GET', {
                        topic: topic,
                        word: word,
                        iteration: ITERATION
                    }),
                    resolve, reject
                )
            })
        },

        searchByVerb: function (topic, word, ITERATION) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/verb', 'GET', {
                        topic: topic,
                        word: word,
                        iteration: ITERATION
                    }),
                    resolve, reject
                )
            })
        },
        searchByAdjective: function (topic, word, ITERATION) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/search/adjective', 'GET', {
                        topic: topic,
                        word: word,
                        iteration: ITERATION
                    }),
                    resolve, reject
                )
            })
        },

        getConceptsForVerbatims: function (verbatimList) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/concepts', 'POST', {
                        verbatims: verbatimList
                    }),
                    resolve, reject
                )
            })

        },

        getKeywordsForVerbatims: function (verbatimList) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/keywords', 'POST', {
                        verbatims: verbatimList
                    }),
                    resolve, reject
                )
            })

        },

        getTagsForVerbatims: function (verbatimList) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/tags', 'POST', {
                        verbatims: verbatimList
                    }),
                    resolve, reject
                )
            })
        },

        updateTagForVerbatim: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/tags/support', 'POST', data),
                    resolve, reject
                )
            })
        },

        updateFavorites: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/support/tagged', 'POST', data),
                    resolve, reject
                )
            })
        },

        getNERForVerbatims: function (verbatimList) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/ner', 'POST', {
                        verbatims: verbatimList
                    }),
                    resolve, reject
                )
            })
        },

        searchCommandsInWordClouds: function (type, commandList) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/wordcloud/search', 'POST', {
                        commandList: commandList,
                        type: type
                    }),
                    resolve, reject
                )
            })
        },
    },

    TAGGED: {
        EXPORT: {
            tagged: function (topics, tags) {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/dashboard/verbatims/plugins/export/tags', 'POST', {
                            topics: JSON.stringify(topics),
                            tags: JSON.stringify(tags),
                        }),
                        resolve, reject
                    )
                })
            },
            insights: function (topics, insightids) {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/dashboard/verbatims/plugins/export/insights', 'POST', {
                            topics: JSON.stringify(topics),
                            insightids: JSON.stringify(insightids),
                        }),
                        resolve, reject
                    )
                })
            },
        },
    },

    INSIGHTS: {
        getInsightsForProject: function (topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/get/insights', 'GET', {
                        topic: topic
                    }),
                    resolve, reject
                )
            })
        },

        getInsight: function (id) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/get/insight/byid', 'GET', {
                        id: id
                    }),
                    resolve, reject
                )
            })
        },

        createInsight: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/create', 'POST', data),
                    resolve, reject
                )
            })
        },

        editInsight: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/edit', 'POST', data),
                    resolve, reject
                )
            })
        },

        removeInsight: function (id, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/remove', 'POST', {
                        id: id,
                        topic: topic,
                    }),
                    resolve, reject
                )
            })
        },

        addTagsToInsight: function (id, query, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/add/tags', 'POST', {
                        id: id,
                        topic: topic,
                        query: query
                    }),
                    resolve, reject
                )
            })
        },

        removeTagFromInsight: function (id, tag, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/remove/tags', 'POST', {
                        id: id,
                        topic: topic,
                        tag: tag
                    }),
                    resolve, reject
                )
            })
        },

        getTaggedVerbatims: function (id, iteration, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/get/verbatims/tagged', 'POST', {
                        id: id,
                        iteration: iteration,
                        topic: topic
                    }),
                    resolve, reject
                )
            })
        },

        getPredictedVerbatims: function (id, iteration, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/get/verbatims/predicted', 'POST', {
                        id: id,
                        iteration: iteration,
                        topic: topic
                    }),
                    resolve, reject
                )
            })
        },

        setRelevanceForPrediction: function (topic, insightid, vid, relevance) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/set/predicted/relevance', 'POST', {
                        topic: topic,
                        insightid: insightid,
                        vid: vid,
                        relevance: relevance
                    }),
                    resolve, reject
                )
            })
        },

        ifCompiled: function (id, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/if/compiled', 'GET', {
                        id: id,
                        topic: topic,
                    }),
                    resolve, reject
                )
            })
        },

        getSIndex: function (id, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/get/sindex', 'POST', {
                        id: id,
                        topic: topic,
                    }),
                    resolve, reject
                )
            })
        },

        getModalFeatures: function (id, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/get/features', 'GET', {
                        id: id,
                        topic: topic,
                    }),
                    resolve, reject
                )
            })
        },

        removeFeature: function (id, insightid) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/remove/feature', 'POST', {
                        id: id,
                        insightid: insightid,
                    }),
                    resolve, reject
                )
            })
        },

        reCompileModal: function (insightid, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/insights/recompile/modal', 'POST', {
                        insightid: insightid,
                        topic: topic,
                    }),
                    resolve, reject
                )
            })
        }

    },

    DATASETS: {
        getDatasets: function () {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/tags/fetch/dataset', 'GET', null),
                    resolve, reject
                )
            })
        },

        requestSample: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/upload/dataset/request/sample', 'POST', data),
                    resolve, reject
                )
            })
        }
    },

    TAGS: {
        getTags: function (topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/tags/fetch/tags', 'POST', {
                        topic: topic
                    }),
                    resolve, reject
                )
            })
        },

        getVerbatims: function (tagList, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/tags/fetch/verbatims', 'POST', {
                        tagList: tagList,
                        topic: topic,
                    }),
                    resolve, reject
                )
            })
        },

        getAssociatedTags: function (tagList, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/tags/fetch/associated/tags', 'POST', {
                        tagList: tagList,
                        topic: topic
                    }),
                    resolve, reject
                )
            })
        }
    },

    SENTIMENT: {

        getSentimentStatistics: function (topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/sentiment/get/statistics', 'POST', {
                        topic: topic
                    }),
                    resolve, reject
                )
            })
        },

        getVerbatimsByTopicAndPolarity: function (topic, type, iteration) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/sentiment/get/verbatims/polarity', 'POST', {
                        topic: topic,
                        type: type,
                        iteration: iteration
                    }),
                    resolve, reject
                )
            })
        }

    },

    MULTIPLEVERBATIMS: {
        getSimiliraties: function (topic, verbatimList) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/multipleverbatims/similarity', 'POST', {
                        topic: topic,
                        verbatimList: JSON.stringify(verbatimList),
                    }),
                    resolve, reject
                )
            })
        }
    },

    HEATMAP: {
        getGlobal: function (topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/heatmap/get/global', 'POST', {
                        topic: topic
                    }),
                    resolve, reject
                )
            })
        }
    },

    STATS: {
        getEmotion: function (searchmode, commandList, languageMap, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/stats/emotion', 'POST', {
                        commandList: JSON.stringify(commandList),
                        searchmode: searchmode,
                        topic: topic,
                        languagemap: JSON.stringify(languageMap),
                    }),
                    resolve, reject
                )
            })
        },

        getNER: function (searchmode, commandList, languageMap, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/stats/ner', 'POST', {
                        commandList: JSON.stringify(commandList),
                        searchmode: searchmode,
                        topic: topic,
                        languagemap: JSON.stringify(languageMap),
                    }),
                    resolve, reject
                )
            })
        },

        getWordCloud: function (searchmode, targetmode, commandList, languageMap, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/stats/wordcloud', 'POST', {
                        commandList: JSON.stringify(commandList),
                        searchmode: searchmode,
                        targetmode: targetmode,
                        topic: topic,
                        languagemap: JSON.stringify(languageMap),
                    }),
                    resolve, reject
                )
            })
        },

        exportdataset: function (searchmode, commandList, languageMap, topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/stats/exportdataset', 'POST', {
                        commandList: JSON.stringify(commandList),
                        searchmode: searchmode,
                        topic: topic,
                        languagemap: JSON.stringify(languageMap),
                    }),
                    resolve, reject
                )
            })
        },

        SEARCH: {
            setToDefault: function () {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/dashboard/verbatims/plugins/stats/search/default', 'POST', {}),
                        resolve, reject
                    )
                })
            },

            addSearchLevel: function (data, onProgressDownload) {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/dashboard/verbatims/plugins/stats/search/addlevel', 'POST', data, onProgressDownload),
                        resolve, reject
                    )
                })
            },

            removeSearchLevel: function (data) {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/dashboard/verbatims/plugins/stats/search/removelevel', 'POST', data),
                        resolve, reject
                    )
                })
            },

            loadMoreVerbatims: function (topic) {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/dashboard/verbatims/plugins/stats/search/loadmore', 'POST', {
                            topic: topic
                        }),
                        resolve, reject
                    )
                })
            }
        },
    },

    SEARCHAGGREGATE: {
        getQueue: function () {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugins/aggregate/search/getsearchlist', 'POST', {}),
                    resolve, reject
                )
            })
        }
    },

    DASHBOARD: {
        getUser: function () {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user', 'GET', {}),
                    resolve, reject
                )
            })
        },

        getUserTopics: function (username, action, projectid) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/topics', 'GET', {
                        username: username,
                        action: action,
                        projectid: projectid
                    }),
                    resolve, reject
                )
            })
        },

        updateUserTopic: function (username, action, data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/topics/edit', 'POST', {
                        username: username,
                        action: action,
                        data: JSON.stringify(data)
                    }),
                    resolve, reject
                )
            })
        },

        getVerbatimCountForTopics: function (topics) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/topics/count', 'POST', {
                        topics: JSON.stringify(topics)
                    }),
                    resolve, reject
                )
            })
        },

        getTopicDetails: function (topic) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/topic/detail', 'POST', {
                        topic: topic
                    }),
                    resolve, reject
                )
            })
        },

        getUsersAndOrganizationsAndProjects: function () {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/organizations', 'GET', {}),
                    resolve, reject
                )
            })
        },

        getAllRadarlyFeatures: function() {
            return new Promise(function(resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax(
                        '/dashboard/radarly/user/project',
                        'GET',
                        {}
                    ),
                    resolve,
                    reject
                )
            })
        },

        getAllRadarlyDashboard: function(project) {
            return new Promise(function(resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax(
                        '/dashboard/radarly/user/dashboard',
                        'POST',
                        {project: project}
                    ),
                    resolve,
                    reject
                )
            })
        },

        getAllRadarlyFocus: function(project, dashboard) {
            return new Promise(function(resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax(
                        '/dashboard/radarly/user/focus',
                        'POST',
                        {project: project, dashboard: dashboard}
                    ),
                    resolve,
                    reject
                )
            })
        },

        updateTopicDetails: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/topic/update', 'POST', {
                        data: JSON.stringify(data)
                    }),
                    resolve, reject
                )
            })
        },

        getProjects: function () {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/project', 'GET', {}),
                    resolve, reject
                )
            })
        },

        getProjectDetails: function (projectid) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/project/detail', 'GET', {
                        projectid: projectid
                    }),
                    resolve, reject
                )
            })
        },

        createProject: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/d/user/project/manage', 'POST', data),
                    resolve, reject
                )
            })
        }
    },

    CADASHBOARD: {
        WCA: {
            getCountsPerTopic: function (topics, words) {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/dashboard/ca/wca', 'POST', {
                            topics: JSON.stringify(topics),
                            words: JSON.stringify(words),
                            action: "getcountspertopic",
                        }),
                        resolve, reject
                    )
                })
            }
        },

        ICA: {
            getQuantificationPerTopic: function (topics, insightids, insightBag) {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/dashboard/ca/ica', 'POST', {
                            topics: JSON.stringify(topics),
                            insightids: JSON.stringify(insightids),
                            insightBag: JSON.stringify(insightBag),
                            action: "getquantificationspertopic",
                        }),
                        resolve, reject
                    )
                })
            }
        },
    },

    ADMIN: {
        getUserById: function (id) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/admin/get/user/by/id', 'GET', {
                        id: id,
                    }),
                    resolve, reject
                )
            })
        },

        USER: {
            submitForm: function (data) {
                return new Promise(function (resolve, reject) {
                    CORE.handleRequest(
                        CORE.ajax('/admin/post/form/user', 'POST', data),
                        resolve, reject
                    )
                })
            }
        }
    },

    EXTRACTPROGRESS: {
        fetchDatasets: function () {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/extraction/get/datasets', 'GET', {}),
                    resolve, reject
                )
            })
        },

        getProgress: function (id) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/extraction/get/dataset/progress', 'GET', {id: id}),
                    resolve, reject
                )
            })
        }
    },

    LOG: {
        ERROR: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/log/error', 'POST', data),
                    resolve, reject
                )
            })
        },

    },

    UTILS: {
        HASH: function (action, string) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/utils/hash', 'POST', {action: action, string: string}),
                    resolve, reject
                )
            })
        },

        HISTORY: function (mode, action, content) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/utils/history', 'POST', {mode: mode, action: action, content: content}),
                    resolve, reject
                )
            })
        },
    },

    CLEANER: {
        doSearch: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugin/cleaner/search', 'POST', {
                        topic: data.topic,
                        mode: data.mode,
                        query: JSON.stringify(data.query)
                    }),
                    resolve, reject
                )
            })
        },

        disableVerbatims: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugin/cleaner/disable', 'POST', {
                        topic: data.topic,
                        mode: data.mode,
                        query: JSON.stringify(data.query)
                    }),
                    resolve, reject
                )
            })
        },

        getHistory: function (topic, iteration) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugin/cleaner/get/history', 'GET', {
                        topic: topic,
                        iteration: iteration,
                    }),
                    resolve, reject
                )
            })
        },

        undoChange: function (data) {
            return new Promise(function (resolve, reject) {
                CORE.handleRequest(
                    CORE.ajax('/dashboard/verbatims/plugin/cleaner/history/undochange', 'POST', data),
                    resolve, reject
                )
            })
        },

    },
}
