$(document).ready(function () {
    $('.dropdown').dropdown()
    $('input[name="title"]').focusout(checkTopicNameValidity)


    getDropdownContents()

    if (stage == 2) {
        var queryParams = QueryString()
        if (_.isUndefined(queryParams.m))
            loadSampleDataset()
        else {
            showMessage(queryParams.m)
        }
    }
})

//
// STAGE 1
//

function getDropdownContents() {
    var container
    API.DASHBOARD.getUsersAndOrganizationsAndProjects()
        .then(function (r) {
            container = $('select[name="project"]')
            r.projects.forEach(function (o) {
                container.append(
                    TEMPLATE.DASHBOARD.FORM.ITEM
                        .replace('$NAME$', o.name)
                        .replace('$VALUE$', o._id)
                )
            })
        })
}

function checkTopicNameValidity() {
    var form = $('#form-dataset')
    var data = form.form('get values')
    if (data.project == "") {
        // TODO dataset variables verification on client side, to avoid uploading file if variables in bad
        //API.DATASETS.checkTopicNameValidity(data).then(function (r) {})
        //$('button[type="submit"]').attr("disabled","disabled")
    }
}

//
// STAGE 2
//

var DATA
function loadSampleDataset() {
    var container = $('#container-preview')
    container.dimmer('show')

    API.DATASETS.requestSample($('#tmp-data').data()).then(function (res) {
        console.log("response from request sample", res)
        if (res.s) {
            DATA = res
            // project
            $('.vproject').text(res.project)
            // topic
            $('.vtopic').text(res.topic)
            // language
            $('.vlanguage').text(res.lang)
            // number verbatims
            $('.vcount').text(res.count)

            // verbatims
            var containerVerbatims = $('#list-verbatims')
            var appendVerbatimQueue = []
            DATA.verbatims.forEach(function (d, i) {
                appendVerbatimQueue.push(
                    appendVerbatim(
                        containerVerbatims,
                        d.text,
                        0,
                        'preview-' + i,
                        d.sentiment,
                        {},
                        "-verbatims"
                    )
                )
            })

            var procedLink = $('#upload-options-proceed').find('input[type="hidden"]').val()
            $('#upload-options-proceed').find('a').attr('href', procedLink + "&samplefile=" + res.samplepath)
            appendToVerbatimList(appendVerbatimQueue, {c: res.c}, $(), manuallyAppendKeywords)
            container.dimmer('hide')
        } else {
            window.location = res.m
        }
    })
}

function manuallyAppendKeywords() {
    var CONCEPTCOLORS = COLORS.CONCEPT
    var iter = 0
    $.each($('.verbatim'), function () {
        var self = $(this)
        var K = DATA.verbatims[iter].keywords
        var keywords = []
        K.forEach(function (k) {
            keywords.push({
                c: k[1],
                score: k[0]
            })
        })


        var keywordsAdded = []
        var list = self.find('.keyword-list')
        if (keywords != undefined) {
            keywords.forEach(function (keyword) {
                var keywordStr = keyword.c
                if (keywordsAdded.indexOf(keywordStr) == -1) {
                    var signalStrength
                    if (keyword.score > 0.50)
                        signalStrength = 'STRONG'
                    else if (keyword.score > 0.25)
                        signalStrength = 'NORMAL'
                    else
                        signalStrength = 'WEAK'

                    list.append(
                        TEMPLATE.VERBATIM.VERBATIMCONCEPT
                            .replace('$COLOR$', CONCEPTCOLORS[signalStrength])
                            .replace('$CONCEPT$', keywordStr)
                            .replace('$SCORE$', keyword.score.toFixed(2))
                            .replace('$DATA$', 'data-action="keyword" data-word="' + keywordStr + '"')
                    )
                    keywordsAdded.push(keywordStr)
                }
            });
        }


        iter++
    })
}

function updateMenuText() {
}

function showMessage(m) {
    var FailMessage = {
        EXCESSDATABIG: "Oops you cannot upload this dataset. " +
        "You have run out of Big Projects on your License. You can create a new Medium Project",
        EXCESSDATASMALL: "Oops you cannot upload this dataset. " +
        "You have run out of Big and Medium Projects. Please contact " +
        "<a href='mailto:hello@thedatastrategy.com'>InsightE</a> to extend your license.",
        NOTUTF8: "Your file was not recognized as UTF-8. InsightE requires all dataset files to be UTF-8 encoded.",
    }

    $('#upload-options-proceed').remove()
    $('#container-main')
        .empty()
        .append(
            '<div style="text-align:center">' +
            '   <h1 style="color:black;padding-top:100px;"> ' + FailMessage[m] + '</h1>' +
            '</div>'
        )
}