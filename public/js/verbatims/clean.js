function _VerbatimCleaner(container) {
    var self = this
    this.modal = $(container)
    this.SETTINGS = {
        topic: null,
        container: {
            dropdownSearchMode: '.dropdown-search-mode',
            inputSearch: '.input-search',
            buttonSearch: '.button-search',
            buttonDisabled: '.trigger-disable-verbatims',
            tabMenuHeader: '.menu-tab-header',
            listVerbatim: '.list-verbatim',
            listHistory: '.list-history',
            countVerbatim: '.count-verbatim',
            refreshReviewTrigger: '.refresh-review-trigger',
        },
        modes: {
            phrase: 'phrase'
        },
        history: {
            iteration: 0,
        },
    }

    this.updateReviewView = function () {
        self.SETTINGS.history.iteration = 0
        API.CLEANER.getHistory(self.SETTINGS.topic, self.SETTINGS.history.iteration).then(function (r) {
            var container = $(self.SETTINGS.container.listHistory)
            container.empty()
            r.r.forEach(function (item) {
                var date = new Date(item.date)
                var query
                if (item.query.mode == self.SETTINGS.modes.phrase) {
                    query = item.query.query
                } else {
                    query = ""
                    item.query.query.forEach(function (q1) {
                        q1.forEach(function (q2) {
                            query += q2 + ", "
                        })
                    })
                }
                container.append(
                    TEMPLATE.CLEANER.HISTORY.ITEM
                        .replace('$DATA$', 'data-id="' + item._id + '"')
                        .replace('$QUERY$', query)
                        .replace('$MODE$', item.query.mode)
                        .replace('$nbVerbatims$', item.nbVerbatims)
                        .replace('$DATE$', date.yyyymmdd())
                        .replace('$META$', item.user)
                )
            })
        })
    }

    this.updateViews = function () {
        self.updateReviewView()
    }

    this.open = function () {
        self.modal.modal('setting', 'closable', false).modal('show')
    }

    this.init = function () {
        // modal
        self.modal.modal({
            onShow: self.updateViews
        })

        // dropdown search modes
        self.modal.find(self.SETTINGS.container.dropdownSearchMode)
            .dropdown('set selected', 'keyword')

        // input search
        self.modal.find(self.SETTINGS.container.inputSearch)
            .on('keyup', function (e) {
                if (e.keyCode == 13) {
                    self.doSearch()
                }
            })

        // button search
        self.modal.find(self.SETTINGS.container.buttonSearch)
            .on('click', self.doSearch)

        // tab
        self.modal.find(self.SETTINGS.container.tabMenuHeader)
            .find('.item')
            .tab({})


        // button disable
        self.modal.find(self.SETTINGS.container.buttonDisabled)
            .on('click', self.triggerDisable)

        //
        // test
        // self.setTopic('Energie')
        // self.open()
        // $('#container-clean-verbatims .input-search').val('barack obama')
        // $('#container-clean-verbatims .button-search').click()
        // $('[data-tab="preview"]').click()
        // setTimeout(function () {
        //     // self.modal.find(self.SETTINGS.container.buttonDisabled).click()
        // }, 1000)
    }

    this.setTopic = function (topic) {
        self.SETTINGS.topic = topic
    }

    this.getParameters = function () {
        return {
            topic: self.SETTINGS.topic,
            mode: self.modal.find(self.SETTINGS.container.dropdownSearchMode).dropdown('get value'),
            query: self.modal.find(self.SETTINGS.container.inputSearch).val(),
        }
    }

    this.ifValidParameters = function (data) {
        return data.query.trim() != "";
    }

    this.populateVerbatimList = function (r, callback) {
        var container = self.modal.find(self.SETTINGS.container.listVerbatim)
        container.empty()
        r.verbatims.forEach(function (verbatim) {
            var formatted_text = verbatim.verbatim
            var keywords
            if (!_.isArray(r.q.query)) {
                keywords = [[r.q.query]]
            } else {
                keywords = r.q.query
            }

            for (var i = 0; i < keywords.length; i++) {
                for (var j = 0; j < keywords[i].length; j++) {
                    var keyword = keywords[i][j]
                    formatted_text = formatFeedText(keyword, formatted_text, i + j)
                }
            }

            container.append(
                TEMPLATE.CLEANER.VERBATIM.ITEM
                    .replace('$VERBATIM$', formatted_text)
            )
        })
        if (!_.isUndefined(callback))
            callback()
    }

    this.updateSearchStatistics = function (r, callback) {
        var container = self.modal.find(self.SETTINGS.container.countVerbatim)
        container.text(r.c)
    }

    this.doSearch = function () {
        var data = self.getParameters()
        var mode = data.mode
        var query = data.query

        if (self.ifValidParameters(data)) {
            if (mode != self.SETTINGS.modes.phrase) {
                data.query = CommandParser.parse(query)
            }

            API.CLEANER.doSearch(data).then(function (r) {
                if (r.s) {
                    self.populateVerbatimList(r)
                    self.updateSearchStatistics(r)
                }
            })
        } else {
            console.warn('Verbatim Cleaner, parameters not valid!')
        }
    }

    this.triggerDisable = function () {
        var data = self.getParameters()
        var mode = data.mode
        var query = data.query

        if (self.ifValidParameters(data)) {
            if (mode != self.SETTINGS.modes.phrase) {
                data.query = CommandParser.parse(query)
            }
            API.CLEANER.disableVerbatims(data).then(function (r) {
                if (r.s) {
                    var container = self.modal.find(self.SETTINGS.container.listVerbatim)
                    container.empty()
                    container.append('<h1> Verbatims Disabled </h1>')
                }
            })
        }
    }

    this.undoChange = function (o) {
        var $this = $(o).parent().parent().parent()
        var data = $this.data()
        data.topic = self.getParameters().topic
        API.CLEANER.undoChange(data).then(function (r) {
            if (r.s) {
                $this.slideUp()
            }
        })
    }

    // init
    this.init()

}




