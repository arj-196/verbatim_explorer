$(document).ready(function () {
    $.each($('#verbatim-selected-options').find('.verbatim-selected-options-trigger'), function () {
        $(this).click(onClickVerbatimSelectedOptions)
    })
})

var onClickVerbatimSelectedOptions = function () {
    var self = $(this)
    var mode = self.data().mode
    if (mode == "similarity") {
        if (config.VERBATIMLIST.length > 1)
            actionMVGetSimilarities()
    } else if (mode == "unselect-all") {
        config.VERBATIMLIST = []
        toggleMVOptonsContainer('hide')
        $.each($('.verbatim-selected'), function () {
            var self = $(this)
            formatSelectedVerbatim(self, "unselect")
        })
    }
}

var selectVerbatim = function (o) {
    var self = $(o)
    var verbatimContainer = self.parent().parent().parent()
    var id = verbatimContainer.data().id

    if (self.prop('checked')) {
        // select
        verbatimContainer.addClass('verbatim-selected')
        formatSelectedVerbatim(verbatimContainer, "select")
        if (!_.contains(config.VERBATIMLIST, id)) {
            config.VERBATIMLIST.push(id)
        }
    } else {
        // unselect
        verbatimContainer.removeClass('verbatim-selected')
        formatSelectedVerbatim(verbatimContainer, "unselect")
        config.VERBATIMLIST.splice(_.indexOf(config.VERBATIMLIST, id), 1)
    }

    if (config.VERBATIMLIST.length > 1) {
        toggleMVOptonsContainer('show')
    } else {
        toggleMVOptonsContainer('hide')
    }

    defaultMVActions()
}

var toggleMVOptonsContainer = function (mode) {
    var optionsContainer = $('#verbatim-selected-options')
    if (mode == 'show') {
        if (optionsContainer.data().mode != "show" && config.VERBATIMLIST.length > 1) {
            if (!config.MV.ACTIONS.SIMILARITY) {
                optionsContainer.fadeIn()
                optionsContainer.data("mode", "show")
            }
        }
    } else {
        if (optionsContainer.data().mode != "hide") {
            optionsContainer.fadeOut()
            optionsContainer.data("mode", "hide")
        }
    }
}

var defaultMVActions = function () {
    if (config.VERBATIMLIST.length > 1) {


        // similarity
        if (config.MV.ACTIONS.SIMILARITY)
            actionMVGetSimilarities()
    } else {
        // close all multiple
        toggleMultipleVerbatimPanel('hide')
    }
}

var formatSelectedVerbatim = function (container, mode) {
    if (mode == "select") {
        container.css({
            boxShadow: 'rgb(33, 133, 208) 2px 2px 20px 2px',
        })
        container.find('.action-select-verbatim').prop('checked', true)
    } else if (mode == "unselect") {
        container.css({
            boxShadow: '0 2px 4px 0 rgba(34,36,38,.12),0 2px 10px 0 rgba(34,36,38,.08)',
        })
        container.find('.action-select-verbatim').prop('checked', false)
    }
}

var toggleMultipleVerbatimPanel = function (overwrite) {
    var container = $('#multiple-verbatim-panel')
    var mode
    if (!overwrite) {
        mode = container.data().mode
    } else {
        mode = overwrite
    }

    if (mode == "show") {
        toggleMVOptonsContainer('hide')
        //container.fadeIn()
        if (container.css('display') == 'none') {
            container.transition('fly right')
            container.data("mode", "hide")
        }
    } else {
        if (container.css('display') != 'none') {
            //container.fadeOut()
            container.transition('fly right')
            container.data("mode", "show")
            toggleMVOptonsContainer('show')
            config.MV.SIMILARITEMLIST = []
            setVerbatimsToDefaultState()
        }
    }
}

var actionMVGetSimilarities = function () {
    var topic = getTopic()
    API.MULTIPLEVERBATIMS.getSimiliraties(topic, config.VERBATIMLIST)
        .then(function (r) {
            console.log("response", r)
            toggleMultipleVerbatimPanel('show')
            createMVSimilarityList(r.similarities)
            config.MV.ACTIONS.SIMILARITY = true
        })
}

var actionMVCloseSimilarities = function () {
    config.MV.ACTIONS.SIMILARITY = false
    toggleMultipleVerbatimPanel('hide')
}

var COLOUMORDER = [
    "keywords",
    "nouns",
    "adjectives",
    "verbs",
    "terms"
]



var createMVSimilarityList = function (data) {
    var container = $('#multiple-verbatim-panel-list')
    container.empty()
    // header
    container.append(TEMPLATE.VERBATIM.MV.HEADER.replace('$HEADER$', 'SIMILARITY'))

    var order = _.sortBy(
        _.map(
            _.keys(data.words),
            function (num) {
                return parseInt(num)
            }
        ),
        function (num) {
            return num
        })

    for (var i = order.length - 1; i >= 0; i--) {
        // breadcrumb
        var idBreadCrumb = "mv-bc-" + i
        container.append(TEMPLATE.VERBATIM.MV.BREADCRUMB.CONTAINER.replace('$ID$', idBreadCrumb))
        $('#' + idBreadCrumb).append(buildBreadCrumb(order[i]))

        // subdataset
        var subdataset = data.words[order[i]]
        var subdatasetKeys = _.keys(subdataset)

        var idSubDataset = "mv-subdata-" + i
        container
            .append(TEMPLATE.VERBATIM.MV.NEWLINE)
            .append(TEMPLATE.VERBATIM.MV.ITEM.CONTAINER.replace('$ID$', idSubDataset))

        var subDatasetContainer = $('#' + idSubDataset)

        COLOUMORDER.forEach(function (key) {
            var color = COLORS.SIMILARITY.keys[key]
            if (subdatasetKeys.indexOf(key) != -1) {
                // append column
                if (_.keys(subdataset[key]).length > 0) {
                    var idITEMCONTAINER = "mv-item-container-" + i + "-" + key
                    subDatasetContainer.append(
                        TEMPLATE.VERBATIM.MV.ITEM.SUBCONTAINER
                            .replace('$LISTID$', idITEMCONTAINER)
                            .replace('$HEADER$', key)
                    )
                    // append column items
                    var itemContainer = $('#' + idITEMCONTAINER)
                    var d = subdataset[key]
                    var items = _.keys(d)
                    items.forEach(function (item) {
                        itemContainer.append(
                            TEMPLATE.VERBATIM.MV.ITEM.ITEM
                                .replace('$NAME$', item)
                                .replace('$COLOR$', color)
                                .replace('$FUNC$', 'onClickSilimarItem(this)')
                                .replace('$DATA$', 'data-ids=' + JSON.stringify(d[item]) + '' +
                                    ' data-mode="similar" data-type="' + key +'"')
                        )
                    })
                }
            }
        })
        // divider
        container.append(TEMPLATE.VERBATIM.MV.DIVIDER)
    }
}

var buildBreadCrumb = function (n) {
    var MAXLENGTH = 5
    var string = ""

    for (var i = 1; i < n + 1; i++) {
        if (i < MAXLENGTH) {
            string += TEMPLATE.VERBATIM.MV.BREADCRUMB.ITEM.replace('$NAME$', 'V' + i)
            if (i != n) {
                string += TEMPLATE.VERBATIM.MV.BREADCRUMB.DIVIDER
            }
        }
    }

    if (n == MAXLENGTH) {
        string += TEMPLATE.VERBATIM.MV.BREADCRUMB.ITEM.replace('$NAME$', 'V' + n)

    } else if (n > MAXLENGTH) {
        string += TEMPLATE.VERBATIM.MV.BREADCRUMB.ITEM.replace('$NAME$', '...')
            + TEMPLATE.VERBATIM.MV.BREADCRUMB.DIVIDER
            + TEMPLATE.VERBATIM.MV.BREADCRUMB.ITEM.replace('$NAME$', 'V' + n)
    }

    return string
}

var onClickSilimarItem = function (o) {
    var self = $(o)
    var data = self.data()

    if(!self.hasClass('ar-item-selected')){
        // on
        self.addClass('ar-item-selected')
            .css({
                color: 'white !important',
                boxShadow: '0 0 15px black'
            })
    } else {
        // off
        self.removeClass('ar-item-selected')
            .css({
                color: 'black !important',
                boxShadow: 'none'
            })
    }

    var intersectingIds = null
    $.each($('.ar-item-selected'), function(){
        if(intersectingIds == null){
            intersectingIds = $(this).data().ids
        } else {
            intersectingIds = _.intersection(intersectingIds, $(this).data().ids)
        }
    })
    config.MV.SIMILARITEMLIST = intersectingIds
    MVHightlightVerbatims(config.MV.SIMILARITEMLIST)
}

var MVHightlightVerbatims = function (ids) {
    if(!ids || ids.length == 0)
        setVerbatimsToDefaultState()
    else {
        $.each($('.verbatim'), function () {
            var self = $(this)
            if(ids.indexOf(self.data().id) == -1){
                self.dimmer('show')
            } else {
                self.dimmer('hide')
            }
        })
    }
}

var setVerbatimsToDefaultState = function () {
    $('.verbatim').dimmer('hide')
}
