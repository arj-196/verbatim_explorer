//
// Input Search
//

function SearchEngine(name) {
    this.name = name
    this.turnOffDefault = function () {
        config.VERBATIMLIST = []
        config.PREVIOUS_SEARCH_TEXT = null
        toggleMVOptonsContainer('hide')
        closeSearchDropdown()
    }

    this.turnOnDefault = function () {
        config.SEARCHMODE = this.name
        config.VERBATIMLIST = []
        config.IFDEFAULTLOAD = false
        config.IFWORDCLOUDSEARCH = false
        toggleMVOptonsContainer('hide')
        toggleMultipleVerbatimPanel('hide')
    }

    this.turnOff = function () {
        this.turnOffDefault()
    }

    this.turnOn = function () {
        this.turnOnDefault()
    }
}

var conceptSearchEngine = new SearchEngine("concept")
var keywordSearchEngine = new SearchEngine("keyword")
var textSearchEngine = new SearchEngine("text")
var tagSearchEngine = new SearchEngine("tag")
Dashboard.Engines.push(conceptSearchEngine)
Dashboard.Engines.push(keywordSearchEngine)
Dashboard.Engines.push(textSearchEngine)
Dashboard.Engines.push(tagSearchEngine)

var dropdownListEngine = new Engine("dropdownlist")

function changeSearchMode(mode) {
    Dashboard.turnOn(mode)
}

function searchForText(ifSaveHistory, ifLoadMore) {
    var self = $('#input-search')
    var topic = getTopic()
    if (!_.isNull(topic)) {
        var text = self.val()
        if (!ifLoadMore) {
            TOPICITERATION = 0
        }
        if (topic.trim() != "") {
            if (text.trim() == "") {
                defaultVerbatims()
            } else {
                searchDBForVerbatims(text, topic, ifSaveHistory, ifLoadMore)
            }
        }
        StatsSearchEngine.defaultState()
    }
}

function getCommandListObject() {
    var text = $('#input-search').val()
    return [CommandParser.parse(text), CommandParser.parse_not(text)]
}

var searchDBForVerbatims = function (text, topic, ifSaveHistory, ifLoadMore) {
    var commandList = getCommandListObject()
    // popup message
    popup('Loading Verbatims For ' + config.SEARCHMODE.toUpperCase()
        + ': <strong>' + text + '</strong>')

    Dashboard.search(config.SEARCHMODE, topic, commandList, ifLoadMore)
    if (!ifLoadMore) {
        runDefaultStatistics(commandList, topic)
    }
    // save to history
    if (ifSaveHistory)
        addSearchTextToHistory(text)
}

dropdownListEngine.fillList = function () {
    if (config.SEARCHMODE == "keyword")
        dropdownListEngine.fillListKeyword()
    else if (config.SEARCHMODE == "concept")
        dropdownListEngine.fillListConcept()
    else if (config.SEARCHMODE == "text")
        dropdownListEngine.fillListTerm()
    else if (config.SEARCHMODE == "tag")
        dropdownListEngine.fillListTag()
}

//
// SearchEngine.turnOff overwrites
//

conceptSearchEngine.turnOff = function () {
    config.CONCEPTSEARCH.CONCEPTTOLANGMAP = {}
}

//
// SearchEngine.search definitions
//

keywordSearchEngine.search = function (topic, commandList, ifLoadMore) {
    API.VERBATIMS.getVerbatimsByKeywordForCommandList(topic, commandList, TOPICITERATION)
        .then(function (res) {
            var sortedData = res.r.sort(function (a, b) {
                return d3.descending(a.score, b.score)
            })
            appendSearchedVerbatims(keywordSearchEngine.name, commandList, topic, sortedData, res.c,
                ifLoadMore, res.k, "keyword")
        })
}

conceptSearchEngine.getLanguageMap = function (commandList) {
    var languageMap = {}
    if (commandList) {
        commandList.forEach(function (cL1) {
            if (!_.isNull(cL1) && !_.isUndefined(cL1)) {
                cL1.forEach(function (cL) {
                    cL.forEach(function (command) {
                        languageMap[command] = config.CONCEPTSEARCH.CONCEPTTOLANGMAP[command]
                    })
                })
            }
        })
    }
    return languageMap
}

conceptSearchEngine.search = function (topic, commandList, ifLoadMore) {
    var languageMap = this.getLanguageMap(commandList)
    API.VERBATIMS.searchByConcept(topic, commandList, languageMap, TOPICITERATION)
        .then(function (res) {
            var sortedData = res.verbatims.sort(function (a, b) {
                return d3.descending(a.score, b.score)
            })
            appendSearchedVerbatims(conceptSearchEngine.name, commandList, topic, sortedData, res.c,
                ifLoadMore, res.keywords, null)
        })
}

textSearchEngine.search = function (topic, commandList, ifLoadMore) {
    API.VERBATIMS.searchByTerm(topic, commandList, TOPICITERATION)
        .then(function (res) {
            var sortedData = res.r.sort(function (a, b) {
                return d3.descending(a.score, b.score)
            })
            appendSearchedVerbatims(textSearchEngine.name, commandList, topic, sortedData, res.c,
                ifLoadMore, res.k, "term")
        })
}

tagSearchEngine.search = function (topic, commandList, ifLoadMore) {
    API.VERBATIMS.searchByTag(topic, commandList, TOPICITERATION)
        .then(function (res) {
            var sortedData = res.r.sort(function (a, b) {
                return d3.descending(a.score, b.score)
            })
            appendSearchedVerbatims(tagSearchEngine.name, commandList, topic, sortedData, res.c,
                ifLoadMore, [], null)
        })
}

var appendSearchedVerbatims = function (engineName, commandList, topic, sortedData, count, ifLoadMore, keywords, keywordField) {
    config.IFDEFAULTLOAD = false
    if (!ifLoadMore) {
        refreshWordCloud(engineName, commandList, topic)
    }
    var container = $('#container-verbatims')
    if (!ifLoadMore)
        container.empty()

    var appendVerbatimQueue = []
    sortedData.forEach(function (data) {
        var formatted_text = data.verbatim
        for (var i = 0; i < keywords.length; i++) {
            if (keywords[i]) {
                var keyword
                if (keywordField)
                    keyword = keywords[i][keywordField]
                else
                    keyword = keywords[i]

                formatted_text = formatFeedText(keyword, formatted_text, i)
            }
        }
        var score = 0
        if (!_.isUndefined(data.score))
            score = data.score.toFixed(2)
        appendVerbatimQueue.push(appendVerbatim(container, formatted_text,
            score, data._id, data.options, data))
    })

    // if no results found
    if (sortedData.length > 0)
        appendToVerbatimList(appendVerbatimQueue, {c: count})
    else if (sortedData.length == 0 && TOPICITERATION == 0) {
        container.append(
            TEMPLATE.VERBATIM.NOFOUNDVERBATIM
                .replace('$TEXT$', "No Verbatims Found")
                .replace('$ID$', "")
        )
    }
    else {
        $('#container-verbatims-load-more').remove()
    }
}


//
// search dropdown function
//

dropdownListEngine.open = function () {
    var container = $('#container-search-dropdown')
    if (!container.data('show')) {
        container.data('show', true)
        container.find('.mode')
            .text(config.SEARCHMODE.toLowerCase())
        container
            .transition('fade down')
    }
}

function closeSearchDropdown() {
    dropdownListEngine.close()
}

dropdownListEngine.close = function () {
    var container = $('#container-search-dropdown')
    if (container.data('show')) {
        container.data('show', false)
        container
            .transition('fade down')
        dropdownListEngine.empty()
    }
}

var appendDropdownListItems = function () {

}

dropdownListEngine.fillListKeyword = function () {
    var container = $('#container-search-dropdown-list')
    var topic = getTopic()
    var text = $('#input-search').val()
    if (text.trim() != "") {
        var commandList = CommandParser.parse(text)
        var keywordsToBeLoaded = [], keywordRegexToBeLoaded = []
        commandList.forEach(function (cL) {
            cL.forEach(function (keyword) {
                if (keyword.trim() != "") {
                    keywordsToBeLoaded.push(API.VERBATIMS.getKeywords(topic, keyword))
                    keywordRegexToBeLoaded.push(API.VERBATIMS.getKeywordsRegex(topic, keyword))
                }
            })
        })
        Promise.all(keywordsToBeLoaded)
            .then(function (r) {
                Promise.all(keywordRegexToBeLoaded)
                    .then(function (rRegex) {
                        dropdownListEngine.empty()
                        for (var i = 0; i < r.length; i++) {
                            var keywordList = r[i], keywordRegexList = rRegex[i]
                            var appendedKeywords = []
                            var color = getColorForItem(i)
                            keywordList.forEach(function (keyword) {
                                if (appendedKeywords.indexOf(keyword.keyword) == -1) {
                                    container.append(
                                        TEMPLATE.VERBATIM.SEARCHDROPDOWNLISTITEM
                                            .replace('$NAME$', keyword.keyword)
                                            .replace('$COLOR$', color)
                                            .replace('$FUNC$', "onClickSearchDropdownItemKeyword(this, " + i + ")")
                                    )
                                    appendedKeywords.push(keyword.keyword)
                                }
                            })
                            container.append("<span>|</span>")
                            keywordRegexList.forEach(function (keyword) {
                                if (appendedKeywords.indexOf(keyword.keyword) == -1) {
                                    container.append(
                                        TEMPLATE.VERBATIM.SEARCHDROPDOWNLISTITEM
                                            .replace('$NAME$', keyword.keyword)
                                            .replace('$COLOR$', color)
                                            .replace('$FUNC$', "onClickSearchDropdownItemKeyword(this, " + i + ")")
                                    )
                                    appendedKeywords.push(keyword.keyword)
                                }
                            })
                            container.append('<br/><div style="width:100%;height:7px;padding:0;margin:0 0 5px 0;' +
                                'border-bottom: dashed 1px lightgray;"></div>')
                        }
                    })
            })
    }
}

dropdownListEngine.fillListTerm = function () {
    var container = $('#container-search-dropdown-list')
    var topic = getTopic()
    var text = $('#input-search').val()
    if (text.trim() != "") {
        var commandList = CommandParser.parse(text)
        var termsToBeLoaded = [], termRegexToBeLoaded = []
        commandList.forEach(function (cL) {
            cL.forEach(function (keyword) {
                if (keyword.trim() != "") {
                    termsToBeLoaded.push(API.VERBATIMS.getTerms(topic, keyword))
                    termRegexToBeLoaded.push(API.VERBATIMS.getTermsRegex(topic, keyword))
                }
            })
        })
        Promise.all(termsToBeLoaded)
            .then(function (r) {
                Promise.all(termRegexToBeLoaded)
                    .then(function (rRegex) {
                        dropdownListEngine.empty()
                        for (var i = 0; i < r.length; i++) {
                            var termList = r[i], termRegexList = rRegex[i]
                            var appendedKeywords = []
                            var color = getColorForItem(i)
                            termList.forEach(function (keyword) {
                                if (appendedKeywords.indexOf(keyword.term) == -1) {
                                    container.append(
                                        TEMPLATE.VERBATIM.SEARCHDROPDOWNLISTITEM
                                            .replace('$NAME$', keyword.term)
                                            .replace('$COLOR$', color)
                                            .replace('$FUNC$', "onClickSearchDropdownItemTerm(this, " + i + ")")
                                    )
                                    appendedKeywords.push(keyword.term)
                                }
                            })
                            container.append("<span>|</span>")
                            termRegexList.forEach(function (keyword) {
                                if (appendedKeywords.indexOf(keyword.term) == -1) {
                                    container.append(
                                        TEMPLATE.VERBATIM.SEARCHDROPDOWNLISTITEM
                                            .replace('$NAME$', keyword.term)
                                            .replace('$COLOR$', color)
                                            .replace('$FUNC$', "onClickSearchDropdownItemTerm(this, " + i + ")")
                                    )
                                    appendedKeywords.push(keyword.term)
                                }
                            })
                            container.append('<br/><div style="width:100%;height:7px;padding:0;margin:0 0 5px 0;' +
                                'border-bottom: dashed 1px lightgray;"></div>')
                        }
                    })
            })
    }
}

dropdownListEngine.fillListConcept = function () {
    var container = $('#container-search-dropdown-list')
    var topic = getTopic()
    var text = $('#input-search').val()
    if (text.trim() != "") {
        var commandList = CommandParser.parse(text)
        var conceptsToBeLoaded = []
        var conceptList = []
        commandList.forEach(function (cL) {
            cL.forEach(function (keyword) {
                if (keyword.trim() != "") {
                    conceptsToBeLoaded.push(API.VERBATIMS.getConceptsForSearch(topic, keyword))
                    conceptList.push(keyword)
                }
            })
        })
        Promise.all(conceptsToBeLoaded)
            .then(function (r) {
                dropdownListEngine.empty()
                for (var i = 0; i < r.length; i++) {
                    var conceptList = r[i]
                    var appendedConcepts = []
                    var color = getColorForItem(i)
                    conceptList.forEach(function (concept) {
                        var parsedConcept = ConceptParser.parse(concept.concept)
                        var lang = ConceptParser.getLang(concept.concept)
                        if (appendedConcepts.indexOf(parsedConcept) == -1) {
                            container.append(
                                TEMPLATE.VERBATIM.SEARCHDROPDOWNLISTITEM
                                    .replace('$NAME$', parsedConcept)
                                    .replace('$COLOR$', color)
                                    .replace('$FUNC$', "onClickSearchDropdownItemConcept(this, " + i + ")")
                                    .replace('$OPTIONAL$', "data-lang='" + lang + "'")
                            )
                            appendedConcepts.push(parsedConcept)
                        }
                    })
                    container.append('<br/><div style="width:100%;height:7px;padding:0;margin:0 0 5px 0;' +
                        'border-bottom: dashed 1px lightgray;"></div>')
                }

                // get languages for concepts not selected from dropdown list
                var bufferMap = {}
                conceptList.forEach(function (concept) {
                    if (config.CONCEPTSEARCH.CONCEPTTOLANGMAP[concept] == undefined) {
                        if (bufferMap[concept] == undefined) {
                            $.each($('#container-search-dropdown-list').find('.label'), function () {
                                var self = $(this)
                                var text = self.text().trim()
                                if (bufferMap[text] == undefined)
                                    bufferMap[text] = self.data().lang
                                if (config.CONCEPTSEARCH.CONCEPTTOLANGMAP[text] == undefined)
                                    config.CONCEPTSEARCH.CONCEPTTOLANGMAP[text] = self.data().lang
                            })
                        }
                    }
                })
            })
    }
}

dropdownListEngine.fillListTag = function () {
    var container = $('#container-search-dropdown-list')
    var topic = getTopic()
    var text = $('#input-search').val()
    if (text.trim() != "") {
        var commandList = CommandParser.parse(text)
        var tagsToBeLoaded = []
        commandList.forEach(function (cL) {
            cL.forEach(function (keyword) {
                if (keyword.trim() != "") {
                    tagsToBeLoaded.push(API.VERBATIMS.getTags(topic, keyword))
                }
            })
        })
        Promise.all(tagsToBeLoaded)
            .then(function (r) {
                dropdownListEngine.empty()
                for (var i = 0; i < r.length; i++) {
                    var tagList = r[i]
                    var appendedKeywords = []
                    var color = getColorForItem(i)
                    tagList.forEach(function (tag) {
                        if (appendedKeywords.indexOf(tag) == -1) {
                            container.append(
                                TEMPLATE.VERBATIM.SEARCHDROPDOWNLISTITEM
                                    .replace('$NAME$', tag)
                                    .replace('$COLOR$', color)
                                    .replace('$FUNC$', "onClickSearchDropdownItemTag(this, " + i + ")")
                            )
                            appendedKeywords.push(tag)
                        }
                    })
                    container.append('<br/><div style="width:100%;height:7px;padding:0;margin:0 0 5px 0;' +
                        'border-bottom: dashed 1px lightgray;"></div>')
                }
            })
    }
}

dropdownListEngine.empty = function () {
    $('#container-search-dropdown-list')
        .empty()
}

function replaceCommandListByWordOnIndex(word, index) {
    var inputSearch = $('#input-search')
    var commandList = CommandParser.parse(inputSearch.val())
    var newCommandList = []
    var iteration = 0
    if (!_.isNull(commandList)) {
        commandList.forEach(function (cL) {
            var newCL = []
            cL.forEach(function (command) {
                if (iteration == index) {
                    newCL.push(word)
                } else {
                    newCL.push(command)
                }
                iteration++
            })
            newCommandList.push(newCL)
        })
    }
    inputSearch.val(CommandParser.toString(newCommandList))
}

//
// On click dropdown item
//

function onClickSearchDropdownItemTerm(o, i) {
    var self = $(o)
    var text = self.text().trim()
    replaceCommandListByWordOnIndex(text, i)
    dropdownListEngine.fillListTerm()
}

function onClickSearchDropdownItemTag(o, i) {
    var self = $(o)
    var text = self.text().trim()
    replaceCommandListByWordOnIndex(text, i)
    dropdownListEngine.fillListTag()
}

function onClickSearchDropdownItemKeyword(o, i) {
    var self = $(o)
    var text = self.text().trim()
    replaceCommandListByWordOnIndex(text, i)
    dropdownListEngine.fillListKeyword()
}

function onClickSearchDropdownItemConcept(o, i) {
    var self = $(o)
    var text = self.text().trim()
    replaceCommandListByWordOnIndex(text, i)
    config.CONCEPTSEARCH.CONCEPTTOLANGMAP[text] = self.data().lang
}

//
// Wordcloud Search
//

var onClickWord = function (word, ifLoadMore) {
    config.VERBATIMLIST = []
    // first time wordcloud click or change in wordcloud mode
    if (!config.IFWORDCLOUDSEARCH || config.WORDCLOUD.PREVIOUSMODE != word.action || config.WORDCLOUD.PREVIOUSWORD.id != word.id) {
        TOPICITERATION = 0
        config.WORDCLOUD.PREVIOUSMODE = word.action
        config.WORDCLOUD.PREVIOUSWORD = word
        $('#container-verbatims').empty()
    } else if (!ifLoadMore) {
        // choosing same search as previous
        return
    }

    config.IFDEFAULTLOAD = false
    config.IFWORDCLOUDSEARCH = true
    var topic = getTopic()
    popup('Loading Verbatims For ' + word.action.toUpperCase() + ': <strong>' + word.text + '</strong>')

    if (word.action == 'concept') {
        // TODO deprecated for moment
    }

    else if (word.action == 'noun') {
        API.VERBATIMS.searchByNoun(topic, word.id, TOPICITERATION)
            .then(function (res) {
                if (res.status) {
                    config.PREVIOUS_SEARCH_TEXT = null
                    appendVerbatimResultsFromPOS(res, 1)
                }
            })

    } else if (word.action == 'verb') {
        API.VERBATIMS.searchByVerb(topic, word.id, TOPICITERATION)
            .then(function (res) {
                if (res.status) {
                    config.PREVIOUS_SEARCH_TEXT = null
                    appendVerbatimResultsFromPOS(res, 3)
                }
            })

    } else if (word.action == 'adjective') {
        API.VERBATIMS.searchByAdjective(topic, word.id, TOPICITERATION)
            .then(function (res) {
                if (res.status) {
                    config.PREVIOUS_SEARCH_TEXT = null
                    appendVerbatimResultsFromPOS(res, 3)
                }
            })
    }
}

function appendVerbatimResultsFromPOS(res, i) {
    var container = $('#container-verbatims')
    var appendVerbatimQueue = []
    res.r.forEach(function (data) {
        var formatted_text = formatFeedText(res.k, data.verbatim, i)
        appendVerbatimQueue.push(appendVerbatim(container, formatted_text, data.score.toFixed(2), data._id, data.options, data))
    })
    appendToVerbatimList(appendVerbatimQueue, {c: res.c})

    // if no results found
    if (res.r.length == 0)
        container.append(
            TEMPLATE.VERBATIM.NOFOUNDVERBATIM
                .replace('$TEXT$', "No Verbatims Found")
        )
}

//
// Search History
//

var addSearchTextToHistory = function (text) {
    var container = $('#container-search-history').find('ul')
    var ifDuplicate = false
    $.each(container.find('li:contains("' + text + '")'), function () {
        var self = $(this)
        if (self.text() == text)
            ifDuplicate = true
    })

    if (!ifDuplicate) {
        if (container.find('li').length > config.SEARCH_HISTORY_MAX_LENGTH) {
            container.find('li').last().remove()
        }
        container.prepend(
            '<li onclick="searchTextHistoryOnClick(this)">' + text + '</li>'
        )
    }
}

var searchTextHistoryOnClick = function (obj) {
    var self = $(obj)
    var text = self.text()
    $('#input-search').val(text)
    searchForText(false)

    // remove and prepend to top
    self.remove()
    $('#container-search-history').find('ul').prepend(
        '<li onclick="searchTextHistoryOnClick(this)">' + text + '</li>'
    )
}


//
// Stats Aggregated Search
//

function addWordToCommandList(word) {
    // DEPRECATED
    var inputSearch = $('#input-search')
    var commandList = CommandParser.parse(inputSearch.val())
    var newCommandList = []
    if (_.isNull(commandList)) {
        newCommandList = [[word]]
    } else {
        var andList = commandList[commandList.length - 1]
        andList.push(word)
        newCommandList = commandList
        newCommandList[newCommandList.length - 1] = andList
    }
    inputSearch.val(CommandParser.toString(newCommandList))
    searchForText(true, false)
}
