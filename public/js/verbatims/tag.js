$(document).ready(function () {
    $('#input-tagged-verbatims')
        .on('keypress', function (e) {
            if (e.which === 13) { // if is enter
                TaggerManager.addTag()
            }
        })
    // setTimeout(function () {
    //     $('[data-tab="insight"]').click()
    // }, 500)
})

function initSearchTags() {
    var topic = getTopic()
    var searchTags = $('#container-tagged-verbatims').find('.search-tag')
    searchTags
        .search({
            apiSettings: {
                url: '/dashboard/verbatims/autocomplete/tag?q={query}&t=' + topic
            },
            searchFields: [
                'title'
            ],
            searchFullText: false,
            error: {
                noResults: 'No tags found',
                serverError: 'There was an issue with querying the server.',
            },
        })

    searchTags.on('keydown', function (e) {
        if (e.which == 13 || e.keyCode == 13) {
            searchTags.search('hide results')
        }
    })
}

var TaggerManager = new _TaggerManager(
    $('#container-tagged-verbatims'),
    $('#input-tagged-verbatims')
)

function _TaggerManager(modalContainer, inputContainer) {
    var _self = this
    this.modal = modalContainer
    this.input = inputContainer

    this.activateTagPopup = function (o) {
        var container = _self.modal
        var self = $(o)
        container.data('id', self.parent().parent().data().id)

        container.find('.tag-list').empty()
        $.each(self.parent().find('.tag-container').find('span'), function () {
            container.find('.tag-list').append(
                TEMPLATE.VERBATIM.VERBATIMLISTTAG.replace('$TAG$', $(this).text())
            )
        })
        _self.showTagPopup()
    }

    this.showTagPopup = function () {
        _self.modal
            //.modal('setting', 'closable', false)
            .modal('setting', 'duration', 300)
            .modal('show')
    }

    var ptnTag = new RegExp(' ', 'g')
    this.addTag = function () {
        var input = _self.input
        var container = _self.modal
        var text = input.val().trim().replace(ptnTag, '_')
        var id = container.data().id
        var tagList = []
        $.each(container.find('.tag-list').find('.label'), function () {
            var self = $(this)
            tagList.push(self.text().toLowerCase())
        })

        if (text != "") {
            if (tagList.indexOf(text.toLowerCase()) == -1) {

                container.find('.tag-list').append(
                    TEMPLATE.VERBATIM.VERBATIMLISTTAG
                        .replace("$TAG$", text)
                )

                var verbatimContainer = $('.verbatim[data-id="' + id + '"]').find('.tag-container')

                verbatimContainer
                    .append(
                        TEMPLATE.VERBATIM.VERBATIMTAG
                            .replace("$TAG$", text)
                    )

                var tags = []
                $.each(verbatimContainer.find('span'), function () {
                    tags.push($(this).text())
                })

                if(typeof (getTopic) == "function"){
                    API.VERBATIMS.updateTagForVerbatim({
                        ACTION: 'ADD',
                        TOPIC: getTopic(),
                        ID: id,
                        tags: tags,
                    }).then(function (res) {
                        if (!res.status) {
                            popup("Count not add tag")
                        }
                    })
                } else {
                    // fetching topic from verbatimID
                    API.VERBATIMS.getVerbatimByID(id).then(function (verbatim) {
                        API.VERBATIMS.updateTagForVerbatim({
                            ACTION: 'ADD',
                            TOPIC: verbatim.topic,
                            ID: id,
                            tags: tags,
                        }).then(function (res) {
                            if (!res.status) {
                                popup("Count not add tag")
                            }
                        })
                    })
                }
            }
        }
    }

    this.removeTag = function (o) {
        var self = $(o)
        var container = $('#container-tagged-verbatims')
        var text = self.parent().text()
        var id = container.data().id

        API.VERBATIMS.updateTagForVerbatim({
            ACTION: 'REMOVE',
            ID: id,
            tag: text
        }).then(function (res) {
            if (!res.status) {
                popup("Could not remove tag")
            } else {
                self.parent().remove()
                var verbatimContainer = $('.verbatim[data-id="' + id + '"]').find('.tag-container')
                $.each(verbatimContainer.find('span'), function () {
                    if ($(this).text() == text) {
                        $(this).remove()
                    }
                })

            }
        })
    }

}
