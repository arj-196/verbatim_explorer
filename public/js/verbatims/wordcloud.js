//
// Wordcloud
//
var MAXWORDCLOUDENTRIES = 300
var wordcloudWidth = null, wordcloudHeight = null
var fill = d3.scale.category20b()
var scale = d3.scale.linear().range([15, 100])
var LISTWORDCLOUDS = ["concepts", "keywords", "terms", "nouns", "adjectives", "verbs"]

var refreshWordCloud = function (mode, commandList, topic) {
    LISTWORDCLOUDS.forEach(function (wordcloudtype) {
        if (wordcloudtype == "keywords") {
            updateWordCloudKeyword(commandList, topic)
        } else if (wordcloudtype == "terms") {
            updateWordCloudTerm(commandList, topic)
        } else if (wordcloudtype == "concepts") {
            updateWordCloudConcept(commandList, topic)
        }
    })
}


var createWordCloudKeyword = function (query, topic) {
    return new Promise(function (resolve, reject) {
        API.VERBATIMS.getWordCloudKeyword(query, topic, {})
            .then(function (r) {
                if (r.status) {
                    var id = '#container-wordcloud-keywords'
                    formatDataForWordcloud(r, id, false, 'keyword', resolve)
                    if (r.r.length == 0) {
                        closeWordCloud(id)
                    } else {
                        openWordCloud(id)
                    }
                    // statistics plugin
                    StatisticsEngine.run("keywords", r)
                } else {
                    reject()
                }
            })

    })
}

var updateWordCloudKeyword = function (commandList, topic, preFetchedData) {
    return new Promise(function (resolve, reject) {
        var id = '#container-wordcloud-keywords'
        var o = $(id)
        o.find('.dimmer .text').text("LOADING WORDS")
        if (!o.find('.dimmer .text').hasClass('loader')) {
            o.find('.dimmer .text').addClass('loader')
        }
        o.dimmer('show')
        if(_.isUndefined(preFetchedData)){
            // fetch data
            API.STATS.getWordCloud(config.SEARCHMODE, "keyword", commandList, {}, topic)
                .then(function (r) {
                    formatDataForWordcloud(r.stats, id, false, 'keyword', function () {
                        resolve()
                        $(id).dimmer('hide')
                    })
                    if (r.stats.r.length == 0) {
                        closeWordCloud(id)
                    }
                    // statistics plugin
                    StatisticsEngine.run("keywords", r.stats)
                })
        } else {
            formatDataForWordcloud(preFetchedData, id, false, 'keyword', function () {
                resolve()
                $(id).dimmer('hide')
            })
            // predefined data
            if (preFetchedData.r.length == 0) {
                closeWordCloud(id)
            }
            // statistics plugin
            StatisticsEngine.run("keywords", preFetchedData)
        }
    })
}

var createWordCloudTerm = function (query, topic) {
    return new Promise(function (resolve, reject) {
        // fetch data
        API.VERBATIMS.getWordCloudTerm(query, topic, {})
            .then(function (r) {
                if (r.status) {
                    var id = '#container-wordcloud-terms'
                    formatDataForWordcloud(r, id, false, 'term', resolve)
                    if (r.r.length == 0) {
                        closeWordCloud(id)
                    } else {
                        openWordCloud(id)
                    }
                    // statistics plugin
                    StatisticsEngine.run("terms", r)
                } else {
                    reject()
                }
            })
    })
}

var updateWordCloudTerm = function (commandList, topic, preFetchedData) {
    return new Promise(function (resolve, reject) {
        var id = '#container-wordcloud-terms'
        var o = $(id)
        o.find('.dimmer .text').text("LOADING WORDS")
        if (!o.find('.dimmer .text').hasClass('loader')) {
            o.find('.dimmer .text').addClass('loader')
        }
        o.dimmer('show')
        if(_.isUndefined(preFetchedData)){
            // fetch data
            API.STATS.getWordCloud(config.SEARCHMODE, "text", commandList, {}, topic)
                .then(function (r) {
                    formatDataForWordcloud(r.stats, id, false, 'term', function () {
                        resolve()
                        $(id).dimmer('hide')
                    })
                    if (r.stats.r.length == 0) {
                        closeWordCloud(id)
                    }
                    // statistics plugin
                    StatisticsEngine.run("terms", r.stats)
                })
        } else {
            formatDataForWordcloud(preFetchedData, id, false, 'term', function () {
                resolve()
                $(id).dimmer('hide')
            })
            // predefined data
            if (preFetchedData.r.length == 0) {
                closeWordCloud(id)
            }
            // statistics plugin
            StatisticsEngine.run("terms", preFetchedData)

        }
    })
}

var createWordCloudConcept = function (query, topic) {
    return new Promise(function (resolve, reject) {
        // fetch data
        API.VERBATIMS.getWordCloudConcept(query, topic, {})
            .then(function (r) {
                if (r.status) {
                    var id = '#container-wordcloud-concepts'
                    formatDataForWordcloud(r, id, true, 'concept', resolve)
                    if (r.r.length == 0) {
                        closeWordCloud(id)
                    } else {
                        openWordCloud(id)
                    }
                    // statistics plugin
                    StatisticsEngine.run("concepts", r)
                } else {
                    reject()
                }
            })
    })
}

var updateWordCloudConcept = function (commandList, topic, preFetchedData) {
    return new Promise(function (resolve, reject) {
        var id = '#container-wordcloud-concepts'
        var o = $(id)
        o.find('.dimmer .text').text("LOADING CONCEPTS")
        if (!o.find('.dimmer .text').hasClass('loader')) {
            o.find('.dimmer .text').addClass('loader')
        }
        o.dimmer('show')


        if(_.isUndefined(preFetchedData)){
            // fetch data
            API.STATS.getWordCloud(config.SEARCHMODE, "concept", commandList, {}, topic)
                .then(function (r) {
                    formatDataForWordcloud(r.stats, id, true, 'concept', function () {
                        resolve()
                        $(id).dimmer('hide')
                    })
                    if (r.stats.r.length == 0) {
                        closeWordCloud(id)
                    } else {
                        openWordCloud(id)
                    }
                    // statistics plugin
                    StatisticsEngine.run("concepts", r.stats)
                })

        } else {
            formatDataForWordcloud(preFetchedData, id, true, 'concept', function () {
                resolve()
                $(id).dimmer('hide')
            })

            // predefined data
            if (preFetchedData.r.length == 0) {
                closeWordCloud(id)
            }
            // statistics plugin
            StatisticsEngine.run("concepts", preFetchedData)
        }
    })
}

var createWordCloudNoun = function (query, topic) {
    return new Promise(function (resolve, reject) {
        // fetch data
        API.VERBATIMS.getWordCloudNoun(query, topic, {})
            .then(function (r) {
                if (r.status) {
                    var id = '#container-wordcloud-nouns'
                    formatDataForWordcloud(r, id, false, 'noun', resolve)
                    if (r.r.length == 0) {
                        closeWordCloud(id)
                    } else {
                        openWordCloud(id)
                    }
                } else {
                    reject()
                }
            })
    })
}

var createWordCloudVerbs = function (query, topic) {
    return new Promise(function (resolve, reject) {
        // fetch data
        API.VERBATIMS.getWordCloudVerb(query, topic, {})
            .then(function (r) {
                if (r.status) {
                    var id = '#container-wordcloud-verbs'
                    formatDataForWordcloud(r, id, false, 'verb', resolve)
                    if (r.r.length == 0) {
                        closeWordCloud(id)
                    } else {
                        openWordCloud(id)
                    }
                } else {
                    reject()
                }
            })
    })
}

var createWordCloudAdjective = function (query, topic) {
    return new Promise(function (resolve, reject) {
        // fetch data
        API.VERBATIMS.getWordCloudAdjective(query, topic, {})
            .then(function (r) {
                if (r.status) {
                    var id = '#container-wordcloud-adjectives'
                    formatDataForWordcloud(r, id, false, 'adjective', resolve)
                    if (r.r.length == 0) {
                        closeWordCloud(id)
                    } else {
                        openWordCloud(id)
                    }
                } else {
                    reject()
                }
            })
    })
}

var formatDataForWordcloud = function (r, target, ifconcept, action, resolve) {

    var container = $(target)
    wordcloudHeight = container.height()
    wordcloudWidth = container.width()
    var data = r.r
    var list = []

    // format data
    data.forEach(function (d) {
        var text
        if (ifconcept)
            text = ConceptParser.parse(d._id)
        else
            text = d._id
        list.push({text: text, size: d.value, id: d._id, action: action})
    })

    var words = list
        .filter(function (d) {
            return +d.size > 0
        })
        .sort(function (a, b) {
            return d3.descending(a.size, b.size)
        })
        .slice(0, 1000)

    scale
        .domain([
            d3.min(words, function (d) {
                return d.size
            }),
            d3.max(words, function (d) {
                return d.size
            })
        ])

    d3.layout.cloud().size([wordcloudWidth, wordcloudHeight])
        .words(words)
        .padding(0)
        .rotate(function () {
            return ~~(Math.random() * 2) * 90
        })
        .font("Impact")
        .fontSize(function (d) {
            return scale(d.size)
        })
        .on('end', function () {
            drawWordCloud(words, target)
            labelWordCloudTabMenuItem(action, r.c)
            resolve()
        })
        .start()
}

var labelWordCloudTabMenuItem = function (action, count) {
    var labelContainer = $('#wordcloud-tab-menu').find('.' + action)
    labelContainer
        .find('.label').remove()

    // color
    var color
    if (count == 0) {
        color = COLORS.WORDCLOUD.LABEL.NONE
    } else if (count <= MAXWORDCLOUDENTRIES / 2) {
        color = COLORS.WORDCLOUD.LABEL.NORMAL
    } else {
        color = COLORS.WORDCLOUD.LABEL.HIGH
    }

    labelContainer
        .append(
            TEMPLATE.WORDCLOUD.LABEL
                .replace('$VALUE$', count)
                .replace('$COLOR$', color)
        )
}

var drawWordCloud = function (words, target) {
    var container = $(target)
    container.find('svg').remove()

    d3.select(target).append('svg')
        .attr("width", wordcloudWidth)
        .attr("height", wordcloudHeight)
        .append("g")
        .attr("transform", "translate(" + (wordcloudWidth / 2) + ", " + (wordcloudHeight / 2) + ")")
        .selectAll("text")
        .data(words)
        .enter().append("text")
        .style("font-size", function (d) {
            return d.size + "px"
        })
        .style("font-family", "Impact")
        .style("fill", function (d, i) {
            return fill(i)
        })
        .attr("text-anchor", "middle")
        .attr("transform", function (d) {
            return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')'
        })
        .text(function (d) {
            return d.text
        })
    //.style('cursor', 'pointer')
    //.on('click', onClickWord)  // DEPRECATED onclickword
}

var closeWordCloud = function (_id) {
    var container = $(_id)
    container.find('.dimmer .text').text("Oops! Ran out of words")
    container.find('.dimmer .text').removeClass('loader')
    container.dimmer('show')
}

var openWordCloud = function (_id) {
    var container = $(_id)
    container.dimmer('hide')
}

var defaultWordClouds = function () {
    var body = $('#pusher')
    var topic = getTopic()
    Promise.all(
        [
            createWordCloudKeyword({}, topic),
            createWordCloudTerm({}, topic),
            createWordCloudConcept({}, topic),
            //createWordCloudNoun({}, topic),
            //createWordCloudVerbs({}, topic),
            //createWordCloudAdjective({}, topic)

        ]
    ).then(function () {
        body.dimmer('hide')

    })

}
