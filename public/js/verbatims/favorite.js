// TODO remove file favorite.js, as it is DEPRECATED
//
// Tagged
// TODO
//  |||
//  VVV
//
var showAllFavorite = function () {
    var self = $(this)

    config.PREVIOUS_SEARCH_TEXT = ""
    if (self.data().filter == "off") {
        self.data('filter', 'on')
            .removeClass('ar-inactive')
            .addClass('ar-active')
        config.IFFILTERTAGGED = true
        defaultVerbatims()

    } else {
        self.data('filter', 'off')
            .removeClass('ar-active')
            .addClass('ar-inactive')
        config.IFFILTERTAGGED = false
        defaultVerbatims()
    }

}

var closeAllFavorite = function () {
    $.each($('.ar-tag-selected'), function () {
        $(this)
            .removeClass('ar-tag-selected')
            .addClass('ar-tag-unselected')
    })
    fetchAllFavorite(null)
}

var fetchAllFavorite = function (next) {
    API.VERBATIMS.updateFavorites({
        ACTION: 'FETCH',
        TOPIC: getTopic()
    }).then(function (res) {
        if (res.status) {
            var content = res.r
            if (next == null) {
                // default protocol
                $.each($('.ar-tag'), function () {
                    var self = $(this)
                    if (content.indexOf(self.parent().parent().data().id) != -1)
                        self
                            .removeClass('ar-tag-unselected')
                            .addClass('ar-tag-selected')
                })
            } else {
                next(content)
            }
        }
    })
}

var toggleFavorite = function (o) {
    var self = $(o)

    // action select
    if (self.hasClass('ar-tag-unselected')) {
        self
            .removeClass('ar-tag-unselected')
            .addClass('ar-tag-selected')

        addFavorite(self.parent().parent().data().id)
    }
    // action remove
    else {
        self
            .removeClass('ar-tag-selected')
            .addClass('ar-tag-unselected')
        removeFavorite(self.parent().parent().data().id)
    }

}

var onclickFavoriteRemoveFavorite = function (o) {
    var self = $(o)
    self.parent().parent().fadeOut()
    removeFavorite(self.parent().parent().data().id)
}

var addFavorite = function (id) {
    API.VERBATIMS.updateFavorites({
        ACTION: 'ADD',
        TOPIC: getTopic(),
        ID: id
    }).then(function (res) {
        if (!res.status) {
            popup("Could not tag verbatim")
        }
    })
}

var removeFavorite = function (id) {
    API.VERBATIMS.updateFavorites({
        ACTION: 'REMOVE',
        TOPIC: getTopic(),
        ID: id
    }).then(function (res) {
        if (!res.status) {
            popup("Could not remove tagged verbatim")
        }
    })

}
