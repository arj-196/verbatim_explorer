//
// Topics
//

function openChooseDataSetModal() {
    var container = $('#container-dataset')
    container.find('.error.message').removeClass('visible')
    container.modal('setting', 'closable', false)
        .modal('show')
}

function closeChooseDataSetModal() {
    $('#container-dataset').modal('hide')
}

var onclickTopic = function (topic) {
    // add variable to InteractionHistory
    InteractionHistory.setVariable('t', {
        topic: topic.topic,
        name: topic.name
    })

    var body = $('#pusher')
    body.dimmer('show')
    config.IFLOADMORE = true // reset loadmore to true
    TOPICITERATION = 0 // reset ITERATION
    config.IFDEFAULTLOAD = true
    config.PREVIOUS_SEARCH_TEXT = null
    config.IFLOADMORE = true
    config.IFWORDCLOUDSEARCH = false
    config.MV.ACTIONS.SIMILARITY = false
    config.VERBATIMLIST = []
    VERBATIMTOHIGHLIGHTMAP = []
    toggleMultipleVerbatimPanel('hide')
    toggleMVOptonsContainer('hide')
    VerbatimCleaner.setTopic(topic.topic)

    // topic name
    $('#menu-nav-text-2').html(
        TEMPLATE.VERBATIM.TOPICHEADER
            .replace('$NAME$', topic.name)
            .replace('$DATA$', "data-topic='" + topic.topic + "'")
    )

    // load verbatims
    defaultVerbatims()

    $('#container-verbatims-init-box').remove()
    popup('Loading Topic: <strong>' + topic.name + '</strong>')

    //
    // reset previous search
    config.PREVIOUS_SEARCH_TEXT = null
    //fetchAllFavorite(null) // TODO

    // close dataset modal
    closeChooseDataSetModal()

    // setting aggregate search to default
    StatsSearchEngine.defaultState()

    // setup tags autocompleter
    initSearchTags()
}

var getTopic = function () {
    var c = $('#menu-nav-text-2').find('strong')
    if (c.length > 0)
        return c.data().topic
    else
        return null
}

//
// Btn return to default
//
var defaultVerbatims = function () {
    WindowManager.addActionInProgressQueue("default_verbatims")
    var topic = getTopic()
    TOPICITERATION = 0
    config.IFLOADMORE = true
    config.PREVIOUS_SEARCH_TEXT = null
    config.IFDEFAULTLOAD = true
    defaultWordClouds()
    createVerbatimList(topic)
    runDefaultStatistics([], topic)
    WindowManager.emptyProgressQueue()
}
