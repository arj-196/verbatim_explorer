//
// Verbatim List
//

function HighlighterEngine() {
}
var highLighter = new HighlighterEngine()

var createVerbatimList = function (topic) {
    $('#container-verbatims').empty()
    getVerbatimsByDefaultOrder(topic, 0)
}

var getVerbatimsByDefaultOrder = function (topic, ITERATION) {
    var container = $('#container-verbatims')
    API.VERBATIMS.getVerbatims(topic, ITERATION)
        .then(function (res) {
            if (res.status) {
                //console.log("got verbatims for ", topic, res)
                var data = res.r
                var appendVerbatimQueue = []
                data.forEach(function (d) {
                    appendVerbatimQueue.push(appendVerbatim(container, d.verbatim, d.score, d._id, d.options, d))
                })
                appendToVerbatimList(appendVerbatimQueue, {c: res.c})
                if (appendVerbatimQueue.length == 0)
                    config.IFLOADMORE = false
            }
        })
}

var loadMoreVerbatims = function () {
    $('#container-verbatims-load-more').remove()
    var topic = getTopic()
    if (config.IFLOADMORE && topic.trim() != "") {
        TOPICITERATION++
        if (config.IFDEFAULTLOAD) {
            getVerbatimsByDefaultOrder(topic, TOPICITERATION)
        }
        else {
            if (config.IFWORDCLOUDSEARCH) {
                onClickWord(config.WORDCLOUD.PREVIOUSWORD, true)
            } else {
                config.PREVIOUS_SEARCH_TEXT = null
                searchForText(false, true)
            }
        }
    }
}

var appendVerbatim = function (container, text, score, id, options, data, idpostfix) {
    return new Promise(function (resolve) {
        var idVerbatim = 'verbatim-' + id
        if (!_.isUndefined(idpostfix))
            idVerbatim += idpostfix

        if ($('#' + idVerbatim).length == 0) {  // make sure not to render same verbatim twice

            var tmp = TEMPLATE.VERBATIM.VERBATIM
                .replace('$TEXT$', text)
                .replace('$SCORE$', score)
                .replace('$ID$', id)
                .replace('$VERBATIMID$', idVerbatim)

            // polarity
            if (!_.isUndefined(options)) {
                var o = getColorAndFontForPolarity(options.polarity)
                tmp = tmp
                    .replace('$ICONPOLARITY$', o.f)
                    .replace('$COLORPOLARITY$', o.c)
                    .replace('$POLARITY$', options.polarity.toFixed(3))
            }
            container.append(tmp)

            var verbatimContainer = $('#' + idVerbatim)

            // emotions
            if (!_.isUndefined(data.emotion)) {
                var emotionContainer = verbatimContainer.find('.emotion-list')
                var emotionKeys = _.keys(data.emotion)
                emotionKeys.forEach(function (key, index) {
                    var list = data.emotion[key]
                    if (list.length > 0) {
                        var idList = "emotion-list-" + index + "-" + data._id
                        var color = COLORS.EMOTIONREPLACE[key.toLowerCase()]
                        emotionContainer.append(
                            TEMPLATE.VERBATIM.EMOTION.LISTCONTAINER
                                .replace('$ID$', idList)
                                .replace('$COLOR$', color)
                                .replace('$NAME$', key.toUpperCase())
                        )

                        var emotionSubList = $('#' + idList)
                        list.forEach(function (item) {
                            emotionSubList.append(
                                TEMPLATE.VERBATIM.EMOTION.ITEM
                                    .replace('$NAME$', item)
                                    .replace('$COLOR$', color)
                                    .replace('$DATA$', 'data-action="emotion" data-emotion="' + key + '" data-word="'
                                        + item + '" data-color="' + color + '"')
                            )
                        })
                    }
                })
            }

            // default options to ignore
            var verbatimDefaultKeys = ["_id", "topic", "verbatim", "emotion", "urls", "indexnb", "sentiment",
                "options", "score", "mentions", "hashtags", "domainList", "enabled"]
            var newKeys = _.difference(_.keys(data), verbatimDefaultKeys)  // takes any keys not in verbatimDefaultKeys
            if (!_.isEmpty(newKeys)) {
                var additionalContainer = verbatimContainer.find('.additional')
                newKeys.forEach(function (key) {
                    var icon, margin = 0

                    // icon
                    if (!_.isUndefined(ICONS[key.toUpperCase()])) {
                        if (_.isObject(ICONS[key.toUpperCase()])) {
                            icon = ICONS[key.toUpperCase()][data[key].toUpperCase()]
                        } else {
                            icon = ICONS[key.toUpperCase()]
                        }

                        if (!_.isUndefined(icon)) {
                            margin = 40
                        }
                    }

                    additionalContainer.append(
                        TEMPLATE.VERBATIM.VERBATIMADDITIONALITEM
                            .replace('$HEADER$', key)
                            .replace('$VALUE$', data[key])
                            .replace('$ICON$', icon)
                            .replace('$MARGINLEFT$', margin)
                    )
                })
            }

        }

        resolve()
    })
}

var appendToVerbatimList = function (appendVerbatimQueue, data, overwriteContainer, onDone) {
    Promise.all(appendVerbatimQueue)
        .then(function () {
            if (!_.isNull(data.c))
                updateMenuText(data.c)
            //fetchAllFavorite()
            // append load more container
            if (appendVerbatimQueue.length >= MAXSEARCHLENGTH) {
                if (_.isUndefined(overwriteContainer))
                    $('#container-verbatims').append(
                        TEMPLATE.VERBATIM.LOADMORE
                    )
                else {
                    overwriteContainer.append(
                        TEMPLATE.VERBATIM.LOADMORE
                    )
                }
            }

            fetchVerbatimConcepts()
                .then(function () {
                    fetchVerbatimKeywords()
                        .then(function () {
                            highLighter.highlightAll()
                        })
                })

            fetchVerbatimTags()
            fetchVerbatimNERs()

            // final verbatim design edits
            formatVerbatims()
            // initialize popups
            $('.popup-element').popup()

            if (!_.isUndefined(onDone))
                onDone()
        })
}

var getColorAndFontForPolarity = function (polarity) {

    var POLARITY_THRESHOLD = 0.2
    var POLARICONS = {
        positive: "smile",
        neutral: "meh",
        negative: "frown"
    }
    var colors = COLORS.SENTIMENT
    var c, f, index
    if ((polarity - POLARITY_THRESHOLD) > 0) {
        // positive
        index = Math.floor((polarity - POLARITY_THRESHOLD) * 10)
        if (index == 0) {
            f = POLARICONS.neutral
            c = colors.neutral
        } else {
            f = POLARICONS.positive
            c = colors.positive[index]
        }
    } else {
        index = Math.abs(Math.floor((polarity - POLARITY_THRESHOLD) * 10))
        if (index < 4) {
            f = POLARICONS.neutral
            c = colors.neutral
        } else {
            f = POLARICONS.negative
            c = colors.negative[index]
        }
    }
    return {f: f, c: c}
}

var fetchVerbatimConcepts = function () {
    return new Promise(function (resolve, reject) {
        var CONCEPTCOLORS = COLORS.CONCEPT

        var verbatimList = []
        $.each($('.verbatim'), function () {
            var self = $(this)
            var data = self.data()
            if (data.fetchConcept == undefined)
                verbatimList.push(data.id)
        })

        API.VERBATIMS.getConceptsForVerbatims(verbatimList)
            .then(function (res) {
                //console.log("get concepts", res)
                if (res.status) {
                    var conceptMap = res.r;
                    $.each($('.verbatim'), function () {
                        var self = $(this)
                        var data = self.data()
                        if (data.fetchConcept == undefined) {
                            var conceptsAdded = []
                            var concepts = conceptMap[data.id]
                            if (!_.isUndefined(conceptMap[data.id]))
                                self.data('fetchConcept', false)

                            var list = self.find('.concept-list')
                            if (concepts)
                                concepts.forEach(function (concept) {
                                    var conceptStr = ConceptParser.parse(concept.c)
                                    if (conceptsAdded.indexOf(conceptStr) == -1) {
                                        var signalStrength
                                        if (concept.score > 2)
                                            signalStrength = 'STRONG'
                                        else if (concept.score > 1)
                                            signalStrength = 'NORMAL'
                                        else
                                            signalStrength = 'WEAK'

                                        list.append(
                                            TEMPLATE.VERBATIM.VERBATIMCONCEPT
                                                .replace('$COLOR$', CONCEPTCOLORS[signalStrength])
                                                .replace('$CONCEPT$', conceptStr)
                                                .replace('$SCORE$', concept.score.toFixed(2))
                                                .replace('$DATA$', 'data-action="concept" data-word="' + conceptStr + '"')
                                        )
                                        conceptsAdded.push(conceptStr)
                                    }
                                });
                        }
                    });
                    resolve()
                }
            })
    })
}

function fetchVerbatimKeywords() {
    return new Promise(function (resolve, reject) {
        var CONCEPTCOLORS = COLORS.CONCEPT
        var verbatimList = []
        $.each($('.verbatim'), function () {
            var self = $(this)
            var data = self.data()
            if (data.fetchKeywords == undefined)
                verbatimList.push(data.id)
        })

        API.VERBATIMS.getKeywordsForVerbatims(verbatimList)
            .then(function (res) {
                //console.log("get keywords", res)
                if (res.status) {
                    var keywordMap = res.r;
                    $.each($('.verbatim'), function () {
                        var self = $(this)
                        var data = self.data()
                        if (data.fetchKeywords == undefined) {
                            var conceptsAdded = []
                            var keywords = keywordMap[data.id]
                            if (!_.isUndefined(keywordMap[data.id]))
                                self.data('fetchKeywords', false)

                            var list = self.find('.keyword-list')
                            if (keywords != undefined) {
                                keywords.forEach(function (keyword) {
                                    var keywordStr = keyword.c
                                    if (conceptsAdded.indexOf(keywordStr) == -1) {
                                        var signalStrength
                                        if (keyword.score > 0.50)
                                            signalStrength = 'STRONG'
                                        else if (keyword.score > 0.25)
                                            signalStrength = 'NORMAL'
                                        else
                                            signalStrength = 'WEAK'

                                        list.append(
                                            TEMPLATE.VERBATIM.VERBATIMCONCEPT
                                                .replace('$COLOR$', CONCEPTCOLORS[signalStrength])
                                                .replace('$CONCEPT$', keywordStr)
                                                .replace('$SCORE$', keyword.score.toFixed(2))
                                                .replace('$DATA$', 'data-action="keyword" data-word="' + keywordStr + '"')
                                        )
                                        conceptsAdded.push(keywordStr)
                                    }
                                });
                            }
                        }
                    })
                    resolve()
                }
            })
    })
}

function fetchVerbatimTags() {
    return new Promise(function (resolve, reject) {
        var verbatimList = []
        $.each($('.verbatim'), function () {
            var self = $(this)
            var data = self.data()
            if (data.fetchTags == undefined)
                verbatimList.push(data.id)
        })

        API.VERBATIMS.getTagsForVerbatims(verbatimList)
            .then(function (res) {
                if (res.status) {
                    var tagMap = res.r;
                    $.each($('.verbatim'), function () {
                        var self = $(this)
                        var data = self.data()
                        if (data.fetchTags == undefined) {
                            var tagsAdded = []
                            if (!_.isUndefined(tagMap[data.id]))
                                self.data('fetchTags', false)

                            if (tagMap[data.id] != undefined) {
                                var tags = tagMap[data.id]
                                var list = self.find('.tag-container')
                                if (tags)
                                    tags.forEach(function (tag) {
                                        if (tagsAdded.indexOf(tag) == -1) {

                                            list.append(
                                                TEMPLATE.VERBATIM.VERBATIMTAG
                                                    .replace('$TAG$', tag)
                                            )
                                            tagsAdded.push(tag)
                                        }
                                    });
                            }
                        }

                    })
                }
            })
            .finally(function () {
                resolve()
            })
    })
}

function fetchVerbatimNERs() {
    return new Promise(function (resolve, reject) {
        var verbatimList = []
        $.each($('.verbatim'), function () {
            var self = $(this)
            var data = self.data()
            if (data.fetchNERS == undefined)
                verbatimList.push(data.id)
        })

        API.VERBATIMS.getNERForVerbatims(verbatimList)
            .then(function (res) {
                if (res.status) {
                    var entityMap = res.r;
                    $.each($('.verbatim'), function () {
                        var self = $(this)
                        var data = self.data()
                        if (data.fetchNERS == undefined) {
                            if (!_.isUndefined(entityMap[data.id]))
                                self.data('fetchNERS', false)


                            if (entityMap[data.id] != undefined) {
                                var entities = entityMap[data.id]
                                var nerContainer = self.find('.ner-list')

                                if (entities)
                                    _.keys(entities).forEach(function (key) {
                                        var idList = "ner-list-" + key + "-" + data.id
                                        nerContainer.append(
                                            TEMPLATE.VERBATIM.NER.LISTCONTAINER
                                                .replace('$NAME$', key.toUpperCase())
                                                .replace('$ID$', idList)
                                                .replace('$ICON$', ICONS.NER[key])
                                        )

                                        var words = _.keys(entities[key])
                                        var nerList = $('#' + idList)
                                        words.forEach(function (word) {
                                            nerList.append(
                                                TEMPLATE.VERBATIM.NER.ITEM
                                                    .replace('$NAME$', word)
                                                    .replace('$DATA$', 'data-action="ner" data-word="' + word + '"')
                                            )
                                        })
                                    })
                            }
                        }
                    })
                }
            })
            .finally(function () {
                resolve()
            })
    })
}

function formatVerbatims() {
    $.each($('.verbatim'), function () {
        var self = $(this)
        if (self.find('.verbatim-score > .value').text() == 0)
            self.find('.verbatim-score').css('visibility', 'hidden')
    })

}

//
// Highlighting
//
// TODO highlighter deleting a word, if removing other stuff. Figure out why.
var VERBATIMTOHIGHLIGHTMAP = {}
function onclickHighlightWord(o) {
    var self = $(o)
    var data = self.data()
    var container

    // fetching verbatim contianer
    if (data.action == "emotion" || data.action == "ner") {
        container = self.parent().parent().parent()
    } else if (data.action == "keyword" || data.action == "concept") {
        container = self.parent().parent()
    }
    highLighter.highlight(o, data, container)
}

highLighter.highlight = function (o, data, container) {
    var id = container.attr('id'), action
    var self = $(o)

    if (!self.hasClass('ar-selected-item')) {
        // on
        action = 'on'
    } else {
        self.removeClass('ar-selected-item')
            .css({
                color: 'black !important',
                boxShadow: 'none'
            })
    }

    if (_.isUndefined(VERBATIMTOHIGHLIGHTMAP[id])) {
        // initial
        var patterns = new RegExp('<span.*?>(.*?)<\/span>', 'g')
        var text = container.find('.verbatim-text').html().replace(patterns, '$1')
        VERBATIMTOHIGHLIGHTMAP[id] = {
            cache: text,
            queue: [],
            id: id
        }
    }

    if (action == 'on') {
        VERBATIMTOHIGHLIGHTMAP[id].queue.push({
            word: data.word,
            action: data.action,
            emotion: data.emotion
        })
    } else {
        var index = -1
        for (var i = 0; i < VERBATIMTOHIGHLIGHTMAP[id].queue.length; i++) {
            var obj = VERBATIMTOHIGHLIGHTMAP[id].queue[i]
            if (obj.word == data.word && obj.action == data.action) {
                index = i
            }
        }
        VERBATIMTOHIGHLIGHTMAP[id].queue.splice(index, 1)
    }

    highLighter.highlightAll()
}

highLighter.highlightAll = function () {

    _.keys(VERBATIMTOHIGHLIGHTMAP).forEach(function (id) {
        var formattedText = VERBATIMTOHIGHLIGHTMAP[id].cache
        VERBATIMTOHIGHLIGHTMAP[id].queue.forEach(function (d) {
            $('#' + VERBATIMTOHIGHLIGHTMAP[id].id)
                .find('.' + d.action + '-list')
                .find('.label[data-word="' + d.word + '"]')
                .addClass('ar-selected-item')
                .css({
                    color: 'white !important',
                    boxShadow: '0 0 6px black'
                })

            formattedText = highLighter.highlightWord(formattedText, d.word, d.action, d.emotion)
        })
        $('#' + VERBATIMTOHIGHLIGHTMAP[id].id).find('.verbatim-text').html(formattedText)
    })
}

highLighter.highlightWord = function (text, word, action, emotion) {
    var color
    if (action == "emotion")
        color = COLORS.EMOTIONREPLACE[emotion.toLowerCase()]
    else
        color = ""

    var replaceBy = TEMPLATE.VERBATIM.EMOTION.ITEMREPLACE
        .replace('$COLOR$', color)
        .replace('$TEXT$', word)

    return formatFeedTextCustom(word, text, replaceBy)
}
