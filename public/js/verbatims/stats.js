$(document).ready(function () {
    $('.trigger-stats-toggle-display').click(toggleSizeStatsContainer)
    $('#container-download').modal()
})

function toggleSizeStatsContainer() {
    var self = $(this)
    if (self.data().action == "large") {
        Layout.toggleDisplayModeContainerStatistics(1, false)
    } else {
        Layout.toggleDisplayModeContainerStatistics(2, false)
    }
}

function _StatisticsEngine() {

    this.run = function (type, data) {
        if (config.STATS) {
            var self = this
            self.plugins[type].fns.forEach(function (fn) {
                self.plugins[type]._fns[fn](type, data)
            })
        }
    }

    this.createListTerms = function (type, data) {
        createStatsTopList(type, data)
    }

    this.createListConcept = function (type, data) {
        data.r = _.map(data.r.slice(0, MAXENTRIESSTATS), function (d) {
            d._id = ConceptParser.parse(d._id)
            return d
        })
        createStatsTopList(type, data)
    }

    this.createListNER = function (type, data, preFetchedData) {
        if (_.isUndefined(preFetchedData)) {
            var languageMap = {}
            var commandList = data.cmd
            var topic = data.topic
            if (config.SEARCHMODE == "concept") {
                commandList.forEach(function (cL1) {
                    if (!_.isNull(cL1) && !_.isUndefined(cL1)) {
                        cL1.forEach(function (cL) {
                            cL.forEach(function (command) {
                                languageMap[command] = config.CONCEPTSEARCH.CONCEPTTOLANGMAP[command]
                            })
                        })
                    }
                })
            }
            API.STATS.getNER(config.SEARCHMODE, commandList, languageMap, topic)
                .then(function (r) {
                    createStatsTopList("ner", r.stats)
                })
        } else {
            createStatsTopList("ner", preFetchedData)
        }
    }

    this.emotionChart = function (type, data, preFetchedData) {
        if (_.isUndefined(preFetchedData)) {
            var languageMap = {}
            var commandList = data.cmd
            if (config.SEARCHMODE == "concept") {
                commandList.forEach(function (cL1) {
                    if (!_.isNull(cL1) && !_.isUndefined(cL1)) {
                        cL1.forEach(function (cL) {
                            cL.forEach(function (command) {
                                languageMap[command] = config.CONCEPTSEARCH.CONCEPTTOLANGMAP[command]
                            })
                        })
                    }
                })
            }

            API.STATS.getEmotion(config.SEARCHMODE, commandList, languageMap, data.topic)
                .then(function (r) {
                    drawStatsTopChart("emotion", r.stats)
                })
        } else {
            drawStatsTopChart("emotion", preFetchedData)
        }
    }

    this.plugins = {
        emotion: {
            fns: ["emotionChart"],
            _fns: {
                emotionChart: this.emotionChart,
            }
        },
        ner: {
            fns: ["createListNER"],

            _fns: {
                createListNER: this.createListNER,
            }
        },
        keywords: {
            fns: ["createListTerms"],
            _fns: {
                createListTerms: this.createListTerms,
            }
        },
        terms: {
            fns: ["createListTerms"],
            _fns: {
                createListTerms: this.createListTerms,
            }
        },
        concepts: {
            fns: ["createListConcept"],
            _fns: {
                createListConcept: this.createListConcept,
            }
        },
    }

}
var MAXENTRIESSTATS = 40
var StatisticsEngine = new _StatisticsEngine()

function runDefaultStatistics(commandList, topic) {
    if (config.STATS) {
        StatisticsEngine.run("emotion", {cmd: commandList, topic: topic})
        StatisticsEngine.run("ner", {cmd: commandList, topic: topic})
    }
}

function toggleStatsContainer(mode) {
    var self = $(this)
    if (self.data().mode == "show") {
        self.data("mode", "hide")
        self.addClass('green')
        config.STATS = true
        Manager.verbatimFormatLayout(2)
    } else {
        self.removeClass('green')
        self.data("mode", "show")
        config.STATS = false
        Manager.verbatimFormatLayout(1)
    }
}

var createStatsTopList = function (type, data) {
    var container, idStr

    if (type == "ner") {
        var NERTYPES = ["ORGANIZATION", "LOCATION", "PERSON"]
        container = $('.content-statistics').find('.top-lists-entities').find('.list-content')
        NERTYPES.forEach(function (nertype) {
            idStr = 'stat-list-' + nertype.toLowerCase()
            // empty container
            $('#' + idStr).find('.list').empty()
            if (!_.isUndefined(data) && !_.isUndefined(data[nertype])) {
                var listContainer = $('#' + idStr).find('.list')
                data[nertype].forEach(function (d) {
                    listContainer.append(
                        TEMPLATE.VERBATIM.STAT.LIST.ITEM
                            .replace('$NAME$', d.name)
                            .replace('$VALUE$', d.frequency)
                            .replace('$COLOR$', COLORS.STATS.TYPES[nertype.toLowerCase()])
                            .replace('$DATA$', 'data-type="' + type + '" data-nertype="' + nertype + '"  data-id="' + d.name + '"')
                    )
                })
            }
        })

    } else {
        var id, value
        if (type == "hashtags" || type == "mentions" || type == "domains") {
            id = "name";
            value = "frequency"
        } else {
            id = "_id";
            value = "value"
        }

        container = $('.content-statistics').find('.top-lists').find('.list-content')
        idStr = 'stat-list-' + type
        // empty container
        $('#' + idStr).find('.list').empty()
        container = $('#' + idStr).find('.list')
        var appendedItems = {}
        data.r.slice(0, MAXENTRIESSTATS).forEach(function (d) {
            if (_.isUndefined(appendedItems[d[id]])) {
                container.append(
                    TEMPLATE.VERBATIM.STAT.LIST.ITEM
                        .replace('$NAME$', d[id])
                        .replace('$VALUE$', d[value])
                        .replace('$COLOR$', COLORS.STATS.TYPES[type])
                        .replace('$DATA$', 'data-type="' + type + '" data-id="' + d[id] + '"')
                )
                appendedItems[d[id]] = 0
            }
        })
    }
}

function drawStatsTopChart(type, stats) {

    var container, idStr, chartContainer, heightChart
    if (type == "emotion") {
        container = $('.content-statistics').find('.top-charts').find('.list-content')
        idStr = 'stat-chart-' + type
        // remove container if already exists
        $('#' + idStr).find('svg').remove()

        chartContainer = $('#' + idStr)
        chartContainer.dimmer('show')

        if (!_.isUndefined(stats)) {
            // bar plot for emotions
            if (stats.emotions.length > 0) {
                createBarPlot('#' + idStr, sortStatsData(type, stats.emotions))
            }
            // word list for hashtags
            createStatsTopList("hashtags", {r: stats.hashtags})
            // word list for mentions
            createStatsTopList("mentions", {r: stats.mentions})
            // word list for domains
            createStatsTopList("domains", {r: stats.domains})

            chartContainer.dimmer('hide')
        } else {
            // remove container if already exists
            $('#' + idStr).find('svg').remove()
        }
    } else if (type == "ner123") {
        // DEPRECATED
        //var NERTYPES = ["ORGANIZATION", "LOCATION"]
        //container = $('.content-statistics').find('.top-lists-entities').find('.list-content')
        //
        //NERTYPES.forEach(function (nertype) {
        //    idStr = 'stat-chart-' + nertype
        //    //remove container if already exists
        //    $('#' + idStr).parent().remove()
        //
        //    container.append(
        //        TEMPLATE.VERBATIM.STAT.CHART.CONTAINER
        //            .replace('$ID$', idStr)
        //            .replace('$NAME$', nertype)
        //    )
        //    chartContainer = $('#' + idStr)
        //    chartContainer.dimmer('show')
        //    heightChart = $('.content-statistics').height() - 77
        //    chartContainer.height(heightChart + "px")
        //})
        //
        //API.STATS.getNER(searchmode, commandList, languageMap, topic)
        //    .then(function (r) {
        //        NERTYPES.forEach(function (nertype) {
        //            idStr = 'stat-chart-' + nertype
        //            chartContainer = $('#' + idStr)
        //
        //            if (!_.isUndefined(r.stats[nertype])) {
        //                createPiePlot('#' + idStr, r.stats[nertype])
        //                chartContainer.dimmer('hide')
        //            } else {
        //                //remove container
        //                chartContainer.parent().remove()
        //            }
        //        })
        //    })
    }
}

function sortStatsData(type, stats) {
    var ORDEREMOTION = [
        // fr
        "tranquillité",
        "joie",
        "surprise",
        "tristesse",
        "peur",
        "terreur",
        "colère",
        "fureur",
        "dégoût",
        "coupure avec ses émotions",

        // en
        //'Open',
        'happy',
        //'Alive',
        'love',
        //'Interested',
        'positive',
        'depressed',
        'confused',
        'helpless',
        'sad',
        //'Indifferent',
        'afraid',
        'hurt',
        'angry',


        // de
        'glücklich',
        'behaglich',
        // 'zufrieden',

        'überraschen',
        'unbequem',
        'traurig',
        'panik',

        'wut',
        'wütend',
        'ekel',

    ]
    var list = []
    if (type == "emotion") {
        ORDEREMOTION.forEach(function (e) {
            stats.forEach(function (s) {
                if (s.name.toLowerCase() == e) {
                    list.push(s)
                }
            })
        })
    }
    return list
}

function createBarPlot(_id, data) {
    var container = $(_id)
    container.find('svg').remove()
    var margin = {top: 4, right: 0, bottom: 30, left: 11},
        width = container.width() - margin.left - margin.right,
        height = container.height() - margin.top - margin.bottom
    var x = d3.scale.ordinal().rangeRoundBands([0, width], .1)
    var y = d3.scale.linear().range([height, 0])
    var xAxis = d3.svg.axis().scale(x).orient("bottom")
    var yAxis = d3.svg.axis().scale(y).orient("left").ticks(0, "%")

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function (d) {
            return "<strong style='font-size:12px;font-family:roboto-black'>Frequency:</strong>" +
                " <span style='color:red'>" + (d.frequency * 100).toFixed(0) + " % </span>"
        })

    var svg = d3.select(_id).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    svg.call(tip)

    x.domain(data.map(function (d) {
        return d.name
    }))
    y.domain([0, d3.max(data, function (d) {
        return d.frequency
    })])

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")

    svg.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function (d) {
            return x(d.name)
        })
        .attr("width", x.rangeBand())
        .attr("y", function (d) {
            return y(d.frequency)
        })
        .attr("height", function (d) {
            return height - y(d.frequency)
        })
        .style("fill", function (d) {
            return COLORS.EMOTIONREPLACE[d.name.toLowerCase()]
        })
        .on('click', function (d) {
        })
    //.on('mouseover', function (d) {
    //    tip.show(d)
    //})
    //.on('mouseout', tip.hide)

}

function createPiePlot(_id, data) {
    var container = $(_id)
    var margin = {top: 4, right: 0, bottom: 30, left: 10},
        width = container.width(),
        height = container.height(),
        radius = Math.min(width, height) / 2

    var fill = d3.scale.category20b()
    var color = d3.scale.ordinal()
        .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

    var arc = d3.svg.arc()
        .outerRadius(radius - 10)
        .innerRadius(0);

    var labelArc = d3.svg.arc()
        .outerRadius(radius - 40)
        .innerRadius(radius - 40);

    var pie = d3.layout.pie()
        .sort(null)
        .value(function (d) {
            return d.frequency;
        });

    var svg = d3.select(_id).append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var g = svg.selectAll(".arc")
        .data(pie(data))
        .enter().append("g")
        .attr("class", "arc");

    g.append("path")
        .attr("d", arc)
        .style("fill", function (d, i) {
            return getColor(i, 0)
        });

    g.append("text")
        .attr("transform", function (d) {
            return "translate(" + labelArc.centroid(d) + ")";
        })
        .attr("dy", ".35em")
        .text(function (d) {
            return d.data.name;
        });
}

function type(d) {
    d.frequency = +d.frequency;
    return d;
}

function statsExportDataset() {
    var topic = getTopic()
    if (topic) {
        var commandList = getCommandListObject()
        if (_.isNull(commandList)) {
            commandList = []
        }
        var container = $('#container-download')
        container.find('.pending').css('display', 'block')
        container.find('.done').css('display', 'none')
        container.modal('show')

        API.STATS.exportdataset(config.SEARCHMODE, commandList, conceptSearchEngine.getLanguageMap(commandList), topic)
            .then(function (r) {
                if (!_.isUndefined(r.f)) {
                    container.modal('show')
                    container.find('.container-download-href').attr('href', r.f)
                    container.find('.pending').fadeOut("slow", function () {
                        container.find('.done').fadeIn("slow")
                    })
                }
            })
    }
}

function onClickStatsItem(o) {
    var self = $(o)
    //addWordToCommandList(self.data().id)
    StatsSearchEngine.addSearchLevel(self.data())
}

function onClickStatsBredCrumb(o) {
    var self = $(o)
    StatsSearchEngine.removeSearchLevel(self, self.data())
}


function _StatsSearchEngine() {
    var self = this

    this.defaultState = function (ifLoadDefaultVerbatims) {
        WindowManager.addActionInProgressQueue("stats_search_default_content")
        if (ifLoadDefaultVerbatims) {
            defaultVerbatims()
        }

        // turning everything off
        API.STATS.SEARCH.setToDefault().then(function (r) {
            if (r.s) {
                var container = $('#breadcrumb-stat-search')
                container.find('.section').remove()
                container.find('.divider').remove()
                container.parent().find('.header').addClass('hidden')
            }
            WindowManager.resolveActionInProgressQueue("stats_search_default_content")
        })
    }

    this.refreshDashboardContent = function (r) {
        if (r.m == 'DEFAULTVERBATIMS') {
            self.defaultState(true)
        } else {
            // wordclouds
            updateWordCloudKeyword(null, null, r.res.wordcloud.keyword)
            updateWordCloudTerm(null, null, r.res.wordcloud.text)
            updateWordCloudConcept(null, null, r.res.wordcloud.concept)

            // emotions + hashtags + mentions + domains
            StatisticsEngine.emotionChart(null, null, r.res.general)

            // ners
            StatisticsEngine.createListNER(null, null, r.res.ner)

            // keywords
            var container = $('#breadcrumb-stat-search'), keywords = []
            $.each(container.find('.section'), function () {
                var self = $(this)
                keywords.push({word: self.find('.button').data().id})
            })

            var containerVerbatim = $('#container-verbatims')
            containerVerbatim.empty()  // force empty verbatims
            appendSearchedVerbatims(null, null, null, r.res.verbatims, r.res.count, true, keywords, 'word')

            if(!_.isUndefined(r.res.ifloadmore) && r.res.ifloadmore) {
                containerVerbatim.append(
                    TEMPLATE.VERBATIM.LOADMORE2
                        .replace('$FN$', 'onclick="StatsSearchEngine.loadMoreVerbatims()"')
                )
            }
        }
    }

    this.loadMoreVerbatims = function () {
        WindowManager.addActionInProgressQueue("stats_search_loadmore")
        var topic = getTopic()
        API.STATS.SEARCH.loadMoreVerbatims(topic).then(function (r) {

            // keywords
            var container = $('#breadcrumb-stat-search'), keywords = []
            $.each(container.find('.section'), function () {
                var self = $(this)
                keywords.push({word: self.find('.button').data().id})
            })

            $('#container-verbatims-load-more').remove()
            var containerVerbatim = $('#container-verbatims')
            appendSearchedVerbatims(null, null, null, r.res.verbatims, null, true, keywords, 'word')

            if(!_.isUndefined(r.res.ifloadmore) && r.res.ifloadmore) {
                containerVerbatim.append(
                    TEMPLATE.VERBATIM.LOADMORE2
                        .replace('$FN$', 'onclick="StatsSearchEngine.loadMoreVerbatims()"')
                )
            }

            WindowManager.resolveActionInProgressQueue("stats_search_loadmore")
        })
    }

    this.toggleStatisticsContainer = function (mode) {
        var container = $('#progress-default')
        if (mode == "show") {
            container.find('.progress').progress({percent: 10})
            container.fadeIn('fast')
                .fadeIn()
        } else if (mode == "hide") {
            container.fadeOut('fast')
        }
    }

    this.statsProgress = function (p) {
        var container = $('#progress-default')
        var percent = p * 100
        container.find('.progress').progress({percent: percent})
    }

    this.clearTextSearch = function () {
        var container = $('#input-search')
        container.val("")
    }

    this.addSearchLevel = function (data) {
        self.clearTextSearch()
        self.toggleStatisticsContainer('show')
        WindowManager.addActionInProgressQueue("stats_search_add_search_level")
        // adding a new search level
        API.STATS.SEARCH.addSearchLevel({
                id: data.id,
                type: data.type,
                topic: getTopic()
            },
            self.statsProgress
        ).then(function (r) {
            if (r.s) {
                self.addNewSection(data)
                self.refreshDashboardContent(r)
            } else {
                popup("Error! we could not append " + data.id + " to the search")
            }
            self.toggleStatisticsContainer('hide')
            WindowManager.resolveActionInProgressQueue("stats_search_add_search_level")
        })
    }

    this.removeSearchLevel = function ($this, data) {
        self.clearTextSearch()

        self.toggleStatisticsContainer('show')
        // removing a search level
        WindowManager.addActionInProgressQueue("stats_search_remove_search_level")
        API.STATS.SEARCH.removeSearchLevel({
            id: data.id,
            type: data.type,
            topic: getTopic()
        }).then(function (r) {
            if (r.s) {
                var container = $this.parent().parent()
                if ($this.parent().is(container.find('.section').first())) {
                    // if first section element
                    $this.parent().next().remove()
                    $this.parent().remove()
                } else {
                    // if not first
                    $this.parent().remove()
                    $.each(container.find('.divider'), function () {
                        var $div = $(this)
                        if (!$div.next().hasClass('section')) {
                            $div.remove()
                        }
                    })
                }

                if (container.find('.section').length == 0) {
                    container.parent().find('.header').addClass('hidden')
                }
                self.refreshDashboardContent(r)
            } else {
                popup("Error! we could not remove" + data.id + " to the search")
            }
            self.toggleStatisticsContainer('hide')
            WindowManager.resolveActionInProgressQueue("stats_search_remove_search_level")
        })
    }

    this.addNewSection = function (data) {
        var container = $('#breadcrumb-stat-search')
        // make header visible
        container.parent().find('.header').removeClass('hidden')

        // adding divider
        if (container.find('.section').length != 0) {
            container.append(TEMPLATE.VERBATIM.STAT.BREADCRUMB.DIVIDER)
        }

        // adding section
        var color, type
        if (data.type == 'ner') {
            type = data.nertype.toLowerCase()
            color = COLORS.STATS.TYPES[type]
        } else {
            type = data.type.toLowerCase()
            color = COLORS.STATS.TYPES[type]
        }
        container.append(
            TEMPLATE.VERBATIM.STAT.BREADCRUMB.ITEM
                .replace('$NAME$', data.id)
                .replace('$TYPE$', type)
                .replace('$COLOR$', color)
                .replace('$DATA$', 'data-id="' + data.id + '" data-type="' + data.type + '"')
        )
    }

    self.defaultState()
}
var StatsSearchEngine = new _StatsSearchEngine()