var TEMPLATE = {

    VERBATIM: {

        HEADER: {
            COUNT: '<span style="color:blue"> $COUNT$ / <span style="color:grey"> $GLOBALCOUNT$ </span></span>',
        },

        SEARCH: '<div id="container-search" style="width:100%;position:relative;">' +
        '    <div class="ui icon input" style="width: 55%;margin-right: 2px;">' +
        //'        <i class="search icon"></i>' +
        '        <input id="input-search" type="text" placeholder="Search..." style="">' +
        '    </div>' +
        '    <div class="ui icon buttons">' +
        '       <button class="ui icon blue button" id="input-search-trigger">' +
        '           <i class="icon search"></i>' +
        '       </button>' +
        '       <div id="input-search-modes" class="ui floating dropdown labeled icon button" ' +
        '               style="z-index:100;border-right: solid 1px grey;">' +
        '           <i class="filter icon"></i>' +
        '           <span class="text">Search Mode</span>' +
        '           <div class="menu">' +
        '               <div class="divider"></div>' +
        '               <div class="header">' +
        '                   <i class="options icon"></i>' +
        '                   Search Modes' +
        '               </div>' +
        '               <div class="scrolling menu">' +
        '                   <div class="item" data-value="concept">' +
        '                       <div class="ui blue empty circular label"></div>' +
        '                       Concept' +
        '                   </div>' +
        '                   <div class="item" data-value="keyword">' +
        '                       <div class="ui red empty circular label"></div>' +
        '                       Keyword' +
        '                   </div>' +
        '                   <div class="item" data-value="text">' +
        '                      <div class="ui green empty circular label"></div>' +
        '                          Text' +
        '                   </div>' +
        '                   <div class="item" data-value="tag">' +
        '                      <div class="ui black empty circular label"></div>' +
        '                          Tag' +
        '                   </div>' +
        '               </div>' +
        '           </div>' +
        '       </div>' +
        //'       <div id="btn-aggregate" class="ui icon button" style="border-right: solid 1px grey;" data-state="hide"' +
        //'               onclick="toggleAggregateSearchModal(this)">' +
        //'           <i class="settings icon cursor" style=""></i>' +
        //'       </div>' +
        '       <div id="btn-stats" class="ui icon green button" data-mode="hide">' +
        '           <i class="bar chart icon cursor" style=""></i>' +
        '       </div>' +
        '       <div class="ui icon grey top right pointing dropdown button" style="border-left: solid 1px;">' +
        '           <i class="wrench icon"></i>' +
        '           <div class="menu">' +
        '               <div class="header"></div>' +
        '               <div class="cursor item" onclick="statsExportDataset(this)">Export</div>' +
        '           </div>' +
        '           <div class="menu">' +
        '               <div class="header"></div>' +
        '               <div class="cursor item" onclick="statsExportDataset(this)">Export</div>' +
        '               <div class="cursor item" onclick="VerbatimCleaner.open()">Clean Verbatims</div>' +
        '           </div>' +
        '       </div>' +
        '    </div>' +
        '    <!--<div id="btn-show-tagged" class="ui icon button ar-inactive" data-filter="off">-->' +
        '    <!--<i class="heart icon cursor" style=""></i>-->' +
        '    <!--</div>-->' +
        '    <!-- DROPDOWN CONTAINER -->' +
        '    <div id="container-search-dropdown" class="ui segment" style="position:absolute;top:27px;left:25px;' +
        '                width:80%;min-height:100px;max-height:250px;overflow-x:hidden;overflow-y:auto' +
        '                background:white;z-index:700;padding:10px 14px;display:none;"' +
        '                data-show="false">' +
        '        <span class="mode" style="font-size:11px;color:#B7B1B1;"></span>' +
        '        <i class="remove icon cursor" style="color:#B7B1B1;float:right;" onclick="closeSearchDropdown()"></i>' +
        '        <div style="padding:0;margin:7px 0;border-bottom: dotted 1px lightgrey"></div>' +
        '        <div id="container-search-dropdown-list">' +
        '        </div>' +
        '    </div>' +
        '    <!-- / DROPDOWN CONTAINER -->' +
        '</div>'
        ,

        SEARCHDROPDOWNLISTITEM: '<span class="ui label cursor $COLOR$" onclick="$FUNC$" style="margin:3px;" $OPTIONAL$>$NAME$</span>'
        ,

        TOPIC: '<div class="item topic cursor f-r-b" data-topic="$TOPIC$" data-name="$DATASETNAME$" ' +
        '   onclick="onclickTopic(this)">$NAME$' +
        '</div>'
        ,

        TOPICS: {
            PROJECT: '<div class="item" data-value="$VALUE$"> ' +
            '    <div style="width:11px;height:11px;background:$BACKGROUND$;display:inline-block;margin-right:6px;"></div> ' +
            '    $NAME$ ' +
            '</div> '
            ,

            DATASET: '<div class="item" data-value="$VALUE$"> ' +
            '    <div style="width:11px;height:11px;background:$BACKGROUND$;display:inline-block;margin-right:6px;"></div> ' +
            '    $NAME$ ' +
            '</div> '
            ,

            DIRECTORY: '<div id="$ID$" class="four wide column"> ' +
            '    <div class="ui small header">$DIRNAME$</div> ' +
            '    <div class="ui list"> ' +
            '    </div> ' +
            '</div> '
            ,
        }
        ,

        TOPICHEADER: '<strong $DATA$>$NAME$</strong>'
        ,

        VERBATIM: '<li id="$VERBATIMID$" data-id="$ID$" class="ui raised segment verbatim" ' +
        '    style="min-height: 60px;padding-bottom: 47px;position: relative;"> ' +
        '    <div class="verbatim-score ui right floated statistic"> ' +
        '        <div class="value" style="font-size: 16px;margin-right:21px;">$SCORE$</div> ' +
        '        <div class="label" style="font-size: 9px;"></div> ' +
        '    </div> ' +
        '    <div class="verbatim-additional" style="position: absolute; top: 13px; right: 6px"> ' +
        '        <i class="$ICONPOLARITY$ icon cursor popup-element" data-content="sentiment: $POLARITY$" ' +
        '           data-variation="inverted" ' +
        '           style="font-size:20px;color:$COLORPOLARITY$;"></i> ' +
        '    </div> ' +
        '    <p class="verbatim-text">$TEXT$</p> ' +
        '    <div class="concept-list"></div> ' +
        '    <div class="verbatim-divider" style="text-align: left;"> ' +
        '        <label class="c" style="font-size:10px;color: lightgrey">c</label> ' +
        '        <hr style="border: solid #F2F2F2 1px; margin: 0"> ' +
        '        <label class="k" style="font-size:10px;color: lightgrey">k</label> ' +
        '    </div> ' +
        '    <div class="keyword-list"></div>' +
        '    <div class="verbatim-divider" style="text-align: left;">' +
        '        <label style="font-size:10px;visibility: hidden;">xxx</label>' +
        '        <hr style="border: dotted #F2F2F2 1px; margin: 0">' +
        '        <label class="e" style="font-size:10px;color: lightgrey">e</label>' +
        '    </div>' +
        '    <div class="emotion-list ui four column grid"></div>' +
        '    <div class="verbatim-divider" style="text-align: left;"> ' +
        '        <label style="font-size:10px;visibility: hidden;">xxx</label> ' +
        '        <hr style="border: dotted #F2F2F2 1px; margin: 0"> ' +
        '        <label class="e" style="font-size:10px;color: lightgrey"></label> ' +
        '    </div> ' +
        '    <div class="ner-list ui four column grid" style="padding-top: 10px;"></div> ' +
        '    <div class="verbatim-options" style="position: absolute; bottom: 5px; right: 5px"> ' +
        '        <span class="tag-container"> </span> ' +
        '        <i class="tags icon cursor" style="color: darkolivegreen" onclick="TaggerManager.activateTagPopup(this)"></i> ' +
        '        <!--// TODO has issues due to problem with iterative verbatim rendering--> ' +
        '        <!--//<i class="heart icon cursor ar-tag ar-tag-unselected" onclick="toggleFavorite(this)"></i> --> ' +
        '        <!--//<div class="ui checkbox" style="position: relative;top: 3px;">--> ' +
        '            <!--//<input class="action-select-verbatim" type="checkbox" onclick="selectVerbatim(this)" />--> ' +
        '            <!--//<label></label>--> ' +
        '        <!--//</div>--> ' +
        '    </div>' +
        '    <div> ' +
        '        <div class="additional ui horizontal list" style="margin-top:25px;"> ' +
        '        </div> ' +
        '    </div> ' +
        '</li>'
        ,

        VERBATIMADDITIONALITEM: '<div class="item" style="margin-left:$MARGINLEFT$px;"> ' +
        '    <i class="$ICON$ icon"></i> ' +
        '    <div class="content" style="padding-left:0;"> ' +
        '        <div class="header" style="font-size:9px;padding-bottom:2px;">$HEADER$</div> ' +
        '        $VALUE$ ' +
        '    </div> ' +
        '</div> '
        ,

        VERBATIMTAG: '<span class="" style="font-weight: bold; margin-right: 10px">$TAG$</span>'
        ,

        VERBATIMLISTTAG: '<span class="ui label">$TAG$<i class="remove circle icon cursor" onclick="TaggerManager.removeTag(this)"></i></span>'
        ,

        VERBATIMCONCEPT: '<div class="ui $COLOR$ label cursor" style="margin:4px;" $DATA$ onclick="onclickHighlightWord(this)">$CONCEPT$<div class="detail">$SCORE$</div></div>'
        ,

        NOFOUNDVERBATIM: '<li id="$ID$" class="ui segment" style="min-height: 60px;">' +
        '<div class="ui right floated statistic">' +
        '</div>' +
        '<h3 style="text-align: center;"><strong>$TEXT$</strong></h3>' +
        '<div style="width:100%;height:22px;"></div>' +
        '</li>',

        TAGGEDVERBATIM: '<li class="ui segment" style="min-height: 60px;padding-bottom: 6px;position: relative">' +
        '<div class="ui right floated statistic">' +
        '<div class="value" style="font-size: 16px;">$SCORE$</div>' +
        '<div class="label" style="font-size: 9px;"></div>' +
        '</div>' +
        '<p>$TEXT$</p>' +
        '<div style="position: absolute; bottom: 5px; right: 5px">' +
        '<i class="remove icon cursor ar-remove-tag" data-id="$ID$" onclick="onclickFavoriteRemoveFavorite(this)" style="color: darkred"></i>' +
        '</div>' +
        '</li>'
        ,

        LOADMORE: '<li id="container-verbatims-load-more" class="ui raised very padded text container segment cursor"' +
        '    style="padding:30px;text-align: center;margin-bottom:15px;background:black;color:white;opacity: 0.6;width:100%" ' +
        '       onclick="loadMoreVerbatims(this)">' +
        '    <h4 style="font-family:roboto-black">LOAD MORE</h4>' +
        '</li>',

        LOADMORE2: '<li id="container-verbatims-load-more" class="ui raised very padded text container segment cursor"' +
        '    style="padding:30px;text-align: center;margin-bottom:15px;background:black;color:white;opacity: 0.6;width:100%" ' +
        '       $FN$>' +
        '    <h4 style="font-family:roboto-black">LOAD MORE</h4>' +
        '</li>',

        INSIGHT: {
            INSIGHT: {
                CONTAINER: '<div class="item" style="position:relative;padding: 5px 5px 5px 10px;" $DATA$ $FN$> ' +
                //'    <i class="remove circle icon cursor" ' +
                //'       style="position:absolute;right:10px;top:6px;color:maroon;font-size:19px" $FNREMOVE$></i> ' +
                //'    <i class="idea middle aligned icon"></i> ' +
                '    <div class="content" style="position:relative;"> ' +
                '        <a class="header" style="padding-right:35px;">$INSIGHTNAME$</a> ' +
                '        <div class="description" style="margin-top:2px;">$INSIGHTINFO$</div> ' +
                '        <div style="position:absolute;top:10px;right:5px;"> ' +
                '            <i class="pencil cursor icon" $FNEDIT$></i>' +
                '        </div> ' +
                '    </div> ' +
                '</div> '
            },

            TAG: {
                CONTAINER: '<span class="ui label" $FN$ $DATA$> ' +
                '    $TAGNAME$ ' +
                '    <i class="remove circle icon cursor" $FNREMOVE$></i> ' +
                '</span> '
                ,
            },

            LOADMORE: '<li class="container-loadmore ui raised very padded text container segment cursor"' +
            '    style="padding:30px;text-align: center;margin-bottom:15px;background:black;color:white;opacity: 0.6;width:100%" ' +
            '       $FN$>' +
            '    <h4 style="font-family:roboto-black">LOAD MORE</h4>' +
            '</li>',


            RELEVANCE: {
                CONTAINER: '<div style="display: inline-block;" data-id="$ID$" data-containerid="$CID$"> ' +
                '    <div class="ui red deny mini button" style="background:#C00C0C;" onclick="InsightManager.onClickPredictionNotRelevant(this)"> ' +
                '        no ' +
                '    </div> ' +
                '    <div class="ui positive right labeled icon mini button" onclick="InsightManager.onClickPredictionRelevant(this)"> ' +
                '        Is Relevant ' +
                '        <i class="checkmark icon"></i> ' +
                '    </div> ' +
                '</div> ',
            },

            FEATURE: {
                CONTAINER: '<div class="ui labeled button feature" tabindex="0" style="position:relative;margin-bottom:10px;" ' +
                '     $DATA$ $FUNC$> ' +
                '    <div class="ui $COLOR$ button" style="padding-left:5px;"> ' +
                '        <i class="icon" style="width:82px;font-family: roboto;font-size: 12px;">$TYPE$</i> ' +
                '        $VALUE$ ' +
                '    </div> ' +
                '    <a class="ui basic $COLOR2$ left pointing label"> ' +
                '        $WEIGHT$ ' +
                '    </a> ' +
                '    <i class="remove circle icon cursor" ' +
                '       style="position:absolute;right:-12px;top:-5px;color:#232323;font-size:17px;" $FNREMOVE$></i> ' +
                '</div> '
                ,
            },
        },

        MV: {
            HEADER: '<h4 style="font-family:roboto-bold;border-bottom: solid 1px lightgrey;">$HEADER$</h4>',

            SUBHEADER: '<h5 style="margin:0 0 0 10px;color:$COLOR$">$HEADER$</h5>',

            BREADCRUMB: {
                CONTAINER: '<div class="ui breadcrumb" id="$ID$"></div>',
                ITEM: '<div class="active section" style="font-family: roboto-bold;">$NAME$</div>',
                DIVIDER: '<div class="divider">+</div>',
            },

            NEWLINE: '<div style="width:100%;height:10px;"></div>',

            DIVIDER: '<div style="width:100%;height:20px;border-bottom: solid 1px lightgrey;margin-bottom: 18px;"></div>',

            ITEM: {
                CONTAINER: '<div id="$ID$" class="ui three column grid" style="height:auto;"></div>',
                SUBCONTAINER: '<div class="column"><h5>$HEADER$</h5><div id="$LISTID$"></div></div>',
                ITEM: '<div class="ui label cursor $COLOR$" style="margin:4px;font-family:roboto-black;" ' +
                'onclick="$FUNC$" $DATA$>$NAME$</div>',
            }
        },

        EMOTION: {
            LISTCONTAINER: '<div id="$ID$" class="column">' +
            '    <span style="font-family:roboto-black;margin-left:7px;font-size:8px;color:$COLOR$;">$NAME$</span>' +
            '    <div class="list"></div>' +
            '</div>'
            ,

            ITEM: '<div class="ui label cursor" style="margin:4px;font-family:roboto-black;color:white;background:$COLOR$;" $DATA$ ' +
            '       onclick="onclickHighlightWord(this)">' +
            '   $NAME$' +
            '</div>'
            ,

            ITEMREPLACE: '<span class="ar-found-emotion" style="color:$COLOR$;text-decoration:underline;font-family:roboto-black;">' +
            '   $TEXT$' +
            '</span>'
            ,
        },

        NER: {

            LISTCONTAINER: '<div id="$ID$" class="column">' +
            '    <span style="font-family:roboto-black;margin-left:7px;font-size:8px;color:$COLOR$;">' +
            '        <i class="ui icon $ICON$ $ICONCOLOR$" style="font-size:14px;"></i> $NAME$' +
            '    </span>' +
            '    <div class="list"></div>' +
            '</div>'
            ,

            ITEM: '<div class="ui label cursor $COLOR$" style="margin:4px;font-family:roboto-black;" $DATA$' +
            '   onclick="onclickHighlightWord(this)">' +
            '   $NAME$ ' +
            '</div>'
            ,

        },

        STAT: {
            LIST: {
                CONTAINER: '<div id="$ID$" style="margin-bottom:10px;"> ' +
                '    <h2 class="ui sub header"> ' +
                '        $NAME$ ' +
                '    </h2> ' +
                '    <div class="list"></div> ' +
                '</div> '
                ,

                ITEM: '<div class="ui grid label divided $COLOR$ cursor" style="font-size:14px;margin:4px; ' +
                '    font-family:roboto-black;width:100%;" $DATA$ onclick="onClickStatsItem(this)"> ' +
                '    <div class="ten wide column" style="padding:8px;"> ' +
                '        $NAME$ ' +
                '    </div> ' +
                '    <div class="six wide column" style="padding:8px;text-align:right;"> ' +
                '        $VALUE$ ' +
                '    </div> ' +
                '</div> '

                //ITEM: '<div class="ui label $COLOR$" style="margin:4px;font-family:roboto-black;" $DATA$> ' +
                //'    $NAME$ ' +
                //'</div> '
                ,
            },

            CHART: {
                CONTAINER: '<div style="margin-top: 10px;"> ' +
                '   <h2 class="ui sub header"> ' +
                '       $NAME$ ' +
                '   </h2> ' +
                '   <div id="$ID$" class="chart" style="height:180px;border-bottom: solid 1px lightgray;"> ' +
                '       <div class="ui inverted dimmer">' +
                '           <div class="ui text loader" style="font-family: roboto-black;">Loading Chart</div>' +
                '       </div>' +
                '   </div> ' +
                '</div> '
                ,
            },

            BREADCRUMB: {
                ITEM: '<div class="section" style="margin-bottom:10px;"> ' +
                '    <div class="ui button $COLOR$" style="cursor:no-drop" $DATA$ onclick="onClickStatsBredCrumb(this)"> ' +
                '        <i class="icon" style="width:82px;font-family: roboto;font-size: 12px;"> $TYPE$ </i> ' +
                '        $NAME$ ' +
                '    </div> ' +
                '</div> '
                ,

                DIVIDER: '<i class="plus icon divider" style="margin-right:10px;"></i>'
                ,

            },
        }
    }
    ,

    TAGGED: {
        OPTIONS: {
            CONTAINER: '<div class="ui icon blue top right pointing dropdown button" style="border-left: solid 1px;">' +
            '    <i class="wrench icon"></i>' +
            '    <div class="menu">' +
            '        <div class="header">Dashboard Options</div>' +
            '        <div class="options-list"></div>' +
            '    </div>' +
            '</div>'
            ,

            ITEM: '<a class="cursor item" $FUNC$>$ITEM$</a>'
            ,
        }
    },

    WORDCLOUD: {
        LABEL: '<div class="floating ui $COLOR$ label" style="top:1px;font-size:7px;">$VALUE$</div>',
    }
    ,

    TAGS: {

        DATASET: '<span class="dataset ui item big blue label cursor" ' +
        '       onclick="$FUNC$" style="margin: 0 5px 10px 10px;background:$BACKGROUND$ !important;" ' +
        '       data-topic="$TOPIC$" data-name="$DATASETNAME$">$NAME$' +
        '</span>'
        ,

        TAG: '<div class="ui card item cursor ar-tag-container" data-id="$TAGID$" data-index="$TAGINDEX$" ' +
        'style="width: 100%;margin: 0 7px 7px 0" onclick="tagOnClick(this)">' +
        '<div class="content" style="text-align: center;">' +
        '<span class="" style="font-size:15px;font-weight:bold;">$TAG$</span>' +
        '</div>' +
        '</div>'

    },


    SENTIMENT: {

        CONTAINER: '<li id="$CONTAINERID$" class="ui fluid container segment ar-dataset-container"  ' +
        'data-topic="$TOPIC$" style="height:300px;overflow:hidden"> ' +
        '<div class="ui grid" style="height:300px"> ' +
        '<div class="three wide column" style="border-right: solid 1px #ECECEC;border-bottom: solid 1px #ECECEC;"> ' +
        '    <h4 class="ar-dataset-header">$NAMEDATASET$</h4> ' +
        '    <div class="ar-divider" style=""></div> ' +
        '    <div class="ui middle aligned divided list"> ' +
        '        <div class="item"> ' +
        '            <div class="content"> ' +
        '                <span style="font-size:10px;">Total comments</span> ' +
        '                <span class="text-total-comments" ' +
        '                style="margin-left:5px;font-weight:bold;font-family: Roboto, serif;"></span> ' +
        '                </div> ' +
        '            </div> ' +
        '        <div class="item"> ' +
        '            <div class="content"> ' +
        '                <span style="font-size:10px;">Very Positive</span> ' +
        '                <span class="text-total-comments-very-positive" ' +
        '                style="margin-left:5px;font-weight:bold;font-family: Roboto, serif;color:#1AC645"></span> ' +
        '                </div> ' +
        '            </div> ' +
        '        <div class="item"> ' +
        '            <div class="content"> ' +
        '                <span style="font-size:10px;">Positive</span> ' +
        '                <span class="text-total-comments-positive" ' +
        '                style="margin-left:5px;font-weight:bold;font-family: Roboto, serif;color:#7AA184"></span> ' +
        '                </div> ' +
        '            </div> ' +
        '        <div class="item"> ' +
        '            <div class="content"> ' +
        '                <span style="font-size:10px;">Neutral</span> ' +
        '                <span class="text-total-comments-neutral" ' +
        '                style="margin-left:5px;font-weight:bold;font-family: Roboto, serif;color:#8F918F"></span> ' +
        '                </div> ' +
        '            </div> ' +
        '        <div class="item"> ' +
        '            <div class="content"> ' +
        '                <span style="font-size:10px;">Negative</span> ' +
        '                <span class="text-total-comments-negative" ' +
        '                style="margin-left:5px;font-weight:bold;font-family: Roboto, serif;color:#A50016"></span> ' +
        '                </div> ' +
        '            </div> ' +
        '        <div class="item"> ' +
        '            <div class="content"> ' +
        '                <span style="font-size:10px;">Very Negative</span> ' +
        '                <span class="text-total-comments-very-negative" ' +
        '                style="margin-left:5px;font-weight:bold;font-family: Roboto, serif;color:#EA374F"></span> ' +
        '                </div> ' +
        '            </div> ' +
        '        </div> ' +
        '    </div> ' +
        '<div id="$BARPLOTID$" class="ten wide column ar-barplot" style=""></div> ' +
        '<div class="three wide column" style="height: 110%;position: relative;"> ' +
        '    <div class="ar-container-options" style="position:absolute;top:5px;right:5px;"> ' +
        '        <i class="minus circle icon cursor" style="color:maroon;" data-dataset="$DATASET$" ' +
        '        onclick="removeDataset(this)"></i> ' +
        '        </div> ' +
        '    <div style="text-align: center;padding-top:110px;"> ' +
        '        <i class="icon ar-overall-sentiment-icon" style="font-size: 100px;"></i> ' +
        '        </div> ' +
        '    </div> ' +
        '</div> ' +
        '</li>'
        ,

        VERBATIMLIST: '<div class="ui grid" style="height:500px;">' +
        '    <div class="sixteen column verbatim-list-container" style="position: relative;padding-top:33px;">' +
        '       <div class="" style="position: absolute;top:10px;right:10px;">' +
        '           <i class="close icon cursor" style="color:grey;" onclick="closeVerbatimList(this)"></i>' +
        '       </div>' +
        '       <div class="" style="position: absolute;top:15px;left:12px;">' +
        '               <span style="font-size:11px;color:lightgrey;">verbatims for sentiment:' +
        '                   <span class="sentiment" style="color:grey;font-family: roboto-black;"></span></span>' +
        '       </div>' +
        '       <div class="progress-loader" style="position:absolute;left:10px;bottom:10px;width:100px;height:100px;">' +
        '          <div class="ui dimmer" style="">' +
        '              <div class="ui text loader" style="font-family: roboto-black;">LOADING VERBATIMS</div>' +
        '          </div>' +
        '       </div>' +
        '       <ul class="verbatim-list" style="padding:10px;overflow-y:auto;height:450px;"></ul>' +
        '       <div class="verbatim-list-options" ' +
        '               style="position:absolute;right:30px;top:0;padding:20px;width:200px;">' +
        '           <span class="ui olive circular label ar-load-more-text cursor" onclick="loadMoreVerbatims(this)"' +
        '                   style="font-family: roboto-black;position: absolute;right: 15px;bottom: 10px;">LOAD MORE</span>' +
        '       </div>' +
        '   </div>' +
        '</div>'
    }
    ,
    HEATMAP: {

        DIRECTORYNAME: '<div class="item" style="margin-bottom:5px;margin-top:20px;">' +
        '   <h5 class="ui header">$DIR$</h5>' +
        '</div>'
        ,

        DIRECTORY: '<div class="item">' +
        '<i class="folder icon"></i>' +
        '<div class="content">' +
        '<div class="header cursor" data-show="off" onclick="toggleDisplayDirectory(this)">$DIRNAME$</div>' +
        '<div class="list ar-directory" data-dir-name="$DIR$" data-name="$NAME$" style="display: none;">' +
        '</div>' +
        '</div>' +
        '</div>'
        ,

        SUBDIRECTORY: '<div class="item" onclick="onClickItem(this)" $DATA$>' +
        '<i class="caret right icon"></i>' +
        '<div class="content">' +
        '<div class="ar-directory-item cursor" data-href="$HREF$">' +
        '<div class="description">$ITEM$</div>' +
        '</div>' +
        '</div>' +
        '</div>'
    },

    DASHBOARD: {
        DIR: {
            CONTAINER: '<div class="sixteen wide column" style=""> ' + // DEPRECATE DIRECTORIES : five => sixteen
            '    <div id="$ID$" class="ui segment container-dir" $DATA$> ' +
            '        <div class="mini ui icon button circular red popup-trigger" ' +
            '             data-content="Remove this folder" onclick="removeDirectory(this)" ' +
            '             style="position: absolute;right:5px;top:5px;"> ' +
            '            <i class="ui icon minus" style=""></i> ' +
            '        </div> ' +
            '        <div class="ui horizontal statistic" style="position:absolute;right:9px;top:20px;"> ' +
            '           <div class="value" style="font-size:14px;"> ' +
            '           </div> ' +
            '           <div class="label" style="font-size:7px;margin-left:3px;padding-top:4px;text-transform:lowercase"> ' +
            '               Verbatims ' +
            '           </div> ' +
            '        </div> ' +
            '        <div class="ui small header" style="color:black;margin-top:20px;">$DIRNAME$</div> ' +
            '        <div class="list-topic" style="width:100%;padding-right: 8px;"> ' +  // DEPRECATE DIRECTORIES list => grid, height: 92% => null
            '        </div> ' +
            '    </div> ' +
            '</div> '
            ,
        },

        PROJECT: {
            CONTAINER: '<div class="item project cursor" style="position:relative;padding-left:5px;" $DATA$ $FUNC$> ' +
            '    <div class="ui feed " style="margin:5px 5px;"> ' +
            '        <div class="event"> ' +
            '            <div class="label" style="background:$BACKGROUND$;"></div> ' +
            '            <div class="content"> ' +
            '                <h4 class="">$NAME$</h4> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '    <i class="pencil icon" style="position:absolute;right:0;top:3px;" onclick="ProjectModal.editProject(this)"></i>' +
            '</div> '
            ,
        },

        DATASET: {
            CONTAINER: '<div class="ui button dataset cursor" style="color:white;margin-bottom:6px;background:$BACKGROUND$;" $FUNC$ $DATA$> ' +
            '       <span>$DATASETNAME$</span> ' +
            '</div> '
            ,
        },

        DETAILS: {
            TABLE: {
                ROW: '<tr style="font-family:roboto;"> ' +
                '    <td><div class="content"><div class="sub header" style="color:grey;">$DESC$</div></div></td> ' +
                '    <td>$VALUE$</td> ' +
                '</tr> ',

                USER: '<div class="ui $COLOR$ label" style="margin-bottom:6px;font-family:roboto-black"> ' +
                '    $TEXT$ ' +
                '</div> '
            }
        },

        FORM: {
            ITEM: '<option value="$VALUE$">$NAME$</option>'
            ,
        },
    },

    MENU: {
        USER: '<div style="margin-right:15px;"><span>Welcome <span style="font-family:roboto-black">$USERNAME$</span></span></div>' +
        '       <div class="ui icon grey top right pointing dropdown button" style="border-left: solid 1px;">' +
        '           <i class="settings icon"></i>' +
        '           <div class="menu">' +
        '               <div class="header">User Options</div>' +
        '               <div id="user-options-list">' +
        '                   <a href="/logout" class="cursor item"><div>Logout</div></a>' +
        '               </div>' +
        '           </div>' +
        '       </div>'
        ,

        USERLESS: '<div class="ui icon grey top right pointing dropdown button" style="border-left: solid 1px;">' +
        '    <i class="settings icon"></i>' +
        '    <div class="menu">' +
        '        <div class="header">User Options</div>' +
        '        <div id="user-options-list">' +
        '            <a href="/logout" class="cursor item"><div>Logout</div></a>' +
        '        </div>' +
        '    </div>' +
        '</div>'
        ,
    },

    CADASHBOARD: {

        DATASET: {
            CONTAINER: '<div class="ui button dataset cursor $BACKGROUND$" ' +
            '     style="position:relative;color:white;margin-bottom:6px;" $FUNC$ ' +
            '     $DATA$> ' +
            '    <span>$DATASETNAME$</span> ' +
            '    <i class="remove circle icon cursor" $FUNCREMOVE$ ' +
            '       style="position:absolute;right:-12px;top:-5px;color:maroon;font-size:17px;"></i> ' +
            '</div> '
        },

        WCA: {
            OPTIONS: {
                CONTAINER: '<h3 class="ui dividing header" style="padding-left:20px;position:relative;">' +
                '    Word Cross Analysis' +
                '    <i class="ui add green icon cursor" style="font-size:20px;position:absolute;right:10px;"' +
                '       onclick="WCAEngine.showModal()"></i>' +
                '</h3>' +
                '<!-- WC WORDS ACTIVATED -->' +
                '<div style="padding:0 20px;">' +
                '    <div class="list-wca-word" style="padding: 20px 10px;max-height:300px;overflow-y:auto;"></div>' +
                '</div>'
                ,
            },

            WORD: {
                ITEM: '<div class="ui button wca-word cursor" ' +
                '     style="font-size:12px;position:relative;color:white;margin-bottom:7px;background:$BACKGROUND$;" $FUNC$ ' +
                '     $DATA$> ' +
                '    <span>$WORD$</span> ' +
                '    <i class="remove circle icon cursor" $FUNCREMOVE$ ' +
                '       style="position:absolute;right:-16px;top:-7px;color:maroon;font-size:17px;"></i> ' +
                '</div> '
                ,

                PREVIEW: '<div class="ui button wca-word cursor" ' +
                '     style="font-size:12px;position:relative;color:white;margin-bottom:7px;background:$BACKGROUND$;" $FUNC$ ' +
                '     $DATA$> ' +
                '    <span>$WORD$</span> ' +
                '</div> '
                ,
            },
        },

        ICA: {
            OPTIONS: {
                CONTAINER: '<h3 class="ui dividing header" style="padding-left:20px;position:relative;">  ' +
                '    Insights Cross Analysis ' +
                '    <i class="ui add green icon cursor" style="font-size:20px;position:absolute;right:10px;"  ' +
                '       onclick="InsightCAEngine.showModal()"></i> ' +
                '</h3>  ' +
                '<!-- ICA INSIGHTS ACTIVATED --> ' +
                '<div style="padding:0 20px;">  ' +
                '    <div class="list-ica-insight" style="padding: 20px 10px;max-height:300px;overflow-y:auto;"></div> ' +
                '</div> '
                ,
            },
        },


        STATS: {
            CONTAINER: '<div id="$CONTAINERID$" class="ui fluid container segment ar-dataset-container" $DATA$ ' +
            '    style="overflow:hidden;position:relative;padding-bottom: 50px;"> ' +
            '    <div class="ui grid" style=""> ' +
            '        <div id="$PLOTID$" class="fifteen wide column content-plot" style="height:500px"></div> ' +
            '        <div class="one wide column" style="height: 110%;position: relative;"> ' +
            '            <div class="ar-container-options" style="position:absolute;top:5px;right:5px;"> ' +
            '                <i class="minus circle icon cursor" style="color:maroon;" $FNREMOVE$></i> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="sixteen wide column"> ' +
            '            <h4 class="ui dividing header">Stats</h4> ' +
            '            <div class="content-list" style="overflow: auto;"></div>' +
            '        </div> ' +
            '    </div> ' +
            '   <div class="message-init" style="position:absolute;top:30%;left:0;width:100%;text-align:center;"> ' +
            '       <h3 class="ui grey header" style="display:inline-block;">$INITMESSAGE$</h3>' +
            '   </div>' +
            '   <div class="ui text dimmer"> ' +
            '       <div class="ui text loader" style="font-family: roboto-black">LOADING STATISTICS ' +
            '       </div> ' +
            '   </div> ' +
            '</div> '

            ,

            TEXT: {
                ITEM: '<div class="item"> ' +
                '    <div class="content"> ' +
                '        <span style="font-size:10px;">$TITLE$</span> ' +
                '        <span class="" ' +
                '              style="margin-left:5px;font-weight:bold;font-family: Roboto, serif;"> ' +
                '            $VALUE$ ' +
                '        </span> ' +
                '    </div> ' +
                '</div> '
                ,
                HEADER: '<h5 class="ui header">$TITLE$</h5>'
            }
        },
    },

    TABLE: {
        CONTAINER: '<table id="$ID$" class="ui compact celled definition table"> ' +
        '    <thead></thead>' +
        '    <tbody></tbody> ' +
        '</table> '
        ,

        HEADER: '<td><strong>$HEADER$</strong></td>'
    },

    EXTRACTPROGRESS: {

        TABLE: {
            ITEM: "<tr $DATA$> " +
            "    <td style='font-weight:bold;'>$TOPICNAME$</td> " +
            "    <td>$PROJECT$</td> " +
            "    <td>$LANG$</td> " +
            "    <td>$STATUS$</td> " +
            "    <td class='content-progress'></td> " +
            "</tr> "
            ,

            STATUS: {
                upload: '<div class="ui blue button" style="width:100%;cursor:inherit;">Uploaded</div>',

                queue: '<div class="ui olive button" style="width:100%;cursor:inherit;">Queued</div>',

                extract: '<div class="ui green button" style="width:100%;cursor:inherit;">Extracting</div>',
            }
        },

        PROGRESS: {
            CONTAINER: '<div class="ui $COLOR$ progress" data-percent="$PERCENT$"> ' +
            '    <div class="bar"> ' +
            '        <div class="progress"></div> ' +
            '    </div> ' +
            '</div> '
            ,
        }
    },

    CLEANER: {
        VERBATIM: {
            ITEM:
            '<div class="item cleaner-verbatim">' +
            '   $VERBATIM$' +
            '</div>',
        },
        HISTORY: {
            ITEM:
            '<div class="ui segment cleaner-history" $DATA$> ' +
            '    <div class="ui grid"> ' +
            '        <div class="ui thirteen wide column"> ' +
            '            <div class="ui feed" style="padding-top:10px;"> ' +
            '                <div class="event"> ' +
            '                    <div class="content"> ' +
            '                        <div class="date">on $DATE$</div> ' +
            '                        <div class="summary" style="padding-left:30px;">' +
            '                           <table class="ui very basic collapsing table"> ' +
            '                               <tbody> ' +
            '                               <tr> ' +
            '                                   <td>mode</td> ' +
            '                                   <td> ' +
            '                                       <span class="ui header" style="font-size:14px;"> ' +
            '                                           <div class="content">$MODE$</div> ' +
            '                                       </span> ' +
            '                                   </td> ' +
            '                               </tr> ' +
            '                               <tr> ' +
            '                                   <td>query</td> ' +
            '                                   <td> ' +
            '                                       <span class="ui header" style="font-size:14px;"> ' +
            '                                           <div class="content">$QUERY$</div> ' +
            '                                       </span> ' +
            '                                   </td> ' +
            '                               </tr> ' +
            '                               <tr> ' +
            '                                   <td>nbVerbatims</td> ' +
            '                                   <td> ' +
            '                                       <span class="ui header" style="font-size:14px;"> ' +
            '                                           <div class="content">$nbVerbatims$</div> ' +
            '                                       </span> ' +
            '                                   </td> ' +
            '                               </tr> ' +
            '                               </tbody> ' +
            '                           </table> ' +
            '                        </div> ' +
            '                        <div class="meta">by $META$</div> ' +
            '                    </div> ' +
            '                </div> ' +
            '            </div> ' +
            '        </div> ' +
            '        <div class="ui three wide column"> ' +
            '            <button class="ui basic button" onclick="VerbatimCleaner.undoChange(this)"> ' +
            '                <i class="icon undo"></i> ' +
            '                Revert ' +
            '            </button> ' +
            '        </div> ' +
            '    </div> ' +
            '</div> '
        },
    },
}