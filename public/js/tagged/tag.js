function selectTagForDataset(t) {
    // add variable to InteractionHistory
    InteractionHistory.setVariable('t', {
        topic: t.topic,
        name: t.name
    })
    emptyVerbatimList()
    tagList = []
    var topic = t.topic
    var name = t.name
    $('#menu-nav-text-2')
        .data('topic', topic)
        .html('<h4>' + name + '</h4>')
    loadDistinctTags(topic, function (data) {
        setupTagLists(data)
    })
    closeChooseDataSetModal()
    InsightManager.init()
}

function loadDistinctTags(topic, next) {
    API.TAGS.getTags(topic)
        .then(function (r) {
            next(r)
        })
}

function setupTagLists(tagList) {
    var container = $('#tag-container').find('.option-tag').find('.list')
    container.empty()
    var container1 = $('#tag-list')
    container1.empty()

    // append topic containers
    for (var i = 0; i < tagList.length; i++) {
        var tag = tagList[i];
        container.append(
            TEMPLATE.TAGS.TAG
                .replace('$TAG$', tag.toUpperCase())
                .replace('$TAGID$', tag)
                .replace('$TAGINDEX$', i)
        )
    }
}

var tagList = []
function tagOnClick(o) {
    var self = $(o)
    var container = $('#tag-container').find('.option-tag').find('.list')
    //var index = getIndexOfJQueryObject(self, container.find('.item'))
    var tag = self.data().id
    var ifContained = _.contains(tagList, tag)

    if (toggleClass(self, 'ar-selected')) {
        if (!ifContained) {
            tagList.push(tag)
            //self.data('index', index)
            //container.prepend(self)
        }
    } else {
        if (ifContained) {
            var prevIndex = self.data().index
            tagList.splice(_.indexOf(tagList, tag), 1)
            //self.remove()
            //insertAtIndex(prevIndex, container, self, '.item')
        }
    }

    refreshVerbatimList()
    getAssociatedTags()
    refreshBreadCrumb()
}

function refreshVerbatimList() {

    var topic = getTopic()
    API.TAGS.getVerbatims(tagList, topic)
        .then(function (r) {
            var container = $('#container-verbatims')
            container.empty()
            var appendVerbatimQueue = []

            r.r.forEach(function (d) {
                appendVerbatimQueue.push(appendVerbatim(container, d.verbatim, 0, d._id, d.options, d))
            })
            appendToTaggedVerbatimList(appendVerbatimQueue, {c: r.c})

            if (tagList.length == 0)
                emptyVerbatimList()
            else if (r.length == 0)
                container.append(
                    TEMPLATE.VERBATIM.NOFOUNDVERBATIM
                        .replace('$ID$', "container-verbatims-load-more")
                        .replace('$TEXT$', "No Verbatims Found")
                )
        })

}

function emptyVerbatimList() {
    $('#container-verbatims')
        .empty()
        .append(
            '<li id="container-verbatims-load-more" class="ui raised very padded text container segment"' +
            'style="margin-top: 5%">' +
            '    <h2 class="ui header">Click on tags to view their Verbatims </h2>' +
            '</li>'
        )

}

var appendToTaggedVerbatimList = function (appendVerbatimQueue, d, overwriteContainer, onDone) {
    appendToVerbatimList(appendVerbatimQueue, d, overwriteContainer, onDone)
}

function searchForTag() {
    var tag = $('#input-search-tag').val().toLowerCase()
    var ifShowAll = false
    if (tag == "") {
        ifShowAll = true
    }
    var container = $('#tag-container').find('.option-tag').find('.list')
    $.each(container.find('.item'), function () {
        var self = $(this)
        var ifFound = false
        var tmp = formatFeedText(tag, self.data().id.toLowerCase(), 0)
        if (tmp != self.data().id.toLowerCase())
            ifFound = true

        if (ifFound) {
            self.fadeIn()
        } else {
            if (!ifShowAll) {
                self.fadeOut()
            } else {
                self.fadeIn()
            }
        }
    })
}

function getAssociatedTags() {
    var topic = getTopic()
    API.TAGS.getAssociatedTags(tagList, topic)
        .then(function (r) {
            $.each($('#tag-container').find('.option-tag').find('.list').find('.item'), function () {
                var self = $(this)
                if (r.indexOf(self.data().id) != -1) {
                    if (!self.hasClass('ar-selected')) {
                        self.addClass('ar-associated')
                    }
                } else {
                    self.removeClass('ar-associated')
                }

            })
        })
}

function refreshBreadCrumb() {
    var string = ""
    for (var i = 0; i < tagList.length; i++) {
        var tag = tagList[i];
        string += "<div class='active section'> " + tag + "</div>"
        if (i < tagList.length - 1)
            string += "<div class='divider'> + </div>"
    }

    $('#menu-nav-text-3').html(
        "<div class='ui breadcrumb'>" + string + " </div>"
    )
}

function updateMenuText() {

}