function _InsightManager() {
    var self = this
    this.container = $('#main-container')
    this.searchInsights = $('#input-search-insight')
    this.searchTags = $('#seg-tags').find('.ui.search')
    this.listInsight = $('.list-insight')
    this.containerVerbatimTagged = $('#container-verbatims-insight-tagged')
    this.containerVerbatimPredicted = $('#container-verbatims-insight-predicted')
    this.containerFeatures = $('#container-modal-features')
    this.triggerReCompileModal = $('#trigger-recompile-modal')
    this.triggerSIndex = $('#trigger-compile-sindex')
    this.containerSIndex = $('#container-sindex')
    this.SETTINGS = {
        ifShowSIndex: false
    }
    this.socket = new SocketClient('insights')

    this.init = function () {
        this.defaultInsightForTopic()
        var topic = getTopic()
        // input search insight
        self.searchInsights
            .search({
                // TODO add topic name here. topic => project
                apiSettings: {
                    url: '/dashboard/verbatims/autocomplete/insight?q={query}&t=' + topic,
                    onResponse: function (response) {
                        self.buildInsightList(response.results)
                        return response;
                    },
                },
                searchFields: [
                    'title'
                ],
                searchFullText: false,
                error: {
                    noResults: 'No insights found',
                    serverError: 'There was an issue with querying the server.',
                },
            })
        self.searchInsights.off('keydown').on('keydown', function (e) {
            if (e.which == 13 || e.keyCode == 13) {
                self.searchInsights.search('hide results')
                if (self.searchInsights.search('get value').trim() == "") {
                    self.defaultInsightForTopic()
                }
            }
        })

        self.searchTags
            .search({
                apiSettings: {
                    url: '/dashboard/verbatims/autocomplete/tag?q={query}&t=' + topic,
                }
            })
        self.searchTags.off('keydown').on('keydown', function (e) {
            if (e.which == 13 || e.keyCode == 13) {
                self.searchTags.search('hide results')
                self.addTagsToInsight()
            }
        })
        self.searchTags.find('.plus.icon').on('click', function (e) {
            self.searchTags.search('hide results')
            self.addTagsToInsight()
        })

        self.triggerSIndex.on('click', self.onClickGetSIndex)

        self.triggerReCompileModal.on('click', self.onClickReCompileModal)
    }

    this.defaultInsightForTopic = function () {
        var topic = getTopic()
        // empty tagged and predicted lists
        self.containerVerbatimPredicted.empty()
        self.containerVerbatimTagged.empty()
        API.INSIGHTS.getInsightsForProject(topic).then(function (insights) {
            self.buildInsightList(insights)
        })
    }

    this.buildInsightList = function (insights) {
        self.container.find('.list-insight').empty() // emptying insights
        self.container.find('.list-insight-tag').empty() // emptying insight tags

        insights.forEach(function (insight) {
            var insightinfo = "number of tags : " + insight.tags.length
            self.container.find('.list-insight').append(
                TEMPLATE.VERBATIM.INSIGHT.INSIGHT.CONTAINER
                    .replace('$INSIGHTNAME$', insight.name)
                    .replace('$INSIGHTINFO$', insightinfo)
                    .replace('$DATA$', 'data-id="' + insight._id + '"')
                    .replace('$FN$', 'onclick="InsightManager.onClickInsight(this)"')
                    .replace('$FNEDIT$', 'onclick="openEditInsightModal(this)"')
            )
        })

        // click first insight by default
        self.container.find('.list-insight').find('.item').first().find('.header').click()
    }

    this.buildTagsList = function (tags) {
        self.container.find('.list-insight-tag').empty()
        tags.forEach(function (tag) {
            self.container.find('.list-insight-tag').append(
                TEMPLATE.VERBATIM.INSIGHT.TAG.CONTAINER
                    .replace('$TAGNAME$', tag)
                    .replace('$DATA$', 'data-tag="' + tag + '"')
                    .replace('$FNREMOVE$', 'onclick="InsightManager.onClickRemoveTag(this)"')
            )
        })
    }

    this.onClickInsight = function (o, overwriteData) {
        // turn ifShowSIndex to false, to avoid showing previous requests
        this.setIfShowSIndex(false)

        var id
        var item = $(o)
        if (_.isUndefined(overwriteData)) {
            self.container.find('.list-insight').find('.item').removeClass('insight-selected')
            id = item.data().id
        }
        else
            id = overwriteData.id

        API.INSIGHTS.getInsight(id).then(function (insight) {
            item.addClass('insight-selected')
            if (!_.isNull(insight)) {
                self.buildTagsList(insight.tags)
                self.buildTaggedVerbatims(insight)
                self.buildPredictedVerbatims(insight)
            }
        })
    }

    this.onClickRemoveInsight = function () {
        var r = confirm("Are you sure you want to delete this insight ? ")
        if (r) {
            var id = self.getSelectedInsight()
            API.INSIGHTS.removeInsight(id, getTopic()).then(function (res) {
                if (res.s) {
                    var currentItem = self.listInsight.find('.item[data-id="' + id + '"]')
                    if (!currentItem.is(self.listInsight.find('.item').first()))
                        currentItem.prev().click()
                    else
                        currentItem.next().click()

                    currentItem.slideUp()
                }
            })
        }
    }

    this.onClickRemoveTag = function (o) {
        var item = $(o)
        var tag = item.parent().data().tag
        var id = self.getSelectedInsight()
        var topic = getTopic()
        API.INSIGHTS.removeTagFromInsight(id, tag, topic).then(function (res) {
            if (res.s)
                self.onClickInsight(null, {id: id})
        })
    }

    this.onClickGetSIndex = function () {
        self.triggerSIndex.addClass('loading')
        var id = self.getSelectedInsight()
        var topic = getTopic()
        self.SETTINGS.ifShowSIndex = true

        self.socket.emit(
            'compile_s_index_req',
            {id: id, topic: topic},
            function (res) {
                if (!res.s)
                    throw new Error("Failure at socket:compile_s_index_request:" + res.m)
            }
        )
    }

    this.setIfShowSIndex = function (state) {
        self.SETTINGS.ifShowSIndex = state
        if (!state) {
            if (!self.containerSIndex.hasClass('hidden')) {
                self.containerSIndex.addClass('hidden')
            }
            self.triggerSIndex.removeClass('loading')
        }
    }

    this.socket.addListener('compile_s_index_res', function (r) {
        if(self.SETTINGS.ifShowSIndex) {
            self.containerSIndex.removeClass('hidden')
            // values
            self.containerSIndex.find('.sindex').text(r.sindex.toFixed(3))
            self.containerSIndex.find('.nbpredicted').text(r.nbpredicted)
            self.containerSIndex.find('.totalcount').text(r.topiccount)
            self.containerSIndex.find('.avgscore').text(r.avscore.toFixed(3))
            self.triggerSIndex.removeClass('loading')
        }
    })

    this.createInsight = function (data, next) {
        API.INSIGHTS.createInsight(data).then(function (res) {
            next(res)
        })
    }

    this.editInsight = function (data, next) {
        API.INSIGHTS.editInsight(data).then(function (res) {
            next(res)
        })
    }

    this.getSelectedInsight = function () {
        var data = $('.insight-selected').data()
        if (!_.isUndefined(data))
            return data.id
        else
            return null
    }

    this.addTagsToInsight = function () {
        var topic = getTopic()
        var query = self.searchTags.search('get value').trim()
        var id = self.getSelectedInsight()
        if (query != "")
            API.INSIGHTS.addTagsToInsight(id, query, topic).then(function (res) {
                if (res.s)
                    self.onClickInsight(null, {id: id})
            })
    }

    var ITERATIONTAGGED = 0
    this.buildTaggedVerbatims = function (insight, ifloadmore) {
        if (_.isUndefined(ifloadmore))
            ITERATIONTAGGED = 0
        else
            ITERATIONTAGGED++

        var topic = getTopic()
        var container = self.containerVerbatimTagged

        container.parent().dimmer('show')
        API.INSIGHTS.getTaggedVerbatims(insight._id, ITERATIONTAGGED, topic).then(function (res) {
            if (_.isUndefined(ifloadmore))
                container.empty()
            else
                container.find('.container-loadmore').remove()

            var appendVerbatimQueue = []

            res.r.forEach(function (d) {
                appendVerbatimQueue.push(appendVerbatim(container, d.verbatim, 0, d._id, d.options, d, "-tagged"))
            })
            appendToTaggedVerbatimList(appendVerbatimQueue, {c: res.c})

            if (res.r.length >= MAXSEARCHLENGTH)
                container.append(
                    TEMPLATE.VERBATIM.INSIGHT.LOADMORE
                        .replace('$FN$', 'onclick="InsightManager.loadMoreTaggedVerbatims(this)"')
                )
            container.parent().dimmer('hide')
        })
    }

    var ITERATIONPREDICTED = 0
    this.buildPredictedVerbatims = function (insight, ifloadmore) {
        if (_.isUndefined(ifloadmore))
            ITERATIONPREDICTED = 0
        else
            ITERATIONPREDICTED++

        var topic = getTopic()
        var container = self.containerVerbatimPredicted
        container.parent().dimmer('show')

        self.socket.emit(
            'get_predicted_verbatims_from_insight_req',
            {id: insight._id, topic: topic, iteration: ITERATIONPREDICTED, ifloadmore: ifloadmore, insight: insight},
            function (res) {
                if (!res.s)
                    throw new Error("Failure at socket:compile_s_index_request:" + res.m)
            }
        )
    }

    self.socket.addListener('get_predicted_verbatims_from_insight_res', function (res) {
        var container = self.containerVerbatimPredicted
        if (_.isUndefined(res.ifloadmore))
            container.empty()
        else
            container.find('.container-loadmore').remove()

        var appendVerbatimQueue = []
        res.r.forEach(function (d) {
            appendVerbatimQueue.push(appendVerbatim(container, d.verbatim, d.score.toFixed(3), d._id, d.options, d, "-predicted"))
        })
        appendToTaggedVerbatimList(appendVerbatimQueue, {c: res.c}, $(), self.addConfirmRelevanceAddon)

        if (res.r.length >= MAXSEARCHLENGTH)
            container.append(
                TEMPLATE.VERBATIM.INSIGHT.LOADMORE
                    .replace('$FN$', 'onclick="InsightManager.loadMorePredictedVerbatims(this)"')
            )
        container.parent().dimmer('hide')

        // build insight semantics list
        self.buildFeatureList(res.insight)

        // hide insight semantics
        self.containerSIndex.addClass('hidden')
    })

    this.addConfirmRelevanceAddon = function () {
        $.each(self.containerVerbatimPredicted.find('.verbatim'), function () {
            var $this = $(this)
            var data = $this.data()
            if (_.isUndefined(data.relevanceAddon)) {
                data.relevanceAddon = true

                $this.find('.verbatim-options').append(
                    TEMPLATE.VERBATIM.INSIGHT.RELEVANCE.CONTAINER
                        .replace('$ID$', data.id)
                        .replace('$CID$', $this.attr('id'))
                )
            }
        })
    }

    this.onClickPredictionRelevant = function (o) {
        var $this = $(o).parent()
        var insightid = self.getSelectedInsight()
        var vid = $this.data().id
        var containerid = $this.data().containerid
        var topic = getTopic()
        $('#' + $this.data().containerid)
            .transition({
                animation: 'fly down',
                onComplete: function () {
                    $('#' + containerid).remove()
                    API.INSIGHTS.setRelevanceForPrediction(topic, insightid, vid, true).then(function (res) {
                        // get verbatim and prepend to tagged list
                        API.VERBATIMS.getVerbatimByID(vid).then(function (v) {
                            var appendVerbatimQueue = [
                                appendVerbatim(self.containerVerbatimTagged, v.verbatim, 0, v._id, v.options, v, "-tagged")
                            ]
                            appendToTaggedVerbatimList(appendVerbatimQueue, {c: 0}, $())

                            // appending loadmore to end of tagged container
                            self.containerVerbatimTagged.find('.container-loadmore').remove()
                            self.containerVerbatimTagged.append(
                                TEMPLATE.VERBATIM.INSIGHT.LOADMORE
                                    .replace('$FN$', 'onclick="InsightManager.loadMoreTaggedVerbatims(this)"')
                            )

                            // checking if need to recompile prediction modal
                            API.INSIGHTS.ifCompiled(insightid, topic).then(function (ifcompiled) {
                                if (!ifcompiled) {
                                    API.INSIGHTS.getInsight(insightid).then(function (insight) {
                                        self.buildPredictedVerbatims(insight)
                                    })
                                }
                            })
                        })
                    })
                }
            })
    }

    this.onClickPredictionNotRelevant = function (o) {
        var $this = $(o).parent()
        var insightid = self.getSelectedInsight()
        var vid = $this.data().id
        var containerid = $this.data().containerid

        // remove verbatim from list
        $('#' + containerid)
            .transition({
                animation: 'fly right',
                onComplete: function () {
                    $('#' + containerid).remove()
                    API.INSIGHTS.setRelevanceForPrediction(getTopic(), insightid, vid, false).then(function (res) {
                    })
                }
            })
    }

    this.loadMorePredictedVerbatims = function (o) {
        $(o).remove()
        var id = self.getSelectedInsight()
        API.INSIGHTS.getInsight(id).then(function (insight) {
            self.buildPredictedVerbatims(insight, true)
        })
    }

    this.loadMoreTaggedVerbatims = function (o) {
        $(o).remove()
        var id = self.getSelectedInsight()
        API.INSIGHTS.getInsight(id).then(function (insight) {
            self.buildTaggedVerbatims(insight, true)
        })
    }


    this.buildFeatureList = function (insight) {
        var topic = getTopic()
        self.containerFeatures.parent().dimmer('show')
        API.INSIGHTS.getModalFeatures(insight._id, topic).then(function (features) {
            self.containerFeatures.empty()
            features.forEach(function (feature) {
                var value
                if (feature.feature.type == "concepts") {
                    value = ConceptParser.parse(feature.feature.value)
                } else {
                    value = feature.feature.value
                }

                var color, type
                if (feature.feature.type == 'ner') {
                    type = feature.feature.nertype.toLowerCase()
                    color = COLORS.STATS.TYPES[type]
                } else {
                    type = feature.feature.type.toLowerCase()
                    color = COLORS.STATS.TYPES[type]
                }
                self.containerFeatures.append(
                    TEMPLATE.VERBATIM.INSIGHT.FEATURE.CONTAINER
                        .replace('$TYPE$', feature.feature.type.toLowerCase())
                        .replace('$VALUE$', value)
                        .replace('$WEIGHT$', feature.feature.weight.toFixed(3))
                        .replace('$COLOR$', color)
                        .replace('$DATA$', 'data-id="' + feature._id + '" data-insightid="' + insight._id + '"')
                        .replace('$FNREMOVE$', 'onclick="InsightManager.removeFeature(this)"')
                )
            })
            self.containerFeatures.parent().dimmer('hide')
        })
    }

    this.removeFeature = function (o) {
        var $this = $(o)
        var data = $this.parent().data()
        API.INSIGHTS.removeFeature(data.id, data.insightid).then(function (r) {
            if (r.s) {
                $this.parent().transition({
                    animation: 'fade out',
                    onComplete: function () {
                        $this.parent().remove()
                    }
                })
            }
        })
    }

    this.onClickReCompileModal = function () {
        self.triggerReCompileModal.addClass('loading')
        var insightid = self.getSelectedInsight()
        var topic = getTopic()
        API.INSIGHTS.reCompileModal(insightid, topic).then(function (r) {
            if (r.s) {
                API.INSIGHTS.getInsight(insightid).then(function (insight) {
                    self.buildPredictedVerbatims(insight)
                    self.triggerReCompileModal.removeClass('loading')
                })
            }
        })
    }

}
var InsightManager = new _InsightManager()