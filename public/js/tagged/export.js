function _Exporter() {
    var self = this
    this.modal = $('#modal-exporter')
    this.modalDownload = $('#modal-download')
    this.SETTINGS = {
        selected: {
            tags: [],
            insights: [],
            topics: [],
        }
    }

    this.socket = new SocketClient('tags')

    this.init = function () {
        self.modal.find('.tab-tag').find('.dropdown')
            .dropdown({
                onChange: function (values) {
                    if (values == "") {
                        self.SETTINGS.selected.tags = []
                    } else {
                        self.SETTINGS.selected.tags = values
                    }
                    self.previewTags()
                }
            })

        self.modal.find('.tab-insight').find('.dropdown')
            .dropdown({
                onChange: function (values) {
                    if (values == "") {
                        self.SETTINGS.selected.insights = []
                    } else {
                        self.SETTINGS.selected.insights = values
                    }
                    self.previewInsights()
                }
            })

        self.modal.find('.tab-topic').find('.dropdown')
            .dropdown({
                onChange: function (values) {
                    var currTopic = getTopic()
                    self.SETTINGS.selected.topics = [currTopic]
                    if (values != "") {
                        values.forEach(function (value) {
                            self.SETTINGS.selected.topics.push(value)
                        })
                    }
                    self.previewTopics()
                }
            })
    }

    this.showModal = function () {
        this.fillDropdowns(function () {
            self.modal.find('.error').removeClass('visible').addClass('hidden')
            self.modal.find('.error').find('.header').text("")
            self.modal.modal('show')
        })
    }

    this.fillDropdowns = function (next) {
        var topic = getTopic()
        // tags
        Promise.join(
            API.TAGS.getTags(topic),
            API.INSIGHTS.getInsightsForProject(topic),
            API.VERBATIMS.getTopics(),
            function (tags, insights, topics) {
                self.fillTagsDropdown(tags)
                self.fillInsightDropdown(insights)
                self.fillTopicsDropdown(topics)
                next()
            })
    }

    this.fillTagsDropdown = function (tags) {
        self.SETTINGS.tags = tags
        var container = self.modal.find('.tab-tag').find('select[name="tags"]')
        container.empty()
        tags.forEach(function (tag) {
            container.append(
                TEMPLATE.DASHBOARD.FORM.ITEM
                    .replace('$NAME$', tag)
                    .replace('$VALUE$', tag)
            )
        })

        self.modal.find('.tab-tag').find('.dropdown').dropdown('set exactly', [])
    }

    this.fillInsightDropdown = function (insights) {
        self.SETTINGS.insights = insights
        var container = self.modal.find('.tab-insight').find('select[name="insights"]')
        container.empty()
        insights.forEach(function (insight) {
            container.append(
                TEMPLATE.DASHBOARD.FORM.ITEM
                    .replace('$NAME$', insight.name)
                    .replace('$VALUE$', insight._id)
            )
        })

        self.modal.find('.tab-insight').find('.dropdown').dropdown('set exactly', [])
    }

    this.fillTopicsDropdown = function (topics) {
        self.SETTINGS.topics = topics.r
        self.SETTINGS.selected.topics = []
        var currTopic = getTopic()
        self.SETTINGS.selected.topics.push(currTopic)
        var container = self.modal.find('.tab-topic').find('select[name="topics"]')
        container.empty()
        topics.r.forEach(function (topic) {
            if (currTopic != topic.id) {
                container.append(
                    TEMPLATE.DASHBOARD.FORM.ITEM
                        .replace('$NAME$', topic.name)
                        .replace('$VALUE$', topic.id)
                )
            }
        })
        self.modal.find('.tab-topic').find('.dropdown').dropdown('set exactly', [])
        self.previewTopics()
    }

    this.selectAllTags = function () {
        self.SETTINGS.tags.forEach(function (tag) {
            var index = self.SETTINGS.selected.tags.indexOf(tag)
            if (index == -1) {
                self.modal.find('.tab-tag').find('.dropdown').dropdown('set selected', tag)
            }
        })
    }

    this.selectAllInsights = function () {
        self.SETTINGS.insights.forEach(function (insight) {
            var index = self.SETTINGS.selected.insights.indexOf(insight._id)
            if (index == -1) {
                self.modal.find('.tab-insight').find('.dropdown').dropdown('set selected', insight._id)
            }
        })
    }

    this.previewTags = function () {
        var container = self.modal.find('.list-tag')
        container.empty()
        self.SETTINGS.selected.tags.forEach(function (tag) {
            container.append(
                TEMPLATE.CADASHBOARD.WCA.WORD.PREVIEW
                    .replace('$BACKGROUND$', 'black')
                    .replace('$WORD$', tag)
            )
        })
    }

    this.previewInsights = function () {
        var container = self.modal.find('.list-insight')
        container.empty()
        self.SETTINGS.selected.insights.forEach(function (iid) {
            var insight = null
            self.SETTINGS.insights.forEach(function (pinsight) {
                if (pinsight._id == iid) {
                    insight = pinsight
                }
            })

            if (!_.isNull(insight)) {
                container.append(
                    TEMPLATE.CADASHBOARD.WCA.WORD.PREVIEW
                        .replace('$BACKGROUND$', 'black')
                        .replace('$WORD$', insight.name)
                )
            }
        })
    }

    this.previewTopics = function () {
        var container = self.modal.find('.list-topic')
        container.empty()
        self.SETTINGS.selected.topics.forEach(function (tid) {
            var topic = null
            self.SETTINGS.topics.forEach(function (ptopic) {
                if (ptopic.id == tid) {
                    topic = ptopic
                }
            })

            if (!_.isNull(topic)) {
                container.append(
                    TEMPLATE.CADASHBOARD.WCA.WORD.PREVIEW
                        .replace('$BACKGROUND$', 'black')
                        .replace('$WORD$', topic.name)
                )
            }
        })
    }

    this.showDownloadModal = function (closable) {
        if(_.isUndefined(closable))
            closable = true

        var container = self.modalDownload
        container.find('.pending').css('display', 'block')
        container.find('.done').css('display', 'none')
        container
            .modal('setting', 'closable', closable)
            .modal('show')

    }

    this.showSuccessMessageDownloadModal = function (r) {
        var container = self.modalDownload
        if (!_.isUndefined(r.route)) {
            container.modal('show')
            container.find('.container-download-href').attr('href', r.route)
            container.find('.pending').fadeOut("slow", function () {
                container.find('.done').fadeIn("slow")
            })
        }
    }

    this.downloadTagged = function () {
        var selected = self.SETTINGS.selected
        if (selected.tags.length > 0) {
            self.showDownloadModal()
            self.socket.emit(
                'export_tags_req',
                {topics: selected.topics, tags: selected.tags},
                function (res) {
                    if (!res.s) {
                        throw new Error("Failure at socket:export_tags_request:" + res.m)
                    }
                })
        }
    }

    this.downloadInsights = function () {
        var selected = self.SETTINGS.selected
        var ifDownload = true, message
        var data = self.modal.find('.tab-insight').find('.form').form('get values')
        var nbPredictedVerbatims = parseInt(data.nbPredictedVerbatims)

        if (selected.insights.length == 0) {
            ifDownload = false
            message = "You must select atleast one insight"
        }

        if(_.isNaN(nbPredictedVerbatims)) {
            ifDownload = false
            message = "Nb. Predicted Verbatims must be a number"
        } else if (nbPredictedVerbatims > 5000 || nbPredictedVerbatims == 0) {
            ifDownload = false
            message = "Nb. Predicted Verbatims must between 1-5000"
        }

        var errorContainer = self.modal.find('.tab-insight').find('.error')
        if(ifDownload) {
            errorContainer.addClass('hidden').removeClass('visible')
            errorContainer.find('.header').text("")

            self.showDownloadModal()
            self.socket.emit(
                'export_insights_req',
                {topics: selected.topics, insightids: selected.insights, nbPredicted: nbPredictedVerbatims},
                function (res) {
                    if (!res.s) {
                        throw new Error("Failure at socket:export_insights_request:" + res.m)
                    }
                })
        } else {
            errorContainer.removeClass('hidden').addClass('visible')
            errorContainer.find('.header').text(message)
        }
    }

    /*
     Socket Responses
     */


    self.socket.addListener('export_tags_res', function (res) {
        if (res.s) {
            self.showDownloadModal(false)
            self.showSuccessMessageDownloadModal(res)
        }
    })

    self.socket.addListener('export_insights_res', function (res) {
        if (res.s) {
            self.showDownloadModal(false)
            self.showSuccessMessageDownloadModal(res)
        }
    })

}
var Exporter = new _Exporter()