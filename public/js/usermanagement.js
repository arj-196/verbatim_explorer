$(document).ready(function () {
    $('#menu-right')
        .append(
            TEMPLATE.MENU.USERLESS
        )

    $('.dropdown').dropdown()
    $('.popup-trigger').popup()
})

function _UserTable() {
    var self = this
    this.modalUser = $('#modal-user')

    this.onClickEditUser = function (o) {
        var $this = $(o)
        var data = $this.parent().parent().data()
        API.ADMIN.getUserById(data.id).then(function (User) {
            self.modalUser.find('input[name="action"]').val("edituser")
            self.modalUser.find('input[name="username"]').val(User._id).prop('disabled', true)
            self.modalUser.find('input[name="firstname"]').val(User.firstname)
            self.modalUser.find('input[name="lastname"]').val(User.lastname)
            self.modalUser.find('input[name="email"]').val(User.email)
            self.modalUser.find('.field-password').addClass('hidden')

            self.modalUser.find('.error.message').addClass('hidden')
            self.modalUser.modal('show')
        })
    }

    this.onClickRemoveUser = function (o) {
        var $this = $(o)
        var data = $this.parent().parent().data()
        var r = confirm("Do you really want to delete " + data.id + " ?")
        if(r) {
            data.action = "removeuser"
            API.ADMIN.USER.submitForm(data).then(function (r) {
                if(r.s) {
                    location.reload()
                }
            })
        }
    }

    this.onClickNewUser = function () {
        self.modalUser.find('input[name="action"]').val("newuser")
        self.modalUser.find('input[name="username"]').val("").prop('disabled', false)
        self.modalUser.find('input[name="firstname"]').val("")
        self.modalUser.find('input[name="lastname"]').val("")
        self.modalUser.find('input[name="email"]').val("")
        self.modalUser.find('input[name="password"]').val("")
        self.modalUser.find('.field-password').removeClass('hidden')


        self.modalUser.find('.error.message').addClass('hidden')
        self.modalUser.modal('show')
    }

    this.submitFormUser = function () {
        var data = self.modalUser.find('.form').form('get values')
        var status = {s: true, m: ""}
        if(data.email.trim() == ""){
            status.s = false
            status.m = "Email cannot be empty"
        }
        if(data.username.trim() == ""){
            status.s = false
            status.m = "Username cannot be empty"
        }
        if(data.action == "newuser" && data.password.trim() == "") {
            status.s = false
            status.m = "Password Cannot be empty"

        }

        if(!status.s) {
            self.modalUser.find('.error.message').removeClass('hidden').find('.header').text(status.m)
        } else {
            API.ADMIN.USER.submitForm(data).then(function (r) {
                console.log("response from submit form user", r)
                if(!r.s) {
                    self.modalUser.find('.error.message').removeClass('hidden').find('.header').text(r.m)
                } else {
                    location.reload()
                }
            })
        }
    }
}

var UserTable = new _UserTable()