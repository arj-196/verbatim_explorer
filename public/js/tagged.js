$(document).ready(function () {
    $('#menu-nav-text')
        .append(
            TEMPLATE.TAGGED.OPTIONS.CONTAINER
        )
        .append(
            TEMPLATE.MENU.USERLESS
        )

    $('#menu-nav-text').find('.options-list')
        .append(
            TEMPLATE.TAGGED.OPTIONS.ITEM
                .replace('$ITEM$', 'Download')
                .replace('$FUNC$', 'onclick="Exporter.showModal()"')
        )

    $('.dropdown').dropdown()
    $('.popup-trigger').popup()
    $('.tabular.menu .item').tab();

    $('#trigger-modal-new-insight').click(openCreateInsightModal)
    $('#trigger-submit-new-insight').click(submitFormCreateInsight)
    $('#modal-new-insight').find('input[name="name"]').on('keydown', function (e) {
        if (e.which === 13 || e.keyCode === 13) { // if is enter
            submitFormCreateInsight()
        }
    })

    $('#container-insight-options').find('.button-remove').click(InsightManager.onClickRemoveInsight)

    //
    // Interation History
    //
    InteractionHistory.createVariable('t')
    InteractionHistory.setInitCallback('t', function (value) {
        selectTagForDataset(value)
    })

    //
    // Exporter
    //
    Exporter.init()

    Manager.initTagged().then(function () {
        setCurrentHeaderLocation('Tagged', 'tagged')

        $('#container-topic-dropdown')
            .addClass('cursor')
            .on('click', openChooseDataSetModal)

        setupDatasets()

        // input search
        $('#input-search-tag')
            .on('keypress', function (e) {
                if (e.which === 13) { // if is enter
                    searchForTag()
                }
            })


        InteractionHistory.init().then(function () {
            if (InteractionHistory.ifAutoStart() && InteractionHistory.ifExistsVariable('t')) {
                InteractionHistory.trigger('t')
            } else {
                openChooseDataSetModal()
                InteractionHistory.setAutoStart(true)
            }
        })

        // test
        // $('.tabular.menu').find('.item[data-tab="insight"]').click()
        //setTimeout(function () {
        //    selectTagForDataset({topic: "Energie", name: "TEST"})
        //    $('.tabular.menu').find('.item[data-tab="insight"]').click()
        //}, 500)
        // setTimeout(function () {
        //     Exporter.showModal()
        // }, 1000)

    })
})
var InteractionHistory = new _InteractionHistory()

function openChooseDataSetModal() {
    var container = $('#container-dataset')
    container.find('.error.message').removeClass('visible')
    container.modal('setting', 'closable', false)
        .modal('show')
}

function closeChooseDataSetModal() {
    $('#container-dataset').modal('hide')
}

function setupDatasets() {
    // initialize topic menu
    new TopicMenu($('#container-dataset'), selectTagForDataset, 'tagged').init()
}

var getTopic = function () {
    var c = $('#menu-nav-text-2')
    if (c.length > 0)
        return c.data().topic
    else
        return null
}

var openCreateInsightModal = function () {
    var container = $('#modal-new-insight')
    container.find('input[name="action"]').val("create")
    container.modal('show')
    container.find('.error').removeClass('visible').addClass('hidden')
}

var openEditInsightModal = function (o) {
    var $this = $(o).parent().parent().parent()
    var data = $this.data()
    API.INSIGHTS.getInsight(data.id).then(function (insight) {
        var container = $('#modal-new-insight')
        container.find('input[name="name"]').val(insight.name)
        container.find('input[name="insightid"]').val(insight._id)
        container.find('input[name="action"]').val("edit")
        container.modal('show')
        container.find('.error').removeClass('visible').addClass('hidden')
    })
}

var submitFormCreateInsight = function () {
    var container = $('#modal-new-insight')
    var form = container.find('.ui.form')
    var data = form.form('get values')
    if (data.name.trim() == "") {
        container.find('.error')
            .removeClass('hidden')
            .addClass('visible')
            .find('.header')
            .text('Name must be defined!')
    } else {
        data.topic = getTopic()
        if (data.action == "create") {
            InsightManager.createInsight(data, function (res) {
                if (!res.s) {
                    container.find('.error')
                        .removeClass('hidden')
                        .addClass('visible')
                        .find('.header')
                        .text(res.m)
                } else {
                    InsightManager.searchInsights.search('set value', data.name)
                    InsightManager.searchInsights.search('query')
                    container.modal('hide')
                }
            })
        }

        if (data.action == "edit") {
            InsightManager.editInsight(data, function (res) {
                if (!res.s) {
                    container.find('.error')
                        .removeClass('hidden')
                        .addClass('visible')
                        .find('.header')
                        .text(res.m)
                } else {
                    InsightManager.searchInsights.search('set value', data.name)
                    InsightManager.searchInsights.search('query')
                    container.modal('hide')
                }
            })
        }
    }
}
var TOPICITERATION = 0
var config = {
    IFLOADMORE: true,
    ifFilterTagged: false,
    PREVIOUS_SEARCH_TEXT: null,
    IFCONCEPTSEARCH: false,
    SEARCH_HISTORY_MAX_LENGTH: 9,
    SEARCHMODE: null,
    CONCEPTSEARCH: {
        CONCEPTTOLANGMAP: {}
    },
    IFDEFAULTLOAD: true,
    IFWORDCLOUDSEARCH: false,
    WORDCLOUD: {
        PREVIOUSMODE: null,
        PREVIOUSWORD: null
    },
    VERBATIMLIST: [],
    MV: {
        ACTIONS: {
            SIMILARITY: false
        },
        SIMILARITEMLIST: []
    },
    EMOTION: {},
    STATS: false
}
