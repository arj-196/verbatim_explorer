// main
$(document).ready(function () {
    Manager.initWordclouds().then(function () {
        initTopicDirectory()
        $('#menu-nav-location').text('Wordclouds')
    });
});

var tmpTopicDirectory =
    '<div class="item">' +
    '<i class="folder icon"></i>' +
    '<div class="content">' +
    '<div class="header cursor" data-show="off" onclick="toggleDisplayDirectory(this)">$DIRNAME$</div>' +
    '<div class="list ar-directory" data-dir-name="$DIR$" style="display: none;">' +
    '</div>' +
    '</div>' +
    '</div>'

var tmpTopicDirectoryItem =
    '<div class="item">' +
    '<i class="caret right icon"></i>' +
    '<div class="content">' +
    '<div class="ar-directory-item cursor" data-href="$HREF$">' +
    '<div class="description">$ITEM$</div>' +
    '</div>' +
    '</div>' +
    '</div>'


var initTopicDirectory = function () {
    d3.json(PATH.WORDCLOUDS.ROOT + PATH.WORDCLOUDS.config, function (config) {
        var container = $('#container-topics-list')
        Object.keys(config['topics']).forEach(function (dir) {
            container.append(
                tmpTopicDirectory
                    .replace('$DIR$', dir)
                    .replace('$DIRNAME$', dir.toUpperCase())
            )

            config['topics'][dir].forEach(function (o) {
                var subContainer = container.find('.ar-directory[data-dir-name="' + dir + '"]')
                subContainer.append(
                    tmpTopicDirectoryItem
                        .replace('$ITEM$', o)
                        .replace('$HREF$', dir + "_" + o)
                )
            });
        });

        $.each($('.ar-directory-item'), function(){
            var self = $(this)
            self.click(onClickTopic)
        });

        $('')
    })
};


var onClickTopic = function () {
    var self = $(this)
    createWordCloud(self.data().href + "_wordcloud.json")
    $('#menu-nav-text-2').html('<strong>' + self.text().toUpperCase() + '</strong>')
}

var createWordCloud = function (file) {
    $('#container-wordcloud-init-box').remove()
    var container = $('#container-wordcloud')
    container.find('svg').remove()

    var width = container.width() - 20, height = container.height()
    var fill = d3.scale.category20()
    var scale = d3.scale.linear().range([10, 120])

    // read data
    d3.json(PATH.WORDCLOUDS.ROOT + file, function (data) {
        console.log(data)

        var list = []
        // format data
        Object.keys(data).forEach(function (key) {
            list.push({text: key, size: parseInt(data[key])})
        });

        var words = list
            .filter(function (d) {
                return +d.size > 0
            })
            .sort(function (a, b) {
                return d3.descending(a.size, b.size)
            })
            .slice(0, 1000)

        scale
            .domain([
                d3.min(words, function (d) {
                    return d.size
                }),
                d3.max(words, function (d) {
                    return d.size
                })
            ])

        d3.layout.cloud().size([width, height])
            .words(words)
            .padding(0)
            //.rotate(function(){return ~~(Math.random() *2) * 90})
            .font("Impact")
            .fontSize(function (d) {
                return scale(d.size)
            })
            .on('end', drawCloud)
            .start()

    })

    var drawCloud = function (words) {
        d3.select('#container-wordcloud').append('svg')
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + (width / 2) + ", " + (height / 2) + ")")
            .selectAll("text")
            .data(words)
            .enter().append("text")
            .style("font-size", function (d) {
                return d.size + "px";
            })
            .style("font-family", "Impact")
            .style("fill", function (d, i) {
                return fill(i)
            })
            .attr("text-anchor", "middle")
            .attr("transform", function (d) {
                return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
            })
            .text(function (d) {
                return d.text;
            })
            //.style('cursor', 'pointer')
            //.on('click', onClickWord)
    }
}

var toggleDisplayDirectory = function (o) {
    var self = $(o)
    console.log(self, self.data())
    if(self.data().show == "off"){
        self.parent().find('.ar-directory').slideDown({duration: 300})
        self.data('show', 'on')
    }
    else{
        self.parent().find('.ar-directory').slideUp({duration: 300})
        self.data('show', 'off')
    }

}