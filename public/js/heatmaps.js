// main
$(document).ready(function () {
    $('#menu-nav-text')
        .append(
            TEMPLATE.MENU.USERLESS
        )
    $('.dropdown').dropdown()

    Manager.initWordclouds().then(function () {
        initTopicDirectory()
        $('#menu-nav-location').text('Heatmaps')
    });
});


var initTopicDirectory = function () {
    API.DASHBOARD.getUser().then(function (user) {
        API.DASHBOARD.getUserTopics(user._id, "heatmap")
            .then(function (r) {
                var topics = r.topics
                var container = $('#container-topics-list')
                _.keys(topics).forEach(function (dir) {
                    if (topics[dir].length > 0){
                        container.append(
                            TEMPLATE.HEATMAP.DIRECTORYNAME
                                .replace('$DIR$', "")
                        )

                        topics[dir].forEach(function (topic) {
                            container.append(
                                TEMPLATE.HEATMAP.DIRECTORY
                                    .replace('$DIR$', topic.topic)
                                    .replace('$DIRNAME$', topic.name)
                                    .replace('$NAME$', topic.name)
                            )
                            var subContainer = container.find('.ar-directory[data-dir-name="' + topic.topic + '"]')
                            subContainer.append(
                                TEMPLATE.HEATMAP.SUBDIRECTORY
                                    .replace('$ITEM$', 'global')
                                    .replace('$HREF$', topic.topic + "_" + 'global')
                                    .replace('$DATA$', 'data-action="global" data-name="'+ topic.name +'" data-topic="' + topic.topic + '"')
                            )
                        })
                    }
                })
            })
    })
};

var onClickItem = function (o) {
    var self = $(o)
    var data = self.data()
    $('#menu-nav-text-3').html(
        "<span style='font-weight:bold;margin-left:20px;'>" + data.name + "</span>"
    )
    if (data.action == "global") {
        API.HEATMAP.getGlobal(data.topic)
            .then(function (r) {
                formatDataForMap(r.r)
            })
    }
}

var formatDataForMap = function (dataList) {
    var formatedData = {
        max: 200,
        data: [],
    }
    dataList.forEach(function (d) {
        var o = {
            lat: parseFloat(d.lat),
            lng: parseFloat(d.lon),
            count: parseInt(d.value),
            city: d.city
        }
        formatedData.data.push(o)
    });

    var container = $('#container-heatmap')
    if(dataList.length > 0){
        container.dimmer('hide')
        $('#container-heatmap-init-box').remove()
        createHeatMap(formatedData)
    } else {
        container.dimmer('show')
    }
}

var map = null;
var createHeatMap = function (data) {

    var baseLayer = L.tileLayer(
        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '...',
            maxZoom: 30
        }
    )

    var cfg = {
        // radius should be small ONLY if scaleRadius is true (or small radius is intended)
        // if scaleRadius is false it will be the constant radius used in pixels
        "radius": 1,
        "maxOpacity": .8,
        // scales the radius based on map zoom
        "scaleRadius": true,
        // if set to false the heatmap uses the global maximum for colorization
        // if activated: uses the data maximum within the current map boundaries
        //   (there will always be a red spot with useLocalExtremas true)
        "useLocalExtrema": true,
        // which field name in your data represents the latitude - default "lat"
        latField: 'lat',
        // which field name in your data represents the longitude - default "lng"
        lngField: 'lng',
        // which field name in your data represents the data value - default "value"
        valueField: 'count'
    }
    var heatmapLayer = new HeatmapOverlay(cfg);

    deleteMap()

    map = new L.Map('container-heatmap', {

        // TODO remove default start position
        center: new L.LatLng(47.074926, 2.419202),
        zoom: 6,
        layers: [baseLayer, heatmapLayer]
    });
    // set layer
    heatmapLayer.setData(data);

}

var deleteMap = function () {
    if (map != null) {
        map.remove()
    }
}

var toggleDisplayDirectory = function (o) {
    var self = $(o)
    if (self.data().show == "off") {
        self.parent().find('.ar-directory').slideDown({duration: 300})
        self.data('show', 'on')
    }
    else {
        self.parent().find('.ar-directory').slideUp({duration: 300})
        self.data('show', 'off')
    }

}














