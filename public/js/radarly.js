$(document).ready(function () {
    $('.dropdown').dropdown()



    getRadarlyProjects()

    $('select[name="radarly_project"]').change(function() {
        getRadarlyDashboards(
            $('select[name="radarly_project"] option:selected').text()
        )
    })

    $('select[name="radarly_dashboard"]').change(function() {
        getRadarlyFocuses(
            $('select[name="radarly_project"] option:selected').text(),
            $('select[name="radarly_dashboard"] option:selected').text()
        )
    })
})

function getRadarlyProjects() {
    var container
    API.DASHBOARD.getAllRadarlyFeatures().then(function(r) {
        container = $('select[name="radarly_project"]')
        r.forEach(function(o) {
            container.append(
                TEMPLATE.DASHBOARD.FORM.ITEM
                    .replace('$NAME$', o)
                    .replace('$VALUE$', o)
            )
        })
    })
}

function getRadarlyDashboards(project) {
    var container
    API.DASHBOARD.getAllRadarlyDashboard(project).then(function(r) {
        container = $('select[name="radarly_dashboard"]')
        r.forEach(function(o) {
            container.append(
                TEMPLATE.DASHBOARD.FORM.ITEM
                    .replace('$NAME$', o)
                    .replace('$VALUE$', o)
            )
        })
    })
}

function getRadarlyFocuses(project, dashboard) {
    var container
    API.DASHBOARD.getAllRadarlyFocus(project, dashboard).then(function(r) {
        container = $('select[name="radarly_focus"]')
        r.forEach(function(o) {
            container.append(
                TEMPLATE.DASHBOARD.FORM.ITEM
                    .replace('$NAME$', o)
                    .replace('$VALUE$', o)
            )
        })
    })
}
