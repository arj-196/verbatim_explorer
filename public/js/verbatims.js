// main
$(document).ready(function () {
    $('#menu-nav-text-4')
        .append(
            TEMPLATE.MENU.USERLESS
        )

    // popup elements
    $('.popup-default-elements').popup()

    // title
    setCurrentHeaderLocation('Verbatims', 'verbatim')

    // accordian
    $('.accordion').accordion()  // TODO check if required ...

    // wordcloud tab
    $('.menu .item').tab()

    // append search input to header
    $('#menu-nav-text-3')
        .empty()
        .append(TEMPLATE.VERBATIM.SEARCH)

    // other dropdowns
    $('.dropdown').dropdown()

    // progress
    $('.progress').progress()

    // search mode dropdown box
    $('#input-search-modes')
        .dropdown({
            onChange: function (text, value, $selectedItem) {
                changeSearchMode($selectedItem.data().value)
            }
        })
        .dropdown('set selected', 'text')

    // search : show all : btn-show-all
    $('#btn-stats')
        .click(toggleStatsContainer)

    // search : show tagged : btn-show-tagged
    $('#btn-show-tagged')
        .click(showAllFavorite)

    // input search
    var inputSearch = $('#input-search')
    var inputSearchTrigger = $('#input-search-trigger')

    // start typing and on enter
    inputSearch
        .on('keydown', function (e) {
            if (e.which === 13 || e.keyCode === 13) { // if is enter
                searchForText(true)
                dropdownListEngine.close()
            } else if (e.keyCode == 27) { // escape key maps to keycode `27`
                dropdownListEngine.close()
            }
            else {
                dropdownListEngine.open()
            }
        })
    inputSearchTrigger.on('click', function () {
        searchForText(true)
        dropdownListEngine.close()
    })

    // when user finishes typing
    var typingTimer;                //timer identifier
    var doneTypingInterval = 300;  //time in ms, 5 second for example

    //on keyup, start the countdown
    inputSearch.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    //on keydown, clear the countdown
    inputSearch.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping() {
        dropdownListEngine.fillList()
    }

    // init modals
    $('#container-tagged-verbatims')
        .modal({
            onHide: closeAllFavorite
        })

    // init modal dataset
    $('#container-topic-dropdown')
        .click(openChooseDataSetModal)

    $('#modal-search-aggregate')
        .modal({
            onShow: showAggregateSearchModal
        })

    //
    // Interation History
    //
    InteractionHistory.createVariable('t')
    InteractionHistory.setInitCallback('t', function (value) {
        onclickTopic(value)
    })

    //
    // Initialize Topic Menu
    //
    new TopicMenu($('#container-dataset'), onclickTopic).init()

    Manager.initVerbatims().then(function () {
        InteractionHistory.init().then(function () {
            if(InteractionHistory.ifAutoStart() && InteractionHistory.ifExistsVariable('t')) {
                setTimeout(function () {
                    InteractionHistory.trigger('t')
                }, 1000)
            } else {
                openChooseDataSetModal()
                InteractionHistory.setAutoStart(true)
            }
        })
    })

})

//
// Initialize Verbatim Cleaner
//
var VerbatimCleaner = new _VerbatimCleaner('#container-clean-verbatims')


//
// Menu Text
//
var updateMenuText = function (count) {
    var topic = getTopic()
    API.VERBATIMS.getTopicCount(topic)
        .then(function (res) {
            var container = $('#menu-nav-text')
            container.html(
                TEMPLATE.VERBATIM.HEADER.COUNT
                    .replace('$COUNT$', count)
                    .replace('$GLOBALCOUNT$', res.c)
            )

            $('#example1').progress()
        })
}

// environment configurations
var TOPICITERATION = 0
var config = {
    IFLOADMORE: true,
    ifFilterTagged: false,
    PREVIOUS_SEARCH_TEXT: null,
    IFCONCEPTSEARCH: false,
    SEARCH_HISTORY_MAX_LENGTH: 9,
    SEARCHMODE: null,
    CONCEPTSEARCH: {
        CONCEPTTOLANGMAP: {}
    },
    IFDEFAULTLOAD: true,
    IFWORDCLOUDSEARCH: false,
    WORDCLOUD: {
        PREVIOUSMODE: null,
        PREVIOUSWORD: null
    },
    VERBATIMLIST: [],
    MV: {
        ACTIONS: {
            SIMILARITY: false
        },
        SIMILARITEMLIST: []
    },
    EMOTION: {
    },
    STATS: false
}



