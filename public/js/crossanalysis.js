$(document).ready(function () {
    $('#menu-nav-text').html(
        TEMPLATE.MENU.USERLESS
    )

    $('.dropdown:not(.ignore-dropdown)').dropdown()
    $('#container-topic-dropdown').css('display', 'none')
    Layout.initCrossAnalysis(1).then(function (err) {
        CADashboard.init()
        openChooseDataSetModal()

        // // test
        // var topic
        // topic = {topic: "Innovation", name: "Innovation"}
        // CADashboard.addTopic(topic)
        // topic = { topic: "Energie", name: "Energie"}
        // CADashboard.addTopic(topic)
        // topic = { topic: "Test Medium 1", name: "Test medium 1"}
        // CADashboard.addTopic(topic)
        //
        // InsightCAEngine.showModal()
        // setTimeout(function () {
        //     $('#modal-wca').find('textarea[name="words"]').val('google,microsoft')
        //     $('#modal-wca').find('.button.green').click()
        //     InsightCAEngine.SETTINGS.INSIGHTS = ["mcdonaldnucleariniran", "mcdonaldaroundcanada2"]
        //     InsightCAEngine.setInsights()
        // }, 1000)
    })
})

function openChooseDataSetModal() {
    var container = $('#container-dataset')
    container.find('.error.message').removeClass('visible')
    container.modal('setting', 'closable', false)
        .modal('show')
}

function closeChooseDataSetModal() {
    $('#container-dataset').modal('hide')
}

var CONFIG = {
    OPanel: $('#container-options'),
    SPanel: $('#container-stats'),
    VPanel: $('#container-verbatims'),
}

// Initiate CADashboard
var CADashboard = new _CADashbord()

// Setting up Statistics Engines
CADashboard.StatEngines.push(InsightCAEngine)
// Setting up Statistics Engines
CADashboard.StatEngines.push(WCAEngine)
