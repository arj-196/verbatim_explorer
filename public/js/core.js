function _Dashboard() {

    this.Engines = []

    this.turnOff = function () {
        this.Engines.forEach(function (engine) {
            engine.turnOff()
        })
    }

    this.turnOn = function (name) {
        this.Engines.forEach(function (engine) {
            if(engine.name == name){
                engine.turnOn()
            } else {
                engine.turnOff()
            }
        })
    }

    this.search = function (name, topic, commandList, ifLoadMore) {
        this.Engines.forEach(function (engine) {
            if(engine.name == name){
                engine.search(topic, commandList, ifLoadMore)
            }
        })
    }

}

function Engine(name) {
    this.name = name
}

var Dashboard = new _Dashboard()


