$(document).ready(function () {
    $('#menu-nav-text-3')
        .append(
            TEMPLATE.MENU.USERLESS
        )
    $('.dropdown').dropdown()

    Manager.initSentiment().then(function () {
        setupDatasetsModal()
        $('#menu-nav-location').text('Sentiment Analysis')

        $('#container-topic-dropdown')
            .addClass('cursor')
            .click(openDatasetModal)

        openDatasetModal()
        // test
        //setTimeout(function () {
        //    onclickTopic({topic: "a_brand_purpose_small", name: "TEST"})
        //}, 500)
    })
})

// test data
var data = {
    topic: 'bp',
    name: 'neutral',
    type: 'NU',
    color: '#8F918F'
}

function setupDatasetsModal() {
    // initialize topic menu
    new TopicMenu($('#container-dataset'), onclickTopic).init()
}

function openDatasetModal() {
    var container = $('#container-dataset')
    container.find('.error.message').removeClass('visible')
    container.modal('setting', 'closable', false)
        .modal('show')
}

function closeDatasetModal() {
    $('#container-dataset')
        .modal('hide')
}

function onclickTopic(t) {
    var topic = t.topic
    var name = t.name

    var activeTopics = []
    $.each($('.ar-dataset-header'), function () {
        var self = $(this)
        activeTopics.push(self.text().toLowerCase())
    })

    if (activeTopics.indexOf(topic.toLowerCase()) == -1) {
        API.SENTIMENT.getSentimentStatistics(topic)
            .then(function (r) {
                var container = appendDatasetContainer(topic, name)
                fillInCountsInDatasetContainer(container, r.counts)
                setOverallSentiment(container, r.max)
                createBarPlot("#" + container.find('.ar-barplot').attr('id'), r.rows)
            })

        // finally close dataset modal
        closeDatasetModal()

    }
}

function removeDataset(o) {
    var self = $(o)
    self.parent().parent().parent().parent().remove()
}

function appendDatasetContainer(topic, name, r) {

    var datasetID = 'dataset-' + topic
    $('#container-dashboard')
        .append(
            TEMPLATE.SENTIMENT.CONTAINER
                .replace('$TOPIC$', topic)
                .replace('$NAMEDATASET$', name)
                .replace('$CONTAINERID$', datasetID)
                .replace('$BARPLOTID$', datasetID + "-barplot")
        )

    return $('#' + datasetID)
}

function fillInCountsInDatasetContainer(container, c) {
    container.find('.text-total-comments').text(c.T) // total count
    container.find('.text-total-comments-very-positive').text(c.VP) // total very positive count
    container.find('.text-total-comments-positive').text(c.P) // total positive count
    container.find('.text-total-comments-neutral').text(c.NU) // total neutral count
    container.find('.text-total-comments-negative').text(c.N) // total negative count
    container.find('.text-total-comments-very-negative').text(c.VN) // total very negative count
}

function setOverallSentiment(container, max) {
    var COLORMAP = {
        VP: COLORS.SENTIMENT.positive[COLORS.SENTIMENT.positive.length],
        P: COLORS.SENTIMENT.positive[2],
        NU: COLORS.SENTIMENT.neutral,
        N: COLORS.SENTIMENT.negative[2],
        VN: COLORS.SENTIMENT.negative[COLORS.SENTIMENT.negative.length]
    }

    var ICONMAP = {
        VP: "smile",
        P: "smile",
        NU: "meh",
        N: "frown",
        VN: "frown"
    }
    container.find('.ar-overall-sentiment-icon')
        .addClass(ICONMAP[max.type])
        .css('color', COLORMAP[max.type])
}

function createBarPlot(_id, data) {
    var container = $(_id)

    var margin = {top: 20, right: 0, bottom: 30, left: 40},
        width = container.width() - margin.left - margin.right,
        height = container.height() - margin.top - margin.bottom
    var x = d3.scale.ordinal().rangeRoundBands([0, width], .1)
    var y = d3.scale.linear().range([height, 0])
    var xAxis = d3.svg.axis().scale(x).orient("bottom")
    var yAxis = d3.svg.axis().scale(y).orient("left").ticks(5, "%")

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function (d) {
            return "<strong style='font-family:roboto-black'>Frequency:</strong> <span style='color:red'>" + (d.frequency * 100).toFixed(0) + " % </span>"
        })

    var svg = d3.select(_id).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    svg.call(tip)


    x.domain(data.map(function (d) {
        return d.name
    }))
    y.domain([0, d3.max(data, function (d) {
        return d.frequency
    })])

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Frequency")

    svg.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function (d) {
            return x(d.name)
        })
        .attr("width", x.rangeBand())
        .attr("y", function (d) {
            return y(d.frequency)
        })
        .attr("height", function (d) {
            return height - y(d.frequency)
        })
        .style("fill", function (d) {
            return d.color
        })
        .on('mouseover', function (d) {
            tip.show(d)
        })
        .on('click', function (d) {
            var containerDataset = $('#dataset-' + d.topic)
            var self = $(this)
            $.each(containerDataset.find('.bar'), function () {
                if(self.is($(this)))
                    $(this).css('opacity', 1)
                else
                    $(this).css('opacity', 0.5)
            })
            getVerbatimsByPolarity(d)
        })
        .on('mouseout', tip.hide)

}

var HEIGHT = {
    DATASET: {
        DEFAULT: 300,
        LOADVERBATIM: 800
    }
}

var CACHE = {}

function getVerbatimsByPolarity(d) {
    var datasetContainer = $('#dataset-' + d.topic)
    appendVerbatimListToDatasetContainer(datasetContainer, function () {

        if (_.isUndefined(CACHE[d.topic]) || CACHE[d.topic].POLARITY != d.type) {
            CACHE[d.topic] = {
                ITERATION: 0,
                POLARITY: d.type
            }
            datasetContainer.find('.progress-loader').dimmer('show')
            datasetContainer.find('.verbatim-list').empty()
            initialFetchVerbatimsForTopicAndPolarity(datasetContainer, d)
        }
    })
}

function appendVerbatimListToDatasetContainer(datasetContainer, next) {
    if (datasetContainer.find('.verbatim-list-container').length == 0)
        datasetContainer.animate({'height': HEIGHT.DATASET.LOADVERBATIM + 'px'}, 400,
            function () {
                datasetContainer.append(TEMPLATE.SENTIMENT.VERBATIMLIST)
                next()
            })
    else
        next()
}

function initialFetchVerbatimsForTopicAndPolarity(datasetContainer, d) {
    datasetContainer.find('.sentiment').text(d.name.toUpperCase())
        .css('color', d.color)
    var verbatimList = datasetContainer.find('.verbatim-list')
    API.SENTIMENT.getVerbatimsByTopicAndPolarity(d.topic, d.type, 0)
        .then(function (r) {
            if (r.r.length > 0) {
                appendToDataSetVerbatimList(r, verbatimList)
            } else {
                verbatimList.append(
                    TEMPLATE.VERBATIM.NOFOUNDVERBATIM
                        .replace('$TEXT$', "NO VERBATIMS FOUND")
                )
            }
            setTimeout(function () {
                datasetContainer.find('.progress-loader').dimmer('hide')
            }, 500)
        })
}

function appendToDataSetVerbatimList(r, verbatimList) {
    var verbatimsToBeAdded = []
    r.r.forEach(function (d) {
        verbatimsToBeAdded.push(appendVerbatim(verbatimList, d.verbatim, 0, d._id, d.options, d))
    })
    appendToVerbatimList(verbatimsToBeAdded, {c: r.c})
}

function closeVerbatimList(o) {
    var self = $(o)
    var datasetContainer = self.parent().parent().parent().parent()
    CACHE[datasetContainer.data().topic] = undefined
    self.parent().parent().parent().remove()
    datasetContainer.animate({'height': HEIGHT.DATASET.DEFAULT + 'px'})
    datasetContainer.find('.bar').css('opacity', 1)

}

function loadMoreVerbatims(o) {
    var self = $(o)
    var datasetContainer = self.parent().parent().parent().parent()
    datasetContainer.find('.progress-loader').dimmer('show')

    var topic = datasetContainer.data().topic
    CACHE[topic].ITERATION++
    API.SENTIMENT.getVerbatimsByTopicAndPolarity(topic, CACHE[topic].POLARITY, CACHE[topic].ITERATION)
        .then(function (r) {
            if (r.r.length > 0)
                appendToDataSetVerbatimList(r, datasetContainer.find('.verbatim-list'))
            setTimeout(function () {
                datasetContainer.find('.progress-loader').dimmer('hide')
            }, 500)
        })
}

function updateMenuText() {

}