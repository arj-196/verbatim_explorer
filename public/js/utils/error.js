window.onerror = function (msg, url, lineNo, columnNo, error) {
    var data = {
        url: url,
        m: msg,
        stack: error.stack
    }
    API.LOG.ERROR(data)
    return false;
}

Promise.onPossiblyUnhandledRejection(function(error){
    throw error
});