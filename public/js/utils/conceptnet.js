var ConceptParser = {

    parse: function (concept) {
        var re = new RegExp("\/c\/.{2,3}\/(.*?)\/")
        var r = re.exec(concept + "/")
        if(r && r.length > 1){
            return r[1].replace(/_/g, ' ')
        } else {
            return null;
        }
    },

    getLang: function (concept) {
        var re = new RegExp("\/c\/(.{2,3})\/")
        var r = re.exec(concept + "/")
        if(r && r.length > 1){
            return r[1].replace(/_/g, ' ')
        } else {
            return null;
        }
    }

}