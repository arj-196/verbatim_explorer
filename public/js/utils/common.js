var COLOR = [
    "blue",
    "green",
    "orange",
    "red",
    "voilet"
]

var COLORITEM = [
    "blue",
    "orange",
    "yellow",
    "green",
    "teal",
    "red",
    "violet",
    "purple",
    "brown",
    "pink",
]

var COLORS = {
    WORDCLOUD: {
        LABEL: {
            NONE: "grey",
            NORMAL: "teal",
            HIGH: "orange"
        },
    },

    SENTIMENT: {
        positive: ["#7AA184", "#7AA184", "#5C996B", "#5C996B", "#3CAA57",
            "#3CAA57", "#1AC645", "#1AC645", "#00E038", "#00E038"],
        neutral: "#8F918F",
        negative: ["#F79BA7", "#F79BA7", "#EE6A7C", "#EE6A7C", "#EA374F",
            "#EA374F", "#C91029", "#C91029", "#A50016", "#A50016"]
    },

    CONCEPT: {
        'STRONG': 'green ',
        'NORMAL': 'olive ',
        'WEAK': ''
    },

    SIMILARITY: {
        keys: {
            keywords: "orange",
            nouns: "teal",
            adjectives: "brown",
            verbs: "pink",
            terms: "olive"
        }
    },

    EMOTION: {
        positive: "olive",
        negative: "orange",
        angry: "red"

        , "Tranquillité": "olive"

        , "Joie": "green"
        , "Surprise": "purple"

        , "Tristesse": "orange"
        , "Peur": "orange"

        , "Colère": "red"
        , "Terreur": "red"
        , "Dégoût": "red"
        , "Fureur": "red"

        , "Coupure avec ses émotions": "grey"
    },

    EMOTIONREPLACE: {
        positive: "#A9C104",
        negative: "#F2711C",

        // fr
        "tranquillité": "#9DB402",
        "joie": "#21BA45",
        "surprise": "#A333C8",
        "tristesse": "#F2711C",
        "peur": "#F2711C",

        "colère": "#DB2828",
        "terreur": "#DB2828",
        "dégoût": "#DB2828",
        "fureur": "#DB2828",
        "coupure avec ses émotions": "#807D7D",


        // en
        'open': "#1D6286",
        'happy': "#21BA45",
        'alive': "#BA721A",
        'love': "#5EC50F",
        'interested': "#2849CC",
        'positive': "#A9C104",
        'angry': "#DB2828",
        'depressed': "#8E283C",
        'confused': "#593139",
        'helpless': "#712481",
        'indifferent': "#463649",
        'afraid': "#F2711C",
        'hurt': "#DB2828",
        'sad': "#B27E4A",

        // de
        'glücklich': '#21BA45',
        'zufrieden': '#BA721A',
        'behaglich': '#1D6286',
        'überraschen': '#A333C8',
        'unbequem': '#593139',
        'traurig': '#B27E4A',
        'ekel': '#DB2828',
        'wut': '#DB2828',
        'wütend': '#DB2828',
        'panik': '#F2711C',

        // , "Coupure avec ses émotions": "grey"
    },

    STATS: {
        TYPES: {
            keywords: "red",
            terms: "teal",
            concepts: "green",
            organization: "brown",
            location: "violet",
            person: "purple",
            hashtags: "blue",
            mentions: "pink",
            domains: "orange",
        }
    },

    PROGRESS: {
        LOW: "grey",
        MEDIUM: "orange",
        HIGH: "olive",
        VERYHIGH: "green",
    },
}

var COLORCHARTS = [
    "#A9C104",
    "#DB2828",
    "#21BA45",
    "#A333C8",
    "#F2711C",
    "#D7DF01",
    "#DB2828",
    "#293C0A",
    "#2E64FE",
    "#A4A4A4",
]

var ICONS = {

    NER: {
        ORGANIZATION: 'world',
        PERSON: 'users',
        LOCATION: 'marker'
    },

    GENDER: {
        MALE: 'male large',
        FEMALE: 'female large',
        UNKNOWN: 'help circle large',
    },

    BRANDPURPOSE: 'announcement large',
}

var MAXSEARCHLENGTH = 15

function getColor(i, j) {
    if (_.isUndefined(j)) {
        if (i < COLOR.length)
            return COLOR[i]
        else
            return COLOR[0]
    } else {
        var l = COLORCHARTS.length
        if (i <= l)
            return COLORCHARTS[i]
        else {
            return COLORCHARTS[i % l]
        }
    }
}

function getColorForItem(i) {
    return COLORITEM[i % COLORITEM.length]
}

function extractDomain(url) {
    var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
    }
    else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':');

    if (domain.length == 1) {
        return domain[0]
    } else if (domain.length > 1) {
        return domain[0] + ":" + domain[1]
    } else {
        return null;
    }
}

function popup(text) {
    // create the notification
    var notification = new NotificationFx({
        message: '<p class="ar-popup">' + text + '</p>',
        wrapper: document.getElementById('main-body'),
        layout: 'growl',
        effect: 'slide',
        ttl: 2000,
        type: 'notice' // notice, warning or error
    });

    notification.show();
}

function formatFeedTextCustom(word, feed, replaceBy) {
    return replaceAll(feed, word, replaceBy);
}

/**
 * Formating tweet to be more visually friendly
 * @param word
 * @param feed
 * @param i
 * @returns {*}
 */
function formatFeedText(word, feed, i) {
    var color = getColor(i)
    return replaceAll(feed, word, '<span class="ar-found" style="color: ' + color + ' !important;">' + word + '</span>');
}

/**
 * Replacing all occurences in a string
 * @param str
 * @param find
 * @param replace
 * @returns {*|string|void|XML}
 */
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'gi'), replace);
}

/**
 * Generating safe regex pattern
 * @param string
 * @returns {*|string|void|XML}
 */
function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function isScrolledIntoView(elem, window) {
    var $elem = elem;
    var $window = window;

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function toggleClass(elem, _class) {
    if (elem.hasClass(_class)) {
        elem.removeClass(_class)
        return false
    } else {
        elem.addClass(_class)
        return true
    }
}

function getIndexOfJQueryObject(container, list) {
    for (var i = 0; i < list.length; i++) {
        var containertmp = $(list[i]);
        if (containertmp.is(container))
            return i
    }
}

function insertAtIndex(i, container, item, type) {
    if (i === 0) {
        container.prepend(item);
        return;
    }
    container.find(type + ":nth-child(" + i + ")").after(item);
}

var QueryString = function () {
    // This function is anonymous, is executed immediately and
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
};

var redirectPageTo = function (url) {
    window.location = url
}

Date.prototype.yyyymmdd = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();
    return yyyy + "-" +  (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
};