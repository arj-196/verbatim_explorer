function _InteractionHistory() {
    var self = this
    this.rootPath = window.location.pathname
    this.root = window.location.origin + window.location.pathname
    this.variables = {}
    this.callbacks = {}
    this.cache = {}

    this.init = function () {
        return new Promise(function (resolve, reject) {
            // initialize cache from window.location.hash
            API.UTILS.HISTORY('interaction', 'get', null).then(function (r) {
                if (!_.isUndefined(r.c)) {
                    API.UTILS.HASH('reveal', r.c).then(function (revealedString) {
                        self.cache = JSON.parse(revealedString)
                        resolve()
                    })
                } else {
                    resolve()
                }
            })
        })
    }

    this.createVariable = function (varname) {
        self.variables[varname] = true
    }

    this.setInitCallback = function (varname, callback) {
        self.callbacks[varname] = callback
    }

    this.setVariable = function (varname, value) {
        self.cache[varname] = value
        self._set_url()
    }

    this._set_url = function () {
        API.UTILS.HASH('hide', JSON.stringify(self.cache)).then(function (hashedString) {
            window.location.hash = hashedString
            API.UTILS.HISTORY('interaction', 'set', hashedString)
        })
    }

    this.getVariable = function (varname) {
        return self.cache[varname]
    }

    this.ifExistsVariable = function (varname) {
        return !_.isUndefined(self.cache[varname])
    }

    this.ifAutoStart = function (force) {
        // check if exist auto_start variable
        if (!_.isUndefined(QueryString().a_s)) {
            return true
        } else {
            return !!(!_.isUndefined(force) && force == true);
        }
    }

    this.setAutoStart = function (state) {
        if (state) {
            if (_.isUndefined(QueryString().a_s)) {
                window.history.pushState(null, "Adding a_s to query parameters", this.rootPath + "?a_s=true");
            }
        }
    }

    this.trigger = function (varname) {
        self.callbacks[varname](self.cache[varname])
    }
}
var InteractionHistory = new _InteractionHistory()