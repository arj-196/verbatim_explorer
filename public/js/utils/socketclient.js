function SocketClient (namespace) {
    var self = this
    this.host = window.location.origin + '/'
    this.socket = io(self.host+ namespace);

    this.addListener = function (onEvent, callback) {
        self.socket.on(onEvent, callback)
    }

    this.emit = function (event, data, callback) {
        self.socket.emit(event, data, callback)
    }
}
