function Table(name) {
    var self = this
    this.id = name + '-table'
    this.headers = []
    this.rows = []

    this.draw = function (container) {
        container.append(
            TEMPLATE.TABLE.CONTAINER
                .replace('$ID$', self.id)
        )

        self._drawHeader()
        self._drawRows()
    }

    this._drawHeader = function () {
        var container = $('#' + self.id).find('thead')
        container.append('<tr></tr>')
        container = container.find('tr')

        container.append('<td></td>')
        self.headers.forEach(function (header) {
            container.append(
                TEMPLATE.TABLE.HEADER
                    .replace('$HEADER$', header)
            )
        })
    }

    this._drawRows = function () {
        var container = $('#' + self.id).find('tbody')
        self.rows.forEach(function (row, index) {
            container.append('<tr class="row-index-' + index + '"></tr>')
            var rowContainer = container.find('tr.row-index-' + index)
            row.forEach(function (d) {
                rowContainer.append(
                    '<td>' + d + '</td>'
                )
            })

        })

    }
}