var _CommandParser = function(){

    this.seperators = {
        and: ' ',
        or: '|',
        not: '!',
    }

    this._parse = function (text) {
        text = text.trim().toLowerCase()
        if(text != ""){
            var self = this
            var cmd = []
            // check for 'or' operators
            var ors = text.split(self.seperators.or)
            ors.forEach(function (o) {
                // check for 'and' operators
                var ands = o.split(self.seperators.and)
                cmd.push(ands)
            });
            return cmd
        } else {
            return null
        }
    }

    this.parse = function (text) {
        // check for 'not' parameter
        var nots = text.split(this.seperators.not)
        // keep left of 'not' parameter
        text = nots[0]
        return this._parse(text)
    }

    this.parse_not = function (text) {
        // check for 'not' parameter
        var nots = text.split(this.seperators.not)
        // keep left of 'not' parameter
        if (nots.length == 2) {
            text = nots[1]
            return this._parse(text)
        } else {
            return null
        }
    }

    this.parse_all = function (text) {
        return this._parse(text.replace('!', ''))
    }

    this.toString = function (commandList) {
        var string = ""
        for (var i = 0; i < commandList.length; i++) {
            var cL = commandList[i];
            for (var j = 0; j < cL.length; j++) {
                string += cL[j]
                if(j != cL.length - 1){
                    string += this.seperators.and
                }
            }

            if(i != commandList.length - 1){
                string += this.seperators.or
            }
        }

        return string
    }

}

var CommandParser = new _CommandParser()
