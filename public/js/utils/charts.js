function BarPlot(container, title, margin, ifFrequency, ifColor) {

    var width = container.width() - margin.left - margin.right,
        height = container.height() - margin.top - margin.bottom
    var x = d3.scale.ordinal().rangeRoundBands([0, width], .1)
    var y = d3.scale.linear().range([height, 0])
    var xAxis = d3.svg.axis().scale(x).orient("bottom")
    var yAxis
    if (ifFrequency) {
        yAxis = d3.svg.axis().scale(y).orient("left").ticks(5, "%")
    } else {
        yAxis = d3.svg.axis().scale(y).orient("left").ticks(5, "")
    }
    var fill = d3.scale.category20()

    this.create = function (_id, data, yAxisLabel) {

        var tip
        if (ifFrequency) {
            tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function (d) {
                    return "<strong style='font-family:roboto-black'>Frequency:</strong> <span style='color:red'>" +
                        (d.frequency * 100).toFixed(0) + " % </span>"
                })
        } else {
            tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function (d) {
                    return "<strong style='font-family:roboto-black'> " + d.name + " :</strong> <span style='color:red'>" +
                        (d.frequency).toFixed(0) + "</span>"
                })
        }

        var svg = d3.select(_id).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

        svg.call(tip)


        x.domain(data.map(function (d) {
            return d.name
        }))
        y.domain([0, d3.max(data, function (d) {
            return d.frequency
        })])

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
        if (data.length > 20) {
            // if more than 20 elements, rotate x axis labels
            var rotateAngle
            if (data.length > 89) {
                rotateAngle = 90
            } else {
                rotateAngle = data.length
            }

            svg
                .selectAll("text")
                .attr("y", 0)
                .attr("x", 9)
                .attr("dy", ".35em")
                .attr("transform", "rotate(" + rotateAngle + ")")
                .style("text-anchor", "start")

        }

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(yAxisLabel)

        svg.selectAll(".bar")
            .data(data)
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) {
                return x(d.name)
            })
            .attr("width", x.rangeBand())
            .attr("y", function (d) {
                return y(d.frequency)
            })
            .attr("height", function (d) {
                return height - y(d.frequency)
            })
            .style("fill", function (d, i) {
                if (ifColor)
                    return d.color
                else
                //return fill(d.frequency)
                    return getColor(i, true)
            })
            .on('mouseover', function (d) {
                tip.show(d)
            })
            .on('mouseout', tip.hide)

        if (title) {
            svg.append("text")
                .attr("x", (width / 2))
                .attr("y", 15 - (margin.top / 2))
                .attr("text-anchor", "right")
                .style("font-size", "12px")
                .style("font-family", "roboto-black")
                .style("fill", "grey")
                .text(title)
        }
    }
}

function StackedBarPlot(container, title, margin, ifFrequency, ifColor) {

    //var margin = {top: 20, right: 20, bottom: 30, left: 40},
    var width = container.width() - margin.left - margin.right,
        height = container.height() - margin.top - margin.bottom;

    var x = d3.scale.ordinal()
        .rangeRoundBands([0, width], .1);

    var y = d3.scale.linear()
        .rangeRound([height, 0]);

    var color = d3.scale.ordinal()
        .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"])
    //var color = {}

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format(".2s"));

    this.create = function (_id, res, yAxisLabel) {
        var svg = d3.select(_id).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var data = res.r

        // filter out if certain key does not exist
        color.domain(d3.keys(data[0]).filter(function(key) { return key !== "name"; }));

        data.forEach(function(d) {
            var y0 = 0;
            d.fields = color.domain().map(function(name) { return {name: name, y0: y0, y1: y0 += +d[name]}; });
            d.total = d.fields[d.fields.length - 1].y1;
        });

        data.sort(function(a, b) { return b.total - a.total; });

        x.domain(data.map(function(d) { return d.name; }));
        y.domain([0, d3.max(data, function(d) { return d.total; })]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(yAxisLabel);

        var state = svg.selectAll(".state")
            .data(data)
            .enter().append("g")
            .attr("class", "g")
            .attr("transform", function(d) { return "translate(" + x(d.name) + ",0)"; });

        state.selectAll("rect")
            .data(function(d) { return d.fields; })
            .enter().append("rect")
            .attr("width", x.rangeBand())
            .attr("y", function(d) { return y(d.y1); })
            .attr("height", function(d) { return y(d.y0) - y(d.y1); })
            .style("fill", function(d) { return color(d.name); });

        var legend = svg.selectAll(".legend")
            .data(color.domain().slice().reverse())
            .enter().append("g")
            .attr("class", "legend")
            .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

        legend.append("rect")
            .attr("x", width - 18)
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", color);

        legend.append("text")
            .attr("x", width - 24)
            .attr("y", 9)
            .attr("dy", ".35em")
            .style("text-anchor", "end")
            .text(function(d) { return d; });

    }
}