function TopicMenu(container, topicOnClickFn, typeTopics) {
    var self = this
    this.container = container

    this.getDatasets = function () {
        var projectid = self.container.find('input[name="project"]').val().trim()
        if (projectid == "") {
            self.showError("error! You must choose a Project!")
        } else {
            if(_.isUndefined(typeTopics))
                typeTopics = 'verbatim'

            API.DASHBOARD.getUserTopics(self.user._id, typeTopics, projectid)
                .then(function (r) {
                    var topics = r.topics
                    var container = self.container.find('.dropdown.dropdown-dataset').find('.menu')
                    container.empty()
                    _.keys(topics).forEach(function (dir) {
                        topics[dir].forEach(function (d) {
                            container.append(
                                TEMPLATE.VERBATIM.TOPICS.PROJECT
                                    .replace('$VALUE$', d.topic)
                                    .replace('$NAME$', d.name)
                                    .replace('$BACKGROUND$', r.projectColors[d.project])
                            )
                        })
                    })
                })
        }
    }

    this.doSearch = function () {
        var projectid = self.container.find('input[name="project"]').val()
        var topicid = self.container.find('input[name="dataset"]').val()
        if (projectid == "" || topicid == "") {
            self.showError("error! Project and Dataset Must be Defined!")
        } else {
            API.DASHBOARD.getTopicDetails(topicid).then(function (topic) {
                topicOnClickFn(topic)
            })
        }
    }

    this.showError = function (errorText) {
        self.container.find('.error.message').addClass('visible')
            .find('.header').text(errorText)
    }

    this.init = function () {
        // projects
        API.DASHBOARD.getUser().then(function (user) {
            self.user = user
            API.DASHBOARD.getProjects()
                .then(function (projects) {
                    var container = self.container.find('.dropdown.dropdown-project').find('.menu')
                    container.empty()
                    projects.forEach(function (project) {
                        container.append(
                            TEMPLATE.VERBATIM.TOPICS.PROJECT
                                .replace('$VALUE$', project._id)
                                .replace('$NAME$', project.name)
                                .replace('$BACKGROUND$', project.color)
                        )
                    })
                })
        })
        // onclick button trigger-topic
        self.container.find('.trigger-topic').on('click', self.doSearch)

        // onclick dropdown dataset
        self.container.find('.dropdown.dropdown-dataset')
            .on('click', self.getDatasets)

        // on change input project
        self.container.find('input[name="project"]').change(function () {
            self.container.find('.dropdown.dropdown-dataset').dropdown('set exactly', null)
        })

    }

}