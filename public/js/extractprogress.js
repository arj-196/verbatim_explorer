$(document).ready(function () {
    EPDashboard.init()
    $('#menu-right')
        .append(
            TEMPLATE.MENU.USERLESS
        )

    $('.dropdown').dropdown()
})

function _ExtractProgressDashboard() {
    var self = this
    this.table = $('#table-dataset')
    this.SETTINGS = {}

    this.init = function () {
        this.setupTable()

        window.setInterval(function(){
            self.setupProgressBars()
        }, 5000);

        window.setInterval(function(){
            self.setupTable()
        }, 30000);
    }

    this.setupTable = function () {
        API.EXTRACTPROGRESS.fetchDatasets().then(function (r) {
            if (r.s) {
                self.SETTINGS.datasets = r.r
                self.fillTable()
                self.setupProgressBars()
            }
        })
    }

    this.fillTable = function () {
        var tableContentOrder = ["extract", "queue", "upload"]
        var container = self.table.find('tbody')
        container.empty()
        tableContentOrder.forEach(function (dType) {
            self.SETTINGS.datasets[dType].forEach(function (d) {
                container.append(
                    TEMPLATE.EXTRACTPROGRESS.TABLE.ITEM
                        .replace('$TOPICNAME$', d.topic)
                        .replace('$PROJECT$', d.project)
                        .replace('$LANG$', d.lang)
                        .replace('$STATUS$', TEMPLATE.EXTRACTPROGRESS.TABLE.STATUS[dType])
                        .replace('$DATA$', 'data-id="' + d._id + '" data-type="' + dType + '"')
                )
            })
        })
    }

    this.setupProgressBars = function () {
        var fetchProgressesCounts = [], containers = []
        $.each($('tr[data-type="extract"]'), function(){
            var $this = $(this)
            var data = $this.data()
            fetchProgressesCounts.push(API.EXTRACTPROGRESS.getProgress(data.id))
            containers.push($this)
        })

        Promise.all(fetchProgressesCounts).then(function (progressCounts) {
            for (var i = 0; i < progressCounts.length; i++) {
                if(progressCounts[i].s) {
                    var percent = parseInt(progressCounts[i].percent)
                    var color
                    if (percent < 20) {
                        color = COLORS.PROGRESS.LOW
                    } else if(percent < 60) {
                        color = COLORS.PROGRESS.MEDIUM
                    } else if(percent < 80) {
                        color = COLORS.PROGRESS.HIGH
                    } else {
                        color = COLORS.PROGRESS.VERYHIGH
                    }
                    
                    var container = containers[i].find('.content-progress')
                    container.empty()
                    container.append(
                        TEMPLATE.EXTRACTPROGRESS.PROGRESS.CONTAINER
                            .replace('$PERCENT$', percent)
                            .replace('$COLOR$', color)
                    )
                }
            }
            $('.progress').progress()
        })
    }

}

var EPDashboard = new _ExtractProgressDashboard()
