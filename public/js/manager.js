var PATH = {
    VERBATIMS: {
        ROOT: '/tmp/verbatims/',
        config: 'config.json'
    },
    WORDCLOUDS: {
        ROOT: '/tmp/wordclouds/',
        config: 'config.json'
    },

    HEATMAPS: {
        CONFIG: {
            ROOT: "/tmp/heatmaps/config/",
            config: 'config.json'
        },
        ROOT: "/tmp/heatmaps/"
    }
};

var _Manager = function () {

    this.initVerbatims = function () {
        return new Promise(function (resolve, reject) {
            //// test
            //Layout.initVerbatims().then(function (err) {
            //    resolve()
            //})

            Layout.initVerbatims().then(function (err) {
                resolve()
                Layout.verbatimFormatLayout(2).then(function (err) {
                    Layout.initVerbatims()
                })
            });
        });
    };

    this.verbatimFormatLayout = function (mode) {
        return new Promise(function (resolve, reject) {
            Layout.verbatimFormatLayout(mode).then(function (err) {
                Layout.initVerbatims().then(function (err) {
                    resolve()
                })
            });
        });
    };

    this.initWordclouds = function () {
        return new Promise(function (resolve, reject) {
            Layout.initWordclouds().then(function (err){
                if(!err)
                    resolve()
                else
                    reject(err)
            })
        })
    }

    this.initHeatMaps = function () {
        return new Promise(function (resolve, reject) {
            Layout.initHeatMaps().then(function (err){
                if(!err)
                    resolve()
                else
                    reject(err)
            })
        })
    }

    this.initTagged= function () {
        return new Promise(function (resolve, reject) {
            Layout.initTagged().then(function (err){
                if(!err)
                    resolve()
                else
                    reject(err)
            })
        })
    }
    
    this.initSentiment = function () {
        return new Promise(function (resolve, reject) {
            resolve()
        })
    }

    this.unHidePriveledged = function () {
        API.DASHBOARD.getUser().then(function (User) {
            if(!_.isUndefined(User.role)){
                $('.role-' + User.role.toLowerCase()).removeClass('hidden')
            }
        })
    }
};

var Manager = new _Manager();
