var InsightCAEngine = new StatEngine('wca')
InsightCAEngine.SETTINGS = {
    CONTAINERAPPENDED: false,
    CONTAINERID: 'ica-container',
    PLOTID: 'ica-plot',
    INSIGHTS: [],
    INSIGHTBAG: null,
}

InsightCAEngine.init = function () {
    if (!this.SETTINGS.CONTAINERAPPENDED) {
        this.appendOptionsContainer()
        this.appendContainer()
        this.SETTINGS.CONTAINERAPPENDED = true
    }
}

InsightCAEngine.appendOptionsContainer = function () {
    CONFIG.OPanel.append(
        TEMPLATE.CADASHBOARD.ICA.OPTIONS.CONTAINER
    )
}

InsightCAEngine.appendContainer = function () {
    CONFIG.SPanel.append(
        TEMPLATE.CADASHBOARD.STATS.CONTAINER
            .replace('$STATNAME$', 'ICA-Results')
            .replace('$CONTAINERID$', this.SETTINGS.CONTAINERID)
            .replace('$PLOTID$', this.SETTINGS.PLOTID)
            .replace('$INITMESSAGE$', 'INIT MESSAGE') // TODO init message
    )
}

InsightCAEngine.getInsightBag = function (next) {

    var insightsToFetch = []
    this.topics.forEach(function (topic) {
        insightsToFetch.push(API.INSIGHTS.getInsightsForProject(topic))
    })

    // getting insights for all available datasets
    Promise.all(insightsToFetch).then(function (insightLists) {
        var insightBag = {
            uniques: [],
            map: {}
        }
        insightLists.forEach(function (insightList) {
            insightList.forEach(function (insight) {
                insightBag.map[insight._id] = insight
                insightBag.uniques.push(insight._id)
            })
        })
        insightBag.uniques = _.unique(insightBag.uniques)

        // callback
        next(insightBag)
    })
}

InsightCAEngine.showModal = function () {
    var self = this
    var container = $('#modal-ica')
    container.find('.error.message').removeClass('visible')
    container.modal('show')
    var insightListContainer = container.find('.dropdown')
    insightListContainer.dropdown({
        onChange: function (values) {
            self.SETTINGS.INSIGHTS = values
            self.previewInsights()
        }
    })
    insightListContainer.dropdown('remove selected', self.SETTINGS.INSIGHTS)

    this.getInsightBag(function (insightBag) {
        var insightListContainer = container.find('select[name="insights"]')
        self.SETTINGS.INSIGHTBAG = insightBag
        insightListContainer.empty()
        insightBag.uniques.forEach(function (iid) {
            var insight = insightBag.map[iid]
            insightListContainer.append(
                TEMPLATE.DASHBOARD.FORM.ITEM
                    .replace('$NAME$', insight.name)
                    .replace('$VALUE$', insight._id)
            )
        })
    })
}

InsightCAEngine.previewInsights = function () {
    var insights = this.SETTINGS.INSIGHTS
    var insightBag = this.SETTINGS.INSIGHTBAG
    var container = $('#modal-ica').find('.insight-list')
    container.empty()
    insights.forEach(function (iid) {
        var insight = insightBag.map[iid]
        if (!_.isUndefined(insight))
            container.append(
                TEMPLATE.CADASHBOARD.WCA.WORD.PREVIEW
                    .replace('$BACKGROUND$', 'black')
                    .replace('$WORD$', insight.name)
            )
    })
}

InsightCAEngine.setInsights = function () {
    var modal = $('#modal-ica')
    var insights = this.SETTINGS.INSIGHTS
    var insightBag = this.SETTINGS.INSIGHTBAG
    var container = CONFIG.OPanel.find('.list-ica-insight')
    container.empty()
    insights.forEach(function (iid) {
        var insight = insightBag.map[iid]
        container.append(
            TEMPLATE.CADASHBOARD.WCA.WORD.ITEM
                .replace('$BACKGROUND$', 'black')
                .replace('$WORD$', insight.name)
                .replace('$DATA$', 'data-id="' + iid + '"')
                .replace('$FUNCREMOVE$', 'onclick="InsightCAEngine.removeInsight(this)"')
        )
    })
    modal.modal('hide')
    this.launch()
}

InsightCAEngine.removeInsight = function (o) {
    var $this = $(o).parent()
    var iid = $this.data().id
    var index = this.SETTINGS.INSIGHTS.indexOf(iid)
    this.SETTINGS.INSIGHTS.splice(index, 1)
    $this.remove()
    this.launch()
}

InsightCAEngine.launch = function () {
    var self = this
    if(this.SETTINGS.INSIGHTS.length > 0 && this.topics.length > 0) {
        $('#' + self.SETTINGS.CONTAINERID).dimmer('show')
        API.CADASHBOARD.ICA.getQuantificationPerTopic(this.topics, this.SETTINGS.INSIGHTS, this.SETTINGS.INSIGHTBAG)
            .then(function (res) {
                console.log("response from ICA launch", res)
                InsightCAEngine.drawPlot(res)
                InsightCAEngine.drawTable(res)
                $('#' + self.SETTINGS.CONTAINERID).dimmer('hide')
            })
    }
}

InsightCAEngine.drawPlot = function (res) {
    $('#' + InsightCAEngine.SETTINGS.CONTAINERID).find('.message-init').css('display', 'none')

    // creating plot
    $('#' + InsightCAEngine.SETTINGS.PLOTID).find('svg').remove()
    new StackedBarPlot(
        $('#' + InsightCAEngine.SETTINGS.PLOTID),
        "SOMETHING",
        {top: 40, right: 10, bottom: 30, left: 30},
        false, // ifFrequency
        false // ifColor
    ).create(
        '#' + InsightCAEngine.SETTINGS.PLOTID,
        res,
        "S-Index"
    )
}

InsightCAEngine.drawTable = function (res) {

    // count lists
    var container = $('#' + InsightCAEngine.SETTINGS.CONTAINERID).find('.content-list')
    container.empty()
    // creating stats table
    var table = new Table("ica")
    // table headers
    var headers = []
    var hpostfix

    if (res.r.length > 0) {
        // hpostfix = _.keys(res.t[0][res.words[0]])
        hpostfix = ["sindex", "avscore", "nbpredicted", "topiccount"]
        res.words.forEach(function (w) {
            hpostfix.forEach(function (hp) {
                headers.push(
                    "<span style='font-style: italic; font-family:roboto-bold;'>" + w +
                    "</span>" + " <br/> <span style='text-decoration: underline;font-size:13px;'>" + hp + "</span>")
            })
        })
    }
    table.headers = headers

    // row data
    var rows = []

    res.t.forEach(function (d) {
        var row = []
        row.push(d.name)
        res.words.forEach(function (w) {
            hpostfix.forEach(function (hp) {
                if(!_.isNull(d[w])) {
                    if(d[w][hp] % 1 != 0) {
                        row.push(d[w][hp].toFixed(3))
                    } else {
                        row.push(d[w][hp])
                    }
                }
                else
                    row.push(0)
            })
        })

        rows.push(row)
    })

    // res.r.forEach(function (d) {
    //     var row = []
    //     row.push(d.name)
    //     headers.forEach(function (h) {
    //         row.push(d[h])
    //     })
    //     rows.push(row)
    // })
    table.rows = rows

    // draw table
    table.draw(container)
}