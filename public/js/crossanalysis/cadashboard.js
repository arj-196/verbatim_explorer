function _CADashbord() {
    var self = this
    this.OPanel = CONFIG.OPanel
    this.SPanel = CONFIG.SPanel
    this.VPanel = CONFIG.VPanel
    this.topics = []
    this.StatEngines = []

    this.init = function () {
        new TopicMenu($('#container-dataset'), this.addTopic).init()
        // init attached engines
        self.initEngines()
    }

    this.changeLayout = function (mode, next) {
        Layout.initCrossAnalysis(mode).then(function (err) {
            if (!_.isUndefined(next))
                next(err)
        })
    }

    this.addTopic = function (topic) {
        var container = self.OPanel.find('.list-datasets')

        if (container.find('.dataset[data-topic="' + topic.topic + '"]').length == 0) {
            // add topic to topic list
            container.append(
                TEMPLATE.CADASHBOARD.DATASET.CONTAINER
                    .replace('$DATASETNAME$', topic.name)
                    .replace('$DATA$', 'data-topic="' + topic.topic + '"')
                    .replace('$BACKGROUND$', getColor(self.topics.length))
                    .replace('$FUNCREMOVE$', "onclick='CADashboard.removeTopic(this)'")
            )
            closeChooseDataSetModal()

            self.topics.push(topic.topic)
            // adding a topic to StatEngines
            self.StatEngines.forEach(function (engine) {
                engine.addTopic(topic.topic)
                engine.launch()
            })
        }
    }

    this.removeTopic = function (o) {
        var $this = $(o)
        var topic = $this.parent().data().topic
        $this.parent().remove()
        self.StatEngines.forEach(function (engine) {
            engine.removeTopic(topic)
            engine.launch()
        })
    }

    this.initEngines = function () {
        self.StatEngines.forEach(function (engine) {
            engine.init()
        })
    }
}

function StatEngine(name) {
    var self = this
    this.name = name
    this.topics = []
    this.plugins = {
        launch: null
    }
    this.SETTINGS = {}

    this.addTopic = function (topic) {
        self.topics.push(topic)
    }

    this.removeTopic = function (topic) {
        var index = self.topics.indexOf(topic)
        this.topics.splice(index, 1)
    }
}
