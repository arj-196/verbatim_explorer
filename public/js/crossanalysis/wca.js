//
// WCA Engine
//
var WCAEngine = new StatEngine('wca')
WCAEngine.SETTINGS = {
    CONTAINERAPPENDED: false,
    CONTAINERID: 'wca-container',
    PLOTID: 'wca-plot',
    INPUTLISTERNERON: false,
    VARIABLES: {
        MODAL: $('#modal-wca')
    },
    WORDS: []
}

WCAEngine.init = function () {
    if (!this.SETTINGS.CONTAINERAPPENDED) {
        this.appendOptionsContainer()
        this.appendContainer()
        this.SETTINGS.CONTAINERAPPENDED = true
    }

    // textarea keyup timer listener
    if (!WCAEngine.SETTINGS.INPUTLISTERNERON) {
        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 400  //time in ms, 5 second for example
        var $input = WCAEngine.SETTINGS.VARIABLES.MODAL.find('textarea[name="words"]')

        //on keyup, start the countdown
        $input.on('keyup', function () {
            clearTimeout(typingTimer)
            typingTimer = setTimeout(doneTyping, doneTypingInterval)
        });

        //on keydown, clear the countdown
        $input.on('keydown', function () {
            clearTimeout(typingTimer)
        });

        //user is "finished typing," do something
        function doneTyping() {
            WCAEngine.previewWords()
        }

        WCAEngine.SETTINGS.INPUTLISTERNERON = true
    }
}


WCAEngine.appendOptionsContainer = function () {
    CONFIG.OPanel.append(
        TEMPLATE.CADASHBOARD.WCA.OPTIONS.CONTAINER
    )
}

WCAEngine.appendContainer = function () {
    CONFIG.SPanel.append(
        TEMPLATE.CADASHBOARD.STATS.CONTAINER
            .replace('$STATNAME$', 'WCA-Results')
            .replace('$CONTAINERID$', this.SETTINGS.CONTAINERID)
            .replace('$PLOTID$', this.SETTINGS.PLOTID)
            .replace('$INITMESSAGE$', 'INIT MESSAGE') // TODO init message
    )
}

WCAEngine.getWords = function (ifCache) {
    if(_.isUndefined(ifCache)) {
        var text = this.SETTINGS.VARIABLES.MODAL.find('textarea[name="words"]').val().trim()
        if (text == "") {
            return null
        } else {
            return _.map(text.split(','), function (w) {
                return w.trim()
            })
        }
    } else {
        return this.SETTINGS.WORDS
    }
}

WCAEngine.previewWords = function () {
    var words = WCAEngine.getWords()
    // append words to modal preview
    var container = this.SETTINGS.VARIABLES.MODAL.find('.word-list')
    container.empty()
    if (words) {
        words.forEach(function (word) {
            if (word != "") {
                container.append(
                    TEMPLATE.CADASHBOARD.WCA.WORD.PREVIEW
                        .replace('$BACKGROUND$', 'black')
                        .replace('$WORD$', word)
                )
            }
        })
    }
}

WCAEngine.setWords = function (ifCache) {
    var words = this.getWords(ifCache)
    if (words) {
        this.SETTINGS.WORDS = words
        var container = CONFIG.OPanel.find('.list-wca-word')
        container.empty()
        words.forEach(function (word) {
            if (word != "") {
                container.append(
                    TEMPLATE.CADASHBOARD.WCA.WORD.ITEM
                        .replace('$BACKGROUND$', 'black')
                        .replace('$WORD$', word)
                        .replace('$DATA$', 'data-word="' + word + '"')
                        .replace('$FUNCREMOVE$', 'onclick="WCAEngine.removeWord(this)"')
                )
            }
        })
        WCAEngine.SETTINGS.VARIABLES.MODAL
            .modal('hide')

        this.launch() // do launch
    } else {
        this.SETTINGS.VARIABLES.MODAL.find('.error.message')
            .addClass('visible')
            .text('Atleast one word must be defined!')
    }
}

WCAEngine.removeWord = function (o) {
    var $this = $(o).parent()
    var word = $this.data().word
    var index = this.SETTINGS.WORDS.indexOf(word)
    this.SETTINGS.WORDS.splice(index, 1)
    this.setWords(true)
    this.launch()
}

WCAEngine.launch = function () {
    var self = this
    if (this.SETTINGS.WORDS.length > 0 && this.topics.length > 0) {
        $('#' + self.SETTINGS.CONTAINERID).dimmer('show')
        API.CADASHBOARD.WCA.getCountsPerTopic(this.topics, this.SETTINGS.WORDS)
            .then(function (res) {
                console.log("response from wca", res)
                WCAEngine.drawPlot(res)
                WCAEngine.drawTable(res)
                $('#' + self.SETTINGS.CONTAINERID).dimmer('hide')
            })
    }
}

WCAEngine.drawPlot = function (res) {
    $('#' + WCAEngine.SETTINGS.CONTAINERID).find('.message-init').css('display', 'none')
    // creating plot
    $('#' + WCAEngine.SETTINGS.PLOTID).find('svg').remove()
    new StackedBarPlot(
        $('#' + WCAEngine.SETTINGS.PLOTID),
        "SOMETHING",
        {top: 40, right: 10, bottom: 30, left: 30},
        false, // ifFrequency
        false // ifColor
    ).create(
        '#' + WCAEngine.SETTINGS.PLOTID,
        res,
        "Counts"
    )
}

WCAEngine.drawTable = function (res) {
    // count lists
    var container = $('#' + WCAEngine.SETTINGS.CONTAINERID).find('.content-list')
    container.empty()
    // creating stats table
    var table = new Table("wca")
    // table headers
    var headers = []
    if (res.r.length > 0) {
        headers = _.difference(_.keys(res.r[0]), ["name", "fields"])
    }
    table.headers = headers

    // row data
    var rows = []
    res.r.forEach(function (d) {
        var row = []
        row.push(d.name)
        headers.forEach(function (h) {
            row.push(d[h])
        })
        rows.push(row)
    })
    table.rows = rows

    // draw table
    table.draw(container)
}

WCAEngine.showModal = function () {
    var container = $('#modal-wca')
    container.find('.error.message').removeClass('visible')
    container
        .modal('show')
}