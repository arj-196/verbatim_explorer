var _Layout = function () {

    this.CACHE = {
        STATS: {
            LAYOUT: {
                DEFAULT: 0.85,
                STATE: 1,
                HEIGHTS: {
                    0: 0.0,
                    1: 0.15,
                    2: 0.55,
                    3: 0.95,
                }
            }
        }
    }

    this.initVerbatims = function () {
        return new Promise(function (resolve, reject) {
            var containerHeight = $('#main-container').height();
            var topicHeight = 200;
            var containerSearchHistory = $('#container-search-history')
            var containerWordCloud = $('.wordcloud-container')
            var multipleVerbatimPanelContainer = $('#multiple-verbatim-panel')
            var multipleVerbatimListContainer = $('#multiple-verbatim-panel-list')

            containerSearchHistory.height(topicHeight)
            containerWordCloud.height((containerHeight - topicHeight + 25));
            multipleVerbatimPanelContainer
                .width(containerWordCloud.width() - 10)
                .height(containerSearchHistory.height() + containerWordCloud.height())
            multipleVerbatimListContainer.height(multipleVerbatimPanelContainer.height() - 100)

            resolve(null);
        });
    };

    this.verbatimFormatLayout = function (mode) {
        var self = this
        return new Promise(function (resolve, reject) {
            var container = $('#main-body')
            var containerVerbatimContent = container.find('.content-verbatim')
                , containerStatistics = container.find('.content-statistics')
            var heightVCNew, heightSNew, heightContainer = container.height()
            var toggleContainerStatsMode = 0 // default do nothing

            // checking Containet Statistics is in fixed mode
            if ($('.content-statistics').data().state != "fixed") {
                toggleContainerStatsMode = 2
            }

            if (mode == 1) {
                heightVCNew = heightContainer
                heightSNew = 0
                containerStatistics.css("display", "none")
                config.STATS = false
            } else if (mode == 2) {
                heightVCNew = heightContainer * self.CACHE.STATS.LAYOUT.DEFAULT
                self.CACHE.STATS.LAYOUT.STATE = 1
                heightSNew = heightContainer - heightVCNew
                containerStatistics.css("display", "block")
                config.STATS = true
            }

            self.toggleDisplayModeContainerStatistics(toggleContainerStatsMode, true).then(function () {
                containerStatistics.animate({height: heightSNew}, 500, function () {
                    containerVerbatimContent.animate({height: heightVCNew}, 500, function () {
                        // re-running search to reinitiate wordcloud generation
                        searchForText(true)
                        resolve()
                    })
                })
            })
        })
    }

    this.toggleDisplayModeContainerStatistics = function (mode, ifdefault) {
        var self = this
        return new Promise(function (resolve, reject) {
            var container = $('.content-statistics'),
                trigger = $('.trigger-stats-toggle-display'),
                newHeight, p

            if (mode == 1) {
                // set to floating mode
                if (self.CACHE.STATS.LAYOUT.STATE < 3) {
                    self.CACHE.STATS.LAYOUT.STATE++
                    p = self.CACHE.STATS.LAYOUT.HEIGHTS[self.CACHE.STATS.LAYOUT.STATE]
                    newHeight = $(window).height() * p
                    container.css({
                        position: "absolute"
                    })
                    container.animate({
                        height: newHeight + 'px'
                    }, 500, function () {
                        resolve()
                    })
                } else {
                    resolve()
                }
            } else if (mode == 2) {
                // set to fixed mode
                if(self.CACHE.STATS.LAYOUT.STATE > 1){
                    self.CACHE.STATS.LAYOUT.STATE--
                    p = self.CACHE.STATS.LAYOUT.HEIGHTS[self.CACHE.STATS.LAYOUT.STATE]
                    newHeight = $(window).height() * p
                    container.animate({
                        height: newHeight + 'px'
                    }, 500, function () {
                        resolve()
                    })
                } else {
                    resolve()
                }
            } else {
                resolve()
            }
        })
    }

    this.initWordclouds = function () {
        return new Promise(function (resolve, reject) {
            // TODO set correct hight constrains for layout
            resolve(null);
        });
    };

    this.initHeatMaps = function () {
        return new Promise(function (resolve, reject) {
            // TODO set correct hight constrains for layout
            resolve(null);
        });
    };

    this.initTagged = function () {
        return new Promise(function (resolve, reject) {
            //var windowHeight = $(window).height()
            //var mainContainer = $('#main-container')
            //mainContainer.height(windowHeight - 80)
            //var containerHeight = mainContainer.height();
            //$('#tag-container').find('.list').height(mainContainer.height() - 100)
            //$('#container-verbatims').height(containerHeight - 25)
            //$('#menu-nav-text-3').css('padding-left', '20px')
            resolve(null);
        });
    }

    this.initCrossAnalysis = function (mode) {
        return new Promise(function (resolve, reject) {
            //var OPanel = $('#container-options')
            var SPanel = $('#container-stats')
            var VPanel = $('#container-verbatims')

            if(mode == 1) {
                SPanel.attr('class', 'ui eleven wide column')
                VPanel.attr('class', 'ui zero wide column').css('display', 'none')
            } else if(mode == 2) {
                SPanel.attr('class', 'ui six wide column')
                VPanel.attr('class', 'ui five wide column').css('display', 'inherit')
            }

            resolve(null)
        })
    }

    var sidebarTrigger = function () {
        $('#trigger-sidebar').click(function () {
            $('.ui.sidebar').sidebar('toggle')
        })
    }

    var highlightCurrentSidebarLink = function () {
        var domainName = extractDomain(window.location.href);
        // main nav
        $.each($('#container-sidebar').find('.item'), function () {
            var self = $(this);
            var url = 'http://' + domainName + self.attr('href');
            if (url == window.location.href) {
                self
                    .css('color', 'lightgrey')
                    .addClass('ar-link-disabled')
                ;
            }
        });
    }

    var initializeDefault = function () {

        sidebarTrigger()
        highlightCurrentSidebarLink()
    };
    initializeDefault();


};

var Layout = new _Layout();


function _WindowManager() {
    var self = this
    this.SETTINGS = {
        progressQueue: []
    }

    this.addActionInProgressQueue = function (action) {
        self.SETTINGS.progressQueue.push(action)
        self.toggleCursorProgress()
    }

    this.resolveActionInProgressQueue = function (action) {
        var index = self.SETTINGS.progressQueue.indexOf(action)
        self.SETTINGS.progressQueue.splice(index, 1)
        self.toggleCursorProgress()
    }

    this.emptyProgressQueue = function () {
        self.SETTINGS.progressQueue = []
        self.toggleCursorProgress()
    }

    this.toggleCursorProgress = function () {
        var body = $('body')
        if(self.SETTINGS.progressQueue.length > 0) {
            body.css('cursor', 'wait')
        } else {
            body.css('cursor', 'inherit')
        }
    }

}
var WindowManager = new _WindowManager()
