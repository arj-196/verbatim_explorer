var LCONFIG = {
    DIR: {
        HEIGHT: 200
    },
    CONTAINER: {}
}

$(document).ready(function () {
    // menu
    if (!_.isUndefined(user)) {
        $('#menu-right').html(
            TEMPLATE.MENU.USER
                .replace('$USERNAME$', user.username)
        )
    }

    $('.ui.sticky').sticky()
    $('.popup-trigger').popup()
    $('.modal').modal()
    $('.dropdown').dropdown()

    var modalNewDir = $('#modal-new-directory')
    modalNewDir.find('.form').find('.button')
        .click(UserWorkspace.addDirectory)
    modalNewDir.find('input').on('keydown', function (e) {
        if (e.which === 13 || e.keyCode === 13) { // if is enter
            UserWorkspace.addDirectory()
        }
    })

    $('#edit-topic-trigger').click(openModalTopic)

    LCONFIG.CONTAINER = {
        LISTDIR: $('#list-dir'),
        MODAL: {
            NEWDIR: $('#modal-new-directory'),
            EDITTOPIC: $('#modal-topic')
        },
    }
    LCONFIG.STATS = {
        PROJECT: {
            INFO: $('#project-info'),
            TOPIC: $('#project-chart-topics'),
            TEAM: $('#project-chart-teams'),
            USER: $('#project-chart-users'),
        }
    }

    UserWorkspace.init()
    getDropdownContents()

    // test
    //setTimeout(function () {
    //    $('.project').first().click()
    //    //setTimeout(function () {
    //    //    $('#edit-topic-trigger').click()
    //    //}, 200)
    //}, 1000)
})


var onDropDirectory = UserWorkspace.onDropDirectory

function addEventDroppable() {
    $.each($('.container-dir'), function () {
        var self = $(this)
        if (_.isUndefined(self.data().droppable)) {
            self.data('droppable', true)
            self.find('.list').droppable({
                drop: onDropDirectory
            })
        }
    })
}

function addEventDraggable() {
    $.each($('.dataset'), function () {
        var self = $(this)
        if (_.isUndefined(self.data().draggable)) {
            self.data('draggable', true)
                .draggable({snap: false, revert: true, containment: "#list-dir"})
        }
    })
}

function adjustDefaultDimensions() {
    adjustHeightDirectory()
}

function adjustHeightDirectory() {
    $.each($('.container-dir'), function () {
        var dir = $(this)
        var height = dir.find('.list-topic').height()
        dir.height(height + 200)

        // DEPRECATED DIRECTORIES
        //// adjust directory height
        //var count = dir.find('.dataset').length
        //var height = LCONFIG.DIR.HEIGHT
        //var heightdataset = $('.dataset').first().height() * 2
        //dir.height(height + (count * heightdataset))
    })
}

var addDirectory = function () {
    LCONFIG.CONTAINER.MODAL.NEWDIR.find('.error-message').text("")
    LCONFIG.CONTAINER.MODAL.NEWDIR.modal('show')
}

var onClickDataSet = function (o) {
    var self = $(o)
    API.DASHBOARD.getTopicDetails(self.data().topic)
        .then(function (r) {
            UserWorkspace.showTopicDetails(r)
        })
}

var onClickProject = function (o) {
    var self = $(o)
    UserWorkspace.showProjectStats(self.data().project)
}

var openModalTopic = function () {
    ModalTopic.open($('.topic-details').find('.topic-title').data().topic)
}

var getDropdownContents = function () {
    var container
    API.DASHBOARD.getUsersAndOrganizationsAndProjects()
        .then(function (r) {
            container = $('#modal-topic').find('select[name="organizations"]')
            r.organizations.forEach(function (o) {
                container.append(
                    TEMPLATE.DASHBOARD.FORM.ITEM
                        .replace('$NAME$', o.name)
                        .replace('$VALUE$', o._id)
                )
            })
            container = $('#modal-topic').find('select[name="users"]')
            r.users.forEach(function (o) {
                container.append(
                    TEMPLATE.DASHBOARD.FORM.ITEM
                        .replace('$NAME$', o.name)
                        .replace('$VALUE$', o._id)
                )
            })

            // DEPRECATE CHOOSING PROJECTS
            //container = $('#modal-topic').find('select[name="project"]')
            //r.projects.forEach(function (o) {
            //    container.append(
            //        TEMPLATE.DASHBOARD.FORM.ITEM
            //            .replace('$NAME$', o.name)
            //            .replace('$VALUE$', o._id)
            //    )
            //})
        })
}

var submitFormTopic = ModalTopic.submitForm

var removeDirectory = UserWorkspace.removeDirectory
