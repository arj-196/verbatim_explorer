module.exports = {

    // landing
    home: "index.html",

    signin: "theme/signin.html",

    signup: "theme/signup.html",

    reset_password: "password_reset.html",

    // admin
    user_management: "user_management.html",

    // extraction progress
    extract_progress: "extract_progress.html",

    // dashboard
    dashboard: "dashboard.html",

    // verbatims
    verbatims: "verbatims.html",

    // tags
    tags: "tagged.html",

    // sentiment
    sentiment: "sentiment.html",

    // stats
    crossanalysis: "crossanalysis.html",

    // wordclouds
    wordclouds: "wordclouds.html",

    // heatmaps
    heatmaps: "heatmaps.html",

    // upload dataset
    upload_dataset: "uploaddataset.html",

    uploaded_dataset_file: "uploaddataset_stage2.html",

    // message
    message: "message.html",

    // error
    error: "error.html",

    radarly: "radarly.html",

    test: "prototype/proto.html"


};
