module.exports = {

    CONF: {
        ME: 'insightE@thedatastrategy.com',
    },

    RAW: {
        BODY: "",
    },

    RESETPASS: {
        SUBJECT: "InsightE: Reset Your Password",
        BODY: '<title xmlns:mc="http://www.w3.org/1999/xhtml">InsightE Mail</title>' +
        '' +
        '<style>' +
        '' +
        '    @import url(https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin);' +
        '' +
        '    .ReadMsgBody {' +
        '        width: 100%;' +
        '        background-color: #ffffff;' +
        '    }' +
        '' +
        '    .ExternalClass {' +
        '        width: 100%;' +
        '        background-color: #ffffff;' +
        '    }' +
        '' +
        '    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {' +
        '        line-height: 100%;' +
        '    }' +
        '' +
        '    html {' +
        '        width: 100%;' +
        '    }' +
        '' +
        '    body {' +
        '        -webkit-text-size-adjust: none;' +
        '        -ms-text-size-adjust: none;' +
        '        margin: 0;' +
        '        padding: 0;' +
        '    }' +
        '' +
        '    table {' +
        '        border-spacing: 0;' +
        '        border-collapse: collapse;' +
        '        table-layout: fixed;' +
        '        margin: 0 auto;' +
        '    }' +
        '' +
        '    table table table {' +
        '        table-layout: auto;' +
        '    }' +
        '' +
        '    img {' +
        '        display: block !important;' +
        '        over-flow: hidden !important;' +
        '    }' +
        '' +
        '    table td {' +
        '        border-collapse: collapse;' +
        '    }' +
        '' +
        '    .yshortcuts a {' +
        '        border-bottom: none !important;' +
        '    }' +
        '' +
        '    img:hover {' +
        '        opacity: 0.9 !important;' +
        '    }' +
        '' +
        '    a {' +
        '        color: #f69679;' +
        '        text-decoration: none;' +
        '    }' +
        '' +
        '    /*Responsive*/' +
        '    @media only screen and (max-width: 640px) {' +
        '        body {' +
        '            width: auto !important;' +
        '        }' +
        '' +
        '        table[class="table-inner"] {' +
        '            width: 90% !important;' +
        '        }' +
        '' +
        '        table[class="table-full"] {' +
        '            width: 100% !important;' +
        '            text-align: center !important;' +
        '        }' +
        '' +
        '        /* Image */' +
        '        img[class="img1"] {' +
        '            width: 100% !important;' +
        '            height: auto !important;' +
        '        }' +
        '    }' +
        '' +
        '    @media only screen and (max-width: 479px) {' +
        '        body {' +
        '            width: auto !important;' +
        '        }' +
        '' +
        '        table[class="table-inner"] {' +
        '            width: 90% !important;' +
        '        }' +
        '' +
        '        table[class="table-full"] {' +
        '            width: 100% !important;' +
        '            text-align: center !important;' +
        '        }' +
        '' +
        '        /* image */' +
        '        img[class="img1"] {' +
        '            width: 100% !important;' +
        '            height: auto !important;' +
        '        }' +
        '    }' +
        '</style>' +
        '' +
        '' +
        '<!-- Editor Frame -->' +
        '<div id="frame" class="ui-sortable">' +
        '    <div class="parentOfBg">' +
        '        <table data-thumb=""' +
        '               data-module="header" data-bgcolor="Main BG" align="center" bgcolor="#f3f6fa" width="100%"' +
        '               border="0" cellspacing="0" cellpadding="0" class="currentTable">' +
        '            <tbody>' +
        '            <tr>' +
        '                <td align="center">' +
        '                    <table align="center" class="table-inner" width="500" border="0" cellspacing="0"' +
        '                           cellpadding="0">' +
        '                        <tbody>' +
        '                        <tr>' +
        '                            <td height="25"></td>' +
        '                        </tr>' +
        '                        <tr>' +
        '                            <td align="center" style="line-height: 0px;">' +
        '                                <!--<img editable="" label="logo"-->' +
        '                                <!--data-crop="false"-->' +
        '                                <!--mc:edit="noti-5-1"-->' +
        '                                <!--style="display:block; line-height:0px; font-size:0px; border:0px;width:250px;margin-bottom:-20px;"-->' +
        '                                <!--src="http://thedatastrategy.com/images/tds-logo.png"-->' +
        '                                <!--alt="img">-->' +
        '                            </td>' +
        '                        </tr>' +
        '                        <tr>' +
        '                            <td height="25"></td>' +
        '                        </tr>' +
        '                        </tbody>' +
        '                    </table>' +
        '                </td>' +
        '            </tr>' +
        '            </tbody>' +
        '        </table>' +
        '    </div>' +
        '    <table data-thumb=""' +
        '           data-module="main-content" data-bgcolor="Main BG" align="center" bgcolor="#f3f6fa" width="100%"' +
        '           border="0" cellspacing="0" cellpadding="0" class="">' +
        '        <tbody>' +
        '        <tr>' +
        '            <td align="center">' +
        '                <table data-bgcolor="Top Content BG" data-bg="Top Content BG"' +
        '                       data-border-bottom-color="Main Content Line" bgcolor="#555555"' +
        '                       background="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2015/11/04/2JvhmI0DQ9GsrzTPgCnARoyu/Notification-5/images/container-bg.png"' +
        '                       style="border-top-left-radius:6px;border-top-right-radius:6px; background-size:100% auto; background-repeat:repeat-x; border-bottom:2px solid #f69679;background:#1b1c1d;"' +
        '                       width="500" border="0" align="center" cellpadding="0" cellspacing="0"' +
        '                       class="table-inner">' +
        '                    <tbody>' +
        '                    <tr>' +
        '                        <td height="50"></td>' +
        '                    </tr>' +
        '                    <tr>' +
        '                        <td align="center">' +
        '                            <table class="table-inner" align="center" width="400" border="0" cellspacing="0"' +
        '                                   cellpadding="0">' +
        '                                <!--title-->' +
        '                                <tbody>' +
        '                                <tr>' +
        '                                    <td data-link-style="text-decoration:none; color:#ffffff;"' +
        '                                        data-link-color="Main Text" data-color="Main Text" data-size="Headline"' +
        '                                        mc:edit="noti-5-2" align="center"' +
        '                                        style="font-family: \'Century Gothic\', Arial, sans-serif; color:#ffffff; font-size:28px;font-weight: bold; letter-spacing: 5px;">' +
            '                                        <singleline label="title">' +
        '                                            <span style="font-family: Lato,\'Helvetica Neue\',Arial,Helvetica,sans-serif;">' +
            '                                            <!--InsightE PlatForm-->' +
        '                                            Insight<span style="color:#58a5ff;">E</span> Plat<span' +
        '                                                    style="color:#58a5ff;">F</span>orm' +
        '                                            </span>' +
        '                                        </singleline>' +
        '                                    </td>' +
        '                                </tr>' +
        '                                <!--end title-->' +
        '                                <tr>' +
        '                                    <td align="center">' +
        '                                        <table width="50" border="0" align="center" cellpadding="0"' +
        '                                               cellspacing="0">' +
        '                                            <tbody>' +
        '                                            <tr>' +
        '                                                <td data-border-bottom-color="Dash" height="20"' +
        '                                                    style="border-bottom:2px solid #f69679;"></td>' +
        '                                            </tr>' +
        '                                            </tbody>' +
        '                                        </table>' +
        '                                    </td>' +
        '                                </tr>' +
        '                                <tr>' +
        '                                    <td height="15"></td>' +
        '                                </tr>' +
        '                                <!--content-->' +
        '                                <tr>' +
        '                                    <!--<td data-link-style="text-decoration:none; color:#ffffff;"-->' +
        '                                    <!--data-link-color="Main Text" data-color="Main Text" data-size="Content"-->' +
        '                                    <!--mc:edit="noti-5-3" align="center"-->' +
        '                                    <!--style="font-family: \'Century Gothic\', Arial, sans-serif; color:#ffffff; font-size:18px; line-height: 28px;">-->' +
            '                                    <!--<multiline label="content">-->' +
        '                                    <!--</multiline>-->' +
        '                                    <!--</td>-->' +
        '                                </tr>' +
        '                                <!--end content-->' +
        '                                <tr>' +
        '                                    <!--<td height="30"></td>-->' +
        '                                </tr>' +
        '                                <tr>' +
        '                                    <!--image-->' +
        '                                    <td align="center" style="line-height: 0px;">' +
        '                                        <img editable="" label="image"' +
        '                                             data-crop="false"' +
        '                                             src="http://thedatastrategy.com/images/tds-logo.png"' +
        '                                             alt="img" width="100%"' +
        '                                             class="img1"' +
        '                                             style="display:block; line-height:0px; font-size:0px; border:0px;width:300px;"' +
        '                                             mc:edit="noti-5-4">' +
        '                                    </td>' +
        '                                    <!--end image-->' +
        '                                </tr>' +
        '                                <tr>' +
        '                                    <td height="40"></td>' +
        '                                </tr>' +
        '                                </tbody>' +
        '                            </table>' +
        '                        </td>' +
        '                    </tr>' +
        '                    </tbody>' +
        '                </table>' +
        '            </td>' +
        '        </tr>' +
        '        </tbody>' +
        '    </table>' +
        '    <table data-thumb=""' +
        '           data-module="cta" data-bgcolor="Main BG" align="center" bgcolor="#f3f6fa" width="100%" border="0"' +
        '           cellspacing="0" cellpadding="0" class="">' +
        '        <tbody>' +
        '        <tr>' +
        '            <td align="center">' +
        '                <table data-bgcolor="CTA BG" bgcolor="#FFFFFF"' +
        '                       style="border-bottom-left-radius:6px;border-bottom-right-radius:6px;" align="center"' +
        '                       class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">' +
        '                    <tbody>' +
        '                    <tr>' +
        '                        <td height="25"></td>' +
        '                    </tr>' +
        '                    <tr>' +
        '                        <td align="center">' +
        '                            <table align="center" class="table-inner" width="420" border="0" cellspacing="0"' +
        '                                   cellpadding="0">' +
        '                                <!--content-->' +
        '                                <tbody>' +
        '                                <tr>' +
        '                                    <td data-link-style="text-decoration:none; color:#f69679;"' +
        '                                        data-link-color="Content" data-size="Content" data-color="Content"' +
        '                                        mc:edit="noti-5-5" align="center"' +
        '                                        style="font-family: \'Century Gothic\', Arial, sans-serif; color:#7f8c8d; font-size:14px; line-height: 28px;">' +
            '                                        <multiline label="content">' +
        '                                            $CONTENT$' +
        '                                        </multiline>' +
        '                                    </td>' +
        '                                </tr>' +
        '                                <!--end content-->' +
        '                                <tr>' +
        '                                    <td height="25"></td>' +
        '                                </tr>' +
        '                                <!--button-->' +
        '                                <!--<tr>-->' +
        '                                <!--<td align="center">-->' +
        '                                <!--&lt;!&ndash;left&ndash;&gt;-->' +
        '                                <!--<table width="197" border="0" align="left" cellpadding="0"-->' +
        '                                <!--cellspacing="0" class="table-full">-->' +
        '                                <!--<tbody>-->' +
        '                                <!--<tr>-->' +
        '                                <!--<td align="right">-->' +
        '                                <!--<table data-border-color="Button Border" class="table-full"-->' +
        '                                <!--border="0" align="right" cellpadding="0"-->' +
        '                                <!--cellspacing="0" style="border:2px solid #f69679;">-->' +
        '                                <!--<tbody>-->' +
        '                                <!--<tr>-->' +
        '                                <!--<td data-link-style="text-decoration:none; color:#f69679;"-->' +
        '                                <!--data-link-color="Content" data-size="Button"-->' +
        '                                <!--mc:edit="noti-5-6" height="40" align="center"-->' +
        '                                <!--style="font-family: \'Open sans\', Arial, sans-serif; color:#f69679; font-size:13px;padding-left: 20px;padding-right: 20px;">-->' +
            '                                <!--<a href="#">-->' +
        '                                <!--<singleline label="button-1">Register Now-->' +
        '                                <!--</singleline>-->' +
        '                                <!--</a>-->' +
        '                                <!--</td>-->' +
        '                                <!--</tr>-->' +
        '                                <!--</tbody>-->' +
        '                                <!--</table>-->' +
        '                                <!--</td>-->' +
        '                                <!--</tr>-->' +
        '                                <!--</tbody>-->' +
        '                                <!--</table>-->' +
        '                                <!--&lt;!&ndash;end left&ndash;&gt;-->' +
        '                                <!--&lt;!&ndash;Space&ndash;&gt;-->' +
        '                                <!--<table width="1" border="0" cellpadding="0" cellspacing="0"-->' +
        '                                <!--align="left">-->' +
        '                                <!--<tbody>-->' +
        '                                <!--<tr>-->' +
        '                                <!--<td height="25"-->' +
        '                                <!--style="font-size: 0;line-height: 0px;border-collapse: collapse;">-->' +
        '                                <!--<p style="padding-left: 24px;">&nbsp;</p>-->' +
        '                                <!--</td>-->' +
        '                                <!--</tr>-->' +
        '                                <!--</tbody>-->' +
        '                                <!--</table>-->' +
        '                                <!--&lt;!&ndash;End Space&ndash;&gt;-->' +
        '                                <!--&lt;!&ndash;right&ndash;&gt;-->' +
        '                                <!--<table width="197" border="0" align="right" cellpadding="0"-->' +
        '                                <!--cellspacing="0" class="table-full">-->' +
        '                                <!--<tbody>-->' +
        '                                <!--<tr>-->' +
        '                                <!--<td align="left">-->' +
        '                                <!--<table data-border-color="Button Border" class="table-full"-->' +
        '                                <!--border="0" align="left" cellpadding="0"-->' +
        '                                <!--cellspacing="0" style=" border:2px solid #f69679;">-->' +
        '                                <!--<tbody>-->' +
        '                                <!--<tr>-->' +
        '                                <!--<td data-link-style="text-decoration:none; color:#f69679;"-->' +
        '                                <!--data-link-color="Content" data-size="Button"-->' +
        '                                <!--mc:edit="noti-5-7" height="40" align="center"-->' +
        '                                <!--style="font-family: \'Open sans\', Arial, sans-serif; color:#f69679; font-size:13px; padding-left: 20px;padding-right: 20px;">-->' +
            '                                <!--<a href="#">-->' +
        '                                <!--<singleline label="button-2">More Detail-->' +
        '                                <!--</singleline>-->' +
        '                                <!--</a>-->' +
        '                                <!--</td>-->' +
        '                                <!--</tr>-->' +
        '                                <!--</tbody>-->' +
        '                                <!--</table>-->' +
        '                                <!--</td>-->' +
        '                                <!--</tr>-->' +
        '                                <!--</tbody>-->' +
        '                                <!--</table>-->' +
        '                                <!--&lt;!&ndash;end right&ndash;&gt;-->' +
        '                                <!--</td>-->' +
        '                                <!--</tr>-->' +
        '                                <!--end button-->' +
        '                                </tbody>' +
        '                            </table>' +
        '                        </td>' +
        '                    </tr>' +
        '                    <tr>' +
        '                        <td height="35"></td>' +
        '                    </tr>' +
        '                    </tbody>' +
        '                </table>' +
        '            </td>' +
        '        </tr>' +
        '        </tbody>' +
        '    </table>' +
        '    <table data-thumb=""' +
        '           data-module="footer" data-bgcolor="Main BG" width="100%" border="0" align="center" cellpadding="0"' +
        '           cellspacing="0" bgcolor="#f3f6fa" class="">' +
        '        <tbody>' +
        '        <tr>' +
        '            <td align="center">' +
        '                <table align="center" class="table-inner" width="500" border="0" cellspacing="0"' +
        '                       cellpadding="0">' +
        '                    <tbody>' +
        '                    <tr>' +
        '                        <td height="20"></td>' +
        '                    </tr>' +
        '                    <tr>' +
        '                        <td data-link-style="text-decoration:underline; color:#f95759;"' +
        '                            data-link-color="Preference" data-size="Preference" data-color="Preference"' +
        '                            mc:edit="noti-5-8" align="center"' +
        '                            style="font-family: \'Open sans\', Arial, sans-serif; color:#95a5a6; font-size:12px;font-style: italic;">' +
            '                            <!--Try view on your-->' +
        '                            <!--<webversion>Browser</webversion>-->' +
        '                            <!--or-->' +
        '                            <!--<unsubscribe>Unsubscribe.</unsubscribe>-->' +
        '                        </td>' +
        '                    </tr>' +
        '                    <tr>' +
        '                        <td height="10"></td>' +
        '                    </tr>' +
        '                    <tr>' +
        '                        <td align="center">' +
        '                            <table border="0" align="center" cellpadding="0" cellspacing="0">' +
        '                                <!--social-->' +
        '                                <tbody>' +
        '                                <tr>' +
        '                                    <td align="center" style="line-height: 0px;">' +
        '                                        <a href="https://www.facebook.com/thedatastrategy/">' +
        '                                            <img editable="" label="social-1" data-crop="false"' +
        '                                                 mc:edit="noti-5-9"' +
        '                                                 style="display:block; line-height:0px; font-size:0px; border:0px;padding-left:5px;padding-right:5px;"' +
        '                                                 src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2015/11/04/2JvhmI0DQ9GsrzTPgCnARoyu/Notification-5/images/facebook.png"' +
        '                                                 alt="img">' +
        '                                        </a>' +
        '                                    </td>' +
        '                                    <td align="center" style="line-height: 0px;">' +
        '                                        <a href="https://twitter.com/thedatastrategy">' +
        '                                            <img editable="" label="social-2" data-crop="false"' +
        '                                                 mc:edit="noti-5-10"' +
        '                                                 style="display:block; line-height:0px; font-size:0px; border:0px;padding-left:5px;padding-right:5px;"' +
        '                                                 src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2015/11/04/2JvhmI0DQ9GsrzTPgCnARoyu/Notification-5/images/twitter.png"' +
        '                                                 alt="img">' +
        '                                        </a>' +
        '                                    </td>' +
        '                                    <td align="center" style="line-height: 0px;">' +
        '                                        <a href="https://www.linkedin.com/company/10169385?trk=vsrp_companies_res_name&trkInfo=VSRPsearchId%3A1346856921457808036748%2CVSRPtargetId%3A10169385%2CVSRPcmpt%3Aprimary">' +
        '                                            <img editable="" label="social-3" data-crop="false"' +
        '                                                 mc:edit="noti-5-11"' +
        '                                                 style="display:block; line-height:0px; font-size:0px; border:0px;padding-left:5px;padding-right:5px;"' +
        '                                                 src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2015/11/04/2JvhmI0DQ9GsrzTPgCnARoyu/Notification-5/images/linkedin.png"' +
        '                                                 alt="img">' +
        '                                        </a>' +
        '                                    </td>' +
        '                                </tr>' +
        '                                <!--end social-->' +
        '                                </tbody>' +
        '                            </table>' +
        '                        </td>' +
        '                    </tr>' +
        '                    <tr>' +
        '                        <td height="25"></td>' +
        '                    </tr>' +
        '                    </tbody>' +
        '                </table>' +
        '            </td>' +
        '        </tr>' +
        '        </tbody>' +
        '    </table>' +
        '</div>'
    }

}