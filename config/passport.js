var db = require('../lib/dbconnection')
var LocalStrategy = require('passport-local').Strategy
var UTILS = require('../lib/utils/common')

module.exports = function (passport) {

    // serialize user
    passport.serializeUser(function (user, done) {
        done(null, user._id)
    })

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        db.client.users.findOne({_id: id}, function (err, user) {
            done(err, user)
        })
    })

    // local signup
    passport.use('local-signup', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, username, password, done) {
            process.nextTick(function () {
                // check to see if username already exists
                db.client.users.findOne({_id: username}, function (err, user) {
                    if (err)
                        return done(err)
                    else {
                        if(user){
                            // username already taken
                            return done(null, false, req.flash('signupMessage', 'This username is already taken'))
                        } else {
                            // create new user
                            var newUser = {
                                _id: username,
                                password: UTILS.generateHash(password)
                            }
                            db.client.users.insert(newUser, function (err) {
                                if(err)
                                    throw err
                                return done(null, newUser)
                            })
                        }
                    }
                })
            })
        }
    ))

    // local signin
    passport.use('local-login', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    },
        function (req, username, password, done) {
            db.client.users.findOne({_id: username}, function (err, user) {
                if(err)
                    return done(err)
                if(!user){
                    // if no user found
                    return done(null, false, req.flash('loginMessage', 'No user found by this username!'))
                } else {
                    if(!UTILS.ifValidPassword(password, user.password))
                        return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'))
                    else {
                        // all good
                        return done(null, user)
                    }
                }
            })
        }
    ))

}