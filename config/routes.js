/**
 * Contains Map of Route to Controller
 *
 */
module.exports = {

    prefix: {
        restricted: "/dashboard/*",

        admin: "/admin/*",
    },

    // defaults
    root: "/",

    logout: "/logout",

    signup: "/signup",

    reset_password: "/password/reset",

    do_reset_password: "/password/reset/do/:USER/:ACCESSCODE",

    log_error: "/log/error",

    //
    // utils
    //
    utils_hash: "/utils/hash",

    utils_history: "/utils/history",

    //
    // Admin
    //
    user_management: "/admin/usermanagement/",

    admin_get_user_by_id: "/admin/get/user/by/id",

    admin_post_form_user: "/admin/post/form/user",

    //
    // Extraction Progress
    //
    extract_progress: "/dashboard/extraction/status",

    extract_progress_fetch_datasets: "/dashboard/extraction/get/datasets",

    extract_progress_get_dataset_progress: "/dashboard/extraction/get/dataset/progress",

    //
    // Dashboards
    //
    dashboard: "/dashboard/home",

    get_user: "/dashboard/d/user",

    get_user_topics: "/dashboard/d/user/topics",

    get_user_topics_edit: "/dashboard/d/user/topics/edit",

    get_user_topics_count: "/dashboard/d/user/topics/count",

    get_user_topic_details: "/dashboard/d/user/topic/detail",

    get_users_and_organizations: "/dashboard/d/user/organizations",

    update_topic_details: "/dashboard/d/user/topic/update",

    get_projects: "/dashboard/d/user/project",

    get_project_details: "/dashboard/d/user/project/detail",

    manage_project: "/dashboard/d/user/project/manage",

    //
    // Upload dataset
    //
    upload_dataset: "/dashboard/upload/dataset",

    upload_dataset_file: "/dashboard/upload/dataset/file/:project/:title/:language",

    upload_dataset_request_sample: "/dashboard/upload/dataset/request/sample",

    upload_dataset_done: "/dashboard/upload/dataset/done/:project/:title/:language",

    //
    // Radarly dashboard
    //

    radarly: "/dashboard/radarly",

    radarly_upload: "/dashboard/radarly/upload",

    // radarly_upload: "/dashboard/radarly/:project/:title/:language/:radarly_project/:radarly_dashboard/:radarly_focu",

    get_radarly_projects: '/dashboard/radarly/user/project',

    get_radarly_dashboards: '/dashboard/radarly/user/dashboard',

    get_radarly_focuses: '/dashboard/radarly/user/focus',

    //
    // Verbatims
    //

    // verbatims
    verbatims: "/dashboard/verbatims",

    verbatim_get_topics: "/dashboard/verbatims/get/topics",

    verbatim_get_topic_count: "/dashboard/verbatims/get/topics/count",

    verbatim_get_verbatim_by_id: "/dashboard/verbatims/get/verbatim/by/id",

    verbatim_get_verbatims: "/dashboard/verbatims/get/verbatims",

    // favorites DEPRECATED
    verbatims_support_favorites: "/dashboard/verbatims/support/tagged",

    // wordcloud
    verbatim_wordcloud_command_search: "/dashboard/verbatims/wordcloud/search",

    verbatims_wordcloud_keyword: "/dashboard/verbatims/wordcloud/keyword",

    verbatims_wordcloud_term: "/dashboard/verbatims/wordcloud/term",

    verbatims_wordcloud_concept: "/dashboard/verbatims/wordcloud/concept",

    verbatims_wordcloud_nouns: "/dashboard/verbatims/wordcloud/noun",

    verbatims_wordcloud_verbs: "/dashboard/verbatims/wordcloud/verb",

    verbatims_wordcloud_adjectives: "/dashboard/verbatims/wordcloud/adjective",

    // search
    verbatim_search_keyword: "/dashboard/verbatims/search/keyword",

    verbatim_search_for_keywords: "/dashboard/verbatims/search/only/keyword",

    verbatim_search_for_keywords_regex: "/dashboard/verbatims/search/only/keyword/regex",

    verbatim_search_for_concepts: "/dashboard/verbatims/search/only/concepts",

    verbatims_search_concept: "/dashboard/verbatims/search/concept",

    verbatims_search_noun: "/dashboard/verbatims/search/noun",

    verbatims_search_verb: "/dashboard/verbatims/search/verb",

    verbatims_search_adjective: "/dashboard/verbatims/search/adjective",

    verbatims_search_tag: "/dashboard/verbatims/search/tag",

    verbatims_search_for_terms: "/dashboard/verbatims/search/only/term",

    verbatims_search_for_tag: "/dashboard/verbatims/search/only/tag",

    verbatims_search_terms: "/dashboard/verbatims/search/term",

    verbatims_search_for_terms_regex: "/dashboard/verbatims/search/only/terms/regex",

    // autocomplete tag
    verbatims_tag_autocomplete: "/dashboard/verbatims/autocomplete/tag",

    // plugin concept
    verbatims_plugins_concepts_getconcepts: "/dashboard/verbatims/plugins/concepts",

    // plugin keyword
    verbatims_plugins_keywords_getkeywords: "/dashboard/verbatims/plugins/keywords",

    // plugin tags
    verbatims_plugins_tags_gettags: "/dashboard/verbatims/plugins/tags",

    verbatims_plugins_tags_support: "/dashboard/verbatims/plugins/tags/support",

    // get ner
    verbatims_plugins_ners_getner: "/dashboard/verbatims/plugins/ner",

    // stats
    verbatim_plugins_stats_emotion: "/dashboard/verbatims/plugins/stats/emotion",

    verbatim_plugins_stats_wordcloud: "/dashboard/verbatims/plugins/stats/wordcloud",

    verbatim_plugins_stats_ner: "/dashboard/verbatims/plugins/stats/ner",

    // export data
    verbatim_plugins_stats_exportdataset: "/dashboard/verbatims/plugins/stats/exportdataset",

    verbatim_plugins_stats_exportdataset_downloading: "/dashboard/verbatims/plugins/stats/exportdataset/download/now/:file",

    // stats search
    verbatim_plugins_stats_search_set_to_default: "/dashboard/verbatims/plugins/stats/search/default",

    verbatim_plugins_stats_search_addlevel: "/dashboard/verbatims/plugins/stats/search/addlevel",

    verbatim_plugins_stats_search_removelevel: "/dashboard/verbatims/plugins/stats/search/removelevel",

    verbatim_plugins_stats_search_loadmore: "/dashboard/verbatims/plugins/stats/search/loadmore",

    // insights
    verbatim_plugins_insight_get_insights: "/dashboard/verbatims/plugins/insights/get/insights",

    verbatim_plugins_insight_get_insight_byid: "/dashboard/verbatims/plugins/insights/get/insight/byid",

    verbatim_plugins_insight_create: "/dashboard/verbatims/plugins/insights/create",

    verbatim_plugins_insight_edit: "/dashboard/verbatims/plugins/insights/edit",

    verbatim_plugins_insight_remove: "/dashboard/verbatims/plugins/insights/remove",

    verbatim_plugins_insight_add_tags: "/dashboard/verbatims/plugins/insights/add/tags",

    verbatim_plugins_insight_remove_tags: "/dashboard/verbatims/plugins/insights/remove/tags",

    verbatim_plugins_insight_get_verbatims_tagged: "/dashboard/verbatims/plugins/insights/get/verbatims/tagged",

    verbatim_plugins_insight_get_verbatims_predicted: "/dashboard/verbatims/plugins/insights/get/verbatims/predicted",

    verbatim_plugins_insight_set_predicted_relevance: "/dashboard/verbatims/plugins/insights/set/predicted/relevance",

    verbatim_plugins_insight_if_compiled: "/dashboard/verbatims/plugins/insights/if/compiled",

    verbatim_plugins_insight_get_s_index: "/dashboard/verbatims/plugins/insights/get/sindex",

    verbatim_plugins_insight_get_features: "/dashboard/verbatims/plugins/insights/get/features",

    verbatim_plugins_insight_remove_feature: "/dashboard/verbatims/plugins/insights/remove/feature",

    verbatim_plugins_insight_recompile_modal: "/dashboard/verbatims/plugins/insights/recompile/modal",

    // verbatim cleaner
    verbatim_plugins_cleaner_search: "/dashboard/verbatims/plugin/cleaner/search",

    verbatim_plugins_cleaner_disable: "/dashboard/verbatims/plugin/cleaner/disable",

    verbatim_plugins_cleaner_get_history: "/dashboard/verbatims/plugin/cleaner/get/history",

    verbatim_plugins_cleaner_history_undo_change: "/dashboard/verbatims/plugin/cleaner/history/undochange",

    // autocomplete insight
    verbatims_insight_autocomplete: "/dashboard/verbatims/autocomplete/insight",

    //
    // search aggregate
    //
    verbatim_searchaggregate_getqueue: '/verbatims/plugins/aggregate/search/getsearchlist',

    //
    // tags
    //

    tags: "/dashboard/tags",

    tags_get_dataset: "/dashboard/tags/fetch/dataset",

    tags_get_tags: "/dashboard/tags/fetch/tags",

    tags_get_tags__associated_to_tag: "/dashboard/tags/fetch/associated/tags",

    tags_get_verbatims_for_tag: "/dashboard/tags/fetch/verbatims",

    tags_plugin_export_tags: "/dashboard/verbatims/plugins/export/tags",

    tags_plugin_export_insights: "/dashboard/verbatims/plugins/export/insights",

    tags_plugin_export_download_file: "/dashboard/verbatims/plugins/export/download/:file",


    //
    // sentiment
    //
    sentiment: "/dashboard/sentiment",

    get_sentiment_statistics: "/dashboard/sentiment/get/statistics",

    get_verbatims_for_topic_and_polarity: "/dashboard/sentiment/get/verbatims/polarity",


    //
    // seletable multiple verbatims
    //
    get_similarities: '/multipleverbatims/similarity',

    //
    // wordclouds
    //

    // default:wordclouds
    wordclouds: "/dashboard/wordclouds",

    //
    // heatmaps
    //

    // default:heatmaps
    heatmaps: "/dashboard/heatmaps",

    heatmaps_get_global: "/dashboard/heatmap/get/global",

    //
    // cross analysis
    //
    crossanalysis: "/dashboard/crossanalysis",

    crossanalysis_wca: "/dashboard/ca/wca",

    crossanalysis_ica: "/dashboard/ca/ica",


    //
    // contact
    //
    contactus: "/contactus",

    // default:errorView
    error: "/err/",

    //---------------------------

    test: "/test"

};
