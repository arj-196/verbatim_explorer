/**
 * Environment Configurations
 *
 */
var _ = require('underscore')

var exitProgress = function (message) {
    console.error(message)
    process.exit()
}

var getHostAndPortFromUrl = function (urlString) {
    url = urlString.split(':')
    return {
        host: url[0],
        port: url[1]
    }
}

// verify if all env variables exists
var requiredEnvParams = [
    'ENV',
    'PORT',
    'MONGO_URL',
    'SUPPORT_URL',
    'SUPPORT_INSIGHT_URL',
    'CONCEPTNET_URL',
    'TMODEL_URL',
    'RADARLY_URL',
    'CLARIFIA_CRED',
    'DIR_DOWNLOAD',
    'DIR_UPLOAD',
    'SWIFT_CONFIG',
]
requiredEnvParams.forEach(function (paramKey) {
    if(_.isUndefined(process.env[paramKey]))
        exitProgress("ERROR CRITICAL: environment variable '" + paramKey + "' required!")
})

var deployEnv = process.env.ENV
console.log("Info: deploy envrionemnt:", deployEnv)

module.exports = {

    port: process.env.PORT,
    MODE: deployEnv,

    mongodb: {
        uri: process.env.MONGO_URL,
    },

    support: {
        host: getHostAndPortFromUrl(process.env.SUPPORT_URL).host,
        port: getHostAndPortFromUrl(process.env.SUPPORT_URL).port
    },

    supportInsight: {
        host: getHostAndPortFromUrl(process.env.SUPPORT_INSIGHT_URL).host,
        port: getHostAndPortFromUrl(process.env.SUPPORT_INSIGHT_URL).port
    },

    conceptnet: {
        host: getHostAndPortFromUrl(process.env.CONCEPTNET_URL).host,
        port: getHostAndPortFromUrl(process.env.CONCEPTNET_URL).port,
        version: '5.4'
    },

    tmodel: {
        host: getHostAndPortFromUrl(process.env.TMODEL_URL).host,
        port: getHostAndPortFromUrl(process.env.TMODEL_URL).port,
    },

    radarly: {
        uri: 'http://' + process.env.RADARLY_URL,
    },

    clarifai: {
        creds: {
            clientId: getHostAndPortFromUrl(process.env.CLARIFIA_CRED).host,
            clientSecret: getHostAndPortFromUrl(process.env.CLARIFIA_CRED).port,
        },

        dir: {
            storage: __dirname + '/../public/client/',
        }
    },

    images: {
        url_prefix: '/client'
    },

    mailer: {
        transport: 'smtp://hello@thedatastrategy.com:pdstds99@smtp.1and1.pl',
        errorReportAddress: 'insighte@thedatastrategy.com',
    },

    dirs: {
        download: process.env.DIR_DOWNLOAD,  // upload
        upload: process.env.DIR_UPLOAD,  // support
    },

    swiftConfig: JSON.parse(process.env.SWIFT_CONFIG)
}

console.log('Info: env variables', module.exports)