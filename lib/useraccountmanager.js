var _ = require('underscore'),
    Promise = require('bluebird'),
    db = require('./dbconnection'),
    UTILS = require('../lib/utils/common')
var Error = require('./error/error'),
    error = new Error();

/**
 * Manager responsible for User Account Management
 */
module.exports = {

    /**
     * Resetting password
     * @param email
     * @param resetUrl
     * @param next
     */
    resetPassword: function (email, resetUrl, next) {
        db.client.users.findOne({email: email}, function (err, User) {
            if (err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                next({s: false, m: {header: "Please try again", content: "We encountered some problem"}})
            } else {
                if (!User) {
                    next({s: false, m: {header: "No User found by this email"}})
                } else {
                    var accessCode = UTILS.generateHash(Date.now() + User._id + User.company + User.email)
                        .replace(new RegExp('/', 'g'), '_')
                    db.client.users.update({_id: User._id}, {$set: {accesscode: {pass: accessCode}}}, function (err) {
                        if (err) {
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                            next({s: false, m: {header: "Please try again", content: "We encountered some problem"}})
                        } else {
                            var url = resetUrl
                                .replace(':USER', User._id)
                                .replace(':ACCESSCODE', accessCode)
                            var mailBody = "Click on the following link to reset your password " +
                                "<a target='_blank' href='" + url + "'>RESETPASS</a>"
                            UTILS.sendMail(User.email, "RESETPASS", mailBody, function (r) {
                                if (r.s) {
                                    next({s: true})
                                } else {
                                    // TODO handle error
                                    next({
                                        s: false,
                                        m: {header: "Please try again", content: "We encountered some problem"}
                                    })
                                }
                            })
                        }
                    })
                }
            }
        })
    },

    /**
     * Authenticating reset password request
     * @param userid
     * @param accessCode
     * @param next
     */
    authenticateResetRequest: function (userid, accessCode, next) {
        db.client.users.findOne({_id: userid}, function (err, User) {
            if (err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                next({s: false, m: {header: "Please try again", content: "We encountered some problem"}})
            } else {
                if (!User) {
                    next({
                        s: false,
                        m: {header: "Something doesn't make sense", content: "No user found by name " + userid}
                    })
                } else {
                    if (_.isUndefined(User.accesscode) || _.isUndefined(User.accesscode.pass)) {
                        next({
                            s: false,
                            m: {
                                header: "Something doesn't make sense",
                                content: "User " + userid + " has not requested for a password reset"
                            }
                        })
                    } else {
                        if (User.accesscode.pass == accessCode) {
                            next({s: true})
                        } else {
                            next({
                                s: false,
                                m: {header: "Please try again", content: "This reset request has expired..."}
                            })
                        }
                    }
                }
            }
        })
    },

    /**
     * Changing password for user
     * @param userid
     * @param accesscode
     * @param newpass
     * @param next
     */
    changePassword: function (userid, accesscode, newpass, next) {
        this.authenticateResetRequest(userid, accesscode, function (r) {
            if (!r.s) {
                next(r)
            } else {
                var hashNewPass = UTILS.generateHash(newpass)
                db.client.users.update({_id: userid}, {$set: {password: hashNewPass}}, function (err) {
                    if (err) {
                        next({s: false})
                    } else {
                        db.client.users.update({_id: userid}, {$unset: {accesscode: ""}}, function (err) {
                            if (err) {
                                next({s: false})
                            } else {
                                next({s: true})
                            }
                        })
                    }
                })
            }
        })
    },

    /**
     * Get all users for a given company
     * @param company
     * @param next
     */
    getAllUsers: function (company, next) {
        db.client.users.find({company: company}).sort({role: -1, _id: 1}).toArray(function (err, users) {
            next(users)
        })
    },

    /**
     * Get user object given the user id
     * @param id
     * @param next
     */
    getUser: function (id, next) {
        db.client.users.findOne({_id: id}, function (err, User) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (User)
                    User.password = null
                next(User)
            }
        })
    },

    /**
     * Check if username is not already used
     * @param username
     * @param type
     * @param value
     * @param next
     */
    validateUserInfo: function (username, type, value, next) {
        var q = {_id: {$ne: username}}
        q[type] = value
        db.client.users.count(q, function (err, count) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                next(count == 0)
            }
        })
    },

    /**
     * Update user information
     * The data object requires the following fields
     * - username
     * - firstname
     * - lastname
     * - email
     * @param company
     * @param data
     * @param next
     */
    updateUser: function (company, data, next) {
        this.validateUserInfo(data.username, 'email', data.email, function (isValid) {
            if (!isValid) {
                next({s: false, m: "you must use a unique email"})
            } else {
                db.client.users.update({_id: data.username, company: company}, {
                    $set: {
                        email: data.email,
                        firstname: data.firstname,
                        lastname: data.lastname
                    }
                }, function (err) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        next({s: true})
                    }
                })
            }
        })
    },

    /**
     * Create new user
     * The data object requies the following fields
     * - username
     * - firstname
     * - lastname
     * - email
     * - password
     * @param company
     * @param data
     * @param next
     */
    newUser: function (company, data, next) {
        var self = this
        self.validateUserInfo("", "_id", data.username, function (isValid) {
            if (!isValid) {
                next({s: false, m: "you must use a unique username"})
            } else {
                self.validateUserInfo("", "email", data.email, function (isValid) {
                    if (!isValid) {
                        next({s: false, m: "you must use a unique email"})
                    } else {
                        // all good
                        var passHash = UTILS.generateHash(data.password)
                        data.password = passHash
                        data.company = company
                        data._id = data.username
                        db.client.users.insert(data, function (err) {
                            if (err)
                                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                            else {
                                next({s: true})
                            }
                        })
                    }
                })
            }
        })
    },

    /**
     * Remove user
     * @param company
     * @param data
     * @param next
     */
    removeUser: function (company, data, next) {
        db.client.users.remove({_id: data.id, company: company}, function (err) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                next({s: true})
            }
        })
    }

}