var _ = require('underscore'),
    Promise = require('bluebird'),
    db = require('./dbconnection')
var Error = require('./error/error'),
    error = new Error();

/**
 * Manager responsible for all User Related Tasks
 */
module.exports = {

    /**
     * Does nothing, just creates a Promise object for getting user
     * @param user
     */
    getUser: function (user) {
        return new Promise(function (resolve, reject) {
            resolve(user)
        })
    },

    /**
     * Get topics for a given username, projectid and the action
     * @param username
     * @param action dashboard|verbatim|sentiment|tagged|heatmap
     * @param projectid
     */
    getTopics: function (username, action, projectid) {
        return new Promise(function (resolve, reject) {
            db.indexes.settings.find({users: {$in: [username]}}).toArray(function (err, topics) {
                if (err) {
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    reject(err)
                } else {
                    // get project color
                    var projectList = _.uniq(_.pluck(topics, "project"))
                    db.client.projects.find({_id: {$in: projectList}}, {color: true}).toArray(function (err, projects) {
                        if (err)
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        else {
                            var projectColors = {}
                            projects.forEach(function (project) {
                                projectColors[project._id] = project.color
                            })
                            // get topics for current projectid
                            var projectTopicList = []
                            topics.forEach(function (topic) {
                                if (topic.project == projectid)
                                    projectTopicList.push(topic)
                            })

                            // format verbatims and send in response
                            if (action == "dashboard") {
                                db.indexes.verbatims.distinct("topic", function (err, verbatimTopics) {
                                    if (err)
                                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                    else {
                                        resolve(
                                            {
                                                topics: sortTopicsInDirectories(
                                                    projectTopicList, _.intersection(verbatimTopics, _.pluck(projectTopicList, "topic"))
                                                ),
                                                projectColors: projectColors
                                            }
                                        )
                                    }
                                })
                            } else if (action == "verbatim" || action == "sentiment" || action == "tagged") {
                                db.indexes.verbatims.distinct("topic", function (err, verbatimTopics) {
                                    if (err)
                                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                    else {
                                        resolve(
                                            {
                                                topics: sortTopicsInDirectories(
                                                    projectTopicList, _.intersection(verbatimTopics, _.pluck(projectTopicList, "topic"))
                                                ),
                                                projectColors: projectColors
                                            }
                                        )
                                    }
                                })
                                //} else if (action == "tagged") {
                                //    db.indexes.verbatimtags.distinct("topic", function (err, verbatimTopics) {
                                //        resolve(
                                //            {
                                //                topics: sortTopicsInDirectories(
                                //                    projectTopicList, _.intersection(verbatimTopics, _.pluck(projectTopicList, "topic"))
                                //                ),
                                //                projectColors: projectColors
                                //            }
                                //        )
                                //    })
                            } else if (action == "heatmap") {
                                db.indexes.heatmaps.distinct("topic", function (err, verbatimTopics) {
                                    if (err)
                                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                    else {
                                        resolve(
                                            {
                                                topics: sortTopicsInDirectories(
                                                    topics, _.intersection(verbatimTopics, _.pluck(topics, "topic"))
                                                ),
                                                projectColors: projectColors
                                            }
                                        )
                                    }
                                })
                            }
                            else {
                                resolve({})
                            }
                        }
                    })

                }
            })
        })
    },

    /**
     * Change the topic directory
     * @param username
     * @param data
     */
    moveTopic: function (username, data) {
        return new Promise(function (resolve, reject) {
            db.indexes.settings.update({
                users: username,
                topic: data.topic
            }, {$set: {dir: data.to}}, function (err) {
                if (err) {
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    reject(err)
                } else {
                    db.indexes.settings.findOne({
                        users: username,
                        topic: data.topic
                    }, function (err, topic) {
                        topic.name = getName(topic)
                        resolve({t: topic})
                    })
                }
            })
        })
    },

    /**
     * Getting total verbatim count for a list of topics
     * @param topics
     */
    getVerbatimCountForTopic: function (topics) {
        return new Promise(function (resolve, reject) {
            var countsToFetch = []
            topics.forEach(function (topic) {
                countsToFetch.push(db.indexes.verbatims.countAsync({topic: topic.topic}))
            })
            Promise.all(countsToFetch).then(function (counts) {
                var totalCount = 0
                counts.forEach(function (count) {
                    totalCount += count
                })
                resolve(totalCount)
            })
        })
    },

    /**
     * Get topic object given the topicname
     * @param topicname
     */
    getTopicDetails: function (topicname) {
        return new Promise(function (resolve, reject) {
            db.indexes.settings.findOne({topic: topicname}, function (err, topic) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    topic.name = getName(topic)
                    topic.users.sort()
                    resolve(topic)
                }
            })
        })
    },

    /**
     * Get all users, organizations and projects
     * @param user
     */
    getUsersAndOrganizationsAndProjects: function (user) {
        return new Promise(function (resolve, reject) {
            db.client.organizations.find({company: user.company}).toArray(function (err, organizations) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                for (var i = 0; i < organizations.length; i++) {
                    organizations[i].name = getName(organizations[i])
                }
                db.client.users.find({company: user.company}).toArray(function (err, users) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    for (var i = 0; i < users.length; i++) {
                        users[i].name = getName(users[i])
                    }
                    db.client.projects.find({company: user.company}).toArray(function (err, projects) {
                        if (err)
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        for (var i = 0; i < projects.length; i++) {
                            projects[i].name = getName(projects[i])
                        }
                        resolve({organizations: organizations, users: users, projects: projects})
                    })
                })
            })
        })
    },

    /**
     * Update topic details
     * The data object requires the following fields
     *  - organizations
     *  - users
     *  - name
     *  - project
     * @param user
     * @param data
     */
    updateTopicDetails: function (user, data) {
        return new Promise(function (resolve, reject) {
            // adding all users that belong to the organizations
            db.client.organizations.find({_id: {$in: data.organizations}}, {users: 1}).toArray(
                function (err, organizations) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        var userList = data.users
                        // adding owner user
                        if (userList.indexOf(user._id) == -1) {
                            userList.push(user._id)
                        }
                        organizations.forEach(function (o) {
                            o.users.forEach(function (user) {
                                if (userList.indexOf(user) == -1)
                                    userList.push(user)
                            })
                        })
                        db.indexes.settings.update({topic: data.topicid}, {
                            $set: {
                                organizations: data.organizations,
                                users: userList,
                                name: data.name,
                                project: data.project,
                            }
                        })
                        resolve(true)
                    }
                })
        })
    },

    /**
     * Get all projects for a user
     * @param user
     */
    getProjects: function (user) {
        return new Promise(function (resolve, reject) {
            db.client.projects.find({company: user.company}).toArray(function (err, projects) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    for (var i = 0; i < projects.length; i++) {
                        projects[i].name = getName(projects[i])
                    }
                    resolve(projects)
                }
            })
        })
    },

    /**
     * Get project object given it's id
     * @param user
     * @param projectid
     */
    getProjectDetails: function (user, projectid) {
        return new Promise(function (resolve, reject) {
            db.indexes.settings.find({project: projectid, company: user.company}).toArray(function (err, topics) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    // get topic verbatim count
                    // get number of topics per organizations
                    // get number of topics per user
                    var countsToFetch = [], orgsTopicCount = {}, usersTopicCount = {}
                    topics.forEach(function (topic) {
                        // topic count
                        countsToFetch.push(db.indexes.verbatims.countAsync({topic: topic.topic}))
                        // organization
                        if (!_.isUndefined(topic.organizations)) {
                            topic.organizations.forEach(function (org) {
                                if (_.isUndefined(orgsTopicCount[org])) {
                                    orgsTopicCount[org] = 1
                                } else {
                                    orgsTopicCount[org]++
                                }
                            })
                        }
                        // users
                        if (!_.isUndefined(topic.users)) {
                            topic.users.forEach(function (user) {
                                if (_.isUndefined(usersTopicCount[user])) {
                                    usersTopicCount[user] = 1
                                } else {
                                    usersTopicCount[user]++
                                }
                            })
                        }
                    })

                    Promise.all(countsToFetch).then(function (counts) {
                        // format topic counts
                        var topicsCountsList = [], totalCount = 0
                        for (var i = 0; i < topics.length; i++) {
                            if (counts[i] > 0) {
                                topicsCountsList.push({
                                    name: getName(topics[i]),
                                    frequency: counts[i]
                                })
                                totalCount += counts[i]
                            }
                        }
                        // format user counts
                        var userTopicCountsList = []
                        _.keys(usersTopicCount).forEach(function (username) {
                            userTopicCountsList.push({
                                name: username,
                                frequency: usersTopicCount[username]
                            })
                        })
                        // format orgs counts
                        var orgsTopicCountsList = []
                        _.keys(orgsTopicCount).forEach(function (org) {
                            orgsTopicCountsList.push({
                                name: org,
                                frequency: orgsTopicCount[org]
                            })
                        })

                        resolve({
                            topicsCount: topicsCountsList,
                            totalCount: totalCount,
                            orgsTopicCount: orgsTopicCountsList,
                            usersTopicCount: userTopicCountsList
                        })
                    })
                }
            })
        })
    },
}

function getName(topic) {
    var name
    if (_.isUndefined(topic.name)) {
        if (_.isUndefined(topic.topic))
            name = topic._id
        else
            name = topic.topic
    } else {
        name = topic.name
    }
    return name
}

function sortTopicsInDirectories(topics, acceptList) {
    var dirs = {
        unnamed: []
    }
    topics.forEach(function (topic) {
        if (_.indexOf(acceptList, topic.topic) != -1) {
            var name = getName(topic)
            topic.name = name
            if (_.isUndefined(topic.dir)) {
                dirs.unnamed.push(topic)
            } else {
                if (_.isUndefined(dirs[topic.dir])) {
                    dirs[topic.dir] = []
                }
                dirs[topic.dir].push(topic)
            }
        }
    })
    return dirs
}