var Promise = require('bluebird')
var db = require('./dbconnection')
var _ = require('underscore')
var Error = require('./error/error'),
    error = new Error();
var MAXVERBATIMS = 20


var BOUND = {
    VN: {MIN: -1, MAX: -0.3},
    N: {MIN: -0.3, MAX: -0.05},
    NU: {MIN: -0.05, MAX: 0.05},
    P: {MIN: 0.05, MAX: 0.3},
    VP: {MIN: 0.3, MAX: 1},
}

/**
 * Manager for Sentiment Dashboard
 */
module.exports = {

    /**
     * Get sentiment statistics for a given topic
     * @param topic
     * @param next
     */
    getStatisticsForTopic: function (topic, next) {
        function fetchCount(query) {
            return db.indexes.verbatims.countAsync(query)
        }

        Promise.join(
            fetchCount({topic: topic}),
            fetchCount(formatRangeQuery(topic, BOUND.VN.MIN, BOUND.VN.MAX)),
            fetchCount(formatRangeQuery(topic, BOUND.N.MIN, BOUND.N.MAX)),
            fetchCount(formatRangeQuery(topic, BOUND.NU.MIN, BOUND.NU.MAX)),
            fetchCount(formatRangeQuery(topic, BOUND.P.MIN, BOUND.P.MAX)),
            fetchCount(formatRangeQuery(topic, BOUND.VP.MIN, BOUND.P.MAX)),

            function (TCount, VNCount, NCount, NUCount, PCount, VPCount) {
                var rows = [
                    {name: "very positive", frequency: VPCount / TCount, color: "#1AC645", type: "VP", topic: topic},
                    {name: "positive", frequency: PCount / TCount, color: "#7AA184", type: "P", topic: topic},
                    {name: "neutral", frequency: NUCount / TCount, color: "#8F918F", type: "NU", topic: topic},
                    {name: "negative", frequency: NCount / TCount, color: "#A50016", type: "N", topic: topic},
                    {name: "very negative", frequency: VNCount / TCount, color: "#EA374F", type: "VN", topic: topic}
                ]

                var max = _.max(rows, function (d) {
                    return d.frequency
                })
                next({
                    counts: {
                        T: TCount,
                        VN: VNCount,
                        N: NCount,
                        NU: NUCount,
                        P: PCount,
                        VP: VPCount
                    },
                    rows: rows,
                    max: max
                })
            }
        )
    },

    /**
     * Get verbatims by polarity
     * @param topic
     * @param type
     * @param iteration
     * @param next
     */
    getVerbatimsByTopicAndPolarity: function (topic, type, iteration, next) {
        db.indexes.verbatims.find(formatRangeQuery(topic, BOUND[type].MIN, BOUND[type].MAX))
            .skip(MAXVERBATIMS * iteration)
            .limit(MAXVERBATIMS)
            .toArray(function (err, items) {
                if(err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    db.indexes.verbatims.count(formatRangeQuery(topic, BOUND[type].MIN, BOUND[type].MAX), function (err, count) {
                        next({r: items, c: count})
                    })
                }
            })
    },

}


function formatRangeQuery(topic, lowerBound, higherBound) {
    return {
        topic: topic,
        $and: [
            {"options.polarity": {$gt: lowerBound}},
            {"options.polarity": {$lte: higherBound}}
        ],
        enabled: true,
    }
}

