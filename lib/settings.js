var Promise = require('bluebird')
var db = require('./dbconnection')
var _ = require('underscore')
var Error = require('./error/error'),
    error = new Error();


/**
 * Module for accessing collection settings
 */
module.exports = {

    /**
     * Get language for a given topic
     * @param topic
     */
    getLang: function (topic) {
        return new Promise(function (resolve, reject) {
            db.indexes.settings.findOne({topic: topic}, function (err, r) {
                if(!_.isUndefined(r.lang)){
                    resolve(r.lang)
                } else {
                    resolve('fr')
                }
            })
        })
    },

    /**
     * Create topic id from project name and topic name
     * @param project
     * @param topic
     * @returns {string}
     */
    getTmpIdProjectTopic: function (project, topic) {
        var id = project + topic
        return id.replace(new RegExp(' ', 'g'), '').toLowerCase()
    },

    /**
     * Check if a certain topic name does not exist in the extraction related collections
     * @param topic
     * @param project
     * @param next
     */
    validTopicName: function (topic, project, next) {
        // check if in schedule queue
        db.schedule.datasets.findOne({topic: topic}, function (err, setting) {
            if(err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if(!setting) {
                    // check if in extraction queue
                    db.schedule.extractions.findOne({topic: topic}, function (err, setting) {
                        if(err)
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        else {
                            if(!setting)
                                // check if in active_processes
                                db.schedule.active_processes.findOne({topic: topic}, function (err, setting) {
                                    if(err)
                                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                    else {
                                        if(!setting)
                                        // check if topic name already exists
                                            db.indexes.settings.findOne({topic: topic}, function (err, setting) {
                                                if(err)
                                                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                                else {
                                                    if(!setting)
                                                        next(true)
                                                    else
                                                        next(false)
                                                }
                                            })
                                        else
                                            next(false)
                                    }
                                })
                            else
                            next(false)
                        }
                    })
                } else
                    next(false)
            }
        })
    }
}
