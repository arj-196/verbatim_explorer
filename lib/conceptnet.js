var Promise = require('bluebird')
var env = require('../config/env')
var ConceptNet = require('concept-net')
var conceptNet = new ConceptNet(env.conceptnet.host, env.conceptnet.port, env.conceptnet.version)
var _ = require('underscore')

/**
 * Module for Communicating with ConcetpNet Server
 */
module.exports = {

    getConcepts: function (concept, lang) {
        return new Promise(function (resolve, reject) {
            conceptNet.lookup('/c/' + lang + '/' + concept,
                {
                    //limit: 10,
                    //offset: 0,
                    //filter: "core"
                },
                function (err, results) {
                    if (err)
                        resolve([])
                    else {
                        var edges = []
                        results.edges.forEach(function (edge) {
                            edges.push({concept: edge.end, rel: edge.rel, weight: edge.weight})
                        })
                        resolve(edges)
                    }
                })
        })
    },

    filterEdges: function (edges) {
        return new Promise(function(resolve, reject){
            var filteredEdges = []
            edges.forEach(function (edge) {
                if (CONCEPTNET_RELATIONS.indexOf(edge.rel) != -1
                    && CONCEPTNET_RELATION_MAPPING[edge.rel].threshold <= edge.weight) {
                    filteredEdges.push(edge)
                }
            })
            resolve(_.sortBy(filteredEdges, 'weight'))
        })
    },

    scoreVerbatims: function (verbatims) {
        var scores = {}
        verbatims.forEach(function (verbatim) {
            var score = 0
            for (var i = 0; i < verbatim.concepts.length; i++) {
                var relationWeight
                if(!_.isUndefined(CONCEPTNET_RELATION_MAPPING[verbatim.relations[i]]))
                    relationWeight = CONCEPTNET_RELATION_MAPPING[verbatim.relations[i]]['weight']
                else
                    relationWeight = 0

                score += verbatim.weights[i] * relationWeight
            }
            scores[verbatim['_id']] = score / 100
        });

        return scores
    },
}

var CONCEPTNET_RELATION_MAPPING = {
    '/r/CapableOf': {'threshold': 0, 'weight': 1},
    '/r/AtLocation': {'threshold': 0, 'weight': 1},
    '/r/CreatedBy': {'threshold': 0, 'weight': 1},
    '/r/DefinedAs': {'threshold': 0, 'weight': 3},
    '/r/DerivedFrom': {'threshold': 0, 'weight': 1},
    '/r/HasA': {'threshold': 0, 'weight': 2},
    '/r/HasContext': {'threshold': 0, 'weight': 1},
    '/r/HasFirstSubevent': {'threshold': 0, 'weight': 1},
    '/r/HasPrerequiste': {'threshold': 0, 'weight': 1},
    '/r/HasProperty': {'threshold': 0, 'weight': 0.5},
    '/r/HasSubevent': {'threshold': 0, 'weight': 0.5},
    '/r/InstanceOf': {'threshold': 0, 'weight': 2},
    '/r/IsA': {'threshold': 0, 'weight': 2},
    '/r/MadeOf': {'threshold': 0, 'weight': 2},
    '/r/MemberOf': {'threshold': 0, 'weight': 2},
    '/r/Mot': {'threshold': 0, 'weight': 1},
    '/r/MotivatedByGoal': {'threshold': 0, 'weight': 1},
    '/r/PartOf': {'threshold': 0, 'weight': 2},
    '/r/ReceivesAction': {'threshold': 0, 'weight': 1},
    '/r/RelatedTo': {'threshold': 0, 'weight': 1},
    '/r/SimilarTo': {'threshold': 0, 'weight': 2},
    '/r/Synonym': {'threshold': 0, 'weight': 3},
    //'/r/TranslationOf': {'threshold': 0, 'weight': 8},
    '/r/UsedFor': {'threshold': 0, 'weight': 1},
    '/r/wordnet/adjectivePertainsTo': {'threshold': 0, 'weight': 1},
    '/r/wordnet/adverbPertainsTo': {'threshold': 0, 'weight': 1},
}

//var CONCEPTNET_RELATION_MAPPING = {
//    '/r/CapableOf': {'threshold': 2.5, 'weight': 1},
//    '/r/AtLocation': {'threshold': 2.5, 'weight': 1},
//    '/r/CreatedBy': {'threshold': 1.5, 'weight': 1},
//    '/r/DefinedAs': {'threshold': 1.1, 'weight': 3},
//    '/r/DerivedFrom': {'threshold': 1.5, 'weight': 1},
//    '/r/HasA': {'threshold': 2.0, 'weight': 2},
//    '/r/HasContext': {'threshold': 1.0, 'weight': 1},
//    '/r/HasFirstSubevent': {'threshold': 2.0, 'weight': 1},
//    '/r/HasPrerequiste': {'threshold': 2.5, 'weight': 1},
//    '/r/HasProperty': {'threshold': 2.0, 'weight': 0.5},
//    '/r/HasSubevent': {'threshold': 2.0, 'weight': 0.5},
//    '/r/InstanceOf': {'threshold': 1.0, 'weight': 2},
//    '/r/IsA': {'threshold': 1.5, 'weight': 2},
//    '/r/MadeOf': {'threshold': 1.5, 'weight': 2},
//    '/r/MemberOf': {'threshold': 1.5, 'weight': 2},
//    '/r/Mot': {'threshold': 2.0, 'weight': 1},
//    '/r/MotivatedByGoal': {'threshold': 2.0, 'weight': 1},
//    '/r/PartOf': {'threshold': 2.0, 'weight': 2},
//    '/r/ReceivesAction': {'threshold': 2.0, 'weight': 1},
//    '/r/RelatedTo': {'threshold': 2.0, 'weight': 1},
//    '/r/SimilarTo': {'threshold': 2.5, 'weight': 2},
//    '/r/Synonym': {'threshold': 1.0, 'weight': 3},
//    '/r/TranslationOf': {'threshold': 1.0, 'weight': 8},
//    '/r/UsedFor': {'threshold': 2.5, 'weight': 1},
//    '/r/wordnet/adjectivePertainsTo': {'threshold': 1.0, 'weight': 1},
//    '/r/wordnet/adverbPertainsTo': {'threshold': 1.0, 'weight': 1},
//}
var CONCEPTNET_RELATIONS = Object.keys(CONCEPTNET_RELATION_MAPPING)