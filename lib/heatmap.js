var Promise = require('bluebird')
var db = require('./dbconnection')
var _ = require('underscore')

/**
 * Manager responsible for Heatmaps Dashbaord
 */
module.exports = {

    /**
     * Get all global figures
     * @param topic
     * @param next
     */
    getGlobalFigures: function (topic, next) {
        db.indexes.heatmaps.find({topic: topic})
            .sort({value: -1})
            .toArray(function (err, items) {
                next({r: items})
            })
    },

}