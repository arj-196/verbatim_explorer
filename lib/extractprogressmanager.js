var _ = require('underscore'),
    Promise = require('bluebird'),
    db = require('./dbconnection'),
    UTILS = require('../lib/utils/common')
var Error = require('./error/error'),
    error = new Error();

/**
 * Manager responsible for Extraction Progress Dashboard
 */
module.exports = {

    /**
     * Get all extraction queued datasets
     * @param user
     * @param callback
     */
    fetchDatasets: function (user, callback) {
        db.schedule.datasets.find({user: user._id}).toArray(function (err, itemsUploaded) {
            if(err){
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                callback({s: false})
            }
            else {
                db.schedule.extractions.find({user: user._id}).toArray(function (err, itemsQueue) {
                    if(err) {
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        callback({s: false})
                    } else {
                        db.schedule.active_processes.find({user: user._id}).toArray(function (err, itemsActive) {
                            if(err) {
                                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                callback({s: false})
                            } else {
                                callback(false, {s: true, r: {upload: itemsUploaded, queue: itemsQueue, extract: itemsActive}})
                            }
                        })
                    }
                })
            }
        })
    },

    /**
     * Get progress for a dataset
     * @param datasetId
     * @param callback
     */
    getDatasetProgress: function (datasetId, callback) {
        db.schedule.active_processes.findOne({_id: datasetId}, function (err, item) {
            if(err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                callback(false, {s: false})
            } else {
                if(item) {
                    db.indexes.verbatims.count({topic: item.topic}, function (err, count) {
                        if(err) {
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                            callback(false, {s: false})
                        } else {
                            if(!_.isUndefined(item.count)) {
                                var percent = (count / item.count) * 100
                                callback(false, {s: true, percent: percent})
                            } else {
                                callback(false, {s: false})
                            }
                        }
                    })
                }
            }
        })
    }

}