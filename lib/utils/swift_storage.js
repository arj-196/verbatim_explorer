var pkgcloud = require('pkgcloud')
var fs = require('fs')
var env = require('../../config/env')
var logger = require('../utils/logger')('SwiftStorage')
var swiftConfig = env.swiftConfig

var config = {
    provider: 'openstack',
    useServiceCatalog: true,
    useInternal: false,
    keystoneAuthVersion: 'v3',
    authUrl: swiftConfig.auth_url,
    tenantId: swiftConfig.project_id,    //projectId from credentials
    domainId: swiftConfig.domainId,
    username: swiftConfig.username,
    password: swiftConfig.password,
    region: swiftConfig.region_name,   //dallas or london region
}

var storageClient = pkgcloud.storage.createClient(config)

module.exports = {
    uploadFile: function (filepath, callback) {
        var targetName = filepath.split('/').slice(-1)[0]
        logger.debug('Uploading File', {filepath: filepath, targetName: targetName})

        storageClient.auth(function (err) {
            if (err) {
                callback(err)
            }
            else {
                var myFile = fs.createReadStream(filepath)
                var upload = storageClient.upload({
                    container: swiftConfig.container_name,
                    remote: targetName,
                })

                upload.on('error', function (err) {
                    callback(err)
                })

                upload.on('success', function (file) {
                    callback(false)
                })

                myFile.pipe(upload)
            }
        })
    }
}