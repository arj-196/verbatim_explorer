const winston = require('winston')


var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            timestamp: function () {
                return new Date()
            },
            formatter: function (options) {
                // Return string will be passed to logger.
                return '[' + options.timestamp().toISOString() + '] ' +
                    '[' + options.level.toUpperCase() + '] ' +
                    (options.message ? options.message : '') +
                    (
                        options.meta && Object.keys(options.meta).length ? '\t' +
                        JSON.stringify(options.meta) : ''
                    )
            }
        })
    ]
});


class LoggerWrapper {
    constructor(name) {
        this.name = name
    }

    log(level, message, payload) {
        message = "[" + this.name + "] " + message
        logger.log(level, message, payload)
    }

    info(message, payload) {
        this.log('info', message, payload)
    }

    debug(message, payload) {
        // TODO change this to debug, problems with logger level
        this.log('info', message, payload)
    }

    warn(message, payload) {
        this.log('warn', message, payload)
    }

    error(message, payload) {
        this.log('error', message, payload)
    }
}

module.exports = (name) => {
    return new LoggerWrapper(name)
}