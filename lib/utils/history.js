var db = require('../dbconnection')
var Error = require('../error/error'),
    error = new Error()

/**
 * Module for User Interaction History
 */
module.exports = {

    /**
     * Set a history layer
     * @param sessionid
     * @param mode
     * @param content
     * @param next
     */
    setContent: function (sessionid, mode, content, next) {
        var updateStatement = {}
        updateStatement[mode] = content
        db.client.sessions.update({_id: sessionid}, {$set: updateStatement}, function (err) {
            if(err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else
                next(true)
        })
    },

    /**
     * Get a history layer
     * @param sessionid
     * @param mode
     * @param next
     */
    getContent: function (sessionid, mode, next) {
        db.client.sessions.findOne({_id: sessionid}, function (err, Session) {
            if(err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                next(Session[mode])
            }
        })
    }

}