var AWS = require('aws-sdk');
var env = require('../../config/env')
var logger = require('../utils/logger')('S3Storage')
var fs = require('fs')
// Create an S3 client
var s3 = new AWS.S3();
var bucketName = 'verbatims'

module.exports = {
    uploadFile: function (filepath, callback) {
        var targetName = filepath.split('/').slice(-1)
        logger.debug('Uploading File', {filepath: filepath, targetName: targetName})
        fs.readFile(filepath, 'utf8', function (err, content) {
            if (err) {
                return callback(err)
            }
            var verbatims = content.split('\n')

            var params = {
                Bucket: bucketName,
                Key: targetName,
                Body: verbatims
            };

            logger.debug('Putting Object in File',
                {filepath: filepath, targetName: targetName, count: verbatims.length})
            s3.putObject(params, function (err, data) {
                if (err)
                    callback(err)
                else
                    callback(false)

                logger.debug('Done Putting Object', {filepath: filepath, targetName: targetName})
            })
        });
    }
}