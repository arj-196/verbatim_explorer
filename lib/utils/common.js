var bcrypt = require('bcrypt-nodejs')
var nodemailer = require('nodemailer')
var ENV = require('../../config/env')
var MailerTool = require('../../config/mailertool')
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';
var _ = require('underscore')
module.exports = {

    // route middleware to make sure a user is logged in
    isLoggedIn: function (req) {

        // if user is authenticated in the session, carry on
        return !!req.isAuthenticated();
    },

    generateHash: function (password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
    },

    ifValidPassword: function (password, passwordEncrypted) {
        return bcrypt.compareSync(password, passwordEncrypted)
    },

    encrypt: function (text) {
        var cipher = crypto.createCipher(algorithm, password)
        var crypted = cipher.update(text, 'utf8', 'hex')
        crypted += cipher.final('hex');
        return crypted;
    },

    decrypt: function (text) {
        var decipher = crypto.createDecipher(algorithm, password)
        var dec = decipher.update(text, 'hex', 'utf8')
        dec += decipher.final('utf8');
        return dec;
    },

    sendMail: function (to, subject, body, next, ifRaw) {
        var mailHTML, mailSubject
        if (_.isUndefined(ifRaw)) {
            mailSubject = MailerTool[subject].SUBJECT
            mailHTML = MailerTool[subject].BODY.replace('$CONTENT$', body)
        } else {
            mailSubject = subject
            mailHTML = body
        }

        var transporter = nodemailer.createTransport(ENV.mailer.transport);
        var mailOptions = {
            from: MailerTool.CONF.ME, // sender address
            to: to, // list of receivers
            subject: mailSubject, // Subject line
            html: mailHTML// html body
        }
        transporter.sendMail(mailOptions, function (error, info) {
            if (!_.isNull(next)) {
                if (error) {
                    next({s: false, err: error})
                } else {
                    next({s: true})
                }
            }
        });
    }
}
