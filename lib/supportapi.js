var Promise = require('bluebird')
var unirest = require('unirest')
var env = require('../config/env')
var _ = require('underscore')
var Settings = require('./settings')
var db = require('../lib/dbconnection')
var Error = require('./error/error'),
    error = new Error();

var callAPI = function (route, data, serverDB) {
    return new Promise(function (resolve, reject) {
        if(_.isUndefined(serverDB) || !serverDB) {
            url = 'http://' + env.support.host + ':' + env.support.port + route
        } else {
            url = 'http://' + env.supportInsight.host + ':' + env.supportInsight.port + route
        }

        unirest
            .post(url)
            .header('Accept', 'application/json')
            .query(data)
            .end(function (res) {
                if (res.code == 200) {
                    var result = res.body
                    resolve(result)
                }
                else {
                    //console.log("inner", res.code, iteration)
                    reject({code: res.code})
                }
            })
    })
}

module.exports = {

    DATASET: {

        processNewDataset: function (project, topic, language, filename, user, company, count) {
            return new Promise(function (resolve, reject) {

                var tmpidprojectdataset = Settings.getTmpIdProjectTopic(project, topic)
                db.schedule.datasets.findOne({_id: tmpidprojectdataset}, function (err, setting) {
                    if(err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        if(!setting){
                            db.schedule.datasets.insert({
                                _id: tmpidprojectdataset,
                                project: project,
                                topic: topic,
                                lang: language,
                                filename: filename,
                                user: user,
                                company: company,
                                count: count
                            }, function (err) {
                                if(err)
                                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                else {
                                    // send file to upload
                                    callAPI(
                                        "/dataset/new/",
                                        {
                                            t: topic,
                                            f: filename
                                        }
                                    ).then(function () {

                                    }).catch(function (err) {
                                        reject(err)
                                    })
                                    resolve()
                                }
                            })
                        }
                    }
                })
            })
        },


        getDatasetSample: function (project, topic, language, filepath, company, user) {
            return new Promise(function(resolve, reject){
                callAPI(
                    "/dataset/sample/",
                    {
                        p: project,
                        t: topic,
                        l: language,
                        f: filepath,
                        c: company,
                        u: user
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        }

    },

    WORDCLOUD: {
        getWordsForWordcloud: function (mode, topic, commandList, query) {
            return new Promise(function(resolve, reject){
                callAPI(
                    "/wordcloud/getwords/",
                    {
                        m: mode,
                        t: topic,
                        q: query,
                        cmd: commandList
                    }
                )
            })
        }
    },

    MULTIPLEVERBATIMS: {

        getConcepts: function (verbatimList, lang) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/mv/similarities/",
                    {
                        v: JSON.stringify(verbatimList),
                        lang: lang
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },
    },

    STATS: {

        getEmotions: function (searchmode, topic, orStatement, commandList, languageMap) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/stats/emotion/",
                    {
                        sm: searchmode,
                        lang: 'fr',
                        t: topic,
                        os: JSON.stringify(orStatement),
                        cl: JSON.stringify(commandList),
                        lm: JSON.stringify(languageMap),
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },

        getNER: function (searchmode, topic, orStatement, commandList, languageMap) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/stats/ner/",
                    {
                        sm: searchmode,
                        lang: 'fr',
                        t: topic,
                        os: JSON.stringify(orStatement),
                        cl: JSON.stringify(commandList),
                        lm: JSON.stringify(languageMap),
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },

        getWordCloud: function (searchmode, targetmode, topic, orStatement, commandList, languageMap) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/stats/wordcloud/",
                    {
                        sm: searchmode,
                        tm: targetmode,
                        lang: 'fr',
                        t: topic,
                        os: JSON.stringify(orStatement),
                        cl: JSON.stringify(commandList),
                        lm: JSON.stringify(languageMap),
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },

        exportDataset: function (searchmode, topic, orStatement, commandList, languageMap) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/stats/exportdataset/",
                    {
                        sm: searchmode,
                        lang: 'fr',
                        t: topic,
                        os: JSON.stringify(orStatement),
                        cl: JSON.stringify(commandList),
                        lm: JSON.stringify(languageMap),
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },

        doSearch: function (sessionid, topic, ifloadmore) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/stats/searchaggregate/",
                    {
                        sid: sessionid,
                        t: topic,
                        ifloadmore: ifloadmore,
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },
    },

    MODAL: {
        compileInsightModal: function (id, topic) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/modals/compile/insight/",
                    {
                        id: id,
                        t: topic,
                    },
                    true
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },

        compileSIndex: function (id, topic) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/modals/compile/insight/sindex/",
                    {
                        id: id,
                        t: topic,
                    },
                    true
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },
    },

    EXPORT: {
        exportTags: function (topics, tags) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/export/tags/",
                    {
                        topics: topics,
                        tags: tags,
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },

        exportInsights: function (topics, insightids, nbPredicted) {
            return new Promise(function (resolve, reject) {
                callAPI(
                    "/export/insights/",
                    {
                        topics: topics,
                        insightids: insightids,
                        nbPredicted: nbPredicted,
                    }
                ).then(function (r) {
                    resolve(r)
                }).catch(function (err) {
                    reject(err)
                })
            })
        },
    },
}

//var t = module.exports
//t.DATASET.processNewDataset(['597b71558f428daa19a83aab63d2b195', '536ded644abf91d79a50c3bddd5a8185'], 'fr')
//    .then(function (r) {
//        var words = r.similarities.words
//        console.log("response", r)
//    })