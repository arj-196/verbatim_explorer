var Promise = require('bluebird')
var db = require('../dbconnection')
var _ = require('underscore')
var UTILS = require('../utils/common')
var InsightManager = require('../verbatimtools/insights')
var SUPPORTAPI = require('../supportapi')
var PROMISIFIED = {
    ifCompiledSIndex: Promise.promisify(InsightManager.ifCompiledSIndex),
    getSIndex: Promise.promisify(InsightManager.getSIndex)
}

/**
 * Cross Analysis Plugin fot Insight Cross Analysis
 */
module.exports = {

    /**
     * Get SIndex of Insights and Topics
     * @param user
     * @param topics
     * @param insightids
     * @param insightBag
     * @param next
     */
    getQuantificationPerTopic: function (user, topics, insightids, insightBag, next) {
        var topicInsightMap = {}
        var ifCompiledToFetch = []
        topics.forEach(function (topic) {
            topicInsightMap[topic] = {}
            insightids.forEach(function (insightid) {
                topicInsightMap[topic][insightid] = ifCompiledToFetch.length
                ifCompiledToFetch.push(PROMISIFIED.ifCompiledSIndex(insightid, topic))
            })
        })
        // resolving all ifCompiled Tasks
        Promise.all(ifCompiledToFetch).then(function (ifCompiledList) {
            var topicSIndexMap = {}
            var sIndexesToFetch = []
            topics.forEach(function (topic) {
                topicSIndexMap[topic] = {}
                insightids.forEach(function (insightid) {
                    topicSIndexMap[topic][insightid] = sIndexesToFetch.length
                    // checking if have to compile sindex or not
                    if (ifCompiledList[topicSIndexMap[topic][insightid]]) {
                        sIndexesToFetch.push(PROMISIFIED.getSIndex(insightid, topic))
                    } else {
                        sIndexesToFetch.push(SUPPORTAPI.MODAL.compileSIndex(insightid, topic))
                    }
                })
            })
            // resolving all sIndexes Tasks
            Promise.all(sIndexesToFetch).then(function (sIndexesList) {
                // finally building the response object
                var topicSIndexResponse = []
                var topicSIndexCompleteResponse = []
                topics.forEach(function (topic) {
                    var entry = {
                        name: topic
                    }
                    var entryComplete = {}
                    insightids.forEach(function (insightid) {
                        if (!_.isUndefined(sIndexesList[topicSIndexMap[topic][insightid]])) {
                            if (_.keys(sIndexesList[topicSIndexMap[topic][insightid]]).indexOf('s') != -1) {
                                // response after compiling
                                entry[insightBag.map[insightid].name] = sIndexesList[topicSIndexMap[topic][insightid]].r.sindex
                                entryComplete[insightBag.map[insightid].name] = sIndexesList[topicSIndexMap[topic][insightid]].r
                            } else {
                                // response from db cached
                                entry[insightBag.map[insightid].name] = sIndexesList[topicSIndexMap[topic][insightid]].sindex
                                entryComplete[insightBag.map[insightid].name] = sIndexesList[topicSIndexMap[topic][insightid]]
                            }
                        }
                        else {
                            entry[insightBag.map[insightid].name] = 0
                            entryComplete[insightBag.map[insightid].name] = null
                        }
                    })
                    topicSIndexResponse.push(entry)
                    entryComplete.name = topic
                    topicSIndexCompleteResponse.push(entryComplete)
                })

                // list of insights
                var insightList = []
                insightids.forEach(function (insightid) {
                    insightList.push(insightBag.map[insightid].name)
                })

                next({r: topicSIndexResponse, words: insightList, t: topicSIndexCompleteResponse})
            })
        })
    }
}