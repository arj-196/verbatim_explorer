var Promise = require('bluebird')
var db = require('../dbconnection')
var VERBATIMMANAGER = require('../verbatims')
var _ = require('underscore')

var promisified = {
    getDisabled: Promise.promisify(VERBATIMMANAGER.getDisabledVerbatimIds)
}

/**
 * Cross Analysis Plugin fot Word Cross Analysis
 */
module.exports = {

    /**
     * Per topic get the number of verabtims per word
     * @param user
     * @param topics
     * @param words
     * @param next
     */
    getCountsPerTopics: function (user, topics, words, next) {
        var self = this
        self.getDisabledVerbatimIdsPerTopic(topics, function (err, topicDisabled) {

            var countsToFetch = []

            // get counts for topics and words
            topics.forEach(function (topic, i) {
                words.forEach(function (word, j) {
                    countsToFetch.push(
                        db.indexes.verbatimterms.countAsync({
                            topic: topic,
                            terms: {$in: [word]},
                            _id: {$nin: topicDisabled[topic]}
                        })
                    )
                })
            })

            Promise.all(countsToFetch).then(function (counts) {
                var k, results = []
                topics.forEach(function (topic, i) {
                    var d = {
                        name: topic
                    }
                    words.forEach(function (word, j) {
                        k = i + j
                        d[word] = counts[k]
                    })
                    results.push(d)
                })

                next({
                    r: results,
                    words: words
                })
            })

        })
    },

    /**
     * Get all disabled verbatim ids for a list of topics
     * returns an object with the topic name as key and a list of verabtim ids as value
     * @param topics
     * @param callback
     */
    getDisabledVerbatimIdsPerTopic: function (topics, callback) {
        // getting disabled verbatims for topics
        var disabledIdsToFetch = []
        topics.forEach(function (topic) {
            disabledIdsToFetch.push(
                promisified.getDisabled(topic)
            )
        })

        Promise.all(disabledIdsToFetch).then(function (disabledVerbatims) {
            var topicDisabled = {}
            for (var i = 0; i < topics.length; i++) {
                topicDisabled[topics[i]] = disabledVerbatims[i]
            }
            callback(false, topicDisabled)
        })
    }
}