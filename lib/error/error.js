var env = require('../../config/env')
var UTILS = require('../utils/common')
var _ = require('underscore')

var Error = function () {
    this.APIError = function (path, message) {
        return {
            s: false,
            e: {
                path: path,
                message: message
            }
        };
    };

    this.renderError = function (path, fn, message, res) {
        res.json({
            path: path,
            fn: fn,
            message: message
        });
    };

    this.STDError = function (path, fn, message, err) {
        if (env.MODE == "dev") {
            this.print("--------", new Date());
            this.print("ERR:", message);
            this.print("FILE:", path);
            this.print("FN:", fn);
        } else {
            if(!_.isUndefined(message)){
                var errSubject = "ERROR LOGGER : " + new Date()
                var errBody = "" +
                    new Date() + "<br>" +
                    "--------" + "<br>" +
                    "FILE:" + "<br>" + path + "<br><br>" +
                    "FN:" + "<br>" + fn + "<br><br>" +
                    "ERR:" + "<br>" + message + "<br><br>"
                UTILS.sendMail(env.mailer.errorReportAddress, errSubject, errBody, null, true)
            }
        }
    };

    this.print = function (title, message) {
        console.error(title, message);
    };
};

/**
 * Module for reporting errors
 */
module.exports = Error;