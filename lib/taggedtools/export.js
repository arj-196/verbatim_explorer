var Promise = require('bluebird')
var db = require('../dbconnection')
var _ = require('underscore')
var SUPPORTAPI = require('../supportapi')
var Error = require('../error/error'),
    error = new Error();

/**
 * Tag Manager Plugin for Exporting Data
 */
module.exports = {

    /**
     * Exporting dataset given a list of tags and a list of tags
     * Communicating with insighte_support_api
     * @param topics
     * @param tags
     * @param next
     */
    exportTags: function (topics, tags, next) {
        SUPPORTAPI.EXPORT.exportTags(topics, tags).then(function (r) {
            next(r)
        })
    },

    /**
     * Exporiting dataset given a list of topics and a list of insightids
     * @param topics
     * @param insightids
     * @param nbPredicted
     * @param next
     */
    exportInsights: function (topics, insightids, nbPredicted, next) {
        SUPPORTAPI.EXPORT.exportInsights(topics, insightids, nbPredicted).then(function (r) {
            next(r)
        })
    },
}