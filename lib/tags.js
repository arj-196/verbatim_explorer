var Promise = require('bluebird')
var db = require('./dbconnection')
var _ = require('underscore')
var VERBATIMMANAGER = require('./verbatims')
var Error = require('./error/error'),
    error = new Error();

/**
 * Manager responsible for Tags Dashboard Related Tasks
 */
module.exports = {

    PLUGINS: {
        EXPORT: require('../lib/taggedtools/export'),
    },

    /**
     * Getting all datasets
     * @param next
     */
    getDatasets: function (next) {
        db.indexes.verbatimtags.distinct("topic", function (err, topics) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                topics.sort(function (a, b) {
                    a = a.toLowerCase()
                    b = b.toLowerCase()
                    if (a < b) return -1;
                    if (b < a) return 1;
                    return 0;
                });
                var topicNamesToBeFetched = []
                topics.forEach(function (item) {
                    topicNamesToBeFetched.push(VERBATIMMANAGER.getTopicName(item))
                });
                Promise.all(topicNamesToBeFetched).then(function (topicnames) {
                    var TopicList = []

                    for (var i = 0; i < topics.length; i++) {
                        TopicList.push({
                            id: topics[i],
                            name: topicnames[i]
                        })
                    }
                    next({status: true, r: TopicList})
                })
            }
        })
    },

    /**
     * Getting all tags
     * @param topic
     * @param next
     */
    getDistinctTags: function (topic, next) {
        VERBATIMMANAGER.getDisabledVerbatimIds(topic, function (err, disabledVerbatimIds) {
            if (err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                next([])
            } else {
                db.indexes.verbatimtags.distinct("tags", {
                    topic: topic,
                    _id: {$nin: disabledVerbatimIds}
                }, function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        next(items.sort())
                    }
                })
            }
        })
    },

    /**
     * Get verbatims for a given array of tags
     * @param tagList
     * @param topic
     * @param next
     */
    getVerbatimsForTags: function (tagList, topic, next) {
        db.indexes.verbatimtags.find({tags: {$all: tagList}, topic: topic}, {_id: 1}).toArray(function (err, items) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                var verbatimIds = _.pluck(items, '_id')
                db.indexes.verbatims.find({
                    _id: {$in: verbatimIds},
                    topic: topic,
                    enabled: true
                }).toArray(function (err, verbatims) {
                    db.indexes.verbatims.count({
                        _id: {$in: verbatimIds},
                        topic: topic,
                        enabled: true
                    }, function (err, count) {
                        next({r: verbatims, c: count})
                    })
                })
            }
        })
    },

    /**
     * Getting associated tags for a given array of tags
     * @param tagList
     * @param topic
     * @param next
     */
    getAssociatedTagsForTags: function (tagList, topic, next) {
        VERBATIMMANAGER.getDisabledVerbatimIds(topic, function (err, disabledVerbatimIds) {
            if (err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                next([])
            } else {
                db.indexes.verbatimtags.distinct("tags", {tags: {$all: tagList}, topic: topic, _id: {$nin: disabledVerbatimIds}}, function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        tagList.forEach(function (tag) {
                            items = _.reject(items, function (t) {
                                return t == tag
                            })
                        })
                        next(items)
                    }
                })
            }
        })
    }
}