var USERMANAGER = require('./usermanager')
var db = require('./dbconnection')
var MAXSMALLCOUNT = 500000
var Error = require('./error/error'),
    error = new Error()
var _ = require('underscore')

/**
 * Manager responsible for all Project related tasks
 */
module.exports = {

    /**
     * Check if project has exceeded it's verbatim count limits
     * @param user
     * @param projectid
     * @param topiccount
     * @param callback
     */
    ifExceedsCountLimit: function (user, projectid, topiccount, callback) {
        var self = this
        USERMANAGER.getProjectDetails(user, projectid).then(function (details) {
            // get current small and big projects
            self.getProjectSizeCounts(user, function (projectSizeCounts) {
                // determine if small project or large
                var newCount = details.totalCount + topiccount
                var projectSize
                if (newCount > MAXSMALLCOUNT)
                    projectSize = "BIG"
                else
                    projectSize = "SMALL"

                db.client.license.findOne({_id: user.company}, function (err, license) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        if (license) {
                            if (projectSize == "BIG") {
                                if (projectSizeCounts.BIG > license.big - 1) {
                                    callback({s: true, m: "BADFILE?m=EXCESSDATABIG"})
                                } else {
                                    callback({s: false})
                                }
                            } else {
                                if (projectSizeCounts.SMALL + projectSizeCounts.BIG > license.small + license.big - 1) {
                                    callback({s: true, m: "BADFILE?m=EXCESSDATASMALL"})
                                } else {
                                    callback({s: false})
                                }
                            }
                        } else {
                            callback({s: false})
                        }
                    }
                })
            })
        })
    },

    /**
     * Get all user projects verbatims counts
     * @param user
     * @param callback
     */
    getProjectSizeCounts: function (user, callback) {
        db.client.projects.find({company: user.company}).toArray(function (err, projects) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                var projectDetailsToFetch = []
                projects.forEach(function (project) {
                    projectDetailsToFetch.push(USERMANAGER.getProjectDetails(user, project._id))
                })

                Promise.all(projectDetailsToFetch).then(function (projectDetailList) {
                    var projects = {
                        BIG: 0,
                        SMALL: 0
                    }
                    projectDetailList.forEach(function (pd) {
                        if (pd.totalCount > MAXSMALLCOUNT)
                            projects.BIG++
                        else
                            projects.SMALL++
                    })
                    callback(projects)
                })
            }
        })
    },

    /**
     * Get company license object
     * @param user
     * @param callback
     */
    getCompanyLicense: function (user, callback) {
        db.client.license.findOne({_id: user.company}, function (err, license) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                callback(license)
            }
        })
    },

    /**
     * Create a new project
     * The data object must have the following fields
     *  - name
     * @param user
     * @param data
     * @returns {*}
     */
    createProject: function (user, data) {
        var self = this
        return new Promise(function (resolve, reject) {
            db.client.projects.findOne({_id: data.name}, function (err, project) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    if (project) {
                        resolve({s: false, m: "Please choose another project name"})
                    } else {
                        var color
                        if (_.isUndefined(data.color)) {
                            color = "#000000"
                        } else {
                            color = data.color
                        }

                        // check if not exceeding limit
                        self.getProjectSizeCounts(user, function (projectSizeCounts) {
                            self.getCompanyLicense(user, function (license) {
                                if (projectSizeCounts.SMALL + projectSizeCounts.BIG > license.small + license.big - 1) {
                                    resolve({
                                        s: false, m: "You have run out of Big and Medium Projects. Please contact " +
                                        "<a href='mailto:hello@thedatastrategy.com'>InsightE</a> to extend your license."
                                    })
                                } else {
                                    db.client.projects.insert({
                                        _id: data.name,
                                        company: user.company,
                                        color: color
                                    }, function (err) {
                                        if (err)
                                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                        else {
                                            resolve({s: true})
                                        }
                                    })
                                }
                            })
                        })
                    }
                }
            })
        })
    },

    /**
     * Edit a project
     * The data object must have the following fields
     *  - id
     *  - name
     * @param user
     * @param data
     * @param callback
     */
    editProject: function (user, data, callback) {
        db.client.projects.findOne({name: data.name}, function (err, project) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (project) {
                    callback({s: false, m: "Please choose another project name"})
                } else {
                    db.client.projects.update({_id: data.id}, {$set: {name: data.name}}, function (err) {
                        if (err)
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        else {
                            callback({s: true})
                        }
                    })
                }
            }
        })
    },

}