var Promise = require('bluebird')
var db = require('./dbconnection')
var _ = require('underscore')
var CONCEPTNET = require('./conceptnet')
var SETTINGS = require('./settings')
var Error = require('./error/error'),
    error = new Error();
var MAXVERBATIMS = 15
var MAXWORDCLOUDENTRIES = 300

/**
 * Manager responsible for all Verbatim Dashboard Related Tasks
 */
module.exports = {

    /**
     * Plugins
     */
    TOOL: {
        FAVORITE: require('./verbatimtools/favorites'),
        CONCEPTS: require('./verbatimtools/concepts'),
        KEYWORDS: require('./verbatimtools/keywords'),
        TAGS: require('./verbatimtools/tags'),
        INSIGHTS: require('./verbatimtools/insights'),
        SEARCH: require('./verbatimtools/search'),
        NER: require('./verbatimtools/ners'),
        STATS: require('./verbatimtools/stats'),
        SEARCHAGGREGATE: require('./verbatimtools/searchaggregate'),
    },

    /**
     * Get all topics
     * @param next
     */
    getTopics: function (next) {
        var self = this
        db.indexes.verbatims.distinct("topic", function (err, topics) {
            if (err)
                error.STDError([__dirname, __filename ].join('/'), "getTopic", err.stack)
            else{
                topics.sort(function (a, b) {
                    a = a.toLowerCase()
                    b = b.toLowerCase()
                    if (a < b) return -1;
                    if (b < a) return 1;
                    return 0;
                });

                var topicNamesToBeFetched = []
                topics.forEach(function (item) {
                    topicNamesToBeFetched.push(self.getTopicName(item))
                });
                Promise.all(topicNamesToBeFetched).then(function (topicnames) {
                    var TopicList = []

                    for (var i = 0; i < topics.length; i++) {
                        TopicList.push({
                            id: topics[i],
                            name: topicnames[i]
                        })
                    }
                    next({status: true, r: TopicList})
                })
            }
        })
    },

    /**
     * Get a topic name given the topic
     * @param topic
     */
    getTopicName: function (topic) {
        return new Promise(function (resolve, reject) {
            db.indexes.settings.findOne({topic: topic}, function (err, item) {
                if(item) {
                    if(_.isUndefined(item.name)){
                        resolve(topic)
                    } else {
                        resolve(item.name)
                    }
                } else {
                    resolve(topic)
                }
            })
        })
    },

    /**
     * Get number of verbatims for a topic
     * @param topic
     * @param next
     */
    getTopicCount: function (topic, next) {
        db.indexes.verbatims.count({topic: topic, enabled: true}, function (err, count) {
            if (err)
                error.STDError([__dirname, __filename ].join('/'), "getTopicCount", err.stack)
            else
                next({status: true, c: count})
        })
    },

    /**
     * Get verbatims for a topic
     * Returns MAXVERBATIMS number of verbatims per iteration
     * @param topic
     * @param iteration
     * @param next
     */
    getVerbatims: function (topic, iteration, next) {
        var self = this
        db.indexes.verbatims.find({topic: topic, enabled: true}).skip(parseInt(iteration * MAXVERBATIMS)).limit(MAXVERBATIMS)
            .toArray(function (err, items) {
                if (err)
                    error.STDError([__dirname, __filename ].join('/'), "getVerbatims", err.stack)
                if (items.length > 0)
                    if (items[0].score == undefined) {
                        for (var i = 0; i < items.length; i++) {
                            items[i].score = 0;
                        }
                    }

                self.getTopicCount(topic, function (r) {
                    next({status: true, r: items, c: r.c})
                })
            })
    },

    /**
     * Get a verbatim by ID
     * @param id
     * @param next
     */
    getVerbatimByID: function (id, next) {
        db.indexes.verbatims.findOne({_id: id}, function (err, verbatim) {
            next(verbatim)
        })
    },

    /**
     * Get multiple verbatims by IDS
     * @param ids
     * @param next
     */
    getVerbatimsByIds: function (ids, next) {
        db.indexes.verbatims.find({_id: {$in: ids}}).toArray(function (err, items) {
            if (err)
                error.STDError([__dirname, __filename ].join('/'), "getVerbatimsByIds", err.stack)
            else
                next(items)
        })
    },

    /**
     * Get the list of ids of all disabled verbatims in a topic
     * @param topic
     * @param callback
     */
    getDisabledVerbatimIds: function (topic, callback) {
        db.indexes.verbatims.find({enabled: false}, {_id: true}).toArray(function (err, items) {
            if (err) {
                error.STDError([__dirname, __filename ].join('/'), "getVerbatimsByIds", err.stack)
                callback(true, null)
            } else {
                callback(false, _.pluck(items, '_id'))
            }
        })
    },

    /**
     * Get Nouns for Wordclouds for a search query
     * @param q
     * @param topic
     * @param next
     */
    getNounsForWordcloud: function (q, topic, next) {
        var query
        if (!q) {
            query = {}
        } else {
            query = {$or: constructOrStatementAssociates(JSON.parse(q))}
        }

        var dbTmp = db.getTemporaryConnection(db.dbNames.wordcloudnouns)
        dbTmp.bind(topic)
        getWordCloudContent(dbTmp, query, topic, next)
    },

    /**
     * Get Keywords for Wordclouds for a search query
     * @param q
     * @param topic
     * @param next
     */
    getKeywordsForWordcloud: function (q, topic, next) {
        var query
        if (!q) {
            query = {}
        } else {
            query = {$or: constructOrStatementAssociates(JSON.parse(q))}
        }
        var dbTmp = db.getTemporaryConnection(db.dbNames.wordcloudkeywords)
        dbTmp.bind(topic)
        getWordCloudContent(dbTmp, query, topic, next)
    },

    /**
     * Get Terms for Wordclouds for a search query
     * @param q
     * @param topic
     * @param next
     */
    getTermsForWordcloud: function (q, topic, next) {
        var query
        if (!q) {
            query = {}
        } else {
            query = {$or: constructOrStatementAssociates(JSON.parse(q))}
        }
        var dbTmp = db.getTemporaryConnection(db.dbNames.wordcloudterms)
        dbTmp.bind(topic)
        getWordCloudContent(dbTmp, query, topic, next)
    },

    /**
     * Get Concepts for Wordclouds for a search query
     * @param q
     * @param topic
     * @param next
     */
    getConceptsForWordcloud: function (q, topic, next) {
        var query, commandList
        if (!q) {
            query = {}
            commandList = []
        } else {
            commandList = JSON.parse(q)
        }

        SETTINGS.getLang(topic)
            .then(function (lang) {

                var conceptsToFetch = [], cmdToConceptMap = {}
                commandList.forEach(function (cL) {
                    cL.forEach(function (command) {
                        cmdToConceptMap[command] = conceptsToFetch.length
                        conceptsToFetch.push(CONCEPTNET.getConcepts(command, lang))
                    })
                })

                Promise.all(conceptsToFetch).then(function (conceptList) {
                    if(q){
                        query = {$or: constructOrStatementAssociatesForConcepts(commandList, conceptList, cmdToConceptMap)}
                    }
                    var dbTmp = db.getTemporaryConnection(db.dbNames.wordcloudconcepts)
                    dbTmp.bind(topic)
                    getWordCloudContent(dbTmp, query, topic, next)
                })

            })
    },

    /**
     * Get Verbs for Wordclouds for a search query
     * @param q
     * @param topic
     * @param next
     */
    getVerbsForWordcloud: function (q, topic, next) {
        var query
        if (!q) {
            query = {}
        } else {
            query = {$or: constructOrStatementAssociates(JSON.parse(q))}
        }
        var dbTmp = db.getTemporaryConnection(db.dbNames.wordcloudverbs)
        dbTmp.bind(topic)
        getWordCloudContent(dbTmp, query, topic, next)
    },

    /**
     * Get Adjectives for Wordclouds for a search query
     * @param q
     * @param topic
     * @param next
     */
    getAdjectivesForWordcloud: function (q, topic, next) {
        var query
        if (!q) {
            query = {}
        } else {
            query = {$or: constructOrStatementAssociates(JSON.parse(q))}
        }
        var dbTmp = db.getTemporaryConnection(db.dbNames.wordcloudadjectives)
        dbTmp.bind(topic)
        getWordCloudContent(dbTmp, query, topic, next)
    },

    /**
     * DEPRECATED
     * Word cloud Concept Search
     * @param type
     * @param commandList
     * @param next
     */
    wordcloudSearchConcept: function (type, commandList, next) {
        // run on default
        next({t: type, c: JSON.parse(commandList)})
    },

    /**
     * DEPRECATED
     * Word cloud Term Search
     * @param type
     * @param commandList
     * @param next
     */
    wordcloudSearchTerm: function (type, commandList, next) {
        if (commandList) {
            commandList = JSON.parse(commandList)
            var listWords = []
            commandList.forEach(function (cL) {
                cL.forEach(function (command) {
                    listWords.push(command)
                })
            })
            var query = {}
            query[db.COLLECTION_FIELD_MAP[type]["word"]] = {$in: listWords}
            wordCloudSearchByQuery(query, type, commandList, next)
        } else {
            next(null)
        }
    },
}

/**
 * Construct Search OrStatement for Associates
 * @param commandList
 * @returns {Array}
 */
function constructOrStatementAssociates(commandList) {
    var orStatement = []
    commandList.forEach(function (command) {
        var keywordList = []
        command.forEach(function (keyword) {
            keywordList.push(keyword)
        });
        orStatement.push({associates: {$all: keywordList}})
    });
    return orStatement
}


/**
 * Construct Search OrStatement for Associates for Concepts
 * @param commandList
 * @param conceptList
 * @param cmdToConceptMap
 * @returns {Array}
 */
function constructOrStatementAssociatesForConcepts(commandList, conceptList, cmdToConceptMap){
    var orStatement = []
    commandList.forEach(function (cL) {
        var andStatement = []
        cL.forEach(function (command) {
            andStatement.push({associates: {$in: _.uniq(_.pluck(conceptList[cmdToConceptMap[command]], 'concept'))}})
        });
        orStatement.push({$and: andStatement})
    });
    return orStatement
}

/**
 * Get WordCloud Data
 * This is a wrapper for finally making the query to the database
 * The database object, search query object and the topic is passed as argument,
 * and the wrapper finally executes the instructions
 * @param dbTmp database base connection
 * @param query search query
 * @param topic targetted topic
 * @param next
 */
function getWordCloudContent(dbTmp, query, topic, next) {
    dbTmp[topic].count(query, function (err, count) {
        var p
        if (count > MAXWORDCLOUDENTRIES)
            p = MAXWORDCLOUDENTRIES / count
        else
            p = 1

        var limit = Math.round((count * p))
        dbTmp[topic].find(query, {_id: 1, value: 1})
            .sort({value: -1})
            .limit(limit)
            .toArray(function (err, itemTop) {
                if (err)
                    error.STDError([__dirname, __filename ].join('/'), "getWordCloudContent", err.stack)
                else {
                    next({status: true, r: itemTop, c: count})
                    dbTmp.close()
                }
            })
    })
}

/**
 * Filter WordCloud Content by Search Query
 * This is a wrapper for quering the database for the wordcloud contents
 * The search query, type of data and search CommandList is passed as arguments
 * and the wrapper finally executes the instructions
 * @param query Search Query
 * @param type type of data (keywords|concepts|etc)
 * @param commandList Search CommandList
 * @param next
 */
function wordCloudSearchByQuery(query, type, commandList, next) {

    var fields = {_id: -1}
    fields[db.COLLECTION_FIELD_MAP[type]["word"]] = 1
    db.indexes[type].find(query, fields)
        .toArray(function (err, items) {
            if (!err) {
                // filter commandList
                var words = _.pluck(items, db.COLLECTION_FIELD_MAP[type]["word"])
                var filteredCommandList = []
                commandList.forEach(function (cL) {
                    var and = []
                    cL.forEach(function (command) {
                        if (words.indexOf(command) != -1) {
                            and.push(command)
                        }
                    })
                    if (and.length > 0)
                        filteredCommandList.push(and)
                })
                if (filteredCommandList.length == 0)
                    filteredCommandList = null

                next({t: type, c: filteredCommandList})
            }
        })
}
