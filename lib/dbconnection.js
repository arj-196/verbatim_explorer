var env = require('../config/env')
var Promise = require('bluebird')
var mongo = Promise.promisifyAll(require('mongoskin'))
var MONGOURL = env.mongodb.uri
var indexes = mongo.db(MONGOURL + "indexes", {native_parser: true})
var client = mongo.db(MONGOURL + "client", {native_parser: true})
var modal = mongo.db(MONGOURL + "modal", {native_parser: true})
var schedule = mongo.db(MONGOURL + "schedule", {native_parser: true})
var cleaner = mongo.db(MONGOURL + "cleaner", {native_parser: true})
var ObjectID = require('mongoskin').ObjectID

// indexes
indexes.bind('settings')
indexes.bind('verbatims')
indexes.bind('concepts')
indexes.bind('keywords')
indexes.bind('terms')
indexes.bind('tags')
indexes.bind('verbatimkeywords')
indexes.bind('verbatimconcepts')
indexes.bind('verbatimnouns')
indexes.bind('verbatimverbs')
indexes.bind('verbatimadjectives')
indexes.bind('verbatimterms')
indexes.bind('adjectives')
indexes.bind('verbs')
indexes.bind('nouns')
indexes.bind('heatmaps')
indexes.bind('insights')

// verbatim tools
indexes.bind('verbatimtags')
indexes.bind('verbatimners')
indexes.bind('verbatimfavorites')

// session
client.bind('sessions')
client.bind('aggregate')
client.bind('users')
client.bind('organizations')
client.bind('projects')
client.bind('license')

// modals
modal.bind('modals')
modal.bind('rejects')
modal.bind('predictions')
modal.bind('settings')

// schedule
schedule.bind('datasets')
schedule.bind('extractions')
schedule.bind('active_processes')

// cleaner
cleaner.bind('history')

module.exports = {
    indexes: indexes,
    client: client,
    modal: modal,
    schedule: schedule,
    cleaner: cleaner,

    objID: ObjectID,
    
    dbNames: {
        wordcloudkeywords: "wckeywords",
        wordcloudterms: "wcterms",
        wordcloudconcepts: "wcconcepts",
        wordcloudnouns: "wcnouns",
        wordcloudverbs: "wcverbs",
        wordcloudadjectives: "wcadjectives",
    },

    COLLECTION_WORD_TYPE_NAME_LIST: ["adjectives", "verbs", "nouns"],

    COLLECTION_FIELD_MAP: {
        "adjectives": {
            "word": "word",
        },
        "adverbs": {
            "word": "word",
        },
        "verbs": {
            "word": "word",
        },
        "nouns": {
            "word": "word",
        },
        "concepts": {
            "word": "concept",
        },

        "keywords": {
            "word": "keyword",
        },
        "terms": {
            "word": "term",
        },
    },

    getTemporaryConnection: function (dbname) {
        return mongo.db(MONGOURL + dbname)
    }
};