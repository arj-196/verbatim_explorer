var db = require('../dbconnection')
var CommandListParser = require('./commandlistparser')
var SUPPORTAPI = require('./../supportapi')
var SETTINGS = require('../settings')
var conceptnet = require('../conceptnet')
var _ = require('underscore')
var Error = require('../error/error'),
    error = new Error();

/**
 * Verbatim Manager Plugin for Statistics
 */
module.exports = {

    /**
     * Getting Emotions content
     * Communicating with insighte_support_api
     * @param searchmode
     * @param commandList
     * @param languageMap
     * @param topic
     * @param next
     */
    getEmotions: function (searchmode, commandList, languageMap, topic, next) {
        constructOrStatement(searchmode, commandList, topic, languageMap, function (orStatement) {
            SUPPORTAPI.STATS.getEmotions(searchmode, topic, orStatement, commandList, languageMap)
                .then(function (r) {
                    next({
                        stats: r,
                        searchmode: searchmode,
                        commandList: commandList,
                        languageMap: languageMap,
                        topic: topic
                    })
                })
        })
    },

    /**
     * Getting NER content
     * Communicating with insighte_support_api
     * @param searchmode
     * @param commandList
     * @param languageMap
     * @param topic
     * @param next
     */
    getNER: function (searchmode, commandList, languageMap, topic, next) {
        constructOrStatement(searchmode, commandList, topic, languageMap, function (orStatement) {
            SUPPORTAPI.STATS.getNER(searchmode, topic, orStatement, commandList, languageMap)
                .then(function (r) {
                    next({
                        stats: r,
                        searchmode: searchmode,
                        commandList: commandList,
                        languageMap: languageMap,
                        topic: topic
                    })
                })
        })
    },

    /**
     * Getting Wordcloud content
     * Communicating with insighte_support_api
     * @param searchmode
     * @param targetmode
     * @param commandList
     * @param languageMap
     * @param topic
     * @param next
     */
    getWordCloud: function (searchmode, targetmode, commandList, languageMap, topic, next) {
        constructOrStatement(searchmode, commandList, topic, languageMap, function (orStatement) {
            SUPPORTAPI.STATS.getWordCloud(searchmode, targetmode, topic, orStatement, commandList, languageMap)
                .then(function (r) {
                    next({
                        stats: r,
                        searchmode: searchmode,
                        commandList: commandList,
                        languageMap: languageMap,
                        topic: topic
                    })
                })
        })
    },

    /**
     * Exporint dataset
     * Communicating with insighte_support_api
     * @param searchmode
     * @param commandList
     * @param languageMap
     * @param topic
     * @param next
     */
    exportDataset: function (searchmode, commandList, languageMap, topic, next) {
        constructOrStatement(searchmode, commandList, topic, languageMap, function (orStatement) {
            SUPPORTAPI.STATS.exportDataset(searchmode, topic, orStatement, commandList, languageMap)
                .then(function (r) {
                    next(r)
                })
        })
    },

    /**
     * Setting Search Queue to Default State for sessionid
     * @param sessionid
     * @param next
     */
    searchSetToDefault: function (sessionid, next) {
        db.client.sessions.update({_id: sessionid}, {$set: {searchList: JSON.stringify([]), iteration: 1}}, function (err) {
            if (err) {
                next({s: false})
            }
            else {
                next({s: true})
            }
        })
    },

    /**
     * Adding a search level for a session id
     * @param sessionid
     * @param type
     * @param word
     * @param topic
     * @param next
     */
    addSearchLevel: function (sessionid, type, word, topic, next) {
        // defining variables for constructing or statement
        var commandList = [[word]], searchmode, languageMap = {}
        if (type == "keywords") {
            searchmode = "keyword"
        } else if (type == "terms" || type == "ner" || type == "hashtags" || type == "mentions") {
            searchmode = "text"
        } else if (type == "concepts") {
            searchmode = "concept"
        } else if (type == "domains") {
            searchmode = "domain"
        } else {
            searchmode = null
        }

        if (!_.isNull(searchmode)) {
            constructOrStatement(searchmode, [commandList, null], topic, languageMap, function (orStatement) {
                var orStatementObj = {s: searchmode, o: orStatement}
                db.client.sessions.findOne({_id: sessionid}, function (err, session) {
                    if (err) {
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        next({s: false})
                    } else {
                        if (_.isUndefined(session.searchList)) {
                            // no previous searchList defined
                            db.client.sessions.update({_id: sessionid}, {
                                $set: {
                                    searchList: JSON.stringify([orStatementObj])
                                }
                            }, function (err) {
                                if (err) {
                                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                    next({s: false})
                                } else {
                                    next({s: true, m: [orStatementObj]})
                                }
                            })
                        } else {
                            var newSearchList = JSON.parse(session.searchList)  // pre-existing search list
                            // checking if same search condition already exists
                            var strOrStatement = JSON.stringify(orStatementObj), ifExists = false
                            newSearchList.forEach(function (storedOrStatement) {
                                if (strOrStatement == JSON.stringify(storedOrStatement))
                                    ifExists = true
                            })
                            if (!ifExists) {
                                newSearchList.push(orStatementObj)
                                db.client.sessions.update({_id: sessionid}, {
                                    $set: {
                                        searchList: JSON.stringify(newSearchList)
                                    }
                                }, function (err) {
                                    if (err) {
                                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                        next({s: false})
                                    } else {
                                        next({s: true, m: newSearchList})
                                    }
                                })
                            } else {
                                next({s: false, m: "Search condition already Exists"})
                            }
                        }
                    }
                })
            })
        } else {
            next({s: false})
        }
    },

    /**
     * Removing a search level for a session id
     * @param sessionid
     * @param type
     * @param word
     * @param topic
     * @param next
     */
    removeSearchLevel: function (sessionid, type, word, topic, next) {
        // defining variables for constructing or statement
        var commandList = [[word]], searchmode, languageMap = {}
        if (type == "keywords") {
            searchmode = "keyword"
        } else if (type == "terms" || type == "ner" || type == "hashtags" || type == "mentions") {
            searchmode = "text"
        } else if (type == "concepts") {
            searchmode = "concept"
        } else if (type == "domains") {
            searchmode = "domain"
        } else {
            searchmode = null
        }

        if (!_.isNull(searchmode)) {
            constructOrStatement(searchmode, [commandList, null], topic, languageMap, function (orStatement) {
                var orStatementObj = {s: searchmode, o: orStatement}
                db.client.sessions.findOne({_id: sessionid}, function (err, session) {
                    if (err) {
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        next({s: false})
                    } else {
                        if (!_.isUndefined(session.searchList)) {
                            var previousSearchList = JSON.parse(session.searchList)  // pre-existing search list
                            var newSearchList = []
                            var strOrStatement = JSON.stringify(orStatementObj)
                            previousSearchList.forEach(function (storedOrStatement) {
                                if (strOrStatement != JSON.stringify(storedOrStatement)) {
                                    newSearchList.push(storedOrStatement)
                                }
                            })
                            db.client.sessions.update({_id: sessionid}, {
                                $set: {
                                    searchList: JSON.stringify(newSearchList)
                                }
                            }, function (err) {
                                if (err) {
                                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                    next({s: false})
                                } else {
                                    next({s: true, m: newSearchList})
                                }
                            })
                        }
                    }
                })
            })
        }
    },

    /**
     * Doing the Search from the session seach queue
     * @param sessionid
     * @param topic
     * @param ifloadmore
     * @param next
     */
    doSearch: function (sessionid, topic, ifloadmore, next) {
        SUPPORTAPI.STATS.doSearch(sessionid, topic, ifloadmore)
            .then(function (r) {
                if (!_.isUndefined(r) && !_.isUndefined(r.r)) {
                    var responseid = r.r
                    db.client.aggregate.findOne({_id: responseid}, function (err, res) {
                        if (err)
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        else {
                            // fetching the verbatims
                            db.indexes.verbatims.find({_id: {$in: res.vids}}).toArray(function (err, verbatims) {
                                res.verbatims = verbatims
                                next(res)
                                // delete tmp result
                                db.client.aggregate.remove({_id: responseid}, function (err) {
                                    if (err)
                                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                })
                            })
                        }
                    })
                } else {
                    next(r)
                }
            })
    }
}

/**
 * Method for constructing the orStatements from commandLists and searchmodes
 * This method is a requirment for communicating with the insighte_support_api
 * @param searchmode
 * @param commandList
 * @param topic
 * @param languageMap
 * @param next
 */
var constructOrStatement = function (searchmode, commandList, topic, languageMap, next) {
    var orStatement

    // findStatement is inverse of notStatement
    var findStatement, notStatement
    if (commandList.length == 0) {
        findStatement = commandList
    } else {
        findStatement = commandList[0]
    }

    // TODO add negation statement here
    if (searchmode == "text") {
        CommandListParser.getTermsFromCommands(findStatement, function (terms, termMap) {
            orStatement = CommandListParser.constructOrStatementTerm(findStatement, terms, termMap)
            next(orStatement)
        })
    } else if (searchmode == "keyword") {
        CommandListParser.getKeywordsFromCommands(findStatement, function (keywords, keywordMap) {
            orStatement = CommandListParser.constructOrStatementKeyword(findStatement, keywords, keywordMap)
            next(orStatement)
        })
    } else if (searchmode == "tag") {
        orStatement = CommandListParser.constructOrStatementTag(findStatement)
        next(orStatement)
    } else if (searchmode == "concept") {
        SETTINGS.getLang(topic).then(function (globalLang) {
            // gettings edges for each command
            var conceptEdgesToBeLoaded = [], conceptToEdgesMap = {}
            commandList.forEach(function (cL1) {
                if (!_.isNull(cL1) && !_.isUndefined(cL1)) {
                    cL1.forEach(function (cL) {
                        cL.forEach(function (command) {
                            var lang
                            if (_.isUndefined(languageMap[command]))
                                lang = globalLang
                            else
                                lang = languageMap[command]
                            conceptToEdgesMap[conceptEdgesToBeLoaded.length] = command
                            conceptEdgesToBeLoaded.push(conceptnet.getConcepts(command, lang))
                        })
                    })
                }
            })
            Promise.all(conceptEdgesToBeLoaded)
                .then(function (edgeList) {
                    // filtering edges
                    var edgesToBeFiltered = []
                    edgeList.forEach(function (edges) {
                        edgesToBeFiltered.push(conceptnet.filterEdges(edges))
                    })
                    Promise.all(edgesToBeFiltered)
                        .then(function (filteredEdges) {
                            // mapping concepts to their edges
                            var conceptToEdges = {}
                            for (var i = 0; i < filteredEdges.length; i++) {
                                //var edges = filteredEdges[i];
                                var edges = edgeList[i];
                                var concept = conceptToEdgesMap[i]
                                conceptToEdges[concept] = {
                                    concept: concept,
                                    edges: edges
                                }
                            }
                            // build db query statement
                            orStatement = CommandListParser.constructOrStatementConcept(
                                findStatement, conceptToEdges, globalLang
                            )
                            next(orStatement)
                        })
                })
        })
    } else if (searchmode == "domain") {
        orStatement = CommandListParser.constructOrStatementDomain(commandList)
        next(orStatement)
    }
}