var Promise = require('bluebird')
var db = require('../dbconnection')
var _ = require('underscore')
var conceptnet = require('../conceptnet')
var CommandListParser = require('./commandlistparser')
var SETTINGS = require('../settings')
var MAXSEARCHLENGTH = 15
var Error = require('../error/error'),
    error = new Error()
var VERBATIMMANAGER = require('../verbatims')
/**
 * Verbatim Manager Plugin for Searching
 * This plugin handles all Search Related Tasks
 */
module.exports = {

    /**
     * Get verbatim ids given a list of concepts
     * @param topic
     * @param conceptList
     * @param next
     */
    getVerbatimsByConcept: function (topic, conceptList, next) {
        if (_.isArray(conceptList)) {
            db.indexes.verbatimconcepts.find({topic: topic, concepts: {$in: conceptList}})
                .toArray(function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else
                        next(items)
                })
        } else {
            console.log("error: connection API conceptnet failed")
            next([])
        }
    },

    /**
     * Get verbatim uds given a list of nouns
     * @param topic
     * @param nounList
     * @param iteration
     * @param next
     */
    getVerbatimsByNoun: function (topic, nounList, iteration, next) {
        if (_.isArray(nounList)) {
            db.indexes.verbatimnouns.find({topic: topic, words: {$in: nounList}})
                .skip(MAXSEARCHLENGTH * iteration)
                .limit(MAXSEARCHLENGTH)
                .toArray(function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack);
                    else
                        next(_.pluck(items, '_id'))
                })
        } else {
            console.log("error: connection API conceptnet failed")
            next([])
        }
    },

    /**
     * Get verbatim ids given a list of verbs list
     * @param topic
     * @param verbList
     * @param iteration
     * @param next
     */
    getVerbatimsByVerb: function (topic, verbList, iteration, next) {
        if (_.isArray(verbList)) {
            db.indexes.verbatimverbs.find({topic: topic, words: {$in: verbList}})
                .skip(MAXSEARCHLENGTH * iteration)
                .limit(MAXSEARCHLENGTH)
                .toArray(function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack);
                    else
                        next(_.pluck(items, '_id'))
                })
        } else {
            console.log("error: connection API conceptnet failed")
            next([])
        }
    },

    /**
     * Get verbatim ids given a list of adjectives
     * @param topic
     * @param adjectiveList
     * @param iteration
     * @param next
     */
    getVerbatimsByAdjective: function (topic, adjectiveList, iteration, next) {
        if (_.isArray(adjectiveList)) {
            db.indexes.verbatimadjectives.find({topic: topic, words: {$in: adjectiveList}})
                .skip(MAXSEARCHLENGTH * iteration)
                .limit(MAXSEARCHLENGTH)
                .toArray(function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack);
                    else
                        next(_.pluck(items, '_id'))
                })
        } else {
            console.log("error: connection API conceptnet failed")
            next([])
        }
    },

    /**
     * Searching for keywords
     * Using MongoDB Text Indexing feature to do search queries
     * @param topic
     * @param keyword
     * @param next
     */
    searchForKeywords: function (topic, keyword, next) {
        db.indexes.keywords.find({
            topics: {$in: [topic]},
            $text: {$search: keyword}
        }).sort({keyword: 1}).toArray(function (err, items) {
            if (_.isArray(items)) {
                next(items.slice(0, 3))
            } else {
                next([])
            }
        })
    },

    /**
     * Searching for keywords using regex patterns
     * Pattern is build as the following ^keyword.*
     * @param topic
     * @param keyword
     * @param next
     */
    searchForKeywordsRegex: function (topic, keyword, next) {
        try {
            var exp = new RegExp('^' + keyword + '*')
            db.indexes.keywords.find({
                topics: {$in: [topic]},
                keyword: {$regex: exp}
            }).sort({keyword: 1}).toArray(function (err, items) {
                if (_.isArray(items)) {
                    next(items.slice(0, 4))
                } else {
                    next([])
                }
            })
        } catch (e) {
            next([])
        }
    },

    /**
     * Searching for terms
     * Using MongoDB Text Indexing feature to do search queries
     * @param topic
     * @param keyword
     * @param next
     */
    searchForTerms: function (topic, keyword, next) {
        db.indexes.terms.find({
            topics: {$in: [topic]},
            $text: {$search: keyword}
        }).sort({term: 1}).toArray(function (err, items) {
            if (_.isArray(items)) {
                next(items.slice(0, 3))
            } else {
                next([])
            }
        })
    },

    /**
     * Searching for terms using regex patterns
     * Pattern is build as the following ^keyword.*
     * @param topic
     * @param keyword
     * @param next
     */
    searchForTermsRegex: function (topic, keyword, next) {
        try {
            var exp = new RegExp('^' + keyword + '*')
            db.indexes.terms.find({
                topics: {$in: [topic]},
                term: {$regex: exp}
            }).sort({term: 1}).toArray(function (err, items) {
                if (_.isArray(items)) {
                    next(items.slice(0, 4))
                } else {
                    next([])
                }
            })
        } catch (e) {
            next([])
        }
    },

    /**
     * Getting concepts for a given keyword and langugage
     * Using the ConceptNet Module for queries the conceptnet server on dbServer
     * @param topic
     * @param keyword
     * @param next
     */
    searchForConcepts: function (topic, keyword, next) {
        SETTINGS.getLang(topic)
            .then(function (lang) {
                conceptnet.getConcepts(keyword, lang).then(function (edges) {
                    conceptnet.filterEdges(edges).then(function (edges) {
                        next(edges)
                    })
                })
            })
    },

    /**
     * Searching for tags using regex pattern
     * Pattern is build as the following ^keyword.*
     * @param topic
     * @param keyword
     * @param next
     */
    searchForTags: function (topic, keyword, next) {

        var keywordRegExp
        if (keyword.trim() != "") {
            keywordRegExp = new RegExp('^' + keyword + '*')
        } else {
            keywordRegExp = new RegExp('.*')
        }

        var searchQuery
        if (!_.isNull(topic) && !_.isArray(topic)) {
            searchQuery = {topic: topic, tags: {$elemMatch: {$regex: keywordRegExp}}}
        } else {
            if (_.isNull(topic))
                searchQuery = {tags: {$elemMatch: {$regex: keywordRegExp}}}
            else
                searchQuery = {topic: {$in: topic}, tags: {$elemMatch: {$regex: keywordRegExp}}}
        }

        db.indexes.verbatimtags.find(searchQuery)
            .limit(5)
            .toArray(
                function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        // filtering tags by regex
                        var tagsAppended = []
                        items.forEach(function (item) {
                            item.tags.forEach(function (tag) {
                                if (!_.contains(tagsAppended, tag)) {
                                    var p = new RegExp('^' + keyword + '*')
                                    if (p.exec(tag)) {
                                        tagsAppended.push(tag)
                                    }
                                }
                            })
                        })
                        if (tagsAppended) {
                            next(tagsAppended.slice(0, 5))
                        }
                    }
                }
            )
    },
    /**
     * Using method searchForTags to suggest autocompletions of tags
     * @param keyword
     * @param topic
     * @param next
     */
    autoCompleteTags: function (keyword, topic, next) {
        var self = this
        db.indexes.settings.findOne({topic: topic}, function (err, setting) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                // get all topics in project
                db.indexes.settings.distinct("topic", {project: setting.project}, function (err, projectTopics) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        if (_.isEmpty(projectTopics))
                            next([])
                        else {
                            self.searchForTags(projectTopics, keyword, function (tags) {
                                var r = []
                                // reformat response for autocompleter
                                tags.forEach(function (tag) {
                                    r.push({title: tag})
                                })
                                next({
                                    results: r
                                })
                            })
                        }
                    }
                })
            }
        })
    },

    /**
     * Getting verbatims by concepts from commandList
     * Language Map holds a dictionary mapping concepts to their languages
     * If a language is not defined for a concept, then the default topic language
     * is used for that concept
     * @param topic
     * @param commandList
     * @param languageMap
     * @param iteration
     * @param next
     */
    // TODO BUG figure out if there is error here!!!! Gonna blow my head!
    getVerbatimsByConceptsForCommandList: function (topic, commandList, languageMap, iteration, next) {
        var self = this
        SETTINGS.getLang(topic)
            .then(function (globalLang) {
                // gettings edges for each command
                var conceptEdgesToBeLoaded = [], conceptToEdgesMap = {}
                commandList.forEach(function (cL1) {
                    if (!_.isNull(cL1) && !_.isUndefined(cL1)) {
                        cL1.forEach(function (cL) {
                            cL.forEach(function (command) {
                                var lang
                                if (_.isUndefined(languageMap[command]))
                                    lang = globalLang
                                else
                                    lang = languageMap[command]
                                conceptToEdgesMap[conceptEdgesToBeLoaded.length] = command
                                conceptEdgesToBeLoaded.push(conceptnet.getConcepts(command, lang))
                            })
                        })
                    }
                })

                Promise.all(conceptEdgesToBeLoaded)
                    .then(function (edgeList) {
                        // filtering edges
                        var edgesToBeFiltered = []
                        edgeList.forEach(function (edges) {
                            edgesToBeFiltered.push(conceptnet.filterEdges(edges))
                        })
                        Promise.all(edgesToBeFiltered)
                            .then(function (filteredEdges) {
                                // mapping concepts to their edges
                                var conceptToEdges = {}
                                for (var i = 0; i < filteredEdges.length; i++) {
                                    //var edges = filteredEdges[i];
                                    var edges = edgeList[i];
                                    var concept = conceptToEdgesMap[i]
                                    conceptToEdges[concept] = {
                                        concept: concept,
                                        edges: edges
                                    }
                                }
                                // build db query statement
                                var OrStatment = CommandListParser.constructOrStatementConcept(commandList[0], conceptToEdges, globalLang)

                                // fetch verbatimIDS by concept or statment
                                self.getVerbatimsIDSByQuery(
                                    db.indexes.verbatimconcepts,
                                    {topic: topic, $or: OrStatment},
                                    MAXSEARCHLENGTH * iteration,
                                    MAXSEARCHLENGTH
                                ).then(function (itemIDS) {
                                    var verbatimIDS = _.pluck(itemIDS, '_id')
                                    // fetch verbaimts
                                    self.getVerbatimsByQuery(db.indexes.verbatims, {
                                        topic: topic,
                                        _id: {$in: verbatimIDS},
                                        enabled: true
                                    })
                                        .then(function (items) {
                                            var scores = conceptnet.scoreVerbatims(itemIDS)
                                            for (var i = 0; i < items.length; i++) {
                                                items[i].score = scores[items[i]['_id']]
                                            }

                                            self.getCountByQuery(db.indexes.verbatimconcepts, {
                                                topic: topic,
                                                $or: OrStatment
                                            })
                                                .then(function (count) {
                                                    next({
                                                        verbatims: items,
                                                        edges: conceptToEdges,
                                                        keywords: _.keys(conceptToEdges),
                                                        c: count
                                                    })
                                                })
                                        })
                                })
                            })
                    })
            })
    },

    /**
     * Getting verbatims by keywords from commandlist
     * @param topic
     * @param commandList
     * @param iteration
     * @param next
     */
    getVerbatimsByKeywordForCommandList: function (topic, commandList, iteration, next) {
        var self = this
        // fetching keywords
        CommandListParser.getKeywordsFromCommands(commandList[0], function (keywords, keywordMap) {
            CommandListParser.getKeywordsFromCommands(commandList[1], function (notKeywords, notKeywordMap) {

                // construct keywordIdList
                var keywordIdList = _.pluck(keywords, '_id')
                // construct keywordIdList
                var notKeywordIdList = _.pluck(notKeywords, '_id')
                // constructing search query
                var orStatement = CommandListParser.constructOrStatementKeyword(commandList[0], keywords, keywordMap)
                var notStatement = CommandListParser.parseForNotStatement(
                    CommandListParser.constructOrStatementKeyword(commandList[1], notKeywords, notKeywordMap)
                )

                var q
                if (_.isNull(notStatement) || _.isUndefined(notStatement)) {
                    q = {topic: topic, $or: orStatement, enabled: true}
                } else {
                    // append
                    notStatement.push({$or: orStatement})
                    q = {topic: topic, $and: notStatement, enabled: true}
                }

                // fetching verbatims _ids
                self.getVerbatimsIDSByQuery(
                    db.indexes.verbatimkeywords,
                    q,
                    MAXSEARCHLENGTH * iteration,
                    MAXSEARCHLENGTH
                )
                    .then(function (items) {
                        // calculating weights verbatims
                        function calculateWeights(items) {
                            var weights = []
                            for (var i = 0; i < items.length; i++) {
                                weights[i] = 0
                                for (var j = 0; j < items[i]['_id_keywords'].length; j++) {
                                    var idKeyword = items[i]['_id_keywords'][j];
                                    if (keywordIdList.indexOf(idKeyword) != -1) {
                                        weights[i] += 1 * items[i]['weight'][j]
                                    }
                                }
                            }
                            return weights
                        }

                        var weights = calculateWeights(items)
                        var verbatimIdList = _.pluck(items, '_id')
                        self.getVerbatimsByQuery(db.indexes.verbatims, {
                            topic: topic,
                            _id: {$in: verbatimIdList},
                            enabled: true
                        })
                            .then(function (verbatims) {
                                // append score to verbatim
                                for (var i = 0; i < verbatims.length; i++) {
                                    verbatims[i].score = weights[i]
                                }
                                // getting count
                                self.getCountByQuery(db.indexes.verbatimkeywords, q).then(function (count) {
                                    // callback
                                    next({status: true, r: verbatims, k: keywords, c: count})
                                })
                            })
                    })
            })
        })
    },

    /**
     * Getting verbatims from terms by command list
     * @param topic
     * @param commandList
     * @param iteration
     * @param next
     */
    getVerbatimsByTermsForCommandList: function (topic, commandList, iteration, next) {
        var self = this
        // fetching terms
        CommandListParser.getTermsFromCommands(commandList[0], function (terms, termMap) {
            CommandListParser.getTermsFromCommands(commandList[1], function (notTerms, notTermMap) {
                // constructing search query
                var orStatement = CommandListParser.constructOrStatementTerm(commandList[0], terms, termMap)
                var notStatement = CommandListParser.parseForNotStatement(
                    CommandListParser.constructOrStatementTerm(commandList[1], notTerms, notTermMap)
                )

                var q
                if (_.isNull(notStatement) || _.isUndefined(notStatement)) {
                    q = {topic: topic, $or: orStatement, enabled: true}
                } else {
                    // append
                    notStatement.push({$or: orStatement})
                    q = {topic: topic, $and: notStatement, enabled: true}
                }

                // fetching verbatim _ids
                self.getVerbatimsIDSByQuery(
                    db.indexes.verbatimterms,
                    q,
                    MAXSEARCHLENGTH * iteration,
                    MAXSEARCHLENGTH
                ).then(function (items) {
                    var verbatimIdList = _.pluck(items, '_id')
                    self.getVerbatimsByQuery(db.indexes.verbatims, {
                        topic: topic,
                        _id: {$in: verbatimIdList},
                        enabled: true
                    })
                        .then(function (verbatims) {
                            for (var i = 0; i < verbatims.length; i++) {
                                verbatims[i].score = 0
                            }
                            // getting count
                            self.getCountByQuery(db.indexes.verbatimterms, q)
                                .then(function (count) {
                                    // callback
                                    next({status: true, r: verbatims, k: terms, c: count})
                                })
                        })
                })
            })
        })
    },

    /**
     * Getting verbatims by tag from command list
     * @param topic
     * @param commandList
     * @param iteration
     * @param next
     */
    getVerbatimsByTagForCommandList: function (topic, commandList, iteration, next) {
        var self = this
        // fetching terms
        var orStatement = CommandListParser.constructOrStatementTag(commandList[0])
        var notStatement = CommandListParser.parseForNotStatement(
            CommandListParser.constructOrStatementTag(commandList[1])
        )


        // make list of tags
        var tags = []
        commandList.forEach(function (cL1) {
            if (!_.isNull(cL1) && !_.isUndefined(cL1)) {
                cL1.forEach(function (cL) {
                    cL.forEach(function (tag) {
                        if (!_.contains(tags, tag))
                            tags.push(tag)
                    })
                })
            }
        })

        var q
        if (_.isNull(notStatement) || _.isUndefined(notStatement)) {
            q = {topic: topic, $or: orStatement} // Tags collection has no enabled field
        } else {
            // append
            notStatement.push({$or: orStatement})
            q = {topic: topic, $and: notStatement}  // Tags collection has no enabled field
        }

        // fetching verbatim _ids
        self.getVerbatimsIDSByQuery(
            db.indexes.verbatimtags,
            q,
            MAXSEARCHLENGTH * iteration,
            MAXSEARCHLENGTH
        ).then(function (items) {
            var verbatimIdList = _.pluck(items, '_id')
            self.getVerbatimsByQuery(db.indexes.verbatims, {topic: topic, _id: {$in: verbatimIdList}, enabled: true})
                .then(function (verbatims) {
                    for (var i = 0; i < verbatims.length; i++) {
                        verbatims[i].score = 0
                    }

                    // TODO filter disabled verbatims

                    // getting count
                    self.getCountByQuery(db.indexes.verbatimtags, q)
                        .then(function (count) {
                            // callback
                            next({status: true, r: verbatims, k: tags, c: count})
                        })

                })
        })
    },

    /**
     * Getting predicted verbatims by insight
     * @param topic
     * @param insight
     * @param iteration
     * @param next
     */
    getPredictedVerbatimsByInight: function (topic, insight, iteration, next) {
        var self = this
        self.getVerbatimsIDSByQuery(
            db.modal.predictions,
            {insightid: insight._id, topic: topic},
            MAXSEARCHLENGTH * iteration,
            MAXSEARCHLENGTH,
            {score: -1}
        ).then(function (items) {
            var verbatimIdList = _.pluck(items, 'vid')
            // extracting scores
            var vidScores = {}
            items.forEach(function (item) {
                vidScores[item['vid']] = item['score']
            })
            if (!_.isEmpty(verbatimIdList)) {
                self.getVerbatimsByQuery(db.indexes.verbatims, {_id: {$in: verbatimIdList}, enabled: true})
                    .then(function (verbatims) {
                        for (var i = 0; i < verbatims.length; i++) {
                            verbatims[i].score = vidScores[verbatims[i]._id]
                        }
                        var sortedVerbatims = _.sortBy(verbatims, 'score').reverse()

                        self.getCountByQuery(db.modal.predictions, {insightid: insight._id, topic: topic})
                            .then(function (count) {
                                // callback
                                next({s: true, r: sortedVerbatims, c: count})
                            })
                    })
            } else {
                next({s: false, r: [], c: 0})
            }
        })
    },

    /**
     * Wrapper for quering the database for verbatims ids
     * The database collection, search query, skip, limit and sort variables
     * are passed into the wrapper, and finally the wrapper executes the database query
     * @param collection
     * @param query
     * @param skip
     * @param limit
     * @param sort
     */
    getVerbatimsIDSByQuery: function (collection, query, skip, limit, sort) {
        return new Promise(function (resolve, reject) {
            if (_.isUndefined(sort)) {
                collection.find(query).skip(skip).limit(limit).toArray(function (err, items) {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(items)
                    }
                })
            } else {
                collection.find(query).sort(sort).skip(skip).limit(limit).toArray(function (err, items) {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(items)
                    }
                })

            }
        })
    },

    /**
     * Wrapper for querying the database for verbatims
     * The database collection and the search query is passed in and the wrapper
     * finally executes the search query
     * @param collection
     * @param query
     */
    getVerbatimsByQuery: function (collection, query) {
        return new Promise(function (resolve, reject) {
            collection.find(query).toArray(function (err, r) {
                if (err) {
                    reject(err)
                } else {
                    resolve(r)
                }
            })
        })
    },

    /**
     * Wrapper for query the database for verbatim count
     * The database collection and the search query is passed in and the wrapper
     * finally executes the search query
     * @param collection
     * @param query
     */
    getCountByQuery: function (collection, query) {
        return new Promise(function (resolve, reject) {
            collection.count(query, function (err, count) {
                if (err) {
                    reject(err)
                } else {
                    resolve(count)
                }
            })
        })
    },

}
