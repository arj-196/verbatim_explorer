var db = require('../dbconnection')
var Error = require('../error/error'),
    error = new Error()
var CommandListParser = require('./commandlistparser')
var VERBATIMMANAGER = require('../verbatims')
var SEARCHMANAGER = require('./search')
var _ = require('underscore')
var Promise = require('bluebird')
var utils = require('../utils/common')
var InsightManager = require('./insights')

var MAXSEARCHLENGTH = 20
var MAXHISTORY = 1000

var dependentCollections = [
    'verbatimadjectives',
    'verbatimconcepts',
    'verbatimkeywords',
    'verbatimners',
    'verbatimnouns',
    'verbatimtags',
    'verbatimterms',
    'verbatimverbs',
]

module.exports = {

    /**
     * Search for verbatims to be disabled
     * data needs the following fields
     *  - topic
     *  - query
     *  - mode [keyword|text|phrase]
     * @param data
     * @param callback
     */
    doSearch: function (data, callback) {
        var self = this
        data.query = JSON.parse(data.query)
        if (data.mode != 'phrase') {
            // keyword or text
            constructOrStatement(data, function (err, orStatement) {
                if (err) {
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    callback(true, {s: false})
                } else {
                    var col = null
                    var query = {topic: data.topic, $or: orStatement}
                    if (data.mode == 'keyword') {
                        col = db.indexes.verbatimkeywords
                    } else if (data.mode == 'text') {
                        col = db.indexes.verbatimterms
                    }
                    SEARCHMANAGER.getVerbatimsIDSByQuery(col, query, 0, MAXSEARCHLENGTH)
                        .then(function (itemIDS) {
                            var verbatimIDS = _.pluck(itemIDS, '_id')
                            SEARCHMANAGER.getVerbatimsByQuery(db.indexes.verbatims, {
                                topic: data.topic,
                                _id: {$in: verbatimIDS},
                                enabled: true
                            })
                                .then(function (items) {
                                    VERBATIMMANAGER.getDisabledVerbatimIds(data.topic, function (err, disabledVerbatims) {
                                        SEARCHMANAGER.getCountByQuery(col, {
                                            topic: data.topic,
                                            $or: orStatement,
                                            _id: {$nin: disabledVerbatims}
                                        }).then(function (count) {
                                            callback(false, {
                                                s: true,
                                                verbatims: items,
                                                c: count,
                                                q: data,
                                            })
                                        })
                                    })
                                })
                        })
                }
            })
        } else {
            // phrase
            // TODO handle Bad Regex Error
            var re = new RegExp('.*' + data.query + '.*', 'i')
            SEARCHMANAGER.getVerbatimsByQuery(db.indexes.verbatims, {
                topic: data.topic,
                enabled: true,
                verbatim: re
            }).then(function (items) {
                callback(false, {
                    s: true,
                    verbatims: items.slice(0, MAXSEARCHLENGTH),
                    c: items.length,
                    q: data,
                })
            })
        }
    },

    /**
     * Disable verbatims by the search query
     * and record disable action
     * @param data
     * @param user
     * @param callback
     */
    disableVerbatims: function (data, user, callback) {
        var self = this
        data.query = JSON.parse(data.query)
        VERBATIMMANAGER.getDisabledVerbatimIds(data.topic, function (err, disabledVerbatims) {
            if (data.mode != 'phrase') {
                // keyword or text
                constructOrStatement(data, function (err, orStatement) {
                    var col = null
                    var query = {topic: data.topic, $or: orStatement}
                    if (data.mode == 'keyword') {
                        col = db.indexes.verbatimkeywords
                    } else if (data.mode == 'text') {
                        col = db.indexes.verbatimterms
                    }
                    col.find(query, {_id: 1}).toArray(function (err, items) {
                        if (err) {
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                            callback(true, {s: false})
                        } else {
                            var ids = _.pluck(items, '_id')
                            var newDisabledIds = _.difference(ids, disabledVerbatims)
                            if (newDisabledIds.length > 0) {
                                db.indexes.verbatims.update(
                                    {_id: {$in: newDisabledIds}, enabled: true},
                                    {$set: {enabled: false}},
                                    {multi: true},
                                    function (err) {
                                        if (err) {
                                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                            callback(true, {s: false})
                                        } else {
                                            callback(false, {s: true})
                                            recordDisableHistory(data, ids, user)
                                            toggleVerbatimsInDependantCollections(newDisabledIds, false)
                                            InsightManager.updateInsightSettingsForTopic(data.topic, false)
                                        }
                                    })
                            }
                        }
                    })
                })
            } else {
                // phrase
                // TODO handle Bad Regex Error
                var re = new RegExp('.*' + data.query + '.*', 'i')
                var query = {
                    topic: data.topic,
                    verbatim: re,
                    enabled: true,
                }
                var col = db.indexes.verbatims
                col.find(query, {_id: 1}).toArray(function (err, items) {
                    if (err) {
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        callback(true, {s: false})
                    } else {
                        var ids = _.pluck(items, '_id')
                        var newDisabledIds = _.difference(ids, disabledVerbatims)
                        if (newDisabledIds.length > 0) {
                            col.update(
                                {_id: {$in: newDisabledIds}},
                                {$set: {enabled: false}},
                                {multi: true},
                                function (err) {
                                    if (err) {
                                        callback(true, {s: false})
                                    } else {
                                        callback(false, {s: true})
                                        recordDisableHistory(data, ids, user)
                                        toggleVerbatimsInDependantCollections(newDisabledIds, false)
                                        InsightManager.updateInsightSettingsForTopic(data.topic, false)
                                    }
                                })
                        }
                    }
                })
            }
        })

    },

    /**
     * Get disable history for a topic
     * @param topic
     * @param iteration
     * @param callback
     */
    getHistory: function (topic, iteration, callback) {
        db.cleaner.history.find({topic: topic})
            .sort({date: -1})
            .skip(iteration * MAXHISTORY).limit(MAXHISTORY)
            .toArray(function (err, items) {
                if (err) {
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    callback(true, {s: false})
                } else {
                    items.forEach(function (item) {
                        item.nbVerbatims = item.ids.length
                        item.ids = null
                    })
                    callback(false, {s: true, r: items})
                }
            })
    },

    /**
     * Undoing disable change given the history record entry id
     * @param data
     * @param callback
     */
    undoDisable: function (data, callback) {
        var self = this
        var id = data.id
        db.cleaner.history.findOne({_id: id}, function (err, item) {
            if (err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                callback(true, {s: false})
            } else {
                // enable verbatims
                db.indexes.verbatims.update({_id: {$in: item.ids}}, {$set: {enabled: true}}, {multi: true}, function (err) {
                    if (err) {
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        callback(true, {s: false})
                    } else {
                        db.cleaner.history.remove({_id: id}, function (err) {
                            if (err) {
                                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                callback(true, {s: false})
                            } else {
                                toggleVerbatimsInDependantCollections(item.ids, true, function () {
                                    // re-calibrate cleaning
                                    self.recalibrateCleaning(data.topic, function (err, r) {
                                        if (r.s) {
                                            callback(false, {s: true})
                                            InsightManager.updateInsightSettingsForTopic(data.topic, false)
                                        } else
                                            callback(true, {s: false})
                                    })
                                })
                            }
                        })
                    }
                })
            }
        })
    },

    recalibrateCleaning: function (topic, callback) {
        var self = this
        var disableVerbatimsAsync = Promise.promisify(self.disableVerbatims)
        db.cleaner.history.find({topic: topic}).sort({date: -1}).toArray(function (err, items) {
            var disableRequestQueue = []
            items.forEach(function (item) {
                var data = item.query
                data.query = JSON.stringify(data.query)
                var user = {_id: item.user}
                disableRequestQueue.push(
                    disableVerbatimsAsync(data, user)
                )
            })

            Promise.all(disableRequestQueue).then(function () {
                callback(false, {s: true})
            })
        })
    },
}

/**
 * Insert the search query + disabledVerbatimIds in cleaner.history collection
 * @param data
 * @param verbatimIds
 * @param user
 */
var recordDisableHistory = function (data, verbatimIds, user) {
    var id = utils.encrypt(data.mode + JSON.stringify(data.query))
    db.cleaner.history.findOne({_id: id}, function (err, item) {
        if (err) {
            error.STDError([__dirname, __filename].join('/'), "", err.stack)
        } else {
            if (!item) {
                db.cleaner.history.insert({
                    _id: id,
                    topic: data.topic,
                    query: data,
                    ids: verbatimIds,
                    user: user._id,
                    date: new Date()
                }, function (err) {
                    if (err) {
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    }
                })
            } else {
                db.cleaner.history.update(
                    {_id: id},
                    {
                        $set: {
                            ids: verbatimIds,
                        }
                    }, function (err) {
                        if (err) {
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        }
                    })
            }
        }
    })
}

/**
 * Constructing OrStatement using the CommandListParser Module
 * @param data
 * @param callback
 */
var constructOrStatement = function (data, callback) {
    var orStatement = null
    if (data.mode == 'keyword') {
        CommandListParser.getKeywordsFromCommands(data.query, function (keywords, keywordMap) {
            orStatement = CommandListParser.constructOrStatementKeyword(data.query, keywords, keywordMap)
            callback(false, orStatement)
        })
    } else if (data.mode == 'text') {
        CommandListParser.getTermsFromCommands(data.query, function (terms, termMap) {
            orStatement = CommandListParser.constructOrStatementTerm(data.query, terms, termMap)
            callback(false, orStatement)
        })
    } else {
        callback(true, null)
    }
}

var toggleVerbatimsInDependantCollections = function (vids, enabled, callback) {
    var fnPromise = function (c, vids, enabled) {
        return new Promise(function (resolve, reject) {
            if (c){
                c.update(
                    {_id: {$in: vids}},
                    {
                        $set: {
                            enabled: enabled
                        }
                    },
                    {multi: true},
                    function (err) {
                        if (err) {
                            reject(err)
                        } else {
                            resolve()
                        }
                    }
                )
            } else {
                resolve()
            }
        })
    }

    var queue = []
    dependentCollections.forEach(function (col) {
        var c = db.indexes[col]
        queue.push(fnPromise(c, vids, enabled))
    })

    Promise.all(queue).then(function () {
        if (!_.isUndefined(callback))
            callback()
    })
}