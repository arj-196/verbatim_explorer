var db = require('../dbconnection')
var Error = require('../error/error'),
    error = new Error();
var MAXCONCEPTS = 10

/**
 * Verbatim Manager Plugin for Concepts
 */
module.exports = {

    /**
     * Get Concepts for Verbatims IDS
     * @param verbatimIds
     * @param next
     */
    getConcepts: function (verbatimIds, next) {
        db.indexes.verbatimconcepts.find({_id: {$in: verbatimIds}})
            .toArray(function (err, items) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    var verbatims = {}
                    items.forEach(function (item) {
                        var concepts = []
                        // create map to remember weight to index
                        var mapWeightToIndex = {}
                        for (var i = 0; i < item.weights.length; i++) {
                            if(mapWeightToIndex[item.weights[i]] == undefined)
                                mapWeightToIndex[item.weights[i]] = [i];
                            else
                                mapWeightToIndex[item.weights[i]].push(i);
                        }

                        // sort weights
                        var sortedWeights = item.weights;
                        sortedWeights.sort(function (a, b) {
                            return b - a
                        })

                        while(concepts.length < MAXCONCEPTS && sortedWeights.length > 0){
                            var w = sortedWeights.shift()
                            var index = mapWeightToIndex[w].shift()
                            concepts.push({
                                c: item.concepts[index],
                                rel: item.relations[index],
                                score: w
                            })
                        }
                        verbatims[item._id] = concepts
                    });
                    next({status: true, r: verbatims})
                }
            })
    }
}
