var db = require('../dbconnection')
var _ = require('underscore')

/**
 * Module for parsing commandlists into or-statements
 */
module.exports = {

    constructOrStatementKeyword: function (commandList, keywords, keywordMap) {
        if (!_.isNull(commandList)) {
            var orStatement = []
            commandList.forEach(function (command) {
                var keywordList = []
                command.forEach(function (keyword) {
                    if (keywords[keywordMap[keyword]])
                        keywordList.push(keywords[keywordMap[keyword]]['_id'])
                })
                orStatement.push({_id_keywords: {$all: keywordList}})
            })
            return orStatement
        } else {
            return null
        }
    },

    constructOrStatementTerm: function (commandList, terms, termMap) {
        if (!_.isNull(commandList)) {
            var orStatement = []
            commandList.forEach(function (command) {
                var termList = []
                command.forEach(function (term) {
                    if (terms[termMap[term]])
                        termList.push(terms[termMap[term]]['_id'])
                })
                orStatement.push({_id_terms: {$all: termList}})
            })
            return orStatement
        } else {
            return null
        }
    },

    constructOrStatementTag: function (commandList) {
        if (!_.isNull(commandList)) {
            var orStatement = []
            commandList.forEach(function (command) {
                var tagList = []
                command.forEach(function (tag) {
                    tagList.push(tag)
                })
                orStatement.push({tags: {$all: tagList}})
            })
            return orStatement
        } else {
            return null
        }
    },

    constructOrStatementConcept: function (commandList, conceptToEdges, lang) {
        if (!_.isNull(commandList)) {
            var orStatement = []
            commandList.forEach(function (command) {
                var andStatement = []
                command.forEach(function (concept) {
                    var conceptList = []
                    if (conceptToEdges[concept]) {
                        conceptToEdges[concept].edges.forEach(function (edge) {
                            conceptList.push(edge.concept)
                        })
                        // by default add command with default language
                        conceptList.push(concept, lang)
                    }
                    andStatement.push({concepts: {$in: _.uniq(conceptList)}})
                })
                orStatement.push({$and: andStatement})
            })
            return orStatement
        } else {
            return null
        }
    },

    constructOrStatementDomain: function (commandList) {
        if (!_.isNull(commandList)) {
            var orStatement = []
            commandList.forEach(function (domainList) {
                var andStatement = {"domainList": {$all: _.uniq(domainList)}}
                orStatement.push(andStatement)
            })
            return orStatement
        } else {
            return null
        }
    }
    ,

    getKeywordsFromCommands: function (commandList, next) {
        var keywordsToFetch = [], keywordMap = {}
        if (!_.isNull(commandList)) {
            commandList.forEach(function (command) {
                command.forEach(function (keyword) {
                    keywordsToFetch.push(db.indexes.keywords.findOneAsync({keyword: keyword}, {
                        keyword: 1,
                        topics: -1
                    }))
                    keywordMap[keyword] = keywordsToFetch.length - 1
                })
            })
            Promise.all(keywordsToFetch).then(function (keywords) {
                next(keywords, keywordMap)
            })
        } else {
            next(null, null)
        }
    }
    ,

    getTermsFromCommands: function (commandList, next) {
        var termsToFetch = [], termMap = {}
        if (!_.isNull(commandList)) {
            commandList.forEach(function (command) {
                command.forEach(function (term) {
                    termsToFetch.push(db.indexes.terms.findOneAsync({term: term}, {term: 1, topics: -1}))
                    termMap[term] = termsToFetch.length - 1
                })
            })
            Promise.all(termsToFetch).then(function (terms) {
                next(terms, termMap)
            })
        } else {
            next(null, null)
        }
    }
    ,

    parseForNotStatement: function (orStatement) {
        if(!_.isNull(orStatement)) {

        var notOrStatment = []
        orStatement.forEach(function (allStatement) {
            var field = _.keys(allStatement)[0]
            var operator = _.keys(allStatement[field])[0]

            // replacing original operator by $nin
            var q = {}
            q[field] = {$nin: allStatement[field][operator]}
            notOrStatment.push(q)
        })
        return notOrStatment

        } else {

        }
    }

}