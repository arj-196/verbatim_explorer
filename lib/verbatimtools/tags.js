var db = require('../dbconnection')
var INSIGHTMANAGER = require('./insights')
var _ = require('underscore')
var Error = require('../error/error'),
    error = new Error();

/**
 * Verbatim Manager Plugin for Tags
 */
module.exports = {

    /**
     * Get tags for verbatims ids
     * @param verbatimIds
     * @param next
     */
    getTags: function (verbatimIds, next) {
        db.indexes.verbatimtags.find({_id: {$in: verbatimIds}})
            .toArray(function (err, items) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    var verbatims = {}
                    items.forEach(function (item) {
                        verbatims[item._id] = item.tags
                    });
                    next({status: true, r: verbatims})
                }
            })
    },

    /**
     * Set tag for a verbatim id
     * @param topic
     * @param id Verbatim id
     * @param tags
     * @param next
     */
    setTags: function (topic, id, tags, next) {
        db.indexes.verbatimtags.update({_id: id}, {
            $set: {
                topic: topic,
                tags: tags
            }
        }, {upsert: true}, function (err) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else
                next({status: true})
        })

        INSIGHTMANAGER.incrementNbChangesForInsightModals(tags, topic)
    },

    /**
     * Removing a tag for a verbatim id
     * @param id Verbatim id
     * @param tag
     * @param next
     */
    removeTag: function (id, tag, next) {
        var self = this
        db.indexes.verbatimtags.findOne({_id: id}, function (err, item) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                var tags = item.tags
                var index = tags.indexOf(tag)
                if (index != -1) {
                    tags.splice(index, 1)
                }

                if (tags.length == 0) {
                    db.indexes.verbatimtags.remove({_id: id}, function (err) {
                        if (err)
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        else
                            next({status: true})
                    })
                } else {
                    self.setTags(item.topic, id, tags, next)
                }
            }
        })
    }
}