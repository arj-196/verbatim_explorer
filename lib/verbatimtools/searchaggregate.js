var Promise = require('bluebird')
var db = require('../dbconnection')
var _ = require('underscore')
var conceptnet = require('../conceptnet')
var SEARCHMANAGER = require('./searchasync')

/**
 * Verbatim Manager Plugin for the Statistics Aggregated Search
 * This plugin handles all Statistics Search Related Tasks
 */
module.exports = {

    /**
     * Get current search queue by session id
     * @param sessionid
     */
    getQueue: function (sessionid) {
        return new Promise(function (resolve, reject) {
            db.client.aggregate.findOne({_id: sessionid}, function (err, r) {
                if (_.isNull(r)) {
                    // no queue
                    db.client.aggregate.insert({_id: sessionid, search: []}, function (err, r) {
                        db.client.aggregate.findOne({_id: sessionid}, function (err, r) {
                            resolve(r)
                        })
                    })
                } else {
                    resolve(r)
                }
            })
        })
    },

    /**
     * Running the search for the given mode
     * @param topic
     * @param mode term|keyword|concept
     * @param commandList
     * @param ITERATION
     */
    runSearch: function (topic, mode, commandList, ITERATION) {
        return new Promise(function (resolve, reject) {
            if (mode == "term") {
                SEARCHMANAGER.getVerbatimsByTermsForCommandList(topic, commandList, ITERATION)
                    .then(function (items) {
                        resolve(_.pluck(items.r, '_id'))
                    })
            } else if (mode == "keyword") {
                SEARCHMANAGER.getVerbatimsByKeywordForCommandList(topic, commandList, ITERATION)
                    .then(function (items) {
                        resolve(_.pluck(items.r, '_id'))
                    })
            } else if (mode == "concept") {
                SEARCHMANAGER.getVerbatimsByConceptsForCommandList(topic, commandList, {}, ITERATION)
                    .then(function (items) {
                        resolve(_.pluck(items.verbatims, '_id'))
                    })
            }
        })
    },

    /**
     * NOT YET USED
     * @param sessionid
     * @param topic
     * @param ITERATION
     */
    aggregate: function (sessionid, topic, ITERATION) {
        var self = this
        return new Promise(function (resolve, reject) {
            self.getQueue(sessionid)
                .then(function (r) {
                    var searchesToRun = []
                    _.each(r.search, function (d) {
                        searchesToRun.push(self.runSearch(topic, "concept", d.cmd, ITERATION))
                    })
                    Promise.all(searchesToRun)
                        .then(function (searchResults) {
                            console.log()
                        })
                })
        })
    }
}
