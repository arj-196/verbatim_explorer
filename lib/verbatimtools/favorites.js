var db = require('../dbconnection')
var Error = require('../error/error'),
    error = new Error();

/**
 * DEPRECATED
 * Verbatim Manager Plugin for Favoriting verbatims 
 */
module.exports = {

    getFavorites: function (topic, next) {
        db.indexes.verbatimfavorites.find({topic: topic}).toArray(function (err, items) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else{
                var tagList = []
                items.forEach(function (item) {
                    tagList.push(item.id)
                });

                next({status: true, r: tagList})
            }
        })
    },

    addFavorite: function (topic, id, next) {
        db.indexes.verbatimfavorites.findOne({topic: topic, id: id}, function (err, item) { // using id not _id not purpose
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (!item)
                    db.indexes.verbatimfavorites.insert({topic: topic, id: id}, function (err) {
                        if (err)
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        else
                            next({status: true})
                    })
                else
                    next({status: true})
            }
        })
    },

    removeFavorite: function (topic, id, next) {
        db.indexes.verbatimfavorites.remove({topic: topic, id: id}, function (err) {
            if(err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else
                next({status: true})
        })
    }

}