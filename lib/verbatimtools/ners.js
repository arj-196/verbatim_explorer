var db = require('../dbconnection')
var Error = require('../error/error'),
    error = new Error();

module.exports = {

    /**
     * Get NERS for an array of verbatim ids
     * @param verbatimIds
     * @param next
     */
    getNERS: function (verbatimIds, next) {
        db.indexes.verbatimners.find({_id: {$in: verbatimIds}})
            .toArray(function (err, items) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    var verbatims = {}
                    items.forEach(function (item) {
                        verbatims[item._id] = item.entities
                    });
                    next({status: true, r: verbatims})
                }
            })
    },

}