var db = require('../dbconnection')
var Error = require('../error/error'),
    error = new Error();

var MAXKEYWORDS = 10

/**
 * Verbatim Manager Plugin for Keywords
 * This plugin handles all Keyword Related Tasks
 */
module.exports = {

    /**
     * Get all keywords given a verbatimID
     * @param verbatimIds
     * @param next
     */
    getKeywords: function (verbatimIds, next) {
        db.indexes.verbatimkeywords.find({_id: {$in: verbatimIds}})
            .toArray(function (err, items) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    var verbatims = {}
                    items.forEach(function (item) {
                        var keywords = []
                        // create map to remember weight to index
                        var mapWeightToIndex = {}
                        for (var i = 0; i < item.weight.length; i++) {
                            if(mapWeightToIndex[item.weight[i]] == undefined)
                                mapWeightToIndex[item.weight[i]] = [i];
                            else
                                mapWeightToIndex[item.weight[i]].push(i);
                        }

                        // sort weight
                        var sortedweight = item.weight;
                        sortedweight.sort(function (a, b) {
                            return b - a
                        })

                        while(keywords.length < MAXKEYWORDS && sortedweight.length > 0){
                            var w = sortedweight.shift()
                            var index = mapWeightToIndex[w].shift()
                            keywords.push({
                                c: item.keywords[index],
                                score: w
                            })
                        }
                        verbatims[item._id] = keywords
                    });
                    next({status: true, r: verbatims})
                }
            })
    }
}
