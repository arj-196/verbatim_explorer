var Promise = require('bluebird')
var db = require('../dbconnection')
var _ = require('underscore')
var SEARCHMANAGER = require('./search')
var Error = require('../error/error'),
    error = new Error();

/**
 * Verbatim Manager Plugin for Insights
 * This plugin handles all Insight Related Tasks
 */
module.exports = {

    /**
     * Getting insights for a given project
     * The project is taken from the topic
     * @param topic
     * @param next
     */
    getInsightForProject: function (topic, next) {
        // getting project from topic
        db.indexes.settings.findOne({topic: topic}, function (err, topic) {
            db.indexes.insights.find({project: topic.project}).toArray(function (err, insights) {
                if (err) {
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                } else {
                    next(insights)
                }
            })

        })
    },

    /**
     * Get the insight object given it's ID
     * @param id
     * @param next
     */
    getInsight: function (id, next) {
        db.indexes.insights.findOne({_id: id}, function (err, insight) {
            if (err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            } else {
                next(insight)
            }
        })
    },

    /**
     * Given a keyword, get all insights from the topic project which matches the keyword_regex_pattern
     * The regex pattern is constructed as .*keyword.*
     * @param keyword
     * @param topic
     * @param next
     */
    autoCompleteInsight: function (keyword, topic, next) {
        db.indexes.settings.findOne({topic: topic}, function (err, setting) {
            db.indexes.insights.find({name: new RegExp('.*' + keyword + '.*'), project: setting.project}).limit(20)
                .toArray(function (err, insights) {
                    for (var i = 0; i < insights.length; i++) {
                        insights[i].title = insights[i].name
                    }
                    next({results: insights})
                })
        })
    },

    /**
     * Creating a new insight
     * The data object holds all the insight information
     * The data object requires the following fields:
     *  - topic : current topic (the topic project is used for the insight creation)
     *  - name : name of the insight
     * @param data
     * @param next
     */
    createInsight: function (data, next) {
        db.indexes.settings.findOne({topic: data.topic}, function (err, topic) {
            var insightid = createInsightID(topic.project, data.name)

            // check if id already exists
            db.indexes.insights.findOne({_id: insightid}, function (err, storedInsight) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    if (storedInsight) {
                        next({s: false, m: "Please choose another name"})
                    } else {
                        db.indexes.insights.insert({
                            _id: insightid,
                            name: data.name,
                            project: topic.project,
                            tags: []
                        }, function (err) {
                            if (err) {
                                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                            } else {
                                next({s: true, id: insightid})
                            }
                        })
                    }
                }
            })
        })
    },

    /**
     * Edit an existing insight
     * The updated insight information is stored in the object data
     * The object data requires the following fields:
     *  - insightid (Not editable)
     *  - name
     * @param data
     * @param next
     */
    editInsight: function (data, next) {
        db.indexes.insights.update({_id: data.insightid}, {$set: {name: data.name}}, function (err) {
            if (err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                next({s: false})
            }
            else {
                next({s: true})
            }
        })
    },

    /**
     * Remove an insight given it's id
     * @param id
     * @param topic
     * @param next
     */
    removeInsight: function (id, topic, next) {
        // removing insight from index
        db.indexes.insights.remove({_id: id}, function (err) {
            if (err) {
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                next({s: false, m: "Error on deleting "})
            } else {
                db.modal.predictions.remove({insightid: id}, function (err) {
                    if (err) {
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        next({s: false, m: "Error on deleting "})
                    } else {
                        db.modal.rejects.remove({insightid: id}, function (err) {
                            if (err) {
                                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                next({s: false, m: "Error on deleting "})
                            } else {
                                next({s: true})
                            }
                        })
                    }
                })
            }
        })
    },

    /**
     * Update an insight's ifCompiled variable in settings
     * Topic is required because the ifCompiled variable
     * is stored for an insight inrespect of a topic
     * @param id
     * @param topic
     * @param ifCompiled
     */
    updateInsightSettings: function (id, topic, ifCompiled) {
        db.modal.settings.update({insightid: id, topic: topic}, {
            $set: {
                compiled: ifCompiled
            }
        }, function (err) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
        })
    },

    /**
     * Update all insight's ifCompiled variable in settings
     * for a given topic
     * @param topic
     * @param ifCompiled
     */
    updateInsightSettingsForTopic: function (topic, ifCompiled) {
        db.modal.settings.update({topic: topic},
            {
                $set: {
                    compiled: ifCompiled
                },
                $unset: {
                    s_index: ""
                }
            },
            {multi: true},
            function (err) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
            })
    },

    /**
     * Adding tags to an Insight
     * @param id InsightID
     * @param tags Array of tags
     * @param topic
     * @param next
     */
    addTagsToInsight: function (id, tags, topic, next) {
        var self = this
        db.indexes.insights.findOne({_id: id}, function (err, insight) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else if (!insight) {
                next({s: false, m: "insight not found!"})
            } else {
                // found insight
                var newTags = _.unique(_.union(insight.tags, tags))
                db.indexes.insights.update({_id: id}, {$set: {tags: newTags}}, function (err) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        next({s: true})
                    }
                })
                // update settings
                self.updateInsightSettings(id, topic, false)
            }
        })
    },

    /**
     * Check if Tag Exists
     * @param keyword
     * @param topic
     * @param next
     */
    findTagsFromQuery: function (keyword, topic, next) {
        var self = this
        db.indexes.settings.findOne({topic: topic}, function (err, setting) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                // get all topics in project
                db.indexes.settings.distinct("topic", {project: setting.project}, function (err, projectTopics) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        if (_.isEmpty(projectTopics))
                            next([])
                        else {
                            // search for tags in projectTopics
                            db.indexes.verbatimtags.find({topic: {$in: projectTopics}, tags: {$in: [keyword]}})
                                .toArray(
                                    function (err, items) {
                                        if (err)
                                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                        else {
                                            if (items.length == 0)
                                                next([])
                                            else {
                                                next([keyword])
                                            }
                                        }
                                    }
                                )
                        }
                    }
                })
            }
        })
    },

    /**
     * Remove a tag from an insight
     * @param id
     * @param tag
     * @param topic
     * @param next
     */
    removeTagFromInsight: function (id, tag, topic, next) {
        var self = this
        db.indexes.insights.findOne({_id: id}, function (err, insight) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (!insight)
                    next({s: false, m: "insight not found!"})
                else {
                    var newTags = _.difference(insight.tags, [tag])
                    db.indexes.insights.update({_id: id}, {$set: {tags: newTags}}, function (err) {
                        if (err)
                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        else {
                            next({s: true})
                        }
                    })
                    // update settings
                    self.updateInsightSettings(id, topic, false)
                }
            }
        })
    },

    /**
     * Get tagged verbatims given the insightID and a topic
     * @param id InsightID
     * @param iteration
     * @param topic
     * @param next
     */
    getTaggedVerbatims: function (id, iteration, topic, next) {
        db.indexes.insights.findOne({_id: id}, function (err, insight) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (insight) {
                    // construct commandList
                    if (insight.tags.length == 0)
                        next({status: true, r: [], k: [], c: 0})
                    else {
                        var commandList = _.map(insight.tags, function (t) {
                            return [t]
                        })
                        SEARCHMANAGER.getVerbatimsByTagForCommandList(topic, [commandList, null], iteration, function (res) {
                            next(res)
                        })
                    }
                }
            }
        })
    },

    /**
     * Get predicted verbatims given the insightID and a topic
     * @param id
     * @param iteration
     * @param topic
     * @param next
     */
    getPredictedVerbatims: function (id, iteration, topic, next) {
        db.indexes.insights.findOne({_id: id}, function (err, insight) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (insight) {
                    SEARCHMANAGER.getPredictedVerbatimsByInight(topic, insight, iteration, function (res) {
                        next(res)
                    })
                }
            }
        })
    },

    /**
     * Get SIndex for an InsightID and a topic
     * @param id
     * @param topic
     * @param callback
     */
    getSIndex: function (id, topic, callback) {
        db.modal.settings.findOne({insightid: id, topic: topic}, function (err, setting) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                callback(false, setting.s_index)
            }
        })
    },

    /**
     * Check if InsightModal has been compiled for an insightID and a topic
     * @param id
     * @param topic
     * @param callback
     */
    ifCompiledInsightModal: function (id, topic, callback) {
        db.modal.settings.findOne({insightid: id, topic: topic}, function (err, setting) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (!setting)
                    callback(false, false)
                else {
                    if (setting.iter < 3) {
                        callback(false, setting.compiled)
                    } else {
                        // if iter (nb of changes) is greater than 2, force recompile
                        callback(false, false)
                    }
                }
            }
        })
    },

    /**
     * Check if compiled SIndex of an insightID and a topic
     * @param id
     * @param topic
     * @param callback
     */
    ifCompiledSIndex: function (id, topic, callback) {
        db.modal.settings.findOne({insightid: id, topic: topic}, function (err, setting) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (!setting)
                    callback(false, false)
                else {
                    callback(false, !_.isUndefined(setting.s_index))
                }
            }
        })
    },

    /**
     * For a given project (project is extracted from the topic) and an array of tags,
     * find the list of insights containing any of these tags,
     * and finally increment the iter value in their settings.
     * Indicating a change in the insight modal.
     * Note: If iter > 2, the InsightModal is recompiled
     * @param tags
     * @param topic
     */
    incrementNbChangesForInsightModals: function (tags, topic) {
        // increment nbchanges for insight with containing tag
        db.indexes.insights.find({tags: {$in: tags}}).toArray(function (err, insights) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                var insightids = _.pluck(insights, '_id')
                db.indexes.settings.findOne({topic: topic}, function (err, setting) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        var project = setting.project
                        // increment iter for all insights from project
                        db.modal.settings.update(
                            {insightid: {$in: insightids}, project: project},
                            {$inc: {iter: 1}}  // increment iter
                        )
                    }
                })
            }
        })

    },

    /**
     * Set Relevance for a Predicted Verbatim
     * Relevance can be relevant or not-relevant
     * If not relevant:
     *  Remove predicted from the predicted list
     * If relevant:
     *  Add the tags associated with the insight to the predicted verbatim
     * @param topic
     * @param insightid
     * @param vid verbatimID
     * @param relevance
     * @param next
     */
    setRelevanceForPrediction: function (topic, insightid, vid, relevance, next) {
        var self = this
        if (relevance) {
            // add prediction to tagged
            db.indexes.insights.findOne({_id: insightid}, function (err, insight) {
                if (err)
                    error.STDError([__dirname, __filename].join('/'), "", err.stack)
                else {
                    if (insight.tags.length > 0) {
                        db.indexes.verbatimtags.findOne({_id: vid}, function (err, item) {
                            if (err)
                                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                            else {
                                if (item) {
                                    // if vid exists in tags
                                    db.indexes.verbatimtags.update(
                                        {_id: vid},
                                        {$push: {tags: {$each: insight.tags}}},
                                        function (err) {
                                            if (err)
                                                error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                            else {
                                                next({s: true})
                                            }
                                        })
                                } else {
                                    // if vid not exist in tags
                                    db.indexes.verbatimtags.insert({
                                        _id: vid,
                                        topic: topic,
                                        tags: insight.tags
                                    }, function (err) {
                                        if (err)
                                            error.STDError([__dirname, __filename].join('/'), "", err.stack)
                                        else {
                                            next({s: true})
                                        }
                                    })
                                }
                            }
                        })
                        // increment changes for insight modal
                        self.incrementNbChangesForInsightModals(insight.tags, topic)
                    }
                }
            })
        }
        // remove prediction from collection
        db.modal.predictions.remove({topic: topic, insightid: insightid, vid: vid}, function (err) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                if (!relevance)  // if relevance is true, response when tags appended (above)
                    next({s: true})
            }
        })
    },

    /**
     * Get all (upto MAXFEATURE) modal features
     * @param id
     * @param topic
     * @param next
     */
    getModalFeature: function (id, topic, next) {
        var MAXFEATURES = 25
        db.modal.modals.find({insightid: id}).sort({"feature.weight": -1}).limit(MAXFEATURES).toArray(function (err, items) {
            next(items)
        })
    },

    /**
     * Remove a certain feature from an insight
     * @param id feature id
     * @param insightid
     * @param next
     */
    removeFeature: function (id, insightid, next) {
        var self = this
        db.modal.modals.findOne({insightid: insightid, _id: id}, function (err, feature) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
            else {
                // add insight to reject list
                db.modal.rejects.insert(feature, function (err) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                })
                // remove insight from modals
                db.modal.modals.remove({insightid: insightid, _id: id}, function (err) {
                    if (err)
                        next({s: false})
                    else {
                        next({s: true})
                        self.setModalCompiledAs(insightid, false)
                    }
                })
            }
        })
    },

    /**
     * Set insight modal compiled, and reset iter to 0
     * @param insightid
     * @param state
     */
    setModalCompiledAs: function (insightid, state) {
        db.modal.settings.update({insightid: insightid}, {
            $set: {compiled: state, iter: 0},
            $unset: {s_index: ""}
        }, function (err) {
            if (err)
                error.STDError([__dirname, __filename].join('/'), "", err.stack)
        })
    }

}

/**
 * Create insight ID given the project name and insight name
 * @param project
 * @param insightname
 * @returns {string}
 */
function createInsightID(project, insightname) {
    return (project + insightname).replace(new RegExp(' ', 'g'), '').toLowerCase()
}

