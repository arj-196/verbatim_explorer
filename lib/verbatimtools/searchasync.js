var Promise = require('bluebird')
var db = require('../dbconnection')
var _ = require('underscore')
var conceptnet = require('../conceptnet')
var CommandListParser = require('./commandlistparser')
var SETTINGS = require('../settings')
var MAXSEARCHLENGTH = 15

function getVerbatimsIDSByQuery(collection, query, skip, limit) {
    return new Promise(function (resolve, reject) {
        collection.find(query).skip(skip).limit(limit).toArray(function (err, items) {
            if (err) {
                reject(err)
            } else {
                resolve(items)
            }
        })
    })
}

function getVerbatimsByQuery(collection, query) {
    return new Promise(function (resolve, reject) {
        collection.find(query).toArray(function (err, r) {
            if (err) {
                reject(err)
            } else {
                resolve(r)
            }
        })
    })
}

function getCountByQuery(collection, query) {
    return new Promise(function (resolve, reject) {
        collection.count(query, function (err, count) {
            if (err) {
                reject(err)
            } else {
                resolve(count)
            }
        })
    })
}

module.exports = {
    getVerbatimsByConcept: function (topic, conceptList, next) {
        if (_.isArray(conceptList)) {
            db.indexes.verbatimconcepts.find({topic: topic, concepts: {$in: conceptList}})
                .toArray(function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack);
                    else
                        next(items)
                })
        } else {
            console.log("error: connection API conceptnet failed")
            next([])
        }
    },

    getVerbatimsByNoun: function (topic, nounList, iteration, next) {
        if (_.isArray(nounList)) {
            db.indexes.verbatimnouns.find({topic: topic, words: {$in: nounList}})
                .skip(MAXSEARCHLENGTH * iteration)
                .limit(MAXSEARCHLENGTH)
                .toArray(function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack);
                    else
                        next(_.pluck(items, '_id'))
                })
        } else {
            console.log("error: connection API conceptnet failed")
            next([])
        }
    },

    getVerbatimsByVerb: function (topic, verbList, iteration, next) {
        if (_.isArray(verbList)) {
            db.indexes.verbatimverbs.find({topic: topic, words: {$in: verbList}})
                .skip(MAXSEARCHLENGTH * iteration)
                .limit(MAXSEARCHLENGTH)
                .toArray(function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack);
                    else
                        next(_.pluck(items, '_id'))
                })
        } else {
            console.log("error: connection API conceptnet failed")
            next([])
        }
    },

    getVerbatimsByAdjective: function (topic, adjectiveList, iteration, next) {
        if (_.isArray(adjectiveList)) {
            db.indexes.verbatimadjectives.find({topic: topic, words: {$in: adjectiveList}})
                .skip(MAXSEARCHLENGTH * iteration)
                .limit(MAXSEARCHLENGTH)
                .toArray(function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack);
                    else
                        next(_.pluck(items, '_id'))
                })
        } else {
            console.log("error: connection API conceptnet failed")
            next([])
        }
    },

    searchForKeywords: function (topic, keyword, next) {
        db.indexes.keywords.find({
            topics: {$in: [topic]},
            $text: {$search: keyword}
        }).sort({keyword: 1}).toArray(function (err, items) {
            if (_.isArray(items)) {
                next(items.slice(0, 3))
            } else {
                next([])
            }
        })
    },

    searchForKeywordsRegex: function (topic, keyword, next) {
        try {
            var exp = new RegExp('^' + keyword + '*')
            db.indexes.keywords.find({
                topics: {$in: [topic]},
                keyword: {$regex: exp}
            }).sort({keyword: 1}).toArray(function (err, items) {
                if (_.isArray(items)) {
                    next(items.slice(0, 4))
                } else {
                    next([])
                }
            })
        } catch (e) {
            next([])
        }
    },

    searchForTerms: function (topic, keyword, next) {
        db.indexes.terms.find({
            topics: {$in: [topic]},
            $text: {$search: keyword}
        }).sort({term: 1}).toArray(function (err, items) {
            if (_.isArray(items)) {
                next(items.slice(0, 3))
            } else {
                next([])
            }
        })
    },

    searchForTermsRegex: function (topic, keyword, next) {
        try {
            var exp = new RegExp('^' + keyword + '*')
            db.indexes.terms.find({
                topics: {$in: [topic]},
                term: {$regex: exp}
            }).sort({term: 1}).toArray(function (err, items) {
                if (_.isArray(items)) {
                    next(items.slice(0, 4))
                } else {
                    next([])
                }
            })
        } catch (e) {
            next([])
        }
    },

    searchForConcepts: function (topic, keyword, next) {
        SETTINGS.getLang(topic)
            .then(function (lang) {
                conceptnet.getConcepts(keyword, lang).then(function (edges) {
                    conceptnet.filterEdges(edges).then(function (edges) {
                        next(edges)
                    })
                })
            })
    },

    searchForTags: function (topic, keyword, next) {
        db.indexes.verbatimtags.find({topic: topic, tags: {$elemMatch: {$regex: new RegExp('^' + keyword + '*')}}})
            .limit(5)
            .toArray(
                function (err, items) {
                    if (err)
                        error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    else {
                        var tagsAppended = []
                        items.forEach(function (item) {
                            item.tags.forEach(function (tag) {
                                if (!_.contains(tagsAppended, tag)) {
                                    var p = new RegExp('^' + keyword + '*')
                                    if (p.exec(tag)) {
                                        tagsAppended.push(tag)
                                    }
                                }
                            })
                        })
                        if (tagsAppended) {
                            next(tagsAppended.slice(0, 5))
                        }
                    }
                }
            )
    },

    getVerbatimsByConceptsForCommandList: function (topic, commandList, languageMap, iteration) {
        return new Promise(function(resolve, reject){
            SETTINGS.getLang(topic)
                .then(function (globalLang) {
                    // gettings edges for each command
                    var conceptEdgesToBeLoaded = [], conceptToEdgesMap = {}
                    commandList.forEach(function (cL) {
                        cL.forEach(function (command) {
                            var lang
                            if (_.isUndefined(languageMap[command]))
                                lang = globalLang
                            else
                                lang = languageMap[command]

                            conceptToEdgesMap[conceptEdgesToBeLoaded.length] = command
                            conceptEdgesToBeLoaded.push(conceptnet.getConcepts(command, lang))
                        })
                    })

                    Promise.all(conceptEdgesToBeLoaded)
                        .then(function (edgeList) {
                            // filtering edges
                            var edgesToBeFiltered = []
                            edgeList.forEach(function (edges) {
                                edgesToBeFiltered.push(conceptnet.filterEdges(edges))
                            })
                            Promise.all(edgesToBeFiltered)
                                .then(function (filteredEdges) {
                                    // mapping concepts to their edges
                                    var conceptToEdges = {}
                                    for (var i = 0; i < filteredEdges.length; i++) {
                                        //var edges = filteredEdges[i];
                                        var edges = edgeList[i];
                                        var concept = conceptToEdgesMap[i]
                                        conceptToEdges[concept] = {
                                            concept: concept,
                                            edges: edges
                                        }
                                    }
                                    // build db query statement
                                    var OrStatment = CommandListParser.constructOrStatementConcept(commandList, conceptToEdges, globalLang)

                                    // fetch verbatimIDS by concept or statment
                                    getVerbatimsIDSByQuery(
                                        db.indexes.verbatimconcepts,
                                        {topic: topic, $or: OrStatment, enabled: true},
                                        MAXSEARCHLENGTH * iteration,
                                        MAXSEARCHLENGTH
                                    ).then(function (itemIDS) {
                                        var verbatimIDS = _.pluck(itemIDS, '_id')
                                        // fetch verbaimts
                                        getVerbatimsByQuery(db.indexes.verbatims, {topic: topic, _id: {$in: verbatimIDS}, enabled: true})
                                            .then(function (items) {
                                                var scores = conceptnet.scoreVerbatims(itemIDS)
                                                for (var i = 0; i < items.length; i++) {
                                                    items[i].score = scores[items[i]['_id']]
                                                }

                                                getCountByQuery(db.indexes.verbatimconcepts, {
                                                    topic: topic,
                                                    $or: OrStatment,
                                                    enabled: true
                                                })
                                                    .then(function (count) {
                                                        resolve({
                                                            verbatims: items,
                                                            edges: conceptToEdges,
                                                            keywords: _.keys(conceptToEdges),
                                                            c: count
                                                        })
                                                    })
                                            })
                                    })
                                })
                        })
                })
        })
    },

    getVerbatimsByKeywordForCommandList: function (topic, commandList, iteration) {
        return new Promise(function(resolve, reject){
            // fetching keywords
            CommandListParser.getKeywordsFromCommands(commandList, function (keywords, keywordMap) {
                // construct keywordIdList
                var keywordIdList = _.pluck(keywords, '_id')
                // constructing search query
                var orStatement = CommandListParser.constructOrStatementKeyword(commandList, keywords, keywordMap)
                // fetching verbatims _ids
                getVerbatimsIDSByQuery(
                    db.indexes.verbatimkeywords,
                    {topic: topic, $or: orStatement, enabled: true},
                    MAXSEARCHLENGTH * iteration,
                    MAXSEARCHLENGTH
                )
                    .then(function (items) {
                        // calculating weights verbatims
                        function calculateWeights(items) {
                            var weights = []
                            for (var i = 0; i < items.length; i++) {
                                weights[i] = 0
                                for (var j = 0; j < items[i]['_id_keywords'].length; j++) {
                                    var idKeyword = items[i]['_id_keywords'][j];
                                    if (keywordIdList.indexOf(idKeyword) != -1) {
                                        weights[i] += 1 * items[i]['weight'][j]
                                    }
                                }
                            }
                            return weights
                        }

                        var weights = calculateWeights(items)
                        var verbatimIdList = _.pluck(items, '_id')
                        getVerbatimsByQuery(db.indexes.verbatims, {topic: topic, _id: {$in: verbatimIdList}, enabled: true})
                            .then(function (verbatims) {
                                // append score to verbatim
                                for (var i = 0; i < verbatims.length; i++) {
                                    verbatims[i].score = weights[i]
                                }
                                // getting count
                                getCountByQuery(db.indexes.verbatimkeywords, {
                                    topic: topic,
                                    $or: orStatement,
                                    enabled: true
                                }).then(function (count) {
                                    // callback
                                    resolve({status: true, r: verbatims, k: keywords, c: count})
                                })
                            })
                    })
            })
        })
    },

    getVerbatimsByTermsForCommandList: function (topic, commandList, iteration) {
        return new Promise(function(resolve, reject){
            // fetching terms
            CommandListParser.getTermsFromCommands(commandList, function (terms, termMap) {
                // constructing search query
                var orStatement = CommandListParser.constructOrStatementTerm(commandList, terms, termMap)
                // fetching verbatim _ids
                getVerbatimsIDSByQuery(
                    db.indexes.verbatimterms,
                    {topic: topic, $or: orStatement, enabled: true},
                    MAXSEARCHLENGTH * iteration,
                    MAXSEARCHLENGTH
                ).then(function (items) {
                    var verbatimIdList = _.pluck(items, '_id')
                    getVerbatimsByQuery(db.indexes.verbatims, {topic: topic, _id: {$in: verbatimIdList}, enabled: true})
                        .then(function (verbatims) {
                            for (var i = 0; i < verbatims.length; i++) {
                                verbatims[i].score = 0
                            }
                            // getting count
                            getCountByQuery(db.indexes.verbatimterms, {topic: topic, $or: orStatement, enabled: true})
                                .then(function (count) {
                                    // callback
                                    resolve({status: true, r: verbatims, k: terms, c: count})
                                })
                        })
                })
            })
        })
    },

    getVerbatimsByTagForCommandList: function (topic, commandList, iteration) {
        return new Promise(function(resolve, reject){
            // fetching terms
            var orStatement = CommandListParser.constructOrStatementTag(commandList)

            // make list of tags
            var tags = []
            commandList.forEach(function (cL) {
                cL.forEach(function (tag) {
                    if (!_.contains(tags, tag))
                        tags.push(tag)
                })
            })

            // fetching verbatim _ids
            getVerbatimsIDSByQuery(
                db.indexes.verbatimtags,
                {topic: topic, $or: orStatement, enabled: true},
                MAXSEARCHLENGTH * iteration,
                MAXSEARCHLENGTH
            ).then(function (items) {
                var verbatimIdList = _.pluck(items, '_id')
                getVerbatimsByQuery(db.indexes.verbatims, {topic: topic, _id: {$in: verbatimIdList}, enabled: true})
                    .then(function (verbatims) {
                        for (var i = 0; i < verbatims.length; i++) {
                            verbatims[i].score = 0
                        }
                        // callback
                        resolve({status: true, r: verbatims, k: tags})
                    })
            })
        })
    },

}

// test
//var t = module.exports
//t.getVerbatimsByTagForCommandList('bp', [['b', 'a'], [ 'bank'] ], function (r) {
//    console.log(r)
//})