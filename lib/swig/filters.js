var Error = require('../error/error'),
    error = new Error();
var routes = require('../../config/routes.js');

/**
 * Additional Filters for Swig
 * @param swig
 */
module.exports = function (swig) {
    swig.setFilter('route', getRoute);
};

var getRoute = function(route) {
    var r = routes[route];
    if(r)
        return r;
    else {
        error.STDError([__dirname, __filename ].join('/'), "getRoute", "unkown route : "  + route)
        return '/undefined'
    }
};
