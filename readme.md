# Setup Files required for deployment

InsightE requires some setup variables that need to be defined.
These configurations are to be put in the file config/env.js

An example env.js:

    module.exports = {
        // the port to run on
        port: 8991,
        // if in mode dev or prod
        MODE: 'prod',

        // mongodb configurations
        mongodb: {
            uri: "mongodb://151.80.41.13:27018/",
        },

        // insighte_support_api configurations
        support: {
            host: "127.0.0.1",
            port: "8010"
        },
        // second insighte_support_api configurations
        // this one usually is deployed on the server
        // where the database is hosted
        supportInsight: {
            host: '127.0.0.1',
            port: '8010'
        },

        // address for the conceptnet server
        conceptnet: {
            host: '151.80.41.13',
            port: '8084',
            version: '5.4'
        },

        // mailing configuratiosn
        mailer: {
            transport: 'smtp://hello@thedatastrategy.com:pdstds99@smtp.1and1.pl',
            errorReportAddress: 'insighte@thedatastrategy.com',
        },


        // directory configurations
        dirs: {
            // where to store files uploaded by user for extraction
            download: '/home/arj/uniperformer/insightetools/scripts/tmp/downloads/',

            // where insighte_support_api stores the sample extracted files
            upload: '/home/arj/verbatim_explorer_support_api/tmp/',
        }
    }

# How to-s

Starting server
--

To start the server use the command:

    node bin/www
This will start the express server.

But on production you must use forever:

    forever start bin/www




# Directory Structure

- **bin**

    Contains all the executable files.

    - www.js : executable to start the server

- **config**

    Contains all the configuration related files

    - mailertools.js : contains all constants required by the mailing service
    - passport.js : contains the passport-js user-account management related configurations
    - routes.js : contains all the routes (end-points) used on InsightE
    - views.js : contains all the view files used on InsightE


- **controller**

    Contains all the controller files

    note:
    - rules.js : Contains the prefixed routes rules

        ex:

        - All routes with prefix 'dashboard/' require authentification

- **lib**

    Contains all the custom modules for insightE

- **public**

    Contains all the assests for the client side

    - public/cmps : bower download directory
    - public/css : stylesheets directory
    - public/data : temporary data
    - public/ext : external javascript libraries not found on bower
    - public/img : images directory
    - public/js : client side javascript files
    - public/theme : assets for landing page design
