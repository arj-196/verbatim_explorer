var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var swig = require('swig');
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);
var env = require('./config/env')
var passport = require('passport')
var flash = require('connect-flash')
var http = require('http');
var app = express();

// session
var store = new MongoDBStore({
        uri: env.mongodb.uri + "client",
        collection: 'sessions'
    });

require('./config/passport')(passport)

// Catch errors
//store.on('error', function(error) {
//    assert.ifError(error);
//    assert.ok(false);
//});

app.use(require('express-session')({
    secret: 'This is a secret',
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
    },
    store: store,
    resave: false,
    saveUninitialized: true,
}));
app.use(passport.initialize());
app.use(passport.session())
app.use(flash())

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
//app.set('view cache', true);
app.set('view cache', false);
swig.setDefaults({cache: false});

// swig filters
require('./lib/swig/filters')(swig);


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// ACTIVITY LOGGER
// app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//app.use(bodyParser({ keepExtensions: true, uploadDir: "public/tmp/uploads" }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Create HTTP server.
 */
var server = http.createServer(app);

// socket io
var io = require('socket.io')(server)

// Load Controllers
require('./controller/RouterLoader')(app, passport, io);


//// error handlers
var VIEWS = require('./config/views')
var Error = require('./lib/error/error'),
    error = new Error();

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        console.error(err)
        error.STDError([__dirname, __filename ].join('/'), "Caught Error", err.stack);
        res.status(err.status || 500);
        res.render(VIEWS.message, {messageid: 'ERROR'});
    });
}

//
// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    console.error(err.stack)
    error.STDError([__dirname, __filename ].join('/'), "Caught Error", err.stack);
    res.render(VIEWS.message, {messageid: 'ERROR'});
});

module.exports = {
    app: app,
    server: server
};
