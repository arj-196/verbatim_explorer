var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var heatmapManager = require('../lib/heatmap')
module.exports = function (app) {

    //
    //  TODO
    //  |||
    //  VVV

    //
    // heatmaps
    //
    var heatmaps = function (req, res) {
        res.render(VIEWS.heatmaps);
    }
    app.get(ROUTES.heatmaps, heatmaps)

    //
    // heatmaps Get Global
    //
    var getGlobalFigures = function (req, res) {
        heatmapManager.getGlobalFigures(req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.heatmaps_get_global, getGlobalFigures)

};
