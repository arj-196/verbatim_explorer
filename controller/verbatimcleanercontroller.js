var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
//var helper = require('../../lib/utils/common');
var express = require('express');
var VERBATIMSMANAGER = require('../lib/verbatims')
var CLEANERMANAGER = require('../lib/verbatimtools/cleaner')

module.exports = function (app) {

    /**
     * Do search
     * @param req
     * @param res
     */
    var doSearch = function (req, res) {
        CLEANERMANAGER.doSearch(req.body, function (err, r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_cleaner_search, doSearch)

    /**
     * Disable Verbatims
     * @param req
     * @param res
     */
    var disableVerbatims = function (req, res) {
        CLEANERMANAGER.disableVerbatims(req.body, req.user, function (err, r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_cleaner_disable, disableVerbatims)

    /**
     * Get Disabled History for Preview Tab
     * @param req
     * @param res
     */
    var getHistory = function (req, res) {
        CLEANERMANAGER.getHistory(req.query.topic, parseInt(req.query.iteration), function (err, r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_plugins_cleaner_get_history, getHistory)

    /**
     * Undo Disable Change From History
     * @param req
     * @param res
     */
    var undoDisable = function (req, res) {
        CLEANERMANAGER.undoDisable(req.body, function (err, r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_cleaner_history_undo_change, undoDisable)

};
