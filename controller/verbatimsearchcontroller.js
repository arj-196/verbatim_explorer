var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
//var helper = require('../../lib/utils/common');
var express = require('express');
var VERBATIMSMANAGER = require('../lib/verbatims')

/**
 * Handles Verbatim Search Requests
 * @param app
 */
module.exports = function (app) {

    /**
     * Search Verbatims by Keyword
     * @param req
     * @param res
     */
    var searchVerbatimsByKeyword = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.getVerbatimsByKeywordForCommandList(req.body.topic,
            JSON.parse(req.body.commandList), parseInt(req.body.iteration), function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.verbatim_search_keyword, searchVerbatimsByKeyword)

    /**
     * Searching Verbatims by Concepts
     * @param req
     * @param res
     */
    var searchVerbatimsByConcept = function (req, res) {
        var commandList = JSON.parse(req.body.commandList)
        var languageMap = JSON.parse(req.body.languagemap)

        VERBATIMSMANAGER.TOOL.SEARCH.getVerbatimsByConceptsForCommandList(req.body.topic, commandList, languageMap,
            parseInt(req.body.iteration), function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.verbatims_search_concept, searchVerbatimsByConcept)

    /**
     * Searching Verbatims by Term
     * @param req
     * @param res
     */
    var searchVerbatimsByTerm = function (req, res) {

        var commandList = JSON.parse(req.body.commandList)
        VERBATIMSMANAGER.TOOL.SEARCH.getVerbatimsByTermsForCommandList(req.body.topic, commandList,
            parseInt(req.body.iteration), function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.verbatims_search_terms, searchVerbatimsByTerm)

    /**
     * Mongo Text Search for Keyword
     * @param req
     * @param res
     */
    var searchForKeywords = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.searchForKeywords(req.body.topic, req.body.keyword, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_search_for_keywords, searchForKeywords)

    /**
     * Regex Search for Keyword
     * @param req
     * @param res
     */
    var searchForKeywordsRegex = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.searchForKeywordsRegex(req.body.topic, req.body.keyword, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_search_for_keywords_regex, searchForKeywordsRegex)

    /**
     * Mongo Text Search for Term
     * @param req
     * @param res
     */
    var searchForTerms = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.searchForTerms(req.body.topic, req.body.keyword, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_search_for_terms, searchForTerms)

    /**
     * Regex Search for Term
     * @param req
     * @param res
     */
    var searchForTermsRegex = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.searchForTermsRegex(req.body.topic, req.body.keyword, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_search_for_terms_regex, searchForTermsRegex)

    /**
     * Search For Concepts
     * @param req
     * @param res
     */
    var searchForConcepts = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.searchForConcepts(req.body.topic, req.body.keyword, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_search_for_concepts, searchForConcepts)

    /**
     * Search for Tag
     * @param req
     * @param res
     */
    var searchForTags = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.searchForTags(req.body.topic, req.body.keyword, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_search_for_tag, searchForTags)

    /**
     * AutoComeleter Tag
     * @param req
     * @param res
     */
    var autoCompleteTag = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.autoCompleteTags(req.query.q, req.query.t, function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatims_tag_autocomplete, autoCompleteTag)

    /**
     * Search Verbatims by Tag
     * @param req
     * @param res
     */
    var searchVerbatimsByTag = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCH.getVerbatimsByTagForCommandList(req.body.topic, JSON.parse(req.body.commandList), parseInt(req.body.iteration), function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_search_tag, searchVerbatimsByTag)


    /**
     * DEPRECATED
     * Searching Verbatims by Noun
     * @param req
     * @param res
     */
    var searchVerbatimsByNoun = function (req, res) {

        VERBATIMSMANAGER.TOOL.SEARCH.getVerbatimsByNoun(req.query.topic, [req.query.word],
            parseInt(req.query.iteration), function (verbatimIds) {
                VERBATIMSMANAGER.getVerbatimsByIds(verbatimIds, function (verbatims) {
                    // appending scores to verbatims
                    for (var i = 0; i < verbatims.length; i++) {
                        verbatims[i]['score'] = 0;
                    }

                    // callback
                    res.json({status: true, r: verbatims, k: req.query.word})
                })
            })
    }
    app.get(ROUTES.verbatims_search_noun, searchVerbatimsByNoun)

    /**
     * DEPRECATED
     * Searching Verbatims by Verb
     * @param req
     * @param res
     */
    var searchVerbatimsByVerb = function (req, res) {

        VERBATIMSMANAGER.TOOL.SEARCH.getVerbatimsByVerb(req.query.topic, [req.query.word],
            parseInt(req.query.iteration), function (verbatimIds) {
                VERBATIMSMANAGER.getVerbatimsByIds(verbatimIds, function (verbatims) {
                    // appending scores to verbatims
                    for (var i = 0; i < verbatims.length; i++) {
                        verbatims[i]['score'] = 0;
                    }

                    // callback
                    res.json({status: true, r: verbatims, k: req.query.word})
                })
            })
    }
    app.get(ROUTES.verbatims_search_verb, searchVerbatimsByVerb)

    /**
     * DEPRECATED
     * Searching Verbatims by Adjective
     * @param req
     * @param res
     */
    var searchVerbatimsByAdjective = function (req, res) {

        VERBATIMSMANAGER.TOOL.SEARCH.getVerbatimsByAdjective(req.query.topic, [req.query.word],
            parseInt(req.query.iteration), function (verbatimIds) {
                VERBATIMSMANAGER.getVerbatimsByIds(verbatimIds, function (verbatims) {
                    // appending scores to verbatims
                    for (var i = 0; i < verbatims.length; i++) {
                        verbatims[i]['score'] = 0;
                    }

                    // callback
                    res.json({status: true, r: verbatims, k: req.query.word})
                })
            })
    }
    app.get(ROUTES.verbatims_search_adjective, searchVerbatimsByAdjective)

};
