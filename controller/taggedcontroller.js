var ROUTES = require('../config/routes.js')
var VIEWS = require('../config/views.js')
var _ = require('underscore')
var TAGSMANAGER = require('../lib/tags')
var fs = require('fs')

/**
 * Handles the Tagged Dashboard
 * @param app
 * @param passport
 * @param io
 */
module.exports = function (app, passport, io) {

    /**
     * Tags Page
     * @param req
     * @param res
     */
    var tagsIndex = function (req, res) {
        res.render(VIEWS.tags)
    }
    app.get(ROUTES.tags, tagsIndex)

    /**
     * Fetch all datasets
     * @param req
     * @param res
     */
    var fetchDatasets = function (req, res) {
        TAGSMANAGER.getDatasets(function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.tags_get_dataset, fetchDatasets)

    /**
     * Fetch all datasets
     * @param req
     * @param res
     */
    var fetchTags = function (req, res) {
        TAGSMANAGER.getDistinctTags(req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.tags_get_tags, fetchTags)

    var fetchVerbatimsForTag = function (req, res) {
        var tagList = _.isArray(req.body['tagList[]']) ? req.body['tagList[]'] : [req.body['tagList[]']]
        var topic = req.body.topic
        TAGSMANAGER.getVerbatimsForTags(tagList, topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.tags_get_verbatims_for_tag, fetchVerbatimsForTag)

    var fetchAssociatedTagsForTags = function (req, res) {
        var tagList = _.isArray(req.body['tagList[]']) ? req.body['tagList[]'] : [req.body['tagList[]']]
        var topic = req.body.topic

        TAGSMANAGER.getAssociatedTagsForTags(tagList, topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.tags_get_tags__associated_to_tag, fetchAssociatedTagsForTags)

    /**
     * Export tags
     * @param req
     * @param res
     */
    var exportTags = function (req, res) {
        var topics = req.body.topics
        var tags = req.body.tags
        TAGSMANAGER.PLUGINS.EXPORT.exportTags(topics, tags, function (r) {
            if(r.s) {
                var ptn = new RegExp('/', 'g')
                var route = ROUTES.tags_plugin_export_download_file.replace(':file',
                    r.f.replace(ptn, ';'))
                r.route = route
                res.json(r)
            } else {
                res.json(r)
            }
        })
    }
    app.post(ROUTES.tags_plugin_export_tags, exportTags)

    /**
     * Export insights
     * @param req
     * @param res
     */
    var exportInsights = function (req, res) {
        var topics = req.body.topics
        var insightids = req.body.insightids
        TAGSMANAGER.PLUGINS.EXPORT.exportInsights(topics, insightids, function (r) {
            if(r.s) {
                var ptn = new RegExp('/', 'g')
                var route = ROUTES.tags_plugin_export_download_file.replace(':file',
                    r.f.replace(ptn, ';'))
                r.route = route
                res.json(r)
            } else {
                res.json(r)
            }
        })
    }
    app.post(ROUTES.tags_plugin_export_insights, exportInsights)

    /**
     * Downloading the export file
     * @param req
     * @param res
     */
    var actuallyDownloadingExport = function (req, res) {
        var ptn = new RegExp(';', 'g')
        var file = req.params.file.replace(ptn, '/')
        res.attachment(file)
        fs.readFile(file, function (err, content) {
            if (err) {
                res.writeHead(400, {'Content-type': 'text/html'})
                console.log(err)
                res.end("No such file")
            } else {
                ////specify Content will be an attachment
                //res.setHeader('Content-disposition', 'attachment filename='+file)
                res.send(content)
                res.end()
            }
        })
    }
    app.get(ROUTES.tags_plugin_export_download_file, actuallyDownloadingExport)

    /**
     * Sockets
     */
    io
        .of('/tags')
        .on('connection', function (socket) {

            /**
             * Exporting Tagged
             */
            socket.on('export_tags_req', function (data, callback) {
                callback({s: true})

                TAGSMANAGER.PLUGINS.EXPORT.exportTags(JSON.stringify(data.topics), JSON.stringify(data.tags), function (r) {
                    if(r.s) {
                        var ptn = new RegExp('/', 'g')
                        var route = ROUTES.tags_plugin_export_download_file.replace(':file', r.f.replace(ptn, ';'))
                        r.route = route
                    }
                    socket.emit('export_tags_res', r)
                })
            })

            /**
             * Exporting Insights
             */
            socket.on('export_insights_req', function (data, callback) {
                callback({s: true})

                TAGSMANAGER.PLUGINS.EXPORT.exportInsights(JSON.stringify(data.topics), JSON.stringify(data.insightids), data.nbPredicted, function (r) {
                    if(r.s) {
                        var ptn = new RegExp('/', 'g')
                        var route = ROUTES.tags_plugin_export_download_file.replace(':file',
                            r.f.replace(ptn, ';'))
                        r.route = route
                    }
                    socket.emit('export_insights_res', r)
                })
            })
            
            
        })
}
