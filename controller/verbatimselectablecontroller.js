var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
//var helper = require('../../lib/utils/common');
var express = require('express');
var VERBATIMSMANAGER = require('../lib/verbatims')
var SUPPORTAPI = require('../lib/supportapi')

module.exports = function (app) {

    /**
     * Get similiarities between multiple verbatims
     * @param req
     * @param res
     */
    var getSimilarities = function (req, res) {
        var ids = JSON.parse(req.body.verbatimList)
        SUPPORTAPI.MULTIPLEVERBATIMS.getConcepts(ids, 'fr')
            .then(function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.get_similarities, getSimilarities)

};
