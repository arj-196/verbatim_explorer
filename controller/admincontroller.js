var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var UserAccountManager = require('../lib/useraccountmanager');

/**
 * Handles the Administration Dashboard
 * @param app
 * @param passport
 */
module.exports = function (app, passport) {

    /**
     * User Management
     * @param req
     * @param res
     */
    var userManagement = function (req, res) {
        UserAccountManager.getAllUsers(req.user.company, function (users) {
            res.render(VIEWS.user_management, {users: users})
        })
    }
    app.get(ROUTES.user_management, userManagement)

    /**
     * Get User by ID
     * @param req
     * @param res
     */
    var getUserByID = function (req, res) {
        UserAccountManager.getUser(req.query.id, function (user) {
            res.json(user)
        })
    }
    app.get(ROUTES.admin_get_user_by_id, getUserByID)

    /**
     * Handles User Form actions
     *  - Editing User Information
     *  - Creating New User
     *  - Removing a User
     * @param req
     * @param res
     */
    var handleFormUser = function (req, res) {
        if (req.body.action == "edituser") {
            UserAccountManager.updateUser(req.user.company, req.body, function (r) {
                res.json(r)
            })
        } else if (req.body.action == "newuser") {
            UserAccountManager.newUser(req.user.company, req.body, function (r) {
                res.json(r)
            })
        } else if (req.body.action == "removeuser") {
            UserAccountManager.removeUser(req.user.company, req.body, function (r) {
                res.json(r)
            })
        }
    }
    app.post(ROUTES.admin_post_form_user, handleFormUser)


};
