var ROUTES = require('../config/routes.js');
var Util = require('../lib/utils/common');
var _ = require('underscore')

module.exports = function (app, passport) {

    //
    // Requring Login Rules
    //
    app.all(ROUTES.prefix.restricted, function (req, res, next) {
        if (Util.isLoggedIn(req)) {
            next()
        } else {
            res.redirect(ROUTES.root)
        }
    })

    //
    // Requring ADMIN priviledge
    //
    app.all(ROUTES.prefix.admin, function (req, res, next) {
        if (Util.isLoggedIn(req)) {
            // requires user to have role ADMIN
            if (!_.isUndefined(req.user.role))
                if (req.user.role.toUpperCase() == "ADMIN")
                    next()
                else
                    res.redirect(ROUTES.dashboard)
            else
                res.redirect(ROUTES.dashboard)
        } else {
            res.redirect(ROUTES.root)
        }
    })


};
