var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
//var helper = require('../../lib/utils/common');
var express = require('express');
var VERBATIMSMANAGER = require('../lib/verbatims')
var CONCEPTNET = require('../lib/conceptnet')
var _ = require('underscore')

/**
 * Handles all Stats Component Related Requests
 *  - Wordcloud Statistics
 *  - Emotion Statistics
 *  - NER Statistics
 *  - Exporting Stats
 *  - Stats Search
 * @param app
 */
module.exports = function (app) {

    /**
     * Get Emotion Statistics for Search Query
     * @param req
     * @param res
     */
    var getVerbatimStatsEmotion = function (req, res) {
        var commandList = JSON.parse(req.body.commandList)
        var languageMap = JSON.parse(req.body.languagemap)
        VERBATIMSMANAGER.TOOL.STATS.getEmotions(req.body.searchmode, commandList, languageMap, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_stats_emotion, getVerbatimStatsEmotion)

    /**
     * Get NER Statustics for Search Query
     * @param req
     * @param res
     */
    var getVerbatimStatsNER = function (req, res) {
        var commandList = JSON.parse(req.body.commandList)
        var languageMap = JSON.parse(req.body.languagemap)
        VERBATIMSMANAGER.TOOL.STATS.getNER(req.body.searchmode, commandList, languageMap, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_stats_ner, getVerbatimStatsNER)

    /**
     * Get WordClouds for Seach Query
     * @param req
     * @param res
     */
    var getVerbatimStatsWordcloud = function (req, res) {
        var commandList = JSON.parse(req.body.commandList)
        var languageMap = JSON.parse(req.body.languagemap)
        VERBATIMSMANAGER.TOOL.STATS.getWordCloud(req.body.searchmode, req.body.targetmode, commandList, languageMap,
            req.body.topic, function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.verbatim_plugins_stats_wordcloud, getVerbatimStatsWordcloud)

    /**
     * Export Dataset for Search Query
     * @param req
     * @param res
     */
    var exportDataset = function (req, res) {
        var commandList = JSON.parse(req.body.commandList)
        var languageMap = JSON.parse(req.body.languagemap)
        VERBATIMSMANAGER.TOOL.STATS.exportDataset(req.body.searchmode, commandList, languageMap, req.body.topic,
            function (r) {
                var ptn = new RegExp('/', 'g')
                var route = ROUTES.verbatim_plugins_stats_exportdataset_downloading.replace(':file',
                    r.f.replace(ptn, ';'))
                res.json({f: route})
            })
    }
    app.post(ROUTES.verbatim_plugins_stats_exportdataset, exportDataset)

    /**
     * Do the actual download for the export
     * @param req
     * @param res
     */
    var actuallyDownloadingExport = function (req, res) {
        var ptn = new RegExp(';', 'g')
        var file = req.params.file.replace(ptn, '/')
        res.attachment(file)
        fs.readFile(file, function (err, content) {
            if (err) {
                res.writeHead(400, {'Content-type': 'text/html'})
                console.log(err);
                res.end("No such file");
            } else {
                ////specify Content will be an attachment
                //res.setHeader('Content-disposition', 'attachment; filename='+file);
                res.send(content)
                res.end()
            }
        })
    }
    app.get(ROUTES.verbatim_plugins_stats_exportdataset_downloading, actuallyDownloadingExport)

    /**
     * Set Stats Search to Default State
     * @param req
     * @param res
     */
    var searchSetToDefault = function (req, res) {
        VERBATIMSMANAGER.TOOL.STATS.searchSetToDefault(req.session.id, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_stats_search_set_to_default, searchSetToDefault)

    /**
     * Add a search level for Stats Search
     * @param req
     * @param res
     */
    var searchAddLevel = function (req, res) {
        VERBATIMSMANAGER.TOOL.STATS.addSearchLevel(req.session.id, req.body.type, req.body.id, req.body.topic,
            function (r) {
                if (r.s) {
                    VERBATIMSMANAGER.TOOL.STATS.doSearch(req.session.id, req.body.topic, false, function (r2) {
                        res.json({
                            s: r.s,
                            m: r.m,
                            req: r,
                            res: r2
                        })
                    })
                } else {
                    res.json(r)
                }
            })
    }
    app.post(ROUTES.verbatim_plugins_stats_search_addlevel, searchAddLevel)

    /**
     * Remove a search level for Stats Search
     * @param req
     * @param res
     */
    var searchRemoveLevel = function (req, res) {
        VERBATIMSMANAGER.TOOL.STATS.removeSearchLevel(req.session.id, req.body.type, req.body.id, req.body.topic, function (r) {
            if(r.s){
                if(r.m.length == 0){
                    res.json({
                        s: true,
                        m: "DEFAULTVERBATIMS"
                    })
                } else {
                    VERBATIMSMANAGER.TOOL.STATS.doSearch(req.session.id, req.body.topic, false, function (r2) {
                        res.json({
                            s: r.s,
                            m: r.m,
                            req: r,
                            res: r2
                        })
                    })
                }
            }
        })
    }
    app.post(ROUTES.verbatim_plugins_stats_search_removelevel, searchRemoveLevel)

    /**
     * Load more verbatims for current Stats Search
     * @param req
     * @param res
     */
    var loadMoreVerbatims = function (req, res) {
        VERBATIMSMANAGER.TOOL.STATS.doSearch(req.session.id, req.body.topic, true, function (r2) {
            res.json({
                res: r2
            })
        })
    }
    app.post(ROUTES.verbatim_plugins_stats_search_loadmore, loadMoreVerbatims)


};
