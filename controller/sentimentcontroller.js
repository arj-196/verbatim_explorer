var ROUTES = require('../config/routes.js')
var VIEWS = require('../config/views.js')
var _ = require('underscore')
var SENTIMENTMANAGER = require('../lib/sentiment')

/**
 * Handles Sentiment Dashboard
 * @param app
 */
module.exports = function (app) {

    /**
     * Sentiment Page
     * @param req
     * @param res
     */
    var sentimentIndex = function (req, res) {
        res.render(VIEWS.sentiment);
    }
    app.get(ROUTES.sentiment, sentimentIndex)

    /**
     * Get Sentiment Statistics
     * @param req
     * @param res
     */
    var getSentimentStastics = function (req, res) {
        SENTIMENTMANAGER.getStatisticsForTopic(req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.get_sentiment_statistics, getSentimentStastics)

    /**
     * Get Verbatims By Topic And Polarity
     * @param req
     * @param res
     */
    var getVerbatimByTopicAndPolarity = function (req, res) {
        SENTIMENTMANAGER.getVerbatimsByTopicAndPolarity(req.body.topic, req.body.type, req.body.iteration,
            function (r) {
                res.json(r)
            }
        )
    }
    app.post(ROUTES.get_verbatims_for_topic_and_polarity, getVerbatimByTopicAndPolarity)
};
