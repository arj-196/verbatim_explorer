var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var UserAccountManager = require('../lib/useraccountmanager');
var Error = require('../lib/error/error'),
    error = new Error();

/**
 * Handles default functionalitites
 *  - login
 *  - logout
 *  - sign in
 *  - reset password
 *  - error logger 
 * @param app
 * @param passport
 */
module.exports = function (app, passport) {

    /**
     * Login page
     * @param req
     * @param res
     */
    var homepage = function (req, res) {
        res.render(VIEWS.signin, {message: req.flash('loginMessage')});
    };
    app.get(ROUTES.root, homepage);

    /**
     * Passport local signup strategy
     */
    app.post(ROUTES.root, passport.authenticate('local-login', {
        successRedirect: ROUTES.dashboard,
        failureRedirect: ROUTES.root,
        failureFlash: true
    }))

    /**
     * Sign up page
     * @param req
     * @param res
     */
    var signup = function (req, res) {
        res.render(VIEWS.signup, {message: req.flash('signupMessage')})
    }
    app.get(ROUTES.signup, signup)

    /**
     * Passport local signup strategy
     */
    app.post(ROUTES.signup, passport.authenticate('local-signup', {
        successRedirect: ROUTES.root,
        failureRedirect: ROUTES.signup,
        failureFlash: true
    }))

    /**
     * Logout page
     * @param req
     * @param res
     */
    var logout = function (req, res) {
        req.logout()
        res.redirect(ROUTES.root)
    }
    app.get(ROUTES.logout, logout)

    /**
     * Request Reset Password page
     * @param req
     * @param res
     */
    var resetPassword = function (req, res) {
        res.render(VIEWS.reset_password, {
            stage: "request_reset"
        })
    }
    app.get(ROUTES.reset_password, resetPassword)

    /**
     * Handle Actual Request for Reset Password
     * @param req
     * @param res
     */
    var handleRequestResetPassword = function (req, res) {
        var resetUrl = req.protocol + '://' + req.get('host') + ROUTES.do_reset_password
        UserAccountManager.resetPassword(req.body.email, resetUrl, function (r) {
            if (r.s) {
                res.render(VIEWS.message, {messageid: "RESETPASS"})
            } else {
                res.render(VIEWS.reset_password, {
                    message: r.m
                })
            }
        })
    }
    app.post(ROUTES.reset_password, handleRequestResetPassword)

    /**
     * Does Password Reset
     * @param req
     * @param res
     */
    var doResetPassword = function (req, res) {

        UserAccountManager.authenticateResetRequest(req.params.USER, req.params.ACCESSCODE, function (r) {
            if (r.s) {
                res.render(VIEWS.reset_password, {
                    stage: "do_reset",
                    user: req.params.USER,
                    accesscode: req.params.ACCESSCODE
                })
            } else {
                res.render(VIEWS.reset_password, {
                    message: r.m
                })
            }
        })
    }
    app.get(ROUTES.do_reset_password, doResetPassword)

    /**
     * Password Reset Done Message
     * @param req
     * @param res
     */
    var handleDoRequestResetPassword = function (req, res) {

        UserAccountManager.changePassword(req.body.user, req.body.accesscode, req.body.newpass, function (r) {
            if (r.s) {
                res.render(VIEWS.message, {
                    messageid: "RESETPASSSUCCESS"
                })
            } else {
                res.redirect(ROUTES.reset_password)
            }
        })
    }
    app.post(ROUTES.do_reset_password, handleDoRequestResetPassword)

    /**
     * Logging Errors
     * @param req
     * @param res
     */
    var logError = function (req, res) {
        var data = req.body
        error.STDError(data.url, data.m, data.stack);
        res.json({s: true})
    }
    app.post(ROUTES.log_error, logError)

    /**
     * Contact Us page
     * @param req
     * @param res
     */
    var contact = function (req, res) {
        res.render(VIEWS.message, {messageid: "CONTACT"})
    }
    app.get(ROUTES.contactus, contact)


    /**
     * Test path
     * @param req
     * @param res
     */
    var prototypeTest = function (req, res) {
        res.render(VIEWS.test)
    }
    app.get(ROUTES.test, prototypeTest)

};
