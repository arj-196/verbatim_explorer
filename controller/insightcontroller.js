var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
//var helper = require('../../lib/utils/common');
var express = require('express');
var VERBATIMSMANAGER = require('../lib/verbatims')
var SUPPORTAPI = require('../lib/supportapi')

/**
 * Handles all Insight Related Requests 
 * @param app
 * @param passport
 * @param io
 */
module.exports = function (app, passport, io) {

    /**
     * Get Insights for Project
     * @param req
     * @param res
     */
    var getInsightsForProject = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.getInsightForProject(req.query.topic, function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_plugins_insight_get_insights, getInsightsForProject)


    /**
     * Get Insight by id
     * @param req
     * @param res
     */
    var getInsightById = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.getInsight(req.query.id, function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_plugins_insight_get_insight_byid, getInsightById)

    /**
     * AutoCompleter Insight
     * @param req
     * @param res
     */
    var autocompleteInsight = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.autoCompleteInsight(req.query.q, req.query.t, function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatims_insight_autocomplete, autocompleteInsight)

    /**
     * Create Insight
     * @param req
     * @param res
     */
    var createInsight = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.createInsight(req.body, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_insight_create, createInsight)

    /**
     * Edit Insight
     * @param req
     * @param res
     */
    var editInsight = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.editInsight(req.body, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_insight_edit, editInsight)

    /**
     * Remove Insight
     * @param req
     * @param res
     */
    var removeInsight = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.removeInsight(req.body.id, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_insight_remove, removeInsight)

    /**
     * Add Tags To Insight
     * @param req
     * @param res
     */
    var addTagsToInsight = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.findTagsFromQuery(req.body.query, req.body.topic, function (r) {
            if(r.length == 0){
                res.json({s: false})
            } else {
                VERBATIMSMANAGER.TOOL.INSIGHTS.addTagsToInsight(req.body.id, r, req.body.topic, function (r) {
                    res.json(r)
                })
            }
        })
    }
    app.post(ROUTES.verbatim_plugins_insight_add_tags, addTagsToInsight)

    /**
     * Remove Tag From Insight
     * @param req
     * @param res
     */
    var removeTagFromInsight = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.removeTagFromInsight(req.body.id, req.body.tag, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_insight_remove_tags, removeTagFromInsight)

    /**
     * Get Tagged Verbatims From Insight
     * @param req
     * @param res
     */
    var getTaggedVerbatimsFromInsight = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.getTaggedVerbatims(req.body.id, req.body.iteration, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_insight_get_verbatims_tagged, getTaggedVerbatimsFromInsight)

    /**
     * Set if Predicted is Relevant or Not
     * @param req
     * @param res
     */
    var setRelevanceForPrediction = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.setRelevanceForPrediction(
            req.body.topic, req.body.insightid, req.body.vid, JSON.parse(req.body.relevance),
            function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.verbatim_plugins_insight_set_predicted_relevance, setRelevanceForPrediction)

    /**
     * If insight is compiled
     * @param req
     * @param res
     */
    var ifCompiled = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.ifCompiledInsightModal(req.query.id, req.query.topic, function (err, r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_plugins_insight_if_compiled, ifCompiled)


    /**
     * Get Modal Features
     * @param req
     * @param res
     */
    var getModalFeatures = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.getModalFeature(req.query.id, req.query.topic, function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_plugins_insight_get_features, getModalFeatures)

    /**
     * Remove a modal feature
     * @param req
     * @param res
     */
    var removeFeature = function (req, res) {
        VERBATIMSMANAGER.TOOL.INSIGHTS.removeFeature(req.body.id, req.body.insightid, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatim_plugins_insight_remove_feature, removeFeature)

    /**
     * Re-Compiling Modal
     * @param req
     * @param res
     */
    var reCompileModal = function (req, res) {
        SUPPORTAPI.MODAL.compileInsightModal(req.body.insightid, req.body.topic).then(function (r) {
            res.json(r)
        })

    }
    app.post(ROUTES.verbatim_plugins_insight_recompile_modal, reCompileModal)

    /**
     * Get Predicted Verbatims From Insight
     * @param req
     * @param res
     */
    var getPredictedVerbatimsFromInsight = function (req, res) {
        // check to see if insight prediction modal has to be re-compiled
        VERBATIMSMANAGER.TOOL.INSIGHTS.ifCompiledInsightModal(req.body.id, req.body.topic, function (err, ifCompiled) {
            if (ifCompiled)
                VERBATIMSMANAGER.TOOL.INSIGHTS.getPredictedVerbatims(
                    req.body.id, req.body.iteration, req.body.topic,
                    function (r) {
                        res.json(r)
                    })
            else {
                // compile the insight modal
                SUPPORTAPI.MODAL.compileInsightModal(req.body.id, req.body.topic).then(function (r) {
                    VERBATIMSMANAGER.TOOL.INSIGHTS.getPredictedVerbatims(
                        req.body.id, req.body.iteration, req.body.topic,
                        function (r) {
                            res.json(r)
                        })
                })
            }
        })
    }
    app.post(ROUTES.verbatim_plugins_insight_get_verbatims_predicted, getPredictedVerbatimsFromInsight)


    /**
     * Sockets
     */
    io
        .of('/insights')
        .on('connection', function (socket) {

            /**
             * Get Predicted Verbatims for an insight
             */
            socket.on('get_predicted_verbatims_from_insight_req', function (data, callback) {
                callback({s: true})

                // check to see if insight prediction modal has to be re-compiled
                VERBATIMSMANAGER.TOOL.INSIGHTS.ifCompiledInsightModal(data.id, data.topic, function (err, ifCompiled) {
                    if (ifCompiled)
                        VERBATIMSMANAGER.TOOL.INSIGHTS.getPredictedVerbatims(
                            data.id, data.iteration, data.topic,
                            function (r) {
                                // last check if predictedList.length == 0
                                if(r.c == 0) {
                                    // compile again
                                    SUPPORTAPI.MODAL.compileInsightModal(data.id, data.topic).then(function (r2) {
                                        VERBATIMSMANAGER.TOOL.INSIGHTS.getPredictedVerbatims(
                                            data.id, data.iteration, data.topic,
                                            function (r3) {
                                                r3.ifloadmore = data.ifloadmore; r3.insight = data.insight
                                                socket.emit('get_predicted_verbatims_from_insight_res', r3)
                                            })
                                    })
                                } else {
                                    r.ifloadmore = data.ifloadmore; r.insight = data.insight
                                    socket.emit('get_predicted_verbatims_from_insight_res', r)
                                }
                            })
                    else {
                        // compile the insight modal
                        SUPPORTAPI.MODAL.compileInsightModal(data.id, data.topic).then(function (r) {
                            VERBATIMSMANAGER.TOOL.INSIGHTS.getPredictedVerbatims(
                                data.id, data.iteration, data.topic,
                                function (r) {
                                    r.ifloadmore = data.ifloadmore; r.insight = data.insight
                                    socket.emit('get_predicted_verbatims_from_insight_res', r)
                                })
                        })
                    }
                })
            })

            /**
             * Get S-Index
             */
            socket.on('compile_s_index_req', function (data, callback) {
                callback({s: true})

                // check to see if SIndex is already compiled
                VERBATIMSMANAGER.TOOL.INSIGHTS.ifCompiledSIndex(data.id, data.topic, function (err, ifCompiled) {
                    if (ifCompiled)
                        VERBATIMSMANAGER.TOOL.INSIGHTS.getSIndex(
                            data.id, data.topic, function (err, r) {
                                socket.emit('compile_s_index_res', r)
                            }
                        )
                    else {
                        SUPPORTAPI.MODAL.compileSIndex(
                            data.id, data.topic).then(
                            function (r) {
                                socket.emit('compile_s_index_res', r.r)
                            }
                        )
                    }
                })
            })



        })
};
