var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var _ = require('underscore')
var WCA = require('../lib/crossanalysis/wca')
var ICA = require('../lib/crossanalysis/ica')

/**
 * Handles the CrossAnalysis Dashboard
 * @param app
 */
module.exports = function (app) {

    /**
     * Cross Analysis Dashboard
     * @param req
     * @param res
     */
    var crossAnalysis = function (req, res) {
        res.render(VIEWS.crossanalysis)
    }
    app.get(ROUTES.crossanalysis, crossAnalysis)

    /**
     * Handles wca module requests
     *  - Gettings Count Per Topic
     * @param req
     * @param res
     */
    var wca = function (req, res) {
        var topics = JSON.parse(req.body.topics)
        var words = JSON.parse(req.body.words)
        var action = req.body.action

        if(action == "getcountspertopic") {
            WCA.getCountsPerTopics(req.user, topics, words, function (r) {
                res.json(r)
            })
        }

    }
    app.post(ROUTES.crossanalysis_wca, wca)

    /**
     * Handles ica module requests
     *  - Getting Quantifications per topic
     * @param req
     * @param res
     */
    var ica = function (req, res) {
        var topics = JSON.parse(req.body.topics)
        var insightids = JSON.parse(req.body.insightids)
        var insightBag = JSON.parse(req.body.insightBag)
        var action = req.body.action

        if(action == "getquantificationspertopic") {
            ICA.getQuantificationPerTopic(req.user, topics, insightids, insightBag, function (r) {
                res.json(r)
            })
        }

    }
    app.post(ROUTES.crossanalysis_ica, ica)
};
