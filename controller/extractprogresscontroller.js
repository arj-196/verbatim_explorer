var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var EXTRACTPROGRESSMANAGER = require('../lib/extractprogressmanager')

/**
 * Handles Dataset Extraction Progress Dashboard
 * @param app
 * @param passport
 */
module.exports = function (app, passport) {

    /**
     * Extraction Progress Landing
     * @param req
     * @param res
     */
    var extractionProgress = function (req, res) {
        res.render(VIEWS.extract_progress, {})
    }
    app.get(ROUTES.extract_progress, extractionProgress)

    /**
     * Fetch datasets for a user
     * @param req
     * @param res
     */
    var fetchDatasets = function (req, res) {
        EXTRACTPROGRESSMANAGER.fetchDatasets(req.user, function (err, r) {
            res.json(r)
        })
    }
    app.get(ROUTES.extract_progress_fetch_datasets, fetchDatasets)

    /**
     * Get progress for a dataset
     * @param req
     * @param res
     */
    var getDatasetProgress = function (req, res) {
        EXTRACTPROGRESSMANAGER.getDatasetProgress(req.query.id, function (err, r) {
            res.json(r)
        })
    }
    app.get(ROUTES.extract_progress_get_dataset_progress, getDatasetProgress)



};
