var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
//var helper = require('../../lib/utils/common');
var express = require('express');
var VERBATIMSMANAGER = require('../lib/verbatims')

module.exports = function (app) {

    /**
     * Get Queue
     * @param req
     * @param res
     */
    var getQueue = function (req, res) {
        VERBATIMSMANAGER.TOOL.SEARCHAGGREGATE.getQueue(req.session.id)
            .then(function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.verbatim_searchaggregate_getqueue, getQueue)

};
