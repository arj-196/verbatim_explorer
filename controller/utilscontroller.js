var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var UserAccountManager = require('../lib/useraccountmanager');
var Error = require('../lib/error/error'),
    error = new Error();
var UTILS = require('../lib/utils/common')
var UTILSHISTORY = require('../lib/utils/history')

/**
 * Handles additional Tools related requests 
 * @param app
 * @param passport
 */
module.exports = function (app, passport) {

    /**
     * Handles hash requests
     *  - encrypts text
     *  - decrypts text
     * @param req
     * @param res
     */
    var hash = function (req, res) {
        if(req.body.action == "hide") {
            var hashedString = UTILS.encrypt(req.body.string)
            res.json(hashedString)
        } 
        if(req.body.action == "reveal") {
            var revealedString = UTILS.decrypt(req.body.string)
            res.json(revealedString)
        }
    }
    app.post(ROUTES.utils_hash, hash)

    /**
     * Handles Interaction History Content
     *  - set history
     *  - get history
     * @param req
     * @param res
     */
    var history = function (req, res) {
        if(req.body.mode == "interaction") {
            if(req.body.action == "set") {
                UTILSHISTORY.setContent(req.sessionID, req.body.mode, req.body.content, function (status) {
                    res.json(status)
                })
            }
            if(req.body.action == "get") {
                UTILSHISTORY.getContent(req.sessionID, req.body.mode, function (content) {
                    res.json({c: content})
                })
            }
        }
    }
    app.post(ROUTES.utils_history, history)

}