var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
//var helper = require('../../lib/utils/common');
var express = require('express');
var VERBATIMSMANAGER = require('../lib/verbatims')
var CONCEPTNET = require('../lib/conceptnet')
var _ = require('underscore')

/**
 * Handles Verbatims Dashboard
 * @param app
 */
module.exports = function (app) {

    /**
     * Verbatims
     * @param req
     * @param res
     */
    var verbatims = function (req, res) {
        res.render(VIEWS.verbatims);
    };
    app.get(ROUTES.verbatims, verbatims);

    /**
     * get distinct verbatim topics
     * @param req
     * @param res
     */
    var getVerbatimsTopic = function (req, res) {
        VERBATIMSMANAGER.getTopics(function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_get_topics, getVerbatimsTopic)

    /**
     * get number verbatims in topics
     * @param req
     * @param res
     */
    var getVerbatimsTopicCount = function (req, res) {
        VERBATIMSMANAGER.getTopicCount(req.query.topic, function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_get_topic_count, getVerbatimsTopicCount)

    /**
     * get verbatims for a topic
     * @param req
     * @param res
     */
    var getVerbatims = function (req, res) {
        VERBATIMSMANAGER.getVerbatims(req.query.topic, req.query.iteration, function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_get_verbatims, getVerbatims)


    /**
     * get verbatim by id
     * @param req
     * @param res
     */
    var getVerbatimsByID = function (req, res) {
        VERBATIMSMANAGER.getVerbatimByID(req.query.id, function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.verbatim_get_verbatim_by_id, getVerbatimsByID)


    /**
     * Wordcloud Keyword
     * @param req
     * @param res
     */
    var getVerbatimsWordcloudKeyword = function (req, res) {
        var query = _.isUndefined(req.body.query) ? null : req.body.query
        VERBATIMSMANAGER.getKeywordsForWordcloud(query, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_wordcloud_keyword, getVerbatimsWordcloudKeyword)

    /**
     * Wordcloud Term
     * @param req
     * @param res
     */
    var getVerbatimsWordcloudTerm = function (req, res) {
        var query = _.isUndefined(req.body.query) ? null : req.body.query
        VERBATIMSMANAGER.getTermsForWordcloud(query, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_wordcloud_term, getVerbatimsWordcloudTerm)

    /**
     * Wordcloud Concept
     * @param req
     * @param res
     */
    var getVerbatimsWordcloudConcept = function (req, res) {
        var query = _.isUndefined(req.body.query) ? null : req.body.query
        VERBATIMSMANAGER.getConceptsForWordcloud(query, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_wordcloud_concept, getVerbatimsWordcloudConcept)

    /**
     * Wordcloud Noun
     * @param req
     * @param res
     */
    var getVerbatimsWordcloudNoun = function (req, res) {
        var query = _.isUndefined(req.body.query) ? null : req.body.query
        VERBATIMSMANAGER.getNounsForWordcloud(query, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_wordcloud_nouns, getVerbatimsWordcloudNoun)

    /**
     * Wordcloud Verb
     * @param req
     * @param res
     */
    var getVerbatimsWordcloudVerb = function (req, res) {
        var query = _.isUndefined(req.body.query) ? null : req.body.query
        VERBATIMSMANAGER.getVerbsForWordcloud(query, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_wordcloud_verbs, getVerbatimsWordcloudVerb)

    /**
     * Wordcloud Adjective
     * @param req
     * @param res
     */
    var getVerbatimsWordcloudAdjective = function (req, res) {
        var query = _.isUndefined(req.body.query) ? null : req.body.query
        VERBATIMSMANAGER.getAdjectivesForWordcloud(query, req.body.topic, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_wordcloud_adjectives, getVerbatimsWordcloudAdjective)

    /**
     * Wordcloud Command Search
     * @param req
     * @param res
     */
    var wordcloudCommandSearch = function (req, res) {
        var commandList = _.isUndefined(req.body.commandList) ? null : req.body.commandList
        if(req.body.type == "concepts"){
            VERBATIMSMANAGER.wordcloudSearchConcept(req.body.type, commandList, function (r) {
                res.json(r)
            })

        } else {
            VERBATIMSMANAGER.wordcloudSearchTerm(req.body.type, commandList, function (r) {
                res.json(r)
            })
        }
    }
    app.post(ROUTES.verbatim_wordcloud_command_search, wordcloudCommandSearch)

    /**
     * Verbatim Tool : Favorites
     * @param req
     * @param res
     */
    var verbatimsFavoriteSupport = function (req, res) {
        var params = req.body

        // fetch tagged list
        if (params.ACTION == 'FETCH') {
            VERBATIMSMANAGER.TOOL.FAVORITE.getFavorites(params.TOPIC, function (r) {
                res.json(r)
            })
        }
        // add to tagged list
        else if (params.ACTION == 'ADD') {
            VERBATIMSMANAGER.TOOL.FAVORITE.addFavorite(params.TOPIC, params.ID, function (r) {
                res.json(r)
            })
        }
        // remove from tagged list
        else if (params.ACTION == 'REMOVE') {
            VERBATIMSMANAGER.TOOL.FAVORITE.removeFavorite(params.TOPIC, params.ID, function (r) {
                res.json(r)
            })
        }
    }
    app.post(ROUTES.verbatims_support_favorites, verbatimsFavoriteSupport)

    /**
     * Get concepts for list of verbatims
     * @param req
     * @param res
     */
    var getConceptsForVerbatims = function (req, res) {
        var verbatims
        if (Object.prototype.toString.call(req.body['verbatims[]']) === '[object Array]')
            verbatims = req.body['verbatims[]']
        else
            verbatims = [req.body['verbatims[]']]

        VERBATIMSMANAGER.TOOL.CONCEPTS.getConcepts(verbatims, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_plugins_concepts_getconcepts, getConceptsForVerbatims)

    /**
     * Get Keywords for a list of verbtaims
     * @param req
     * @param res
     */
    var getKeywordsForVerbatims = function (req, res) {
        var verbatims
        if (Object.prototype.toString.call(req.body['verbatims[]']) === '[object Array]')
            verbatims = req.body['verbatims[]']
        else
            verbatims = [req.body['verbatims[]']]

        VERBATIMSMANAGER.TOOL.KEYWORDS.getKeywords(verbatims, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_plugins_keywords_getkeywords, getKeywordsForVerbatims)

    /**
     * Get tags for a list of verbatims
     * @param req
     * @param res
     */
    var getTagsForVerbatims = function (req, res) {
        var verbatims
        if (Object.prototype.toString.call(req.body['verbatims[]']) === '[object Array]')
            verbatims = req.body['verbatims[]']
        else
            verbatims = [req.body['verbatims[]']]

        VERBATIMSMANAGER.TOOL.TAGS.getTags(verbatims, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_plugins_tags_gettags, getTagsForVerbatims)

    /**
     * Get NER for a list of verbatims
     * @param req
     * @param res
     */
    var getNERForVerbatims = function (req, res) {
        var verbatims
        if (Object.prototype.toString.call(req.body['verbatims[]']) === '[object Array]')
            verbatims = req.body['verbatims[]']
        else
            verbatims = [req.body['verbatims[]']]

        VERBATIMSMANAGER.TOOL.NER.getNERS(verbatims, function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.verbatims_plugins_ners_getner, getNERForVerbatims)

    /**
     * Verbatim Tagged Support
     * @param req
     * @param res
     */
    var verbatimTaggedSupport = function (req, res) {
        if (req.body.ACTION == "ADD") {
            var tags
            if (Object.prototype.toString.call(req.body['tags[]']) === '[object Array]')
                tags = req.body['tags[]']
            else
                tags = [req.body['tags[]']]

            VERBATIMSMANAGER.TOOL.TAGS.setTags(req.body.TOPIC, req.body.ID, tags, function (r) {
                res.json(r)
            })
        } else if (req.body.ACTION == "REMOVE") {
            VERBATIMSMANAGER.TOOL.TAGS.removeTag(req.body.ID, req.body.tag, function (r) {
                res.json(r)
            })
        }
    }
    app.post(ROUTES.verbatims_plugins_tags_support, verbatimTaggedSupport)

};
