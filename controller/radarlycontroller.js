var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var Multer = require('multer')
var supportAPI = require('../lib/supportapi')
var SETTINGS = require('../lib/settings')
var env = require('../config/env')
var PROJECTMANAGER = require('../lib/projectmanager')
var DOWNLOADFOLDER = env.dirs.download
var UPLOADFOLDER = env.dirs.upload
var Error = require('../lib/error/error'),
    error = new Error()
var unirest = require('unirest')

var callAPI = function (route, data) {
    return new Promise(function (resolve, reject) {
        url = env.radarly.uri + route
        unirest
            .post(url)
            .header('Accept', 'application/json')
            .send(data)
            .end(function (res) {
                if (res.code == 200) {
                    var result = res.body
                    resolve(result)
                }
                else {
                    //console.log("inner", res.code, iteration)
                    reject({code: res.code})
                }
            })
    })
}

/**
 * Handles the Uploading Dataset Dashboard
 * @param app
 */
module.exports = function (app) {

    /**
     * Upload dataset dashboard
     * @param req
     * @param res
     */
    /*var uploadDataset = function (req, res) {
        res.render(VIEWS.upload_dataset, req.query);
    };
    app.get(ROUTES.upload_dataset, uploadDataset);
    */
    var radarly = function (req, res) {
        // console.log(res)
        res.render(VIEWS.radarly, req.query);
    }
    app.get(ROUTES.radarly, radarly)

    var radarlyProjects = function(req, res) {
        callAPI(
            '/projects',
            JSON.stringify({})
        ).then(function (r) {
            res.json(r)
        })
    }
    app.get(ROUTES.get_radarly_projects, radarlyProjects)

    var radarlyDashboards = function(req, res) {
        callAPI(
            '/dashboards',
            JSON.stringify({project: req.body.project})
        ).then(function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.get_radarly_dashboards, radarlyDashboards)

    var radarlyFocuses = function(req, res) {
        callAPI(
            '/focuses',
            JSON.stringify({
                project: req.body.project,
                dashboard: req.body.dashboard
            })
        ).then(function (r) {
            res.json(r)
        })
    }
    app.post(ROUTES.get_radarly_focuses, radarlyFocuses)

    var radarlyUpload = function(req, res) {
        var project = req.body.project,
            topic = req.body.title,
            language = req.body.language,
            user = req.user._id,
            company = req.user.company

        // var filename = Date.now()
        var filename = (topic + Date.now() + '.verbatim').replace(/ /g, '_')

        // console.log(req.body)

        var count = callAPI(
            '/search',
            JSON.stringify({
                project: req.body.radarly_project,
                dashboard: req.body.radarly_dashboard,
                path: filename,
                focus: req.body.radarly_focus,
                query: req.body.radarly_query,
                platforms: JSON.stringify(req.body.platforms.split(",")),
                statement: 'True'
            })
        ).then(function (count) {
            if (count > 0) {
                // console.log(r)
                try {
                    // var obj = JSON.parse(data)
                    // verify if dataset is in limit or not
                    PROJECTMANAGER.ifExceedsCountLimit(req.user, project, count, function (ifExceeds) {
                        if (!ifExceeds.s) {
                            supportAPI.DATASET.processNewDataset(project, topic, language, filename, user, company, count)
                            // res.render(VIEWS.message, {messageid: 1})
                        }
                    })
                } catch (exception) {
                    console.log(exception)
                }
                callAPI(
                    '/search',
                    JSON.stringify({
                        project: req.body.radarly_project,
                        dashboard: req.body.radarly_dashboard,
                        path: filename,
                        focus: req.body.radarly_focus,
                        query: req.body.radarly_query,
                        platforms: JSON.stringify(req.body.platforms.split(","))
                    })
                ).then(function (r) {
                    res.json(r)
                })

                res.render(VIEWS.message, {messageid: 1})

            } else if (count == 0) {
                res.render(VIEWS.radarly, {message: 'NORESULT'})
            } else {
                res.render(VIEWS.radarly, {message: 'WRONGQUERY'})
            }
        })
    }
    app.post(ROUTES.radarly_upload, radarlyUpload)

};
