var path = require('path');
var fs = require('fs');

/**
 * Loads all the controller files dynamically
 * @param app
 * @param passport
 * @param io
 */
module.exports = function (app, passport, io) {

    require('./rules')(app, passport, io)

    fs
        .readdirSync(path.join(__dirname))
        .filter(function (file) {
            return (file.indexOf('.') !== 0);
        })
        .forEach(function (file) {
            if (file.slice(-3) !== '.js') return;
            var filename = file.slice(0, file.length - 3);
            if (filename != "RouterLoader" && filename != "rules") {
                require(path.join(__dirname, filename))(app, passport, io);
            }
        });
};
