var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var Multer = require('multer')
var supportAPI = require('../lib/supportapi')
var SETTINGS = require('../lib/settings')
var env = require('../config/env')
var PROJECTMANAGER = require('../lib/projectmanager')
var SwiftStorage = require('../lib/utils/swift_storage')
var DOWNLOADFOLDER = env.dirs.download
var UPLOADFOLDER = env.dirs.upload
var Error = require('../lib/error/error'),
    error = new Error()
var logger = require('../lib/utils/logger')('Cnrt:Dataset')


/**
 * Handles the Uploading Dataset Dashboard
 * @param app
 */
module.exports = function (app) {

    /**
     * Upload dataset dashboard
     * @param req
     * @param res
     */
    var uploadDataset = function (req, res) {
        res.render(VIEWS.upload_dataset, req.query);
    };
    app.get(ROUTES.upload_dataset, uploadDataset);

    /**
     * Multer configurations
     * @type {{upload: *}}
     */
    var multer = {
        /**
         * Handle File Upload
         */
        upload: Multer({
            limit: {fileSize: 10000000, files: 1},
            storage: Multer.diskStorage({
                destination: function (req, files, cb) {
                    cb(null, DOWNLOADFOLDER);
                },
                filename: function (req, file, cb) {
                    cb(null, Date.now() + '.verbatim')
                }
            })
        })
    }

    /**
     * Handles actual file upload
     * @param req
     * @param res
     */
    var uploadDatasetFile = function (req, res) {
        var filename = req.file.filename
        var title = req.body.title
        var project = req.body.project
        var language = req.body.language

        logger.debug('UploadDatasetFile', {method: 'post', params: [project, title, language, filename]})
        res.redirect(
            ROUTES.upload_dataset_file
                .replace(':project', project)
                .replace(':title', title)
                .replace(':language', language)
            + "?file=" + filename
        )

    };
    app.post(
        ROUTES.upload_dataset_file,
        multer.upload.single('dataset'),
        uploadDatasetFile);

    /**
     * Dataset Sample Preview Page
     * @param req
     * @param res
     */
    var uploadDatasetPreview = function (req, res) {
        var project = req.params.project,
            topic = req.params.title,
            language = req.params.language,
            filename = req.query.file

        logger.debug('UploadDatasetPreview', {method: 'get', params: [project, topic, language, filename]})
        SETTINGS.validTopicName(topic, project, function (ifvalid) {
            if (ifvalid) {
                res.render(VIEWS.uploaded_dataset_file, {
                    project: project,
                    topic: topic,
                    language: language,
                    filename: filename,
                });
                logger.debug('IsValidTopicName', {method: 'get', params: [project, topic, language, filename]})
            } else {
                res.redirect(ROUTES.upload_dataset + "?message=BADTOPICNAME")
            }
        })
    };
    app.get(ROUTES.upload_dataset_file, uploadDatasetPreview);


    /**
     * Request sample preview extraction
     * @param req
     * @param res
     */
    var getSampleVerbatimObj = function (req, res) {
        var project = req.body.project,
            topic = req.body.topic,
            language = req.body.language,
            filename = req.body.filename

        logger.debug('GetSampleVerbatimObj', {method: 'get', params: [project, topic, language, filename]})
        supportAPI.DATASET.getDatasetSample(
            project,
            topic,
            language,
            DOWNLOADFOLDER + filename,
            req.user.company,
            req.user._id
        ).then(function (r) {
            if (r.s) {
                logger.debug('VerbatimSampleObject Got Sample Object', r)

                // return sample object
                var filepath = r.r.split('/')
                filepath = filepath[filepath.length - 1]
                var samplefilePath = UPLOADFOLDER + filepath  // TODO temp solution
                var obj
                fs.readFile(samplefilePath, 'utf8', function (err, data) {
                    if (err) error.STDError([__dirname, __filename].join('/'), "", err.stack)
                    try {
                        obj = JSON.parse(data)
                        // verify if dataset is in limit or not
                        PROJECTMANAGER.ifExceedsCountLimit(req.user, project, obj.count, function (ifExceeds) {
                            if (ifExceeds.s) {
                                res.json({s: false, m: ifExceeds.m})
                            } else {
                                // send data
                                obj.s = true
                                var f = samplefilePath.split('/')
                                obj.samplepath = f[f.length - 1]
                                logger.debug('VerbatimSampleObject Sending Response', obj)
                                res.json(obj)
                            }
                        })
                    } catch (exception) {
                    }
                })
            } else {
                res.json({s: false, m: r.m})
            }
        })
    }
    app.post(ROUTES.upload_dataset_request_sample, getSampleVerbatimObj)

    /**
     * Proceeding with scheduling dataset upload
     * @param req
     * @param res
     */
    var proceedWithSchedulingDatasetUpload = function (req, res) {
        var project = req.params.project,
            topic = req.params.title,
            language = req.params.language,
            filename = req.query.file,
            user = req.user._id,
            company = req.user.company,
            samplefilePath = UPLOADFOLDER + req.query.samplefile,
            uploadFilePath = DOWNLOADFOLDER + filename

        logger.debug('Get ProccedWithSchedulingDatasetUpload', {
            project: project,
            topic: topic,
            language: language,
            filename: filename,
            user: user,
            company: company,
            samplefilePath: samplefilePath,
            downloadPath: DOWNLOADFOLDER,
            uploadPath: UPLOADFOLDER,
        })

        fs.readFile(samplefilePath, 'utf8', function (err, data) {
            if (err) error.STDError([__dirname, __filename].join('/'), "", err.stack)
            try {
                var obj = JSON.parse(data)
                // verify if dataset is in limit or not
                PROJECTMANAGER.ifExceedsCountLimit(req.user, project, obj.count, function (ifExceeds) {
                    if (!ifExceeds.s) {
                        res.render(VIEWS.message, {messageid: 1})

                        supportAPI.DATASET.processNewDataset(
                            project, topic, language, filename, user, company, obj.count
                        )

                        // logger.debug("Uploading file", {filepath: uploadFilePath})
                        // // upload to object store
                        // SwiftStorage.uploadFile(DOWNLOADFOLDER + filename, function (err) {
                        //     if (err) {
                        //         error.STDError([__dirname, __filename].join('/'), "", err.stack)
                        //     } else {
                        //         logger.debug("Successfully Uploaded File")
                        //         supportAPI.DATASET.processNewDataset(
                        //             project, topic, language, filename, user, company, obj.count
                        //         )
                        //     }
                        // })
                    }
                })
            } catch (exception) {
            }
        })

    }
    app.get(ROUTES.upload_dataset_done, proceedWithSchedulingDatasetUpload)

};

