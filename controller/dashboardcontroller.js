var fs = require('fs')
var path = require('path')
var ROUTES = require('../config/routes.js');
var VIEWS = require('../config/views.js');
var UserManager = require('../lib/usermanager')
var ProjectManager = require('../lib/projectmanager')

/**
 * Handles the Home Dashboard
 * @param app
 */
module.exports = function (app) {

    /**
     * Dashboard Homepage
     * @param req
     * @param res
     */
    var dashboardHome = function (req, res) {
        res.render(VIEWS.dashboard, {user: req.user});
    };
    app.get(ROUTES.dashboard, dashboardHome);

    /**
     * Get current user
     * @param req
     * @param res
     */
    var getUser = function (req, res) {
        UserManager.getUser(req.user)
            .then(function (user) {
                res.json(user)
            })
    }
    app.get(ROUTES.get_user, getUser)

    /**
     * Get all topics for User in a certain project
     * @param req
     * @param res
     */
    var getTopicsForUser = function (req, res) {
        UserManager.getTopics(req.query.username, req.query.action, req.query.projectid)
            .then(function (topics) {
                res.json(topics)
            })
    }
    app.get(ROUTES.get_user_topics, getTopicsForUser)

    /**
     * Handles User Topic Modifications
     *  - Changing topic directory
     * @param req
     * @param res
     */
    var updateUserTopic = function (req, res) {
        if(req.body.action == "movetopic"){
            UserManager.moveTopic(req.body.username, JSON.parse(req.body.data))
                .then(function (topics) {
                    res.json(topics)
                })
        }
    }
    app.post(ROUTES.get_user_topics_edit, updateUserTopic)

    /**
     * Get verbatim count for a topic
     * @param req
     * @param res
     */
    var getVerbatimCountForTopics = function (req, res) {
        var topics = JSON.parse(req.body.topics)
        UserManager.getVerbatimCountForTopic(topics)
            .then(function (count) {
                res.json(count)
            })
    }
    app.post(ROUTES.get_user_topics_count, getVerbatimCountForTopics)

    /**
     * Get the topic object
     * @param req
     * @param res
     */
    var getTopicDetails = function (req, res) {
        UserManager.getTopicDetails(req.body.topic)
            .then(function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.get_user_topic_details, getTopicDetails)

    /**
     * Gets the Users, Organizations and Projects for a given user's company
     * @param req
     * @param res
     */
    var getUsersAndOrganizationsAndProjects = function (req, res) {
        UserManager.getUsersAndOrganizationsAndProjects(req.user)
            .then(function (r) {
                res.json(r)
            })
    }
    app.get(ROUTES.get_users_and_organizations, getUsersAndOrganizationsAndProjects)

    /**
     * Handles topic modifications 
     * @param req
     * @param res
     */
    var updateTopicDetails = function (req, res) {
        var data = JSON.parse(req.body.data)
        UserManager.updateTopicDetails(req.user, data)
            .then(function (r) {
                res.json(r)
            })
    }
    app.post(ROUTES.update_topic_details, updateTopicDetails)

    /**
     * Get all projects for a user
     * @param req
     * @param res
     */
    var getProjects = function (req, res) {
        UserManager.getProjects(req.user)
            .then(function (r) {
                res.json(r)
            })
    }
    app.get(ROUTES.get_projects, getProjects)

    /**
     * Get a project object
     * @param req
     * @param res
     */
    var getProjectDetails = function (req, res) {
        UserManager.getProjectDetails(req.user, req.query.projectid)
            .then(function (r) {
                res.json(r)
            })
    }
    app.get(ROUTES.get_project_details, getProjectDetails)

    /**
     * Handles project modifications
     *  - create a project
     *  - edit a proejct
     * @param req
     * @param res
     */
    var manageProject = function (req, res) {
        if(req.body.action == "create") {
            ProjectManager.createProject(req.user, req.body)
                .then(function (r) {
                    res.json(r)
                })
        }
        if(req.body.action == "edit") {
            ProjectManager.editProject(req.user, req.body, function (r) {
                    res.json(r)
                })
        }
        
    }
    app.post(ROUTES.manage_project, manageProject)

};
